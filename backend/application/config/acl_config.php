<?php
/* содержит пары класс/метод , с разрешениями
*  ключ строится из частей <directory>/<class_name>/<method_name>
 * not_logged_allowed - 0-доступ только для зарегистрированных, 1-для всех
 * role_required - требуемая роль для logged_user - admin | client | <пусто=любая>
 * permissions_required - требующиеся разрешения. Формат: <permission_name>=><permission_value>. Пример: 'clients'=>1
*/
$config['ACL'] = [
    /* admin-zone/Admins Controller*/
    'admin-zone/admins/index'        => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'admin-zone/admins/login'        => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'admin-zone/admins/logout'       => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ], // связано с тем, что при какой-то мутной утере токена все падает
    'admin-zone/admins/emailvalid'   => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'admin-zone/admins/get'          => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => [] ], // даже если у админа нет permisssion=admins, то он может получить себя
    'admin-zone/admins/update'       => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => [] ], // даже если у админа нет permisssion=admins, то он может изменить себя
    /**/'admin-zone/admins/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['admins'] ],

    /* admin-zone/Cli_priv Controller*/
    /**/'admin-zone/cli_priv/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['clients'] ],

    /* admin-zone/Clients Controller*/
    'admin-zone/clients/hasPrivileges'=> ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'admin-zone/clients/getCourseWebHook'=> ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    /**/'admin-zone/clients/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['clients'] ],

    /* admin-zone/Index Controller*/
    'admin-zone/index/index'        => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'admin-zone/index/getTimezones' => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => [] ],
    /**/'admin-zone/index/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => [] ],

    /* admin-zone/Options Controller*/
    /**/'admin-zone/options/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['products'] ],

    /* admin-zone/Orders Controller*/
    //'admin-zone/orders/getLogs'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => ['orders'] ],
    /**/'admin-zone/orders/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['orders'] ],

    /* admin-zone/Paysystems Controller*/
    'admin-zone/paysystems/getList'      => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => [] ],
    /**/'admin-zone/paysystems/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['paysystems'] ],

    /* admin-zone/Privileges Controller*/
    /**/'admin-zone/privileges/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['privileges'] ],

    /* admin-zone/Products Controller*/
    /**/'admin-zone/products/default'        => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['products'] ],

    /* admin-zone/Items Controller*/
    'admin-zone/items/deleteWithContent'     => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['items','items_delete_with_content'] ],
    'admin-zone/items/getVimeoDownloadLink'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['items', 'items_download_from_vimeo'] ],
    /**/'admin-zone/items/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['items'] ],

    /* admin-zone/Presets Controller*/
    /**/'admin-zone/presets/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['presets'] ],

    /* admin-zone/Groups Controller*/
    /**/'admin-zone/groups/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['groups'] ],

    /* admin-zone/Clients_groups Controller*/
    /**/'admin-zone/clients_groups/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['groups', 'clients'] ],

    /* admin-zone/Presets_items Controller*/
    /**/'admin-zone/presets_items/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['presets'] ],

    /* admin-zone/Groups_presets Controller*/
    /**/'admin-zone/groups_presets/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['presets', 'groups'] ],

    /* admin-zone/Clients_presets Controller*/
    /**/'admin-zone/clients_presets/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['presets', 'clients'] ],

    /* admin-zone/Gifts Controller*/
    /**/'admin-zone/gifts/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['gifts'] ],

    /* admin-zone/Gifts_presets Controller*/
    /**/'admin-zone/gifts_presets/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['gifts','presets'] ],

    /* admin-zone/Pages Controller*/
    /**/'admin-zone/pages/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['pages'] ],

    /* admin-zone/Templates_cats Controller*/
    /**/'admin-zone/templates_cats/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['templates'] ],

    /* admin-zone/Templates Controller*/
    /**/'admin-zone/templates/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['templates'] ],

    /* admin-zone/Instagram_accounts Controller*/
    'admin-zone/instagram_accounts/setStream'  => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    /**/'admin-zone/instagram_accounts/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['instagram_accounts'] ],

    /* admin-zone/Promocodes Controller*/
    /**/'admin-zone/promocodes/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['promocodes'] ],

    /* admin-zone/Presets_cats Controller*/
    //'admin-zone/presets_cats/getDropDownList' => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => [] ],
    /**/'admin-zone/presets_cats/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['presets_cats'] ],

    /* admin-zone/Admingroups Controller*/
    /**/'admin-zone/admingroups/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['admingroups'] ],

    /* admin-zone/Admins_admingroups Controller*/
    /**/'admin-zone/admins_admingroups/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['admins', 'admingroups'] ],

    /* admin-zone/Clients_products Controller*/
    /**/'admin-zone/clients_products/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['clients', 'products'] ],

    /* admin-zone/Products_gifts Controller*/
    /**/'admin-zone/products_gifts/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['gifts', 'products'] ],

    /* admin-zone/Docs Controller*/
    /**/'admin-zone/docs/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['clients', 'docs'] ],

    /* admin-zone/Docs Controller*/
    /**/'admin-zone/trans/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['products'] ],

    /* admin-zone/Tag Controller*/
    /**/'admin-zone/tags/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['tags'] ],

    /* admin-zone/Orders_Tags Controller*/
    /**/'admin-zone/orders_tags/default'  => ['not_logged_allowed'=>0, 'role_required'=>'admin', 'permissions_required' => ['orders', 'tags'] ],

    /* admin-zone/Coutries Controller*/
    /**/'admin-zone/countries/default'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => ['countries_cities'] ],

    /* admin-zone/Cities Controller*/
    /**/'admin-zone/cities/default'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => ['countries_cities'] ],

    /* admin-zone/Workers Controller*/
    'admin-zone/workers/demonize'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => [] ],
    /**/'admin-zone/workers/default'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => [] ],

    /* admin-zone/Transcoders Controller*/
    'admin-zone/transcoders/demonize'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => [] ],
    /**/'admin-zone/transcoders/default'  => ['not_logged_allowed'=>1, 'role_required'=>'admin', 'permissions_required' => [] ],

    /*--------------------------------------------------------------------------------------------------------------------------------------------*/

    /*  Index Controller*/
    'index/index'            => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'index/prolongToken'     => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    /**/'index/default'      => ['not_logged_allowed'=>0, 'role_required'=>'', 'permissions_required' => [] ],

    /*  Clients Controller*/
    'clients/index'            => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/auth'             => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/login'            => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/logout'           => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/register'         => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/activate'         => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/remindPassLink'   => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/resetPass'        => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/emailvalid'       => ['not_logged_allowed'=>1, 'role_required'=>'', 'permissions_required' => [] ],
    'clients/getListOnline'    => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'clients/getDevicesList'   => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'clients/addDevice'        => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    /**/'clients/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Docs Controller*/
    /**/'docs/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Instagram accounts Controller*/
    /**/'instagram_accounts/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Clients_products Controller*/
    /**/'clients_products/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Products Controller*/
    'products/getListForBlogger' => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'products/startStream'       => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'products/stopStream'        => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'products/pauseStream'       => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],
    'products/cronStopStream'    => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [] ],
    /**/'products/default'       => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Optios Controller*/
    /**/'options/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Presets Controller*/
    /**/'presets/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Items Controller*/
    /**/'items/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Promocodes Controller*/
    /**/'promocodes/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Orders Controller*/
    /**/'orders/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Clients_presets Controller*/
    /**/'clients_presets/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Trans Controller*/
    'trans/check'               => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>1 ], //hmac_required - для вызова метода нужен "Content-HMAC"-заголовок
    'trans/pay'                 => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>1 ],
    'trans/fail'                => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>1 ],
    'trans/refund'              => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>1 ],
    /**/'trans/default'         => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>0 ],

    'subs/recurrent'           => ['not_logged_allowed'=>1, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>1 ],
    /**/'subs/default'         => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [], 'hmac_required'=>0 ],

    /*  Tags Controller*/
    /**/'tags/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Countries Controller*/
    /**/'countries/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],

    /*  Cities Controller*/
    /**/'cities/default'      => ['not_logged_allowed'=>0, 'role_required'=>'client', 'permissions_required' => [] ],


];
?>
