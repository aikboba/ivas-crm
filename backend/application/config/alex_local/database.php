<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$db['default']['dsn'] = 'mysql:host=localhost;dbname=ivas-crm';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'ivas-crm';