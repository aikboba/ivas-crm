<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// это (пополняемый) список сатусов и сообщений. В том числе содержит коды ошибок
defined('STATUS_OK') OR define('STATUS_OK', 0);
defined('STATUS_OK_MSG') OR define('STATUS_OK_MSG', "");

defined('STATUS_NOT_FOUND') OR define('STATUS_NOT_FOUND', 10);
defined('STATUS_NOT_FOUND_MSG') OR define('STATUS_NOT_FOUND_MSG', "Информация не найдена");

defined('STATUS_CREATE_FAIL') OR define('STATUS_CREATE_FAIL', 20);
defined('STATUS_CREATE_FAIL_MSG') OR define('STATUS_CREATE_FAIL_MSG', "Создание не удалось");

defined('STATUS_UPDATE_FAIL') OR define('STATUS_UPDATE_FAIL', 30);
defined('STATUS_UPDATE_FAIL_MSG') OR define('STATUS_UPDATE_FAIL_MSG', "Изменение не удалось");

defined('STATUS_DENIED') OR define('STATUS_DENIED', 40);
defined('STATUS_DENIED_MSG') OR define('STATUS_DENIED_MSG', "Недостаточно прав для выполнения операции");

defined('STATUS_MISSING') OR define('STATUS_MISSING', 50);
defined('STATUS_MISSING_MSG') OR define('STATUS_MISSING_MSG', "Не хватает данных");

defined('STATUS_DEL_FAIL') OR define('STATUS_DEL_FAIL', 60);
defined('STATUS_DEL_FAIL_MSG') OR define('STATUS_DEL_FAIL_MSG', "Удаление не удалось");

defined('STATUS_DISABLED') OR define('STATUS_DISABLED', 70);
defined('STATUS_DISABLED_MSG') OR define('STATUS_DISABLED_MSG', "Попытка выполнить операцию над неактивной записью");

defined('STATUS_OPER_NOT_ALLOWED') OR define('STATUS_OPER_NOT_ALLOWED', 80);
defined('STATUS_OPER_NOT_ALLOWED_MSG') OR define('STATUS_OPER_NOT_ALLOWED_MSG', "Эта операция запрещена над указанными данными");

defined('STATUS_TOKEN_MISSING') OR define('STATUS_TOKEN_MISSING', 90);
defined('STATUS_TOKEN_MISSING_MSG') OR define('STATUS_TOKEN_MISSING_MSG', "Токен не найден");

defined('STATUS_TOKEN_EXPIRED') OR define('STATUS_TOKEN_EXPIRED', 100);
defined('STATUS_TOKEN_EXPIRED_MSG') OR define('STATUS_TOKEN_EXPIRED_MSG', "Токен просрочен");

defined('STATUS_TOKEN_WRONG') OR define('STATUS_TOKEN_WRONG', 105);
defined('STATUS_TOKEN_WRONG_MSG') OR define('STATUS_TOKEN_WRONG_MSG', "Неверная роль");

defined('STATUS_NOT_UNIQUE') OR define('STATUS_NOT_UNIQUE', 110);
defined('STATUS_NOT_UNIQUE_MSG') OR define('STATUS_NOT_UNIQUE_MSG', "Информация не уникальна");

defined('STATUS_EMAIL_NOT_VALID') OR define('STATUS_EMAIL_NOT_VALID', 120);
defined('STATUS_EMAIL_NOT_VALID_MSG') OR define('STATUS_EMAIL_NOT_VALID_MSG', "Email не действителен");

defined('STATUS_FILE_UPLOAD_ERROR') OR define('STATUS_FILE_UPLOAD_ERROR', 130);
defined('STATUS_FILE_UPLOAD_ERROR_MSG') OR define('STATUS_FILE_UPLOAD_ERROR_MSG', "Невозможно загрузить файл");

defined('STATUS_INSTAGRAM_LOGIN_FAILED') OR define('STATUS_INSTAGRAM_LOGIN_FAILED', 140);
defined('STATUS_INSTAGRAM_LOGIN_FAILED_MSG') OR define('STATUS_INSTAGRAM_LOGIN_FAILED_MSG', "Ошибка авторизации Instagram");

defined('STATUS_INSTAGRAM_USERDATA_ERROR') OR define('STATUS_INSTAGRAM_USERDATA_ERROR', 150);
defined('STATUS_INSTAGRAM_USERDATA_ERROR_MSG') OR define('STATUS_INSTAGRAM_USERDATA_ERROR_MSG', "Ошибка получения пользовательских данных Instagram");

defined('STATUS_INCORRECT_DATA') OR define('STATUS_INCORRECT_DATA', 160);
defined('STATUS_INCORRECT_DATA_MSG') OR define('STATUS_INCORRECT_DATA_MSG', "Данные неверны");

defined('STATUS_FAIL') OR define('STATUS_FAIL', 170);
defined('STATUS_FAIL_MSG') OR define('STATUS_FAIL_MSG', "Не удалось выполнить операцию");

// это статусы конкретно для транзакций. Да, я знаю, что они пересекаются -----------------------------------------------
defined('STATUS_TRANS_OK') OR define('STATUS_TRANS_OK', 0);
defined('STATUS_TRANS_OK_MSG') OR define('STATUS_TRANS_OK_MSG', "Транзакция ОК");

defined('STATUS_TRANS_INVALID_ORDER') OR define('STATUS_TRANS_INVALID_ORDER', 10);
defined('STATUS_TRANS_INVALID_ORDER_MSG') OR define('STATUS_TRANS_INVALID_ORDER_MSG', "Некорректный номер заказа");

defined('STATUS_TRANS_INVALID_CLIENT') OR define('STATUS_TRANS_INVALID_CLIENT', 11);
defined('STATUS_TRANS_INVALID_CLIENT_MSG') OR define('STATUS_TRANS_INVALID_CLIENT_MSG', "Некорректный AccountId");

defined('STATUS_TRANS_INVALID_AMOUNT') OR define('STATUS_TRANS_INVALID_AMOUNT', 12);
defined('STATUS_TRANS_INVALID_AMOUNT_MSG') OR define('STATUS_TRANS_INVALID_AMOUNT_MSG', "Неверная сумма");

defined('STATUS_TRANS_CANT_ACCEPT') OR define('STATUS_TRANS_CANT_ACCEPT', 13);
defined('STATUS_TRANS_CANT_ACCEPT_MSG') OR define('STATUS_TRANS_CANT_ACCEPT_MSG', "Платеж не может быть принят");

defined('STATUS_TRANS_EXPIRED') OR define('STATUS_TRANS_EXPIRED', 20);
defined('STATUS_TRANS_EXPIRED_MSG') OR define('STATUS_TRANS_EXPIRED_MSG', "Платеж просрочен");
// END это статусы конкретно для транзакций. Да, я знаю, что они пересекаются -----------------------------------------------



defined('STATUS_ERROR_UNKNOWN') OR define('STATUS_ERROR_UNKNOWN', 666);
defined('STATUS_ERROR_UNKNOWN_MSG') OR define('STATUS_ERROR_UNKNOWN_MSG', "Неизвестная ошибка");