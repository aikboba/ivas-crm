<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
|--------------------------------------------------------------------------
|   API DESCRIPTION FOR mailboxlayer SERVICE
|   url https://mailboxlayer.com/ 
|-------------------------------------------------------------------------- 
| 
|   ---- request params
| 
|   smtp       active by default. Set to "0" if you don't want the API to perform SMTP checks
|   format     set to "1" in order to request prettified JSON result set (use only for debugging!)
|   callback   append your preferred JSONP callback function name. (See "JSONP Callbacks" section)
|   catch_all  The mailboxlayer API's real-time verification process does not end with one SMTP check. 
|              Since many email servers out there are configured to catch (receive) all incoming mail traffic, 
|              regardless of the requested email adress's local part, a catch-all detection functionality has been implemented.
|              Due to it's heavy impact on the API's response time, this functionality is inactive by default. 
|              However, you may turn on catch-all detection simply by appending the API's catch_all parameter to the request URL and setting it to 1.
| 
|   ---- response params
| "email"         Contains the exact email address requested
| "did_you_mean"  Contains a did-you-mean suggestion in case a potential typo has been detected.
| "user"          Returns the local part of the request email address. (e.g. "paul" in "paul@company.com")
| "domain"        Returns the domain of the requested email address. (e.g. "company.com" in "paul@company.com")
| "format_valid"  Returns true or false depending on whether or not the general syntax of the requested email address is valid.
| "mx_found"      Returns true or false depending on whether or not MX-Records for the requested domain could be found.
| "smtp_check"    Returns true or false depending on whether or not the SMTP check of the requested email address succeeded.
| "catch_all"     Returns true or false depending on whether or not the requested email address is found to be part of a catch-all mailbox.
| "role"          Returns true or false depending on whether or not the requested email address is a role email address. (e.g. "support@company.com", "postmaster@company.com")
| "disposable"    Returns true or false depending on whether or not the requested email address is a disposable email address. (e.g. "user123@mailinator.com")
| "free"          Returns true or false depending on whether or not the requested email address is a free email address. (e.g. "user123@gmail.com", "user123@yahoo.com")
| "score"         Returns a numeric score between 0 and 1 reflecting the quality and deliverability of the requested email address.
| 
| For transactional email:
|   Score        Quality
| 1.00-0.65       Good
| 0.64-0.33       Medium
| 0.32-0.00       Bad
|
|
|  For marketing email:
|   Score        Quality
| 1.00-0.80       Good
| 0.79-0.49       Medium
| 0.48-0.00       Bad
| 
| 
| Common API errors:
| code      type                                description
| 404    "404_not_found"                  User requested a resource which does not exist.
| 101    "missing_access_key"             User did not supply an Access Key.
| 101    "invalid_access_key"             User entered an invalid Access Key.
| 103    "invalid_api_function"           User requested a non-existent API Function.
| 104    "usage_limit_reached"            User has reached or exceeded his subscription plan's monthly API Request Allowance.
| 210    "no_email_address_supplied"      User did not provide an email address.
| 310    "catch_all_access_restricted"    The user's current subscription plan does not support catch-all detection.
| 999    "timeout"                        An unexpected timeout issue occurred.
*/

$config = [
    'use' => true,
    'api_key' => '49de153cc23eb88a966fbc49cecae323',
    'url' => 'https://apilayer.net/api/check',
    
    'param_api_key' => 'access_key',
    'param_email' => 'email',
    
    'additional_params_request' => [
        'smtp' => 1,
    ],
    
    'check_params_response' => [
        'format_valid' => true,
        'mx_found' => true,
        'smtp_check' => true,
        'disposable' => false,
        'score' => ['>=', 0.33]
    ]
];