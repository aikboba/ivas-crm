<?
// UNCOMMENT THIS------------------------
/*
$envVarsRequired = [
    'BASE_URL',
    'THIS_SITE_URL',

    'MAILER_SMTP_HOST',
    'MAILER_SMTP_USER',
    'MAILER_SMTP_PASSWORD',
    'MAILER_SMTP_PORT',

    'INSTA_USE_INSTAGRAM_DATA_GET',
    'INSTA_DEFAULT_INSTAGRAM_ACCOUNT_LOGIN',
    'INSTA_DEFAULT_INSTAGRAM_ACCOUNT_PASSWORD',
    'INSTA_BOTS_SECRET_SALT', // это лучше не возводить

    'FTP_HOST',
    'FTP_PORT',
    'FTP_USER',
    'FTP_PASSWORD',
    'FTP_PASSIVE',
    'FTP_WWW_DOCS_PATH',
    'FTP_WWW_AVATARS_PATH',
    'FTP_UPLOAD_DOCS_PATH',
    'FTP_UPLOAD_AVATARS_PATH',

    'EMAIL_VALID_USE',
    'EMAIL_VALID_API_KEY',
    'EMAIL_VALID_URL',
    'EMAIL_VALID_ADDITIONAL_PARAMS_REQUEST_SMTP',

    'DATABASE_HOST',
    'DATABASE_USER',
    'DATABASE_NAME',
    'DATABASE_PORT',
    'DATABASE_PASSWORD',

    // не уверен, что это нужно на боевом сервере. Но кое-где, вроде, еще используется для отправок вместо sendGrid
    'GMAIL_SERVER',
    'GMAIL_USERNAME',
    'GMAIL_PASSWORD',
    'GMAIL_PORT',

    'MEMBERS_URL_RESET_PASS',

    //конфиг cloudpayments
    'CP_API_SECRET',
    'CP_PUBLIC_KEY',

    //конфиг messinger
    'MESSINGER_SECRET_SALT',
    'CHAT_SECRET_SALT',
    'CHAT_ENDPOINT',
];

foreach ($envVarsRequired as $varname)
{
    if (!isset($varname, $_ENV))
    {
        throw new Exception("'".$varname."'" . ' _ENV variable required.');
    }
}
*/

// END UNCOMMENT THIS------------------------
$config['BASE_URL'] = $_ENV["BASE_URL"];
$config['THIS_SITE_URL'] = $_ENV["THIS_SITE_URL"];
