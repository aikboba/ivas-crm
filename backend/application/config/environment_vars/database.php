<?php
$db['default']['dsn'] = "mysql:host=". $_ENV["DATABASE_HOST"] . ";dbname=" . $_ENV["DATABASE_NAME"] . ";port=" . $_ENV["DATABASE_PORT"] . ";password=" . $_ENV["DATABASE_PASSWORD"] . ";";
//$db['default']['dbdriver'] = 'pdo';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['hostname'] = $_ENV["DATABASE_HOST"];
$db['default']['port'] = $_ENV["DATABASE_PORT"];
$db['default']['username'] = $_ENV["DATABASE_USER"];
$db['default']['password'] = $_ENV["DATABASE_PASSWORD"];
$db['default']['database'] = $_ENV["DATABASE_NAME"];
$db['default']['encrypt']['ssl_ca'] = (isset($_ENV["DATABASE_CA"])) ? $_ENV["DATABASE_CA"] : $db['default']['encrypt']['ssl_ca'];
