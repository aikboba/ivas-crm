<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// UNCOMMENT THIS------------------------
/*
$config['use'] = boolval($_ENV['EMAIL_VALID_USE']); // true
$config['api_key'] = $_ENV['EMAIL_VALID_API_KEY']; // a8642013d69735113e83374dbcf49bde
$config['url'] = $_ENV['EMAIL_VALID_URL']; // https://apilayer.net/api/check
$config['param_api_key'] = (isset($_ENV['EMAIL_VALID_PARAM_API_KEY'])) ? $_ENV['EMAIL_VALID_PARAM_API_KEY'] : $config['param_api_key']; // access_key
$config['param_email'] = (isset($_ENV['EMAIL_VALID_PARAM_EMAIL'])) ? $_ENV['EMAIL_VALID_PARAM_EMAIL'] : $config['param_email']; // email

$config['additional_params_request']['smtp'] = (isset($_ENV['EMAIL_VALID_ADDITIONAL_PARAMS_REQUEST_SMTP'])) ? intval($_ENV['EMAIL_VALID_ADDITIONAL_PARAMS_REQUEST_SMTP']) : $this->config['additional_params_request']['smtp']; // 1
$config['check_params_response']['format_valid'] = (isset($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_FORMAT_VALID'])) ? boolval($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_FORMAT_VALID']) : $this->config['check_params_response']['format_valid']; // true
$config['check_params_response']['mx_found'] = (isset($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_MX_FOUND'])) ? boolval($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_MX_FOUND']) : $this->config['check_params_response']['mx_found']; // true
$config['check_params_response']['smtp_check'] = (isset($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_SMTP_CHECK'])) ? boolval($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_SMTP_CHECK']) : $this->config['check_params_response']['smtp_check']; // true
$config['check_params_response']['disposable'] = (isset($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_DISPOSABLE'])) ? boolval($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_DISPOSABLE']) : $this->config['check_params_response']['disposable']; // true
$config['check_params_response']['score'] = (isset($_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_SCORE'])) ? $_ENV['EMAIL_VALID_CHECK_PARAMS_RESPONSE_SCORE'] : $this->config['check_params_response']['score']; // '>=0.33'
*/
// END UNCOMMENT THIS------------------------
