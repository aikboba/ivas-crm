<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Константы для библиотеки Messinger
*
*/

$config['messinger_secret_salt'] = (isset($_ENV['MESSINGER_SECRET_SALT'])) ? $_ENV['MESSINGER_SECRET_SALT'] : $config['messinger_secret_salt']; // "glyncgw#5hk8!#_secret_is_not_so-secret";
$config['curl_url'] = (isset($_ENV['CHAT_ENDPOINT'])) ? $_ENV['CHAT_ENDPOINT'] : $config['curl_url'];