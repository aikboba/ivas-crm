<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  JWT contants
*
*/

// JWT encoding algorithm - 
$config['jwt_alg'] = 'HS256';

// Token type 
$config['jwt_typ'] = 'JWT';

// JWT secret key
$config['jwt_key'] = '123123123';

// JWT access token expiration time, min
$config['jwt_access_token_ttl'] = 14400; // minutes - 10 суток!
$config['jwt_refresh_token_ttl'] = 86400; // = 60 days

$config['jwt_default_payload'] = [
    "iss" => "app",
    "sub" => "admin", // по спецификации здесь можно передавать subject. Как вариант - здесь можно таскать "признак" того, какие данные передаются (admin, client ...)
];



