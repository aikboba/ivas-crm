<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['stream_health_max_sec_allowed'] = 120; // число секунд, которое может пройти в состоянии паузы стрима до его принудительной остновки через CRON

// параметры по умолчанию для стрима
$config['stream_default_orientation'] = 'vertical';
$config['stream_default_width'] = 396;
$config['stream_default_height'] = 704;
$config['stream_default_fps'] = 30;
$config['stream_default_origin'] = 'vimeo';

