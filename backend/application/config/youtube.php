<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['key'] = null;
$config['CONFIG_GOOGLE_PATH'] = null;//Сдесь путь до файла json полученного у google console. Нижний конфиг, не заполнять.
$config['GOOGLE_APP_NAME'] = null;
$config['GOOGLE_ID'] = null;
$config['GOOGLE_SECRET'] = null;
$config['GOOGLE_CALLBACK_URL'] = null;
