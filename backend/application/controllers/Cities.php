<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Cities
 * @property Cities_model $cities_model
 * @property Countries_model $countries_model
 * @property StdClass $CI
 */
class Cities extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cities_model');
        $this->load->model('countries_model');

        $this->model = $this->cities_model;
    }

    /** получает данные о городе
     *
     *  @param   int $id  - ID города. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $city_id = intval($this->input->post('id'));
        if (empty($city_id))
        {
            //city_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        /** @var StdClass $city */
        $city = $this->model->getById($city_id);
        if (empty($city))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $city;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список городов по стране
     *
     * @param  int $country_id - ID страны.  берется из $_POST
     * @param  int|null $amount - макс кол-во (limit) возвращаемых записей. Берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $country_id = $this->input->post('country_id');
        if (empty($country_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $amount = $this->input->post('amount');

        $cities = $this->model->getByCountryId($country_id, $amount);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $cities;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** получает список городов отфильтрованный по названию города name
     *
     * @param string $search - искомое название
     * @param int|null $country_id - ID страны
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getFilteredList()
    {
        $search = $this->input->post('search');
        if (empty($search))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $country_id = intval($this->input->post('country_id'));

        $params   = [];
        $params[] = ['cities.name', ' like ', '%'.$search.'%'];

        if (!empty($country_id)) $params[] = ['country_id', '=', $country_id];

        $list = $this->model->getList($params);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

}
