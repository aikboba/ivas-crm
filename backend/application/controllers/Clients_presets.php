<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_presets
 * @property Clients_model  $clients_model
 * @property Presets_model $presets_model
 * @property Clients_presets_model $clients_presets_model
 */
class Clients_presets extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('presets_model');
        $this->load->model('clients_presets_model');

        $this->model = $this->clients_presets_model;
    }

    /** получает данные - запись clients_presets
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_presets_id = $this->input->post('id');
        if (empty($clients_presets_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_presets_id = $this->model->getById($clients_presets_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_presets_id;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список ДОСТУПНЫХ СЕЙЧАС presets по client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAvailablePresets()
    {
        //$client_id = $this->input->post('client_id');
        $client_id = $this->auth->loggedUser->id;
//$client_id=1;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAvailablePresetsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** Доступен ли пресет клиенту (с учетом даты)
     *
     * @param int $preset_id - preset_id - пресет, доступность которого проверяется
     * @param int $client_id - client_id - клиент, для которого проверятся доступность
     *
     * @return  object - JSON-объект формата envelope
     */
    public function isPresetAvailable()
    {
        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = $this->auth->loggedUser->id;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $res = $this->model->isPresetAvailableForClient($preset_id, $client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = intval($res);

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает пресеты по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientId()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPresetsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список клиентов, которые имеют доступ указанному preset_id
     *
     * @param  int $preset_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getClientsListByPresetId()
    {
        $preset_id = $this->input->post('preset_id');

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getClientsByPresetId($preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


}
