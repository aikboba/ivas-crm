<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_products
 * @property clients_model  $clients_model
 * @property products_model $products_model
 * @property clients_products_model $clients_products_model
 */
class Clients_products extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->load->model('clients_model');
        $this->load->model('clients_products_model');

        $this->model = $this->clients_products_model;
    }

    /** получает данные - запись clients_products
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_products_id = $this->input->post('id');
        if (empty($clients_products_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_products_id = $this->model->getById($clients_products_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_products_id;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список ДОСТУПНЫХ СЕЙЧАС products по client_id
     *
     * @param string|null $type - строка, содержащая "нужные" типы продуктов через "," / Пример: 'subscription,preset'
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAvailableProductsListByClientId()
    {
        $type = $this->input->post('type');
        if (!empty($type))
        {
            $types = explode(',', $type);
        }
        else $types = null;

        $client_id = $this->auth->loggedUser->id;
//$client_id=1;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAvailableProductsByClientId($client_id, $types);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** Доступен ли продукт клиенту (с учетом даты)
     *
     * @param int $product_id - продукт, доступность которого проверяется
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return  object - JSON-объект формата envelope
     */
    public function isProductAvailable()
    {
        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = $this->auth->loggedUser->id;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $res = $this->model->isProductAvailableForClient($product_id, $client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = intval($res);

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список clients, для которых есть АКТИВНАЯ связь с указанным product_id
     *
     * @param  int $product_id - берется из $_POST
     * @param  string $dt - дата, для которой строится список. берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAvailableClientsListByProductId()
    {
        $product_id = $this->input->post('product_id');

        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAvailableClientsByProductId($product_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


}
