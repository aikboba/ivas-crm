<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Countries
 * @property Countries_model $countries_model
 */
class Countries extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('countries_model');
        $this->model = $this->countries_model;
    }

    public function index()
    {

    }

    /** получает данные о стране по ID
     *
     *  @param   int id  - ID страны. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $country_id = $this->input->post('id');
        if (empty($country_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $country = $this->model->getById($country_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $country;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные о стране по iso
     *
     *  @param   string iso  - iso-код страны. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByIso()
    {
        $country_iso = $this->input->post('iso');
        if (empty($country_iso))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $country = $this->model->getByIso($country_iso);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $country;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список стран
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

}
