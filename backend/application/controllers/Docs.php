<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Docs
 * @property Docs_model $docs_model
 * @property CI_FTP $ftp
 */
class Docs extends Base_Front_Controller {

    public $upload_path = "";
    public $www_path = "";
    private $ftp_config = [];

    public function __construct()
    {
        parent::__construct();

// параметры FTP --------------------------------------------------------------
        $this->load->config('ftp', true);
        $this->upload_path = $this->config->item('ftp_upload_docs_path', 'ftp'); // это реальная папка ни FTP? куда падают загруженные файлы
        $this->www_path = $this->config->item('ftp_www_docs_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт

        $this->load->library('ftp');
        $this->ftp_config['hostname'] = $this->config->item('ftp_host', 'ftp');
        $this->ftp_config['username'] = $this->config->item('ftp_user', 'ftp');
        $this->ftp_config['password'] = $this->config->item('ftp_password', 'ftp');
        $this->ftp_config['port'] = $this->config->item('ftp_port', 'ftp');
        $this->ftp_config['passive'] = $this->config->item('ftp_passive', 'ftp');
        //$this->ftp_config['debug'] = TRUE;
// END параметры FTP --------------------------------------------------------------

        $this->load->model('docs_model');
        $this->model = $this->docs_model;

        /*
                $source = $_SERVER['DOCUMENT_ROOT']."static/uploads/1.jpg";
                $conn = ftp_connect($conf['hostname']);
                ftp_login($conn, $conf['username'], $conf['password']);
                ftp_chdir($conn, '/www/nexus92.ru/uploads');
                $dir = ftp_pwd($conn);
                ftp_pasv($conn, true);
                ftp_put($conn, '111.jpg',$source, FTP_BINARY);
        echo "<pre>";
        print_r($dir);
        echo "</pre>";
        die();

                ftp_close($conn);
        */
    }

    public function index()
    {

    }

    /** получает данные о документе по ID
     *
     *  @param   int id  - ID документа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $doc_id = $this->input->post('id');
        if (empty($doc_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $doc = $this->model->getById($doc_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $doc;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список документов клиента - только в статусе active
     *
     * @params int $client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getByClientId($client_id, 'active');

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание документа  с загрузкой файла
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $this->ftp->connected = $this->ftp->connect($this->ftp_config);
        if (!$this->ftp->connected)
        {
            $status = STATUS_FAIL;
            $message = "Ошибка: среда передачи недоступна.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $data = [
            'client_id' => $this->auth->loggedUser->id,
        ];
        $title = $this->input->post('title');

        $parts = pathinfo($_FILES['file']['name']);
        $ext = $parts['extension'];
        $basename = $parts['filename'];

        $data['title'] = (empty($title)) ? $basename.".".$ext : $title;
        $data['filename'] = md5(uniqid())."_".$basename.".".$ext;
        $data['mime_type'] = $_FILES['file']['type'];
        $data['status'] = 'active';

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            if ($this->ftp->connected)
            {
                $this->ftp->connect($this->ftp_config);
                if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);
                $this->ftp->upload($_FILES['file']['tmp_name'], $data['filename'], 'binary');
                $this->ftp->close();
            }


            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение документа
     *
     *  @param   int id  - ID документа. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $doc_id = $this->input->post('id');

        if (empty($doc_id))
        {
            //$doc_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $doc = $this->model->getById($doc_id);

        if (!empty($doc))
        {
            // найден - изменяем
            $data = [
                'title' => $this->input->post('title'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($doc_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($doc_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // doc не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление документа
     *
     *  @param   int id  - ID документа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $doc_id = intval($this->input->post('id'));

        if (empty($doc_id))
        {
            //doc_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            /** @var StdClass $data */
            $data = $this->model->getById($doc_id);

            if ($data->client_id != $this->auth->loggedUser->id)
            {
                //кто-то удаляет не свой файл
                $status = STATUS_DENIED;
                $message = STATUS_DENIED_MSG;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }

            $this->db->trans_begin();

            $del_res = $this->model->clientDelete($doc_id); // клиент не удаляет файл, а отмечает удаленным

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
/*
                $this->ftp->connect($this->ftp_config);
                if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);
                @$this->ftp->delete_file($data->filename);
                $this->ftp->close();
*/
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}