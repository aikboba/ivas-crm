<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url'));

        if (!$this->auth->isLogged()) redirect('/', 'refresh');
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        //$this->load->view('layouts/backend/_layout_index', $this->data);
    }
}
