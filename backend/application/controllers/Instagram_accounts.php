<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Instagram_accounts
 * @property Instagram_accounts_model $instagram_accounts_model
 */
class Instagram_accounts extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->config('instagram', true);

        $this->load->model('instagram_accounts_model');
        $this->model = $this->instagram_accounts_model;
    }

    public function index()
    {

    }

    /** получает данные о боте по ID
     *
     *  @param   int id  - ID insta_acc. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $instagram_account_id = $this->input->post('id');
        if (empty($instagram_account_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $instagram_account = $this->model->getById($instagram_account_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $instagram_account;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}