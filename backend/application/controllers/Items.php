<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Items
 * @property Items_model $items_model
 * @property Presets_items_model $presets_items_model
 * @property Vimeo $vimeo
 */
class Items extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('vimeo');

        $this->load->model('presets_items_model');

        $this->load->model('items_model');
        $this->model = $this->items_model;
    }

    public function index()
    {

    }

    /** получает данные о item
     *
     *  @param   int id  - ID item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $item_id = $this->input->post('id');
        if (empty($item_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // доступен ли item этому пользвателю ----------------------------
        $available = $this->presets_items_model->isItemAvailableForClient($item_id, $this->auth->loggedUser->id);
        if (!$available)
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        // END доступен ли item этому пользвателю ------------------------

        // берем из БД
        $item = $this->model->getById($item_id);

        $item->chat = $this->utils->getChatObject($item->id);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $item;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список элементов из удаленного сервиса Vimeo
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getVimeoItemsList()
    {
        $list = $this->vimeo->getVideos();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает информацию о конкретном элементе удаленного сервиса Vimeo, используюя его id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getVimeoItemInfo()
    {
        $resource_string_id = $this->input->post('id');
        if (empty($resource_string_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $info = $this->vimeo->getVideoInfo($resource_string_id);
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = (object)$info;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** Доступен ли item клиенту (с учетом даты)
     *
     * @param int $item_id - item, доступность которого проверяется
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return  object - JSON-объект формата envelope
     */
    public function isItemAvailable()
    {
        $item_id = $this->input->post('item_id');
        if (empty($item_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = $this->auth->loggedUser->id;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $res = $this->presets_items_model->isItemAvailableForClient($item_id, $client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = intval($res);

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список SHOWABLE items для указанного пресета
     *
     *  @param int $preset_id - ID пресеета. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListShowable()
    {
        $client_id = $this->auth->loggedUser->id;

        $preset_id = $this->input->post_get('preset_id');
        if (empty($preset_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->load->model('presets_items_model');
        $list = $this->presets_items_model->getShowableItemsForClientByPreset($client_id, $preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


}
