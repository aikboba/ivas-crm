<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \InstagramAPI\Instagram as IGram;

/**
 * Class Members
 * @property Clients_model $clients_model
 * @property Cli_priv_model $cli_priv_model
 * @property Privileges_model $privileges_model
 * @property Mailer $mailer
 * @property CI_FTP $ftp
 */
class Members extends Base_Front_Controller {

    public $upload_path = "";
    public $www_path = "";
    public $project_domain = "";
    public $members_path = "";
    public $project_name = "";
    private $ftp_config = [];

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url'));
        $this->config->load('cms_consts', true);
        $this->config->load('instagram', true);

// параметры FTP --------------------------------------------------------------
        $this->load->config('ftp', true);
        $this->upload_path = $this->config->item('ftp_upload_avatars_path', 'ftp'); // это реальная папка на FTP, куда падают загруженные файлы
        $this->www_path = $this->config->item('ftp_www_avatars_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт

        $this->load->library('ftp');
        $this->ftp_config['hostname'] = $this->config->item('ftp_host', 'ftp');
        $this->ftp_config['username'] = $this->config->item('ftp_user', 'ftp');
        $this->ftp_config['password'] = $this->config->item('ftp_password', 'ftp');
        $this->ftp_config['port'] = $this->config->item('ftp_port', 'ftp');
        $this->ftp_config['passive'] = $this->config->item('ftp_passive', 'ftp');
        $this->ftp_config['debug'] = TRUE;
// END параметры FTP --------------------------------------------------------------

        $this->data['site_title'] = $this->config->item('members_site_title', 'cms_consts');
        $this->data['members_url'] = $this->config->item('members_url', 'cms_consts');
        $this->data['members_url_auth'] = $this->config->item('members_url_auth', 'cms_consts');

        $this->load->model('clients_model');
        $this->model = $this->clients_model;

        $this->load->model('instagram_accounts_model');

        $this->load->library('mailer');
        $this->mailer->sendType = 'sync';

        $this->project_domain = $this->config->item('BASE_URL', 'cms_consts');
        $this->members_path = $this->project_domain.'/members';
        $this->project_name = $this->config->item('project_name', 'cms_consts');
    }

    public function index()
    {
/*
        $this->load->model('templates_model');
        $res = $this->templates_model->getById(1);
echo "<pre>";
print_r($res);
echo "</pre>";
die();
*/
    }

    /**
     * Получает данные о пользователе (client)
     *
     * @param int $id - id запрашиваемого пользователя
     *
     * @return object | array
     */
    public function get()
    {
        // берем id из POST
        $client_id = $this->input->post('id');
        if (empty($client_id))
        {
            //$client_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
/* решено через ACL
        // доступно только залогиненным
        if (!$this->auth->isLogged())
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
*/
        // берем из БД
        $client = $this->model->getById($client_id);
        $client = (object)$client;

        if (!empty($client))
        {
            // клиент найден
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $client;
        }
        else
        {
            //клиент не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Получает данные о пользователях по списку их ID
     *
     * @param string $ids - json encoded array of client ids
     *
     * @return array of objects
     */
    public function getListByIds()
    {
        // берем id из POST
        $ids_json = $this->input->post('ids');
        if (empty($ids_json))
        {
            //$ids_json нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $IDS = json_decode($ids_json);
        // берем из БД
        $list = $this->model->getByIds($IDS);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $list;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение клиента
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $client_id = $this->auth->loggedUser->id;
        $client = $this->model->getById($client_id);

        if (!empty($client)) {
            // найден - изменяем - можно вооще сказать
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'user_insta_nick' => $this->input->post('user_insta_nick'),
                'email' => $this->input->post('email'),
                'birthday' => $this->input->post('birthday'),
                'first_name' => $this->input->post('first_name'),
                'patronym_name' => $this->input->post('patronym_name'),
                'last_name' => $this->input->post('last_name'),
            ];

            $phone = $this->input->post('phone');
            $data['phone'] = (!empty($phone)) ? $phone : 'NULL';

            $timezone = $this->input->post('timezone');
            if (!empty($timezone)) $data['timezone'] = $timezone;

            $subscribed_system = $this->input->post('subscribed_system');
            if (array_key_exists('subscribed_system', $_POST))  $data['subscribed_system'] = intval(boolval($subscribed_system));

            $subscribed_news = $this->input->post('subscribed_news');
            if (array_key_exists('subscribed_news', $_POST))  $data['subscribed_news'] = intval(boolval($subscribed_news));

            $pass = $this->input->post('pass');
            if (!empty($pass)) {
                $pass_enc = $this->utils->hash_password($pass, $client->salt);
                $data['pass'] = $pass_enc;
            }

            $this->db->trans_begin();
            $upd_res = $this->model->update($client_id, $data);

            if ($upd_res !== false) {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            } else {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        } else {
            // client не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
    }

    /**
     * Логинит юзера , используя один из вариантов (email, user_insta_nick, phone) в качестве логина
     * сохраняет данные в auth
     *
     * @params string $identity
     * @params string $password
     *
     * @return object - JSON-envelope
     */
    public function login()
    {
        //$this->load->model('insta_accounts_model');
        //$this->load->library('encryption');

        $identity = trim($this->input->post('identity'));
        $password = trim($this->input->post('password'));

        $client = $this->model->getByLoginData($identity, $password);

        if ( !empty($client) )
        {
            if (!empty($client->user_insta_id))
            {
                $user_insta_data = $this->get_insta_user_data($client->user_insta_id);

                if (!empty($user_insta_data))
                {
                    $fullname = explode(" ", $user_insta_data->full_name);
                    $uData = [
                        'user_insta_nick' => $user_insta_data->username,
                        'avatar' => $user_insta_data->profile_pic_url,
                        'first_name' => $fullname[0],
                        'last_name' => $fullname[1],
                    ];
                    $this->model->update($client->id, $uData);

                    if (is_object($client)) $client = (array)$client;
                    $client = array_merge($client, $uData); // это ПРОВЕРИТЬ! возможно, это объект
                    $client = (object)$client;
                    /*
                                    $client_data = array(
                                        'id' => $client->id,
                                        'pk' => $user_insta_data->pk,
                                        'username' => $user_insta_data->username,
                                        'full_name' => $user_insta_data->full_name,
                                        'profile_pic_url' => $user_insta_data->profile_pic_url,
                                        'phone' => $client->phone,
                                        'email' => $client->email,
                                    );
                                    $this->session->set_userdata('client_data', $client_data);
                    */
                } //if (!empty($client->user_insta_id))

            }//if (!empty($client->user_insta_id))


            $this->auth->role = 'client';
            $this->auth->loggedUser = $client;

            // создаем токен -------------------------------------------
                $tData = ['sub'=>'client', 'id'=>$client->id];
                // устанавливаем токен в output headers
                $this->auth->tokenSet($tData);
                // сразу его "поднимаем" для заполнения auth->token_data
                $this->auth->tokenGet( 'access', $this->auth->token_string );
            // END создаем токен -------------------------------------------

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );

        } //if ( !empty($client) )
        else
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /**
     * Производит логаут юзера
     *
     * @return object - JSON-envelope
     */
    public function logout()
    {
        $this->auth->logout();

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** Регистрирует пользователя из возвращает envelope с ошибкой
     *
     * @param string instagram_name
     * @param string email
     * @param string phone
     *
     * @return object - JSON-envelope
     */
    public function register()
    {
        $use_instagram_data_get = $this->config->item('use_instagram_data_get', 'instagram');

        $user_insta_nick = trim($this->input->post('instagram_name'));
        $phone = trim($this->input->post('phone'));
        $email = trim($this->input->post('email'));

        if ( empty($email) )
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ( !$this->model->isUnique('email', $email) )
        {
            $status = STATUS_NOT_UNIQUE;
            $message = "Пользователь с таким Email уже зарегистрирован";
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        elseif ( (!empty($phone)) && (!$this->model->isUnique('phone', $phone)) )
        {
            $status = STATUS_NOT_UNIQUE;
            $message = "Пользователь с таким телефоном уже зарегистрирован";
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        elseif ( (!empty($user_insta_nick)) && (!$this->model->isUnique('user_insta_nick', $user_insta_nick)) )
        {
            $status = STATUS_NOT_UNIQUE;
            $message = "Пользователь с таким ником уже зарегистрирован";
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // параметры уникальны - создаем
        $salt = $this->utils->salt();
        $pass = $this->utils->generateRandomString(8);
        $pass_enc = $this->utils->hash_password($pass, $salt);
        $secret_code = md5($this->utils->generateRandomString(10));

        $uData = [
            'user_insta_nick' => $user_insta_nick,
            'email' => $email,
            'pass' => $pass_enc,
            'salt' => $salt,
            'secret_code' => $secret_code,
            'status' => 'inactive'
        ];

        if (!empty($phone)) $uData['phone'] = $phone;

        if ( (!empty($user_insta_nick)) && ($use_instagram_data_get)  )
        {
            $response = $this->getInstaUserData($user_insta_nick);

            if (!empty($response))
            {
                if ($response === STATUS_INSTAGRAM_LOGIN_FAILED)
                {
                    $status = STATUS_INSTAGRAM_LOGIN_FAILED;
                    $message = STATUS_INSTAGRAM_LOGIN_FAILED_MSG;
                    $data = [];

                    return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                }

                if ($response === STATUS_INSTAGRAM_USERDATA_ERROR)
                {
                    $status = STATUS_INSTAGRAM_USERDATA_ERROR;
                    $message = STATUS_INSTAGRAM_USERDATA_ERROR_MSG;
                    $data = [];

                    return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                }

                // данные Instagram получены -------------------------
                $uData['user_insta_pk'] = $response->pk;
                    $fullname = explode(" ", $response->full_name);
                $uData['first_name'] = $fullname[0];
                $uData['last_name'] = $fullname[1];
                $uData['avatar'] = $response->profile_pic_url;
                // END данные Instagram получены -------------------------
            }
        }

        $new_id = $this->model->insert($uData);
        if ($new_id)
        {
            $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
            $activation_url = $this->config->item('members_url_activate', 'cms_consts');
            $activation_link = $activation_url."?code=".$secret_code;
            $html = $mail_header_logo.
                "<p>Поздравляем! Ты в шаге от завершения регистрации на новой платформе Риммы Карамовой!</p>".
                "<p>Осталось лишь активировать свою учетную запись и затем ввести свой логин и пароль, чтобы получить доступ к открывшимся новым возможностям!</p>".
                "<br/>".
                "<p>Ссылка для активации учетной записи: </p><strong><a href='".$activation_link."'>" . $activation_link . "</a></strong>".
                "<br/><br/>".
                "<p>Для авторизации перейди сюда: </p><strong>" . $this->members_path . "</strong>".
                "<p>Ваш логин: </p><strong>" . $uData['email'] . "</strong>".
                "<p>Ваш пароль: </p><strong>" . $pass . "</strong>";

            $eData = [
                'toEmail' => $uData['email'],
                'subject' => "Регистрация в " . $this->project_name,
                'body'    => $html,
            ];
            $this->mailer->send($eData);

            $caller = $this->caller_origin;
            $this->caller_origin = "admin"; // здесь клиент еще неактивен
            $user = $this->model->getById($new_id);
            $this->caller_origin = $caller;

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $user;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        // не удалось сохранить
        $status = STATUS_CREATE_FAIL;
        $message = STATUS_CREATE_FAIL_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Отправляет ссылку для восстановления пароля на email, указанный при регистрации
     *
     * @param string $identity - user_insta_nick, email, phone
     *
     * @return object - JSON-envelope
     */
    public function remindPassLink()
    {
        $identity = $this->input->post('identity');

        if (empty($identity))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $user */
        $user = $this->model->getByRegData($identity, 'active');

        if (empty($user))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $secret_code = $this->utils->generateRandomString(40);
        $res = $this->model->update($user->id, ['secret_code'=>$secret_code]);

        if (!$res)
        {
            // не удалось
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $link = $this->config->item('members_url_reset_pass', 'cms_consts')."/?code=".$secret_code;
        $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
        $html = $mail_header_logo.
                "<p>Кто-то (возможно, Вы сами) попросил напомнить пароль.</p>".
                "<p>Если это были Вы, проследуйте по ссылке: <a href='".$link."'><strong>".$link."'</strong></a></p>";
        $subj = "Восстановление пароля в " . $this->project_name;
        //$this->utils->send_mail_gmail($html, 'Восстановление пароля в anker-crm', $user->email);

        $eData = [
            'toEmail' => $user->email,
            'subject' => $subj,
            'body'    => $html,
        ];
        $this->mailer->send($eData);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Меняет пароли в случае совпадения secret_code и высылает его пользователю на email, указанный при регистрации
     *
     * @param string $code - сукретный код. Берется из GET
     *
     * @return object - JSON-envelope
     */
    public function resetPass()
    {
        $code = $this->input->get_post('code');

        if (empty($code))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $user */
        $user = $this->model->getBySecretCode($code, 'active');
        $salt = $this->model->getSalt($user->id);

        if (empty($user))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $pass = $this->utils->generateRandomString(8);
        $pass_enc = $this->utils->hash_password($pass, $salt);

        $uData = [
            'pass' => $pass_enc,
            'secret_code' => ""
        ];
        $res = $this->model->update($user->id, $uData);

        if (!$res)
        {
            // не удалось
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
        $html = $mail_header_logo.'<p>Держите новый пароль и больше не теряйте! :)</p><p>Ваш новый пароль: <strong>'.$pass.'</strong></p>';
        //$this->utils->send_mail_gmail($html, 'Ваш новый пароль в anker-crm', $user->email);
        $eData = [
            'toEmail' => $user->email,
            'subject' => "Ваш новый пароль в " . $this->project_name,
            'body'    => $html,
        ];
        $this->mailer->send($eData);


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Активирует учетную запись клиента, использую secret_code
     *
     * @param string $code - сукретный код. Берется из GET
     *
     * @return object - JSON-envelope
     */
    public function activate()
    {
        $code = $this->input->get_post('code');

        if (empty($code))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $client */
        $client = $this->model->getBySecretCode($code);

        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $uData = [
            'secret_code' => "NULL",
            'status' => "active",
        ];
        $res = $this->model->update($client->id, $uData);

        if (!$res)
        {
            // не удалось
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
        $html = $mail_header_logo.'<p style="text-align: center;">Ваша учетная запись успешно активирована! :)</center></p>';
        $eData = [
            'toEmail' => $client->email,
            'subject' => $this->project_name." - Активация успешна! ",
            'body'    => $html,
        ];
        $this->mailer->send($eData);


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** Получает из Instagram данные по пользовательскому нику
     * @param string $insta_nick
     *
     * @return StdClass | int - данные или error_code
     */
    protected function getInstaUserData($insta_nick)
    {
        IGram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $ig = new IGram(false, false);
        //$rankToken = \InstagramAPI\Signatures::generateUUID();

        $instagram_auth_data = $this->config->item('default_instagram_account', 'instagram');

        try {
            $ig->login($instagram_auth_data['login'], $instagram_auth_data['password']);

            try
            {
                $user_data = $ig->people->getInfoByName($insta_nick)->asStdClass();

                return $user_data->user;
            }
            catch (\Exception $e)
            {
                return STATUS_INSTAGRAM_USERDATA_ERROR;
            }

        }
        catch (\Exception $e)
        {
                return STATUS_INSTAGRAM_LOGIN_FAILED;
        }

    }

    /** загрузка аватара.
     *
     * @param string $_FILES['file'] - загружаемый файл
     *
     * @return  object - JSON-объект формата envelope
     */
    public function uploadAvatar()
    {
        $client_id = $this->auth->loggedUser->id;

        /** @var StdClass $client */
        $client = $this->model->getById($client_id);

        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $this->ftp->connect($this->ftp_config);
        if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);

        $old_avatar = $client->avatar;

        $parts = pathinfo($_FILES['file']['name']);
        $ext = $parts['extension'];
        $basename = $parts['filename'];

        $filename = md5(uniqid()) . "_" . $basename . "." . $ext;

        $uData = [
            'avatar' => $filename,
        ];
        $affected_rows = $this->model->update($client_id, $uData);

        if ($affected_rows !== false)
        {

            // у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------
            if( (!empty($old_avatar)) && (strpos("://",$old_avatar) === false) )
            {
                @$this->ftp->delete_file($client->avatar);
            }

            $this->ftp->upload($_FILES['file']['tmp_name'], $filename, 'binary');
            // END у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($client_id);

            $this->ftp->close();

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // что-то пошло не так
        $status = STATUS_NOT_FOUND;
        $message = STATUS_NOT_FOUND_MSG;
        $data = [];

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }


    /** удаление аватара
     *
     *  @param   int id  - ID клиента. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function deleteAvatar()
    {
        $client_id = $this->auth->loggedUser->id;

        if (empty($client_id))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $client */
        $client = $this->model->getById($client_id);

        if (empty($client))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->ftp->connect($this->ftp_config);
        if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);

        $old_avatar = $client->avatar;
        // у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------
        if( (!empty($old_avatar)) && (strpos("://",$old_avatar) === false) )
        {
            @$this->ftp->delete_file($client->avatar);
            $uData = [
                'avatar' => null,
            ];
            $affected_rows = $this->model->update($client_id, $uData);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->getById($client_id);

        $this->ftp->close();

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
