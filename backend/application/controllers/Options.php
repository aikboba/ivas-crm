<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Options
 * @property Products_model $products_model
 * @property Options_model $options_model
 * @property Clients_model $clients_model
 * @property Promocodes_model $promocodes_model
 * @property Privileges_model $privileges_model
 * * @property StdClass $CI
 */
class Options extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('options_model');
        $this->load->model('products_model');
        $this->load->model('clients_model');
        $this->load->model('promocodes_model');

        $this->model = $this->options_model;
    }

    /** получает данные об опции
     *
     *  @param   int $id  - ID опции. Берется из post
     *  @param   string|null $promocode  - Строка-промокод. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $option_id = intval($this->input->post_get('id'));
        if (empty($option_id))
        {
            //option_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode = $this->input->post_get('promocode');
        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
            if (empty($promocode))
            {
                //promocode не найден- выходим
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        // берем из БД
        /** @var StdClass $option */
        $option = $this->model->getWithDiscount($option_id, $this->auth->loggedUser, $promocode);
        if (empty($option))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $option;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список опций по продукту
     *
     * @param  int $product_id - берется из $_POST
     * @param   string|null $promocode  - Строка-промокод. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode = $this->input->post_get('promocode');
        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
            if (empty($promocode))
            {
                //promocode не найден- выходим
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = [];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        $params = [];
        $params[] = ['product_id', "=", $product_id];

        $list = $this->model->getList($params);

        if (!empty($list))
        {
            foreach ($list as $key=>$option)
            {
                $list[$key] = $this->model->getWithDiscount($option, $this->auth->loggedUser, $promocode);
            }
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

}
