<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Orders
 * @property Products_model $products_model
 * @property Options_model $options_model
 * @property Paysystems_model $paysystems_model
 * @property Orders_model $orders_model
 * @property Clients_model $clients_model
 * @property Promocodes_model $promocodes_model
 */
class Orders extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->load->model('promocodes_model');
        $this->load->model('paysystems_model');
        $this->load->model('options_model');
        $this->load->model('clients_model');
        $this->load->model('orders_model');
        $this->model = $this->orders_model;
    }

    /** получает данные о заказе
     *
     *  @param   int id  - ID заказа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $order_id = $this->input->post('id');
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $order = $this->model->getById($order_id);
        if ($order->client_id != $this->auth->loggedUser->id)
        {
            //это не заказ клиента
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $order;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список заказов клиента
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $client_id = $this->auth->loggedUser->id;
        $list = $this->model->getByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


    /** получает список заказов в remote-mode vue DataTable формате
     *
     * @params json $server_params - json_encoded(!!!) параметры вида
     * serverParams: {
     *               columnFilters: {first_name: 'john', status: 'active'}, // пары "поле"->"значение" - параметры поиска по колонками(?)
     *               sort: [
     *                       { field: 'created', type: 'desc' },
     *                       { field: 'email', type: 'asc' },
     *               ],
     *               page: 1, // номер страницы
     *               perPage: 10 // сколько записей отображать на страницу
     *               }
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListAdv()
    {
        $params = $order = $limit = null;
        $server_params = $this->input->post('server_params'); // это "serverParams" good vue пакет оно же "filtering"
        $search_params = $this->input->post('search_params'); // это пакет "выборки"

        if (!empty($server_params))
        {
            $server_params = json_decode($server_params);
            $prepared_params = $this->transformServerSideParams($server_params);
            $params = $prepared_params['params'];
            $order = $prepared_params['order'];
            $limit = $prepared_params['limit'];
        }

        // здесь начинается нехитрая магия!
        if (!empty($search_params))
        {
            $search_params = json_decode($search_params);
            foreach ($search_params as $arr)
            {
                $params[] = $arr;
            }
        }

        $client_id = $this->auth->loggedUser->id;
        $params[] = ['client_id', '=', $client_id];

        $list = $this->model->getListAdv($params, $order, $limit, []);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** создание заказа
     *
     *  @param   int option_id  - ID опции продукта. Берется из post
     *  @param   int promocode_id  - промокод, который ввел клиент. Берется из post
     *  @param   string $client_message  - сообщение пользователя.
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $option_id = intval($this->input->post('option_id'));
        $promocode = $this->input->post('promocode');
        $client_message = $this->input->post('client_message');
//$option_id=1;
        if (empty($option_id) )
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
        }

        // получаем клиента
        $client = $this->auth->loggedUser;
//$client=$this->clients_model->getById(1);
        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // получаем указаную опцию
        /** @var StdClass $option */
        $option = $this->options_model->getWithDiscount($option_id, $client, $promocode);
        if (empty($option))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: опция не найдена";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // промокод есть, но применим ли он в данном случае?
        if (!empty($promocode))
        {
            if (!$this->promocodes_model->isApplicable($promocode, $option, $client))
            {
                $promocode = null;
            }
        }

        // получаем продукт по опции
        /** @var StdClass $product */
        $product = $this->products_model->getById($option->product_id);
        if (empty($product))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: продукт не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        };

        $price = $option->price;
        $aData['client'] = $client;
        $aData['product'] = $product;
        $aData['option'] = $option;
        if (!empty($promocode)) $aData['promocode'] = $promocode;
        $data = [
            'option_id' => $option->id,
            'client_id' => $client->id,
            'price' => $price,
            'client_message' => $client_message,
            'additional_data' => json_encode($aData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
        ];

        if (!empty($promocode)) $data['promocode_id'] = $promocode->id;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            //в случае, если price=0 (например, из-за промокода), заказ "самооплачиваем" ------------
            if ($price == 0)
            {
                $this->model->pay($new_id, $option, $product, $client, 0);
            }
            //end в случае, если price=0 (например, из-за промокода), заказ "самооплачиваем" ------------

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** отмена заказа
     *
     *  @param   int id  - ID заказа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function cancel()
    {
        $order_id = $this->input->post('id');
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $order = $this->model->getById($order_id);
        if (empty($order))
        {
            //order_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();

        $cancel_res = $this->model->cancel($order_id);

        if ($cancel_res)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
