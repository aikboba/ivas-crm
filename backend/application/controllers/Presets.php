<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Presets
 * @property Presets_model $presets_model
 * @property Presets_items_model $presets_items_model
 * @property Clients_presets_model $clients_presets_model
 */
class Presets extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('presets_model');
        $this->model = $this->presets_model;
        $this->load->model('presets_items_model');
        $this->load->model('clients_presets_model');

    }

    public function index()
    {

    }

    /** получает данные о пресете
     *
     *  @param   int id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $preset_id = $this->input->post('id');
        $client_id = $this->auth->loggedUser->id;

        if (empty($preset_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        /** @var StdClass $preset */
        $preset = $this->model->getById($preset_id);
        if (empty($preset))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // для клиента уместна проверка, и досутпен ли ему данный пресет?
        if ($this->clients_presets_model->isPresetAvailableForClient($preset_id, $client_id))
        {
            //собираем items ДОСТУПНЫЕ -----------------------------
            //$items = $this->presets_items_model->getItemsByPresetId($preset->id);
            $items = $this->presets_items_model->getAvailableItemsForClientByPreset($client_id, $preset_id);
            $preset->items = $items;
            //END собираем items -----------------------------
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $preset;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список SHOWABLE пресетов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListShowable()
    {
        $client_id = $this->auth->loggedUser->id;

        $list = $this->model->getShowablePresetsForClient($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


}
