<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Products
 *
 * @property Products_model $products_model
 * @property Products_model $model
 * @property Taskmanager $taskmanager
 * @property Instagram_accounts_model $instagram_accounts_model
 * @property Messinger $messinger
 */
class Products extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->model = $this->products_model;

        $this->load->config('messinger', true);
    }

    public function index()
    {

    }

    /** получает данные о продукте
     *
     *  @param   int id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $product_id = $this->input->post_get('id');

            if (empty($product_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $product = $this->model->getById($product_id);

        $product->chat = (object)[];
        $product->chat = $this->utils->getChatObject($product->stream_item_id);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $product;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список бесплатных продуктов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListFree()
    {
        $list = $this->model->getFree();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список ДОСТУПНЫХ СЕЙЧАС К ПОКУПКЕ products по client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListPurchasableProductsByClientId()
    {
        if ($this->auth->isLogged()) $client_id = $this->auth->loggedUser->id;

//        $client_id=1;
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPurchasableProductsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список АКТИВНЫХ продуктов для мобильного приложения
     * 2DO: закрыть через acl_config , либо решить с доступами
     *
     * @param int $client_id - ID лиента-блоггера, которому "принадлежат" продукты
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListForBlogger()
    {
        //$blogger = $this->input->post('client_id');
        $blogger = $this->auth->loggedUser->id;

        $list = $this->model->getByBlogger($blogger);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** реакция на сигнал от мобильного приложения на начало стрима по продукту
     *
     *  @param  int product_id - ID продукта. Берется из post
     *  @param  string orientation - Ориентация экрана vertical | horizontal
     *  @param  int width - ширина трансляции, px
     *  @param  int height - высота трансляции, px
     *  @param  int fps - FPS трансляции
     *
     *  @param  string broadcast - где будет открыт стрим (rtmp | youtube | vimeo) - временно. Для отладочных целей
     *
     * @return  object - JSON-объект формата envelope - содержит только поле steram_host_relay_url - rtmp ссылку, куда стримить приложению-хозяину
     */
    public function startStream()
    {
        $this->load->library('taskmanager');

        $this->load->config('stream', true);

        $orientation = $this->input->post('orientation');
        if (empty($orientation)) $orientation = $this->config->item('stream_default_orientation', 'stream');

        $width = intval($this->input->post('width'));
        if (empty($width)) $width = $this->config->item('stream_default_width', 'stream');

        $height = intval($this->input->post('height'));
        if (empty($height)) $height = $this->config->item('stream_default_height', 'stream');

        $fps = intval($this->input->post('fps'));
        if (empty($fps)) $fps = $this->config->item('stream_default_fps', 'stream');

        $broadcast = intval($this->input->post('broadcast'));

        $product_id = intval($this->input->post_get('product_id'));
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $product */
        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно начать трансляцию, неверный тип продукта";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
/*
        //при попытке начать уже начатую трансляцию Сергей вернет сущетвующую информацию, не создавая новый "транскодер"
        if ($product->stream_status == 'active')
        {
            //стрим уже идет. Попытка повторного начала стрима без завершения предыдущего
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно начать новую трансляцию, т.к. трансляция уже начата";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
*/
        $returnData = []; $task = null;

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/startStream",
            'consumer'  => "transcoderStart",
            'priority'  => 100,
            'start_dt'  => "NULL",
            'data_in'   => [
                            "product_id" => $product_id,
                            "orientation" => $orientation,
                            "width" => $width,
                            "height" => $height,
                            "fps" => $fps,
                            "broadcast" => $broadcast,
                           ],
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на начало стрима в очереди для транскодера
        if (!empty($new_task_id))
        {
            /** @var object $task */
            $task = $this->taskmanager->waitForTask($new_task_id, $timeout = 20); // ожидаю выполнения

            if (!empty($task))
            {
                // задача из tasks_completed выполнена
                if ($task->status == 'error')
                {
                    // выполнена с ошибкой
                    $status = STATUS_FAIL;
                    $message = $task->status_msg;
                }
                else
                {
                    /* выполнена без ошибки - берем из нее данные для начала стрима
                     * ожидаем наличия
                        stream_url_data - данные со ссылками на трансляциям в разном качестве
                        steram_host_relay_url - куда транслирует блоггер с мобилы
                        stream_origin
                        stream_item_guid
                    */
                    $task->data_out = json_decode($task->data_out);
                    $uData = [
                        'stream_status' => 'active',
                        //'stream_url_data' => json_encode((object)$task->data_out->stream_url_data),
                        'stream_url_data' => json_encode((object)$task->data_out->stream_url_data),
                        'stream_host_relay_url' => $task->data_out->stream_host_relay_url,
                        'stream_origin' => $task->data_out->stream_origin,
                        'stream_item_guid' => $task->data_out->stream_item_guid,
                        'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                        'stream_ended' => 'NULL',
                        'stream_clients_informed' => 0,
                        'stream_health' => 'red',
                        'stream_health_last_changed_dt' => (new Datetimeex())->format("Y-m-d H:i:s"),
                    ];

                    $this->db->trans_begin();

                    $upd_res = $this->model->setStream($product_id, $uData);

                    if ($upd_res!==false)
                    {
                        $status = STATUS_OK;
                        $message = STATUS_OK_MSG;
                        //$returnData = (object)[ 'stream_host_relay_url' => $task->data_out->stream_host_relay_url ];
                        //$returnData = (object)[ 'relay_url' => $task->data_out->stream_host_relay_url ]; // ПОКА!!! чтобы не переделывать приложение
                        $returnData = $this->model->getById($product_id);

                        if (!empty($product->instagram_account_id))
                        {
                            // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                            $this->load->model('instagram_accounts_model');
                            $iuData = [
                                'stream_origin' => $uData['stream_origin'],
                                'stream_url_data' => $uData['stream_url_data'],
                                'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                                'stream_ended' => 'NULL',
                                'stream_status' => 'active'
                            ];

                            $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                            // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                        }//if (!empty($product->instagram_account_id))

                        $this->db->trans_commit();

                        // ЭТО УДАЛЕНО В НОВОЙ КОНЦЕПЦИИ  (раньше нужна была задержка перед получением stream_url_data!!!)
                        /*
                        $d = new Datetimeex();
                        $d->add(new DateInterval('PT10S'));
                        $task_data_in = [
                            'system'    => "backend",
                            'publisher' => "products/startStreamUrlDataDelay",
                            'consumer'  => "startStreamUrlDataDelay",
                            'priority'  => 100,
                            'start_dt'  => $d->format('Y-m-d H:i:s'),
                            'data_in'   => [
                                "product_id" => $product_id,
                                "stream_url_data" => (object)$task->data_out->stream_url_data,
                            ],
                        ];
                        $this->taskmanager->createTask($task_data_in); // создаю задачу на отложенный стримУРЛДата
                        */
/* ТЕПЕРЬ ЭТО ДЕЛАЕТСЯ В WORKER на streamHealthChanged
                        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_product';
                        $this->messinger->target = []; //all
                        $this->messinger->data = ['product_id'=>(string)$product_id];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
*/
                    }
                    else
                    {
                        $status = STATUS_FAIL;
                        $message = STATUS_FAIL_MSG;
                        $this->db->trans_rollback();
                    }

                }

            }
            else
            {
                // выполнена с ошибкой
                $status = STATUS_FAIL;
                $message = "Не удалось начать трансляцию";
            }
        }
        else
        {
            // не удалось создать задачу - возвращаем ошибку
            $status = STATUS_FAIL;
            $message = "Не удалось начать трансляцию";
        }

        $data = (!empty($returnData)) ? $data = $returnData : (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** реакция на сигнал от мобильного приложения на окончание стрима по продукту
     *  Данная задача АСИНХРОННАЯ (я не жду ответа от траскодера)
     *
     *  @param   int product_id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function stopStream()
    {
        $this->load->library('taskmanager');

        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $product */
        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно закончить трансляцию, неверный тип продукта";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->stream_status == 'inactive')
        {
            //стрим уже остановлен. Попытка повторного останова стрима
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно закончить трансляцию, т.к. трансляция не начата";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $returnData = (object)[]; $task = null;

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/stopStream",
            'consumer'  => "transcoderStop",
            'data_in'      => ["product_id" => $product_id],
            'priority'  => 100,
            'start_dt'  => "NULL",
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на окончание стрима в очереди для транскодера

        if (!empty($new_task_id))
        {
                $uData = [
                    'stream_status' => 'inactive',
                    'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
                ];

                $this->db->trans_begin();

                $upd_res = $this->model->setStream($product_id, $uData);

                if ($upd_res!==false)
                {
                    $status = STATUS_OK;
                    $message = STATUS_OK_MSG;
                    $returnData = $this->model->getById($product_id);

                    if (!empty($product->instagram_account_id))
                    {
                        // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                        $this->load->model('instagram_accounts_model');
                        $iuData = [
                            //'stream_origin' => $uData['stream_origin'],
                            //'stream_url_data' => $uData['stream_url_data'],
                            'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                            'stream_status' => 'inactive',
                            'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
                        ];

                        $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                        // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                    }//if (!empty($product->instagram_account_id))

                    $this->db->trans_commit();

                    // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                    $this->load->library('messinger');
                    $this->messinger->event_id = $this->utils->generateEventGuid();
                    $this->messinger->action = 'system_refresh_product';
                    $this->messinger->target = []; //all
                    $this->messinger->data = ['product_id'=>(string)$product_id];
                    $this->messinger->send();
                    // END отправляем сообщение "обнови статус продукта" через WS ------------------------------

                }

        }
        $data = (!empty($returnData)) ? $data = $returnData : (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** реакция на сигнал от мобильного приложения на PAUSE стрима по продукту
     *  Данная задача АСИНХРОННАЯ (я не жду ответа от траскодера)
     *
     * ПРИМЕЧАНИЕ: в соответствии с последним изменением, я не создаю транскодеру задачу pauseStream
     *
     *  @param   int product_id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function pauseStream()
    {
        $this->load->library('taskmanager');

        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $product */
        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно приостановить трансляцию, неверный тип продукта";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->stream_status != 'active')
        {
            //стрим уже остановлен. Попытка повторного останова стрима
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно приостановить трансляцию, т.к. трансляция не начата";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $returnData = (object)[];
        $uData = [
            'stream_status' => 'paused',
        ];

        $this->db->trans_begin();

        $upd_res = $this->model->setStream($product_id, $uData);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $returnData = $this->model->getById($product_id);

            if (!empty($product->instagram_account_id))
            {
                // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                $this->load->model('instagram_accounts_model');
                $iuData = [
                    'stream_status' => 'paused',
                ];

                $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
            }//if (!empty($product->instagram_account_id))

            $this->db->trans_commit();
/* // ПОКА WS не рассылаю
            // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
            $this->load->library('messinger');
            $this->messinger->event_id = $this->utils->generateEventGuid();
            $this->messinger->action = 'system_refresh_product';
            $this->messinger->target = []; //all
            $this->messinger->data = ['product_id'=>(string)$product_id];
            $this->messinger->send();
            // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
*/
        }
        $data = (!empty($returnData)) ? $data = $returnData : (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** реакция на сигнал от мобильного приложения на снятие с паузы стрима по продукту
     *  Данная задача АСИНХРОННАЯ (я не жду ответа от траскодера)
     *
     * ПРИМЕЧАНИЕ: в соответствии с последним изменением, я не создаю транскодеру задачу unpauseStream
     *
     *  @param   int product_id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function unpauseStream()
    {
        $this->load->library('taskmanager');

        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $product */
        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно приостановить трансляцию, неверный тип продукта";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->stream_status != 'paused')
        {
            //стрим не на паузе
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно продолжить трансляцию, т.к. трансляция не находится на паузе";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $returnData = (object)[];
        $uData = [
            'stream_status' => 'active',
            'stream_health_last_changed_dt' => (new Datetimeex())->format("Y-m-d H:i:s"),
        ];

        $this->db->trans_begin();

        $upd_res = $this->model->setStream($product_id, $uData);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $returnData = $this->model->getById($product_id);

            if (!empty($product->instagram_account_id))
            {
                // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                $this->load->model('instagram_accounts_model');
                $iuData = [
                    'stream_status' => 'active',
                ];

                $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
            }//if (!empty($product->instagram_account_id))

            $this->db->trans_commit();
            /* // ПОКА WS не рассылаю
                        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_product';
                        $this->messinger->target = []; //all
                        $this->messinger->data = ['product_id'=>(string)$product_id];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
            */
        }
        $data = (!empty($returnData)) ? $data = $returnData : (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** реакция на сигнал от мобильного приложения на PAUSE стрима по продукту
     *  Данная задача АСИНХРОННАЯ (я не жду ответа от траскодера)
     *
     * ПРИМЕЧАНИЕ: в соответствии с последним изменением, я не создаю транскодеру задачу stopStream
     *
     *  @param   int product_id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function pauseStreamOLD()
    {
/*
        $this->load->library('taskmanager');

        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно приостановить трансляцию, неверный тип продукта";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->stream_status == 'inactive')
        {
            //стрим уже остановлен. Попытка повторного останова стрима
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = "Невозможно приостановить трансляцию, т.к. трансляция не начата";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $returnData = (object)[]; $task = null;

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/pauseStream",
            'consumer'  => "transcoderPause",
            'data_in'      => ["product_id" => $product_id],
            'priority'  => 100,
            'start_dt'  => "NULL",
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на окончание стрима в очереди для транскодера

        if (!empty($new_task_id))
        {
            $uData = [
                'stream_status' => 'paused',
            ];

            $this->db->trans_begin();

            $upd_res = $this->model->setStream($product_id, $uData);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $returnData = $this->model->getById($product_id);

                if (!empty($product->instagram_account_id))
                {
                    // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                    $this->load->model('instagram_accounts_model');
                    $iuData = [
                        'stream_status' => 'paused',
                    ];

                    $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                    // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                }//if (!empty($product->instagram_account_id))

                $this->db->trans_commit();

                // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                $this->load->library('messinger');
                $this->messinger->event_id = $this->utils->generateEventGuid();
                $this->messinger->action = 'system_refresh_product';
                $this->messinger->target = []; //all
                $this->messinger->data = ['product_id'=>(string)$product_id];
                $this->messinger->send();
                // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
            }

        }
        $data = (!empty($returnData)) ? $data = $returnData : (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
*/
    }


    /** Метод запускается по CRON и создает задачу по остановке "зависшего" стрима по продукту.
     * Стрим считается зависшим, если его текущий steam_status!=(active или inactive) и при этом с момента последнего изменения стутуса
     * стрима прошло более config.stream_health_max_sec_allowed секунд. Настройка хранится в /configs/stream.php
     *
     * Данная задача АСИНХРОННАЯ (я не жду ответа от траскодера)
     *
     *
     * @return  object - JSON-объект формата envelope
     */
    public function cronStopStream()
    {
        $this->load->library('taskmanager');
        $this->load->config('stream', true);
        $stream_health_max_sec_allowed = $this->config->item('stream_health_max_sec_allowed', 'stream');

        $params = [
            ["product_type='subscription'"],
            ["products.stream_status!='inactive'"],
            ["products.stream_health='red'"],
            ["TIMESTAMPDIFF(SECOND,products.stream_health_last_changed_dt,NOW())>=".$stream_health_max_sec_allowed],
        ];
        $products = $this->model->getByParams($params);

        foreach ($products as $product)
        {

            $task_data_in = [
                'system'    => "transcoder",
                'publisher' => "products/stopStreamCron",
                'consumer'  => "transcoderStop",
                'data_in'      => ["product_id" => $product->id],
                'priority'  => 100,
                'start_dt'  => "NULL",
            ];
            $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на окончание стрима в очереди для транскодера

            if (!empty($new_task_id))
            {
                $uData = [
                    'stream_status' => 'inactive',
                    'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
                ];

                $this->db->trans_begin();
                $upd_res = $this->model->setStream($product->id, $uData);

                if ($upd_res!==false)
                {

                    if (!empty($product->instagram_account_id))
                    {
                        // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                        $this->load->model('instagram_accounts_model');
                        $iuData = [
                            //'stream_origin' => $uData['stream_origin'],
                            //'stream_url_data' => $uData['stream_url_data'],
                            'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                            'stream_status' => 'inactive',
                            'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
                        ];
                        $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                        // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                    }//if (!empty($product->instagram_account_id))

                    $this->db->trans_commit();
/* // ПОКА WS не отправляю
                    // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                    $this->load->library('messinger');
                    $this->messinger->event_id = $this->utils->generateEventGuid();
                    $this->messinger->action = 'system_refresh_product';
                    $this->messinger->target = []; //all
                    $this->messinger->data = ['product_id'=>(string)$product_id];
                    $this->messinger->send();
                    // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
*/
                } //if ($upd_res!==false)

                // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                $this->load->library('messinger');
                $this->messinger->event_id = $this->utils->generateEventGuid();
                $this->messinger->action = 'system_refresh_product';
                $this->messinger->target = []; //all
                $this->messinger->data = ['product_id'=>(string)$product->id];
                $this->messinger->send();
                // END отправляем сообщение "обнови статус продукта" через WS ------------------------------


            } //if (!empty($new_task_id))

        } // foreach products

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** получает список SHOWABLE продуктов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListShowable()
    {
        $client_id = $this->auth->loggedUser->id;

        $list = $this->model->getShowableProductsForClient($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

}
