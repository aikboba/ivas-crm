<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class promocodes
 * @property Promocodes_model $promocodes_model
 * @property Options_model $options_model
 * @property Clients_model $clients_model
 */
class Promocodes extends Base_Front_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('promocodes_model');
        $this->model = $this->promocodes_model;
    }

    public function index()
    {

    }

    /** получает данные о промокоде
     *
     *  @param   int id  - ID промокода. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $promocode_id = $this->input->post('id');
        if (empty($promocode_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $promocode = $this->model->getById($promocode_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $promocode;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** Определяет, может ли быть применен указанный промокод указанным клиентом к указанному продукту/опции
     * (промокод применим, если:
     *  - текущая дата лежит между start_dt и end_dt (при запросе с фронта реализуется моделью)
     *  - он связан с клиентом + с опцией или продуктом
     *  - он связан с продуктом
     *  - он связан с опцией
     *  - если promocodу one_time_only, то он не должен фигурировать ни в одном заказе (если передан client, то ни в одном заказе КЛИЕНТА)
     * )
     *
     * @param string $promocode - сам промокод (строка)
     * @param int $option_id - option_id опции
     *
     * @return int 0 - ошибка или промокод неприменим
     *             1 - промокод применим
     *            -1 - промокод применим, но применение не имеет смысла, т.к. скидка от промокода дает меньший эффект, чем льготы
     */
    public function isApplicable()
    {
        $promocode = $this->input->post('promocode');
        if (empty($promocode))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode = $this->model->getByPromocode($promocode);
        if (empty($promocode))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $option_id = $this->input->post('option_id');
        if (empty($option_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->load->model('options_model');
        $option = $this->options_model->getById($option_id);

        $client = $this->auth->loggedUser;

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->isApplicable($promocode, $option, $client );

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
