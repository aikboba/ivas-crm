<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Subs
 * @property Subs_model $subs_model
 * @property Orders_model $orders_model
 * @property Clients_model $clients_model
 * @property Clients_presets_model $clients_presets_model
 * @property Clients_products_model $clients_products_model
 * @property Clients_groups_model $clients_groups_model
 * @property Products_model $products_model
 * @property Products_gifts_model $products_gifts_model
 * @property Options_model $options_model
 * @property Gifts_presets_model $gifts_presets_model
 */
class Subs extends Base_Front_Controller
{

    private $api_secret_key = "";
    private $public_key = "";
    private $hmac_header = "";
    private $rawSubsData = "";
    private $subsData = [];

    /** @var object order */
    private $order = null;
    /** @var object client */
    private $client = null;
    /** @var object option */
    private $option = null;
    /** @var object product */
    private $product = null;
    /** @var object subs */
    private $subs = null;
    private $amount = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('orders_model');
        $this->load->model('products_model');
        $this->load->model('options_model');
        $this->load->model('clients_model');
        /*
                $this->load->model('clients_products_model');
                $this->load->model('clients_presets_model');
                $this->load->model('clients_groups_model');
                $this->load->model('products_gifts_model');
                $this->load->model('gifts_presets_model');
        */
        $this->load->library('cloudpayments');
        $this->load->model('subs_model');
        $this->model = $this->subs_model;

        $this->load->config('cloudpayments', true);

        $this->hmac_header = $this->config->item('cp_hmac_header', 'cloudpayments');
        $this->rawSubsData = $this->input->raw_input_stream;
        $this->subsData = $_POST;

        if (!empty($this->access_params->hmac_required))
        {
            if (!empty($this->subsData))
            {
                //данные подписки есть и верно сформированы
                $data = $this->model->transform($this->subsData);

                // извлекаем проверяемые данные ----------
                $subs_id = $data['subs_id'];
                $client_id = $data['client_id'];
                $this->amount = $data['amount'];

                $this->subs = $this->model->getBySubsId($subs_id);
                // нет дписки - выходим с отказом(подписка всегда создается при оплате, т.е. она всегда должна быть)
                if(empty($this->subs))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }
                /** @var StdClass order */
                $this->order = $this->orders_model->getByOrdersSubsId($this->subs->id);
                // нет заказа - выходим с отказом
                if (empty($this->order))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->client = $this->clients_model->getById($client_id);
                // нет клиента - выходим с отказом
                if (empty($this->client))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->option = $this->options_model->getById($this->order->option_id);
                // не нашли опцию - выходим с отказом
                if (empty($this->option))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->product = $this->products_model->getById($this->option->product_id);
                // не нашли продукт - выходим с отказом
                if (empty($this->product))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->public_key = $this->cloudpayments->publicKey = $this->product->paysystem_public_key;
                $this->api_secret_key = $this->cloudpayments->privateKey = $this->product->paysystem_api_secret_key;

                // проверка валидности запроса переехала сюда, т.к. параметры public_key+api_secret_key
                // являются частью paysystems, paysystem_id хранится в product
                if (!$this->isRequestValid($this->rawSubsData))
                {
                    // проверка Content-HMAC не пройдена
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $message = STATUS_TRANS_CANT_ACCEPT_MSG;
                    $data = [
                        'code' => $status,
                    ];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }
            }
        }
    }

    /** Проверяет целостность и валидность пришедших от cloudpayments данных
     *
     * @param string $rawSubsData - строка-тело POST-запроса
     *
     * @return boolean
     */
    private function isRequestValid($rawSubsData)
    {

        $headers = $this->input->request_headers();

        if ( (!array_key_exists($this->hmac_header, $headers)) || (empty($headers[ $this->hmac_header ])) ) return false;
        $hmac = $headers[ $this->hmac_header ];

        if (empty($rawSubsData)) return false;

        $sign = base64_encode( hash_hmac('sha256', $rawSubsData, $this->api_secret_key, true) );

        if ($hmac != $sign) return false;

        return true;
    }

    public function index()
    {

    }

    /** получает данные о транзакции по внутреннему ID
     *
     * @param int id  - ID транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $trans_id = $this->input->post('id');
        if (empty($trans_id)) {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // берем из БД
        $trans = $this->model->getById($trans_id);
        if (empty($trans))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $trans;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }

    /** Выполняется в случае, если статус подписки на рекуррентный платеж был изменен.
     *
     * @return string
     */
    public function recurrent()
    {
        if (isset($this->subsData['Id'])) {
            $subsId = $this->subsData['Id'];
            $subsCloudpayments = $this->cloudpayments->getSubscription($subsId);
            if ($subsCloudpayments)
            {
                $subsCloudpayments = $this->subs_model->transform($subsCloudpayments);
                $clientSubs = $this->subs_model->getBySubsId($subsId);
                if($clientSubs->client_id == $subsCloudpayments['client_id'])
                {
                    $this->subs_model->save($clientSubs->id, $subsCloudpayments);
                }
                else {
                    // TO DO: добавить лог об ошибке
                }
            }
            else {
                // TO DO: добавить лог об ошибке
            }
        }

        $status = STATUS_TRANS_OK;
        $message = STATUS_TRANS_OK_MSG;
        $data = [
            'code' => $status,
        ];
        $data = json_encode($data, JSON_PRETTY_PRINT);

        return $this->utils->jsonOut($data);
    }

    /** получает список АКТИВНЫХ подписок по клиента
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $client_id = $this->auth->loggedUser->id;

        // принято решение отдавать все транзакции, независимо от status
        //$caller_origin = $this->caller_origin;
        //$this->caller_origin = 'admin';
        $list = $this->model->getByClientId($client_id);
        //$this->caller_origin = $caller_origin;

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** отменяет подписку с указанным id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function cancel()
    {
        $subs_id  = $this->input->post('id');
        if (empty($subs_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = $this->auth->loggedUser->id;

        $subs = $this->model->getById($subs_id);
        if ((empty($subs)) || ($subs->client_id!=$client_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->load->model('products_model');

        $product = $this->products_model->getById($subs->product_id);
        if (!$product) {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $subs_string_id = $subs->subs_id;

        $this->load->library('cloudpayments');
        $this->cloudpayments->publicKey = $product->paysystem_public_key;
        $this->cloudpayments->privateKey = $product->paysystem_api_secret_key;
        $res = $this->cloudpayments->cancelSubscription($subs_string_id);

        if(!$res)
        {
            $status = STATUS_FAIL;
            $message = STATUS_FAIL_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->model->update($subs->id, ['status'=>'cancelled']);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        //$caller_origin = $this->caller_origin;
        //$this->caller_origin = 'admin';
        $data = $this->model->getById($subs->id);
        //$this->caller_origin = $caller_origin;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}