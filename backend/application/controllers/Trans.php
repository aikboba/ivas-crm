<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Trans
 * @property Trans_model $trans_model
 * @property Orders_model $orders_model
 * @property Clients_model $clients_model
 * @property Clients_presets_model $clients_presets_model
 * @property Clients_products_model $clients_products_model
 * @property Clients_groups_model $clients_groups_model
 * @property Products_model $products_model
 * @property Products_gifts_model $products_gifts_model
 * @property Options_model $options_model
 * @property Gifts_presets_model $gifts_presets_model
 * @property Subs_model $subs_model
 * @property Cloudpayments $cloudpayments
 * @property Mailer $mailer
 */
class Trans extends Base_Front_Controller
{

    private $api_secret_key = "";
    private $public_key = "";
    private $hmac_header = "";
    private $rawTransData = "";
    private $transData = [];

    /** @var object order */
    private $order = null;
    /** @var object client */
    private $client = null;
    /** @var object option */
    private $option = null;
    /** @var object product */
    private $product = null;
    /** @var object subs */
    private $subs = null;
    /** @var object trans */
    private $trans = null;
    private $amount = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('orders_model');
        $this->load->model('products_model');
        $this->load->model('options_model');
        $this->load->model('clients_model');
/*
        $this->load->model('clients_products_model');
        $this->load->model('clients_presets_model');
        $this->load->model('clients_groups_model');
        $this->load->model('products_gifts_model');
        $this->load->model('gifts_presets_model');
*/
        $this->load->model('trans_model');

        $this->load->model('subs_model');
        $this->load->library('cloudpayments');

        $this->load->library('mailer');
        $this->mailer->sendType = 'sync';

        $this->model = $this->trans_model;

        $this->load->config('cloudpayments', true);
        $this->load->config('cms_consts', true);

        $this->hmac_header = $this->config->item('cp_hmac_header', 'cloudpayments');
//$this->api_secret_key = $this->config->item('cp_api_secret_key', 'cloudpayments');
//$this->public_key = $this->config->item('cp_public_key', 'cloudpayments');

        //  $this->transData = (array)json_decode($this->input->raw_input_stream);
        $this->rawTransData = $this->input->raw_input_stream;
        $this->transData = $_POST;

        if (!empty($this->access_params->hmac_required))
        {
            if (!empty($this->transData))
            {
                //данные транзакции есть и верно сформированы
                $data = $this->model->transform($this->transData);

                // извлекаем проверяемые данные ----------
                $order_id = $data['order_id'];
                $client_id = $data['client_id'];
                $this->amount = $data['amount'];

                if (!empty($this->transData['SubscriptionId']))
                {
                    // если пришла подписка пытаемся найти в БД
                    // будет в дальнейшем использоваться для поиска order
                    // т.к. в рекуррентных платежах не приходит InvoiceId
                    $subsId = $this->transData['SubscriptionId'];
                    $this->subs = $this->subs_model->getBySubsId($subsId);
                }

                /** @var StdClass order */
                $this->order = $this->orders_model->getById($order_id);
                if (empty($this->order) && !empty($this->subs)) {
                    // рекуррентный платеж
                    // поиск order по подписке
                    $this->order = $this->orders_model->getLastByOrdersSubsId($this->subs->id);
                    if ($this->order) {
                        $data['order_id'] = $this->order->id;
                    }
                }

                // нет заказа - выходим с отказом
                if (empty($this->order))
                {

                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->client = $this->clients_model->getById($client_id);
                // нет клиента - выходим с отказом
                if (empty($this->client))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $callerOriginBackup = $this->caller_origin;
                $this->caller_origin = 'admin';
                $this->option = $this->options_model->getById($this->order->option_id);
                $this->caller_origin = $callerOriginBackup;
                // не нашли опцию - выходим с отказом
                if (empty($this->option))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->product = $this->products_model->getById($this->option->product_id);
                // не нашли продукт - выходим с отказом
                if (empty($this->product))
                {
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $data = ['code' => $status];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                $this->public_key = $this->cloudpayments->publicKey = $this->product->paysystem_public_key;
                $this->api_secret_key = $this->cloudpayments->privateKey = $this->product->paysystem_api_secret_key;

                // проверка валидности запроса переехала сюда, т.к. параметры public_key+api_secret_key
                // являются частью paysystems, paysystem_id хранится в product
                if (!$this->isRequestValid($this->rawTransData))
                {
                    // проверка Content-HMAC не пройдена
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $message = STATUS_TRANS_CANT_ACCEPT_MSG;
                    $data = [
                        'code' => $status,
                    ];
                    $data = json_encode($data, JSON_PRETTY_PRINT);
                    $this->utils->jsonOut($data);
                    exit();
                }

                // сохраняем транзакцию
                $data['oper'] = $this->methodName;
                $transID = $this->model->createFromData($data);
                $this->trans = $this->model->getById($transID);
            } else {
                $status = STATUS_TRANS_CANT_ACCEPT;
                $data = [
                    'code' => $status,
                ];
                $data = json_encode($data, JSON_PRETTY_PRINT);
                $this->utils->jsonOut($data);
            }
        }
    }

    /** Проверяет целостность и валидность пришедших от cloudpayments данных
     *
     * @param string $rawTransData - строка-тело POST-запроса
     *
     * @return boolean
     */
    private function isRequestValid($rawTransData)
    {
/*
ob_start();
echo date("Y-m-d H:i:s");
echo "\n";
*/
        $headers = $this->input->request_headers();
/*
print_r($headers);
echo "\n";
print_r($rawTransData);
echo "\n";
*/
        if ( (!array_key_exists($this->hmac_header, $headers)) || (empty($headers[ $this->hmac_header ])) ) return false;
        $hmac = $headers[ $this->hmac_header ];
/*
print_r($hmac);
echo "\n";
*/
        if (empty($rawTransData)) return false;

        $sign = base64_encode( hash_hmac('sha256', $rawTransData, $this->api_secret_key, true) );
/*
print_r($sign);
echo "\n";
echo "\n";
$st = ob_get_contents();
file_put_contents('testfile.log',$st);
ob_end_clean();
*/
        if ($hmac != $sign) return false;

        return true;
    }

    public function index()
    {

    }

    /**
     * ИЗМЕНЕНО И ПЕРЕНЕСЕНО В ORDERS_MODEL
     * Функция для проверки возможности проведения текущего платежа
     *
     * @return int
     */
/*
    private function isPaymentPossible()
    {
        // попытка оплатить заказ с неподходящим статусом
        if (($this->order->status != 'new') && ($this->order->status != 'partially_paid')) return STATUS_TRANS_INVALID_ORDER;

        // client_id в заказе и транзакции не совпадают
        if ($this->order->client_id != $this->client->id) return STATUS_TRANS_INVALID_CLIENT;

        // попытка частично оплатить опцию, без возможности частичной оплаты
        if ((!$this->option->is_partial_available) && ($this->amount < $this->order->price)) return STATUS_TRANS_INVALID_AMOUNT;

        // попытка оплатить больше cтоимости или остатка стоимости
        if ($this->order->amount_paid + $this->amount > $this->order->price) return STATUS_TRANS_INVALID_AMOUNT;

        return STATUS_TRANS_OK;
    }
*/
    /** Выполняется после того, как держатель заполнил платежную форму и нажал кнопку «Оплатить».
     *
     * @return string
     */
    public function check()
    {
        $status = false;
        if ($this->option->status != 'active')
        {
            // если опция не активна то подписка этой опции будет отменена чтоб не продолжать рекуррентные платежи
            if (!empty($this->transData['SubscriptionId']))
            {
                $subscription = $this->cloudpayments->getSubscription($this->transData['SubscriptionId']);
                if ($subscription)
                {
                    // отменяем подписку
                    $this->cloudpayments->cancelSubscription($this->transData['SubscriptionId']);
                    // ищем подписку у нас в БД
                    $subs = $this->subs_model->getBySubsId($this->transData['SubscriptionId']);
                    if ($subs)
                    {
                        // если подписка есть то удаляем её
                        $this->subs_model->deleteCompletely($subs->id);
                    }
                }
            }
            //выдаем статус о "не возможности принятия платежа"
            $status = STATUS_TRANS_CANT_ACCEPT;
        }
        // если статус пуст то проверям есть ли возможность принять заказ
        if (!$status) {
            $status = $this->orders_model->isPaymentPossible($this->order, $this->option, $this->client, $this->amount, $this->subs);
        }

        $data = ['code' => $status];
        $data = json_encode($data, JSON_PRETTY_PRINT);
        $this->utils->jsonOut($data);
        exit();
    }

    /** Выполняется в случае, если оплата была отклонена и используется для анализа количества и причин отказов.
     *  Стоит учитывать, что факт отказа в оплате не является конечным — пользователь может оплатить со второго раза.
     *
     * @return string
     */
    public function fail()
    {
        $status = STATUS_TRANS_OK;
        $message = STATUS_TRANS_OK_MSG;
        $data = [
            'code' => $status,
        ];
        $data = json_encode($data, JSON_PRETTY_PRINT);

        return $this->utils->jsonOut($data);
    }

    /** Выполняется в случае, если платеж был возвращен (полностью или частично) по вашей инициативе через API или личный кабинет.
     *
     * @return string
     */
    public function refund()
    {
        // TO DO: непонятно, что здесь делать
        $status = STATUS_TRANS_OK;
        $message = STATUS_TRANS_OK_MSG;
        $data = [
            'code' => $status,
        ];
        $data = json_encode($data, JSON_PRETTY_PRINT);

        return $this->utils->jsonOut($data);
    }


    /** получает данные о транзакции по внутреннему ID
     *
     * @param int id  - ID транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $trans_id = $this->input->post('id');
        if (empty($trans_id)) {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // берем из БД
        $trans = $this->model->getById($trans_id);
        if (empty($trans)) {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $trans;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }

    /** получает данные о транзакции по trans_id
     *
     * @param int trans_id  - trans_ID транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByTransId()
    {
        $trans_id = $this->input->post('trans_id');
        if (empty($trans_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // берем из БД
        $trans = $this->model->getByTransId($trans_id);
        if (empty($trans))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $trans;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }


    /** Выполняется после того, как оплата была успешно проведена — получена авторизация эмитента.
     *
     * @return string
    */
    public function pay()
    {
        // ---- Email при одиночном заказе ----------------
        $html =
            '<p>Спасибо за заказ!</p>'.
            '<p>Вы получили доступ к продукту "{product_name}" на {product_day} дней.</p>'
        ;
        // ---- END Email при одиночном заказе ----------------
        if (!$this->subs && !empty($this->transData['SubscriptionId']))
        {

            // если пришла оплата с подпиской, то сохраняем её в БД для дальнейших манипуляций
            $subscriptionId = $this->transData['SubscriptionId'];

            $subscription = $this->cloudpayments->getSubscription($subscriptionId);
            if ($subscription)
            {
                // ищем другие(старые) подписки по клиенту и продукту, для отмены
                $oldSubs = $this->subs_model->getByClientIdProductId($this->client->id, $this->product->id);
                if (!empty($oldSubs)) {
                    foreach ($oldSubs as $sub) {
                        $this->cloudpayments->cancelSubscription($sub->subs_id);
                        $this->subs_model->deleteCompletely($sub->id);
                    }
                }

                $subs_id = $this->subs_model->createFromSubsData($subscription);
                $this->order->subs_id = $subs_id;
                //платеж с подпиской
                // ---- Email при заказе с подпиской ----------------
                $html =
                    '<p>Спасибо за заказ!</p>'.
                    '<p>Вы получили доступ к продукту "{product_name}" на {product_day} дней.</p>'.
                    '<p>По истечению оплаченного периода, с указанного Вами счета автоматически будут списываться средства для продления услуги - {amount} рублей.</p>'.
                    '<p>Для отключения автоплатежа перейдите сюда: <a href="{profile_link}">{profile_link}</a></p>'
                ;
                // ---- END Email при заказе с подпиской ----------------
            }
            else
            {
                // TO DO: добавить лог об ошибке
            }
        }
        // проверяем возможна ли оплата продукта
        $status = $this->orders_model->isPaymentPossible($this->order, $this->option, $this->client, $this->amount, $this->subs);

        if ($status != STATUS_TRANS_OK)
        {
            $data = ['code' => $status,];
            $data = json_encode($data, JSON_PRETTY_PRINT);

            return $this->utils->jsonOut($data);
        }

        // если есть подписка проверяем какой платеж пришел, рекуррентный или нет
        if ($this->subs)
        {
            //проверка рекурентного платежа
            $statuses = in_array($this->subs->status, ['active', 'pastdue']) && in_array($this->order->status, ['partially_paid', 'paid']);
            if ($statuses && $this->order->price == $this->amount)
            {
                // создать копию order и оплатить её
                $data = [
                    'option_id' => $this->order->option_id,
                    'client_id' => $this->order->client_id,
                    'price' => floatval($this->order->price),
                    'client_message' => $this->order->client_message,
                    'additional_data' => $this->order->additional_data,
                    'promocode_id' => ($this->order->promocode_id ?: 'NULL') ,
                    'subs_id' => $this->subs->id
                ];

                //$this->db->trans_begin();
                // сохраняем копию order
                $newOrderId = $this->orders_model->insertOrUpdate($data);

                if ($newOrderId) {
                    //$this->db->trans_commit();
                    $this->order = $this->orders_model->getById($newOrderId);

                    // обновляем подписку
                    $subscriptionId = $this->transData['SubscriptionId'];
                    $updateSubsData = $this->subs_model->transform($this->cloudpayments->getSubscription($subscriptionId));
                    $this->subs_model->save($this->subs->id, $updateSubsData);
                    $this->subs = $this->subs_model->getById($this->subs->id);
                    $params = [
                        ['trans_id', '=', $this->trans->trans_id]
                    ];
                    $uData = ['order_id' => $newOrderId];
                    // обновляем order у транзакции на копию нового
                    $this->trans_model->updateByParams($params, $uData);
                    //$this->db->trans_commit();
                    // рекурентынй платеж
                    // ---- Email при Рекурентном платеже ----------------
                    $html =
                        '<p>Уважаемый клиент!</p>'.
                        '<p>С Вашего счета была {pay_status} списана сумма {amount} рублей для продления доступа к продукту "{product_name}".</p>'.
                        '<p>Для отключения автоплатежа перейдите сюда: <a href="{profile_link}">{profile_link}</a></p>'
                    ;
                    // ---- END Email при Рекурентном платеже ----------------
                } else {
                    //$this->db->trans_rollback();
                    $status = STATUS_TRANS_CANT_ACCEPT;
                    $message = STATUS_TRANS_CANT_ACCEPT_MSG;

                    $data = ['code' => $status,];
                    $data = json_encode($data, JSON_PRETTY_PRINT);

                    return $this->utils->jsonOut($data);
                }
            }
        }

        $status = $this->orders_model->pay($this->order, $this->option, $this->product, $this->client, $this->amount, $this->subs);
        
        $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
        $project_domain = $this->config->item('BASE_URL', 'cms_consts');
        $html = $mail_header_logo . $html;
        $profileLink = $project_domain.'/my-profile';

        // ---- Email отправка ----------------
        $eData = [
            'toEmail' => $this->client->email,
            'subject' => "Оплата продукта" ,
            'body'    => str_replace(
                ['{pay_status}', '{amount}', '{product_name}', '{product_day}', '{profile_link}'],
                ['успешно', $this->amount, $this->product->name, $this->option->duration, $profileLink],
                $html
            ),
        ];
        $this->mailer->send($eData);
        // ---- END Email отправка ----------------
        $data = ['code' => $status,];
        $data = json_encode($data, JSON_PRETTY_PRINT);

        return $this->utils->jsonOut($data);
    }

}