<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Admins
 * @property Admins_model $admins_model
 * @property Permissions_model $permissions_model
 * @property Admins_admingroups_model $admins_admingroups_model
 * @property Taskmanager $taskmanager
 * @property Tokens_model $tokens_model
 *
 */
class Admins extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->data['admin_url'] = $this->config->item('admin_url');
        $this->data['site_title'] = $this->config->item('site_title');

        $this->load->model('admins_model');
        $this->load->model('permissions_model');

        $this->model = $this->admins_model;

        //$this->load->library('firebase_cloud_message');

        //$deviceToken = 'fhlIw_sbEUs:APA91bH-YB0axTIukQsYA8fmQGxoJRH7a9SrChqT_EvT-NcKKL6IjamAyl_ZOS3IBiozU0EuD3zJ80zWE4yZnSmaQRys41RrOaGAWgH6qQy5XJs9V5de5CommcGs3zhXx8nM_h3tgf_V';
        //$this->firebase_cloud_message->sendNotificationData($deviceToken, ['title' => 'test', 'body' => 'test description']);
        //$this->firebase_cloud_message->subscribeToTopic([$deviceToken], 'test-topic1');
        //$this->firebase_cloud_message->unsubscribeFromTopic([$deviceToken], 'test-topic');
        //$this->firebase_cloud_message->getAppInstance($deviceToken);
        //$this->firebase_cloud_message->getSubscribeTopics($deviceToken);
        //$this->firebase_cloud_message->sendMessageToMultipleDevices([$deviceToken], ['title' => 'test', 'body' => 'test description']);
}

    public function index()
    {
        $this->load->model('cities_model');
        $res = $this->cities_model->getByCountryId(1, 10);
echo "<pre>";
print_r($res);
echo "</pre>";
die();

        /* // для начала стрима из инсты
        $task_data = [
            'system'    => "backend",
            'publisher' => "instaBot/startStream",
            'consumer'  => "startStreamInstagram",
            'priority'  => 100,
            'start_dt'  => "NULL",
            'data_in'   => json_encode(
                [
                    'instagram_account' => [
                                            'id' => '23219474204',
                                            'username' => 'malyshkimoi',
                    ],
                    'stream_url_data' => [
                        'hls' => [
                            'HQ' => "https://live-dev2.rimmakaramova.com:59881/a97affab-a35a-41d5-afa8-3b9f4248a726/hls/index.m3u8",
                            'LQ' => "https://live-dev2.rimmakaramova.com:59881/a97affab-a35a-41d5-afa8-3b9f4248a726/hls/index.m3u8"
                        ],
                    ],
                    'stream_origin' => "vimeo",
                    'stream_item_guid' => "DfgdDFGfggUdd",
                ]
             ),

        ];
        */
/*
         // для окончания стрима из инсты
        $task_data = [
            'system'    => "backend",
            'publisher' => "instaBot/stopStream",
            'consumer'  => "stopStreamInstagram",
            'priority'  => 100,
            'start_dt'  => "NULL",
            'data_in'   => json_encode(
                [
                    'instagram_account' => [
                                            'id' => '23219474204',
                                            'username' => 'malyshkimoi',
                    ],
                    'stream_item_guid' => "DfgdDFGfggUdd",
                ]
             ),

        ];

        $this->load->library('taskmanager');
        $new_task_id = $this->taskmanager->createTask($task_data);
        echo "<pre>";
        print_r($new_task_id);
        echo "</pre>";
        die();
*/
        /*$product_id = trim($this->input->post_get('product_id'));
        if (empty($product_id)) $product_id = 13;
        $this->load->library('taskmanager');

        $task_data_in = [
            'system'    => "backend",
            'publisher' => "transcoder/stopStream",
            'consumer'  => "stopStream",
            'data_in'      => ["product_id" => $product_id],
            'priority'  => 10,
            'start_dt'  => "NULL",
        ];
        $new_task_id = $this->taskmanager->createTask($task_data_in);
echo "<pre>";
print_r($new_task_id);
echo "</pre>";
die();*/


        //$this->taskmanager->waitForTask(1, $timeout = 10);

/*
        $product_id = trim($this->input->post_get('product_id'));
        if (empty($product_id)) $product_id = 16;
        $this->load->library('taskmanager');

        $task_data = [
        'system'    => "transcoder",
        'publisher' => "products/stopStream",
        'consumer'  => "transcoderStop",
        'data'      => ["product_id" => $product_id],
        'priority'  => 10,
        'start_dt'  => "NULL",
        ];
        $this->taskmanager->createTask($task_data);
        //$this->taskmanager->waitForTask(1, $timeout = 10);
*/
/*
$stream_url_data = [
    'hls' => [
        'HQ' => 'hls://high_quality.url',
        'LQ' => 'hls://low_quality.url',
    ],
    'dash' => [
        'HQ' => 'dash://high_quality.url',
        'LQ' => 'dash://low_quality.url',
    ],
];
$st = json_encode((object)$stream_url_data, JSON_PRETTY_PRINT);
$json = json_decode($st);
echo "<pre>";
print_r($st);
echo "</pre>";
die();
*/
/*
        $this->load->model('orders_model');
        $res = $this->orders_model->getByClientId(3);
echo "<pre>";
print_r($res);
echo "</pre>";
die();
*/
        /*$this->load->model('orders_model');
        $res = $this->orders_model->mergeClientsData(3, 2);

echo "<pre>";
        var_dump($res);
echo "</pre>";
die();*/
        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
        /*$this->load->library('messinger');
        $this->messinger->action = 'system_refresh_product';
        $this->messinger->target = []; //all
        $this->messinger->data = ['product_id'=>16];
        $this->messinger->send();*/
        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
/*
        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
        $this->load->library('messinger');
        $this->messinger->action = 'system_refresh_products_list';
        $this->messinger->target = []; //all
        $this->messinger->data = [];
        $this->messinger->send();
        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
*/

        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
        /*$this->load->library('messinger');
        $this->messinger->action = 'system_refresh_products_list';
        $this->messinger->target = []; //all
        $this->messinger->data = [];
        $this->messinger->send();*/
        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------

        /*$this->load->library('cloudpayments');
        $this->cloudpayments->publicKey = 'pk_2e80d52e5c1c65d9b95a6bfeb02de';
        $this->cloudpayments->privateKey = '18a55749ab3a04859176b4a45ddfdced';
        //var_dump($this->cloudpayments->getTokensList());
        //var_dump($this->cloudpayments->test());
        /*var_dump(
                $this->cloudpayments->createSubscription(
                    '477BBA133C182267FE5F086924ABDC5DB71F77BFC27F01F2843F2CDC69D89F05',
                    '10337',
                    'Тест подписки 1000 руб.',
                    'qpdakpvrhumr@mail.ru',
                    1000.00,
                    'RUB',
                    false,
                    null,
                    'Day'
                )
            );*/
        //var_dump($this->cloudpayments->getSubscription('sc_eab9f2553742ae5fd7c35c8fcbd4c'));
        //$updated = ['StartDate' => (new \DateTime('-1 day'))->setTimezone(new DateTimeZone("UTC"))->format('Y-m-d\TH:i:s')];
        //var_dump($this->cloudpayments->updateSubscription('sc_eab9f2553742ae5fd7c35c8fcbd4c', $updated));
        //$subssriptions = $this->cloudpayments->findSubscriptions(10337);

        //var_dump($this->cloudpayments->cancelSubscription('sc_88276b8c9958012bc24c7c634bbe3'));
        /*var_dump(
            $this->cloudpayments->updateSubscription(
                'sc_88276b8c9958012bc24c7c634bbe3',
                [
                    'Description' => 'Тест подписки 100 руб. бла-бла',
                    'Amount' => 100.00
                ]
            )
        );*/
        /*$paramsUpdate = [
            'Address' => 'http://5.187.5.59:8888/api/subs/recurrent',
            'HttpMethod' => 'POST',
            'IsEnabled' => true
        ];
        var_dump($this->cloudpayments->updateCallbackSettings('recurrent', $paramsUpdate));
        var_dump($this->cloudpayments->getCallbackSettings('recurrent'));*/
        //var_dump($this->cloudpayments->getCallbackSettings('refund'));
        //var_dump($this->cloudpayments->findSubscriptions('311'));
        /*print_r($this->cloudpayments->getCallbackSettings('pay'));
        print_r($this->cloudpayments->getCallbackSettings('fail'));
        print_r($this->cloudpayments->getCallbackSettings('confirm'));
        print_r($this->cloudpayments->getCallbackSettings('confirm'));
        print_r($this->cloudpayments->getCallbackSettings('refund'));
        print_r($this->cloudpayments->getCallbackSettings('recurrent'));
        print_r($this->cloudpayments->getCallbackSettings('receipt'));
        print_r($this->cloudpayments->getCallbackSettings('cancel'));
        print_r($this->cloudpayments->getCallbackSettings('kkt'));
        */


/*
        $text = "Привет, {{{USER}}}! Да и ты, {{{user}}}, не болей.
                Очень рады видеть тебя, {{{USER}}}. Сегодня {{{DAY}}}";
        $data = [
            'USER'=>"Alex",
            'DAY'=>"Sunday",
            'xxx'=>"123",
        ];
        $res = $this->utils->substitute($text,$data);
*/
    }

    /**
     * Логинит админа, используя email в качестве логина
     * сохраняет данные в auth
     *
     * @params string $identity - email. Берет из POST
     * @params string $password - пароль. Берет из POST
     *
     * @return object - JSON-envelope
     */
    public function login()
    {
        $identity = trim($this->input->post('identity'));
        $password = trim($this->input->post('password'));

        $admin = $this->model->getByLoginData($identity, $password);

        if ( !empty($admin) )
        {
            $this->auth->role = 'admin';
            $this->auth->loggedUser = $admin;

            $logData = [
                'entity' => 'admin',
                'entity_id' => $admin->id,
                'event' => 'login',
            ];
            $this->model->toLog($logData);

            $d = new Datetimeex('now');
            $uData['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $uData['last_login_datetime'] = $d->format("Y-m-d H:i:s");//$d->getTimestamp();
            $this->model->update($admin->id, $uData);
            $admin = $this->model->getById($admin->id);

            // создаем и сохраняем токен -------------------------------------------
            $token = $this->utils->uuid();
            $tData = [
                        'entity'=>'admin',
                        'entity_id'=>$admin->id,
                        'token' => $token
            ];
            $this->load->model('tokens_model');
            $this->tokens_model->create($tData);
            // устанавливаем токен в output headers для refresh-токена
            $this->auth->tokenSet($token);
            // сразу его "поднимаем" для заполнения auth->token_data
            $this->auth->tokenGet($token);
            // END создаем токен -------------------------------------------

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $admin;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        } //if ( !empty($admin) )
        else
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /**
     * Производит логаут админа
     *
     * @return object - JSON-envelope
     */
    public function logout()
    {
        $logData = [
            'entity' => 'admin',
            'entity_id' => $this->auth->loggedUser->id,
            'event' => 'logout',
        ];
        $this->model->toLog($logData);

        $this->auth->logout();

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** Регистрирует админа или возвращает envelope с ошибкой
     *
     * @param string first_name
     * @param string last_name
     * @param string password
     * @param string email
     * @param string phone
     *
     * @return object - JSON-envelope
     */
    public function register($data = [])
    {
        if (empty($data))
        {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $timezone = $this->input->post('timezone');
            $password = $this->input->post('password');
            $status = $this->input->post('status');
        }
        else
        {
            $first_name = $data['first_name'];
            $last_name = $data['last_name'];
            $phone = $data['phone'];
            $email = $data['email'];
            $timezone = $data['timezone'];
            $password = $data['password'];
            $status = $data['status'];
        }

        if (empty($status)) $status = 'inactive';

        if ( !$this->model->isUnique('email', $email) )
        {
            $status = STATUS_NOT_UNIQUE;
            $message = "Ошибка: администратор с таким Email уже зарегистрирован";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // параметры уникальны - создаем
        $salt = $this->utils->salt();
        $pass_enc = $this->utils->hash_password($password, $salt);

        $uData = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone,
            'password' => $pass_enc,
            'salt' => $salt,
            'status' => $status,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
        ];
        if (!empty($timezone)) $uData['timezone'] = $timezone;

        $new_id = $this->model->insert($uData);
        if ($new_id)
        {
            //$html = "<p>Регистрация администратора завершена.</p><br/><p>Ваш пароль: </p>" . $pass_enc;
            //$this->utils->send_mail_gmail($html, 'Регистрация администратора в RK-CRM', $email);
            $admin = $this->model->getById($new_id); // этого можно и не делать

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $admin;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        // не удалось сохранить
        $status = STATUS_CREATE_FAIL;
        $message = STATUS_CREATE_FAIL_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Отправляет ссылку для восстановления пароля на email, указанный при регистрации
     *
     * @param string $identity - email
     *
     * @return object - JSON-envelope
     */
    public function remindPassLink()
    {
        $identity = $this->input->post('email');

        if (empty($email))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $admin */
        $admin = $this->model->getByEmail($identity, 'active');

        if (empty($admin))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $secret_code = $this->utils->generateRandomString(40);
        $res = $this->model->update($admin->id, ['secret_code'=>$secret_code]);

        if (!$res)
        {
            // не удалось
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $link = $this->config->item('admins_url_reset_pass', 'cms_consts')."/?code=".$secret_code;
        $html = "<p>Кто-то (возможно Вы сами) попросил напомнить пароль. 
                Если это были Вы, проследуйте по ссылке: <a href='".$link."'>".$link."'</a><br/><br/>
                Спасибо, что Вы с нами!</p>";
        $this->utils->send_mail_gmail($html, 'Восстановление пароля администратора в RK-CRM', $admin->email);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /**
     * Меняет пароль в случае совпадения secret_code и высылает его админу на email, указанный при регистрации
     *
     * @param string $code - секретный код. Берется из GET
     *
     * @return object - JSON-envelope
     */
    public function resetPass()
    {
        $code = $this->input->get_post('code');

        if (empty($code))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $admin */
        $admin = $this->model->getBySecretCode($code);

        if (empty($admin))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $pass = $this->utils->generateRandomString(8);
        $pass_enc = $this->utils->hash_password($pass, $admin->salt);

        $res = $this->model->update($admin->id, ['pass'=>$pass_enc, 'secret_code'=>"NULL"]);

        if (!$res)
        {
            // не удалось
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $html = '<p>Ваш новый пароль: '.$pass.'</p>';
        $this->utils->send_mail_gmail($html, 'Новый пароль администратора в RK-CRM', $admin->email);

        $logData = [
            'entity' => 'admin',
            'entity_id' => $admin->id,
            'event' => 'password_changed',
            'additional_data' => $pass
        ];
        $this->model->toLog($logData);


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    // получает данные об админе
    public function get()
    {
        // берем admin_id из POST или сессии
        $admin_id = $this->input->post('id');
        if (empty($admin_id))
        {
            $admin_id = $this->auth->loggedUser->id;
        }

        // это не "залогиненный" - и нет, это нельзя решить через ACL
        // получить админа может или сам админ или другой админ с правами "admins"
        if ( $admin_id != $this->auth->loggedUser->id )
        {
            // admin_id есть, проверяем права из сессии
            if (!$this->auth->isAllowed('admins'))
            {
                $status = STATUS_DENIED;
                $message = STATUS_DENIED_MSG;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        // берем из БД
        $admin = $this->model->getById($admin_id);
        $admin = (object)$admin;

        //только суперадмин может править суперадмина
        if ( ($admin->is_super) && (!$this->auth->loggedUser->is_super) )
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($admin))
        {
            // админ найден
            $permissions = $this->permissions_model->getByAdminId($admin_id);
            $admin->permissions = $permissions;

            //$admingroups = $this->admins_admingroups_model->getAdmingroupsByAdminId($admin_id);
            //$admin->admingroups = $admingroups;

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = (object)$admin;
        }
        else
        {
            //админа нигде нет
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** получает список админов
     *
     *  @param  datetime unpated_since  - выбрать тех, у кого admins.updated>=$updated_since. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** Создает админа или возвращает envelope с ошибкой
     *
     * @param string first_name
     * @param string last_name
     * @param string password
     * @param string email
     * @param string phone
     *
     * @return object - JSON-envelope
     */
    public function create()
    {
        $data = [];
        $data['first_name'] = $this->input->post('first_name');
        $data['last_name'] = $this->input->post('last_name');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');
        $data['timezone'] = $this->input->post('timezone');
        $data['password'] = $this->input->post('password');
        $data['status'] = $this->input->post('status');

        return $this->register($data);
/*
      $uData = [
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'status' => 'inactive',
        ];
        $timezone = $this->input->post('timezone');
        if (!empty($timezone)) $uData['timezone'] = $timezone;
        $password = $this->input->post('password');

        $permissions = $this->input->post('permissions');

        if ( !$this->model->isUnique('email', $uData['email'] ) )
        {
            $status = STATUS_NOT_UNIQUE;
            $message = "Ошибка: администратор с таким Email уже зарегистрирован";
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        // параметры уникальны - создаем
        $salt = $this->utils->salt();
        $pass_enc = $this->utils->hash_password($password, $salt);
        $uData['password'] = $pass_enc;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($uData);

        if ($new_id)
        {
            //adding permissions -------------------------------------------------
            $permissions['admin_id'] = $new_id;
            $this->permissions_model->save($permissions);
            //end adding permissions -------------------------------------------------

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = $new_id;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
*/
    }

    // изменение админа
    public function update()
    {
        // берем admin_id из POST или сессии
        $admin_id = $this->input->post('id');

        if (empty($admin_id))
        {
            $admin_id = $this->auth->loggedUser->id;
        }

        // это не "залогиненный" - и нет, это нельзя решить через ACL
        // изменить админа может или сам админ или другой админ с правами "admins"
        if ( $admin_id != $this->auth->loggedUser->id )
        {
            // admin_id есть, проверяем права из сессии
            if (!$this->auth->isAllowed('admins'))
            {
                $status = STATUS_DENIED;
                $message = STATUS_DENIED_MSG;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        // берем из БД
        $admin = $this->model->getById($admin_id);
        $admin = (object)$admin;

        //только суперадмин может править суперадмина
        if ( ($admin->is_super) && (!$this->auth->loggedUser->is_super) )
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($admin))
        {
            // найден - изменяем
            $uData = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
            ];
            $timezone = $this->input->post('timezone');
            if (!empty($timezone)) $uData['timezone'] = $timezone;

            $status = $this->input->post('status');
            if (!in_array($status, ['active','inactive','banned','deleted'])) $status = 'inactive';
            if (!empty($status)) $uData['status'] = $status;

            $password = $this->input->post('password');
            if (!empty($password))
            {
                $pass_enc = $this->utils->hash_password($password, $admin->salt);
                $uData['password'] = $pass_enc;
            }

            $this->db->trans_begin();

            $upd_res = $this->model->update($admin_id, $uData);

            // изменение permissions ----------------------
                $permissions = $this->input->post('permissions');
                if (isset($permissions))
                {
                    $permissions['admin_id'] = $admin->id;
                    $this->permissions_model->save($permissions, true); // сохраняем с полным обновлением прав
                }
            // изменение permissions ----------------------

            // если это текущий - сохраняем в сессию
            if (($upd_res!==false) && ($this->db->trans_status() === TRUE) )
            {
                if ($admin_id == $this->auth->loggedUser->id)
                {
                    $this->auth->userLogged = $admin;
                }

                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();

                $logData = [
                    'entity' => 'admin',
                    'entity_id' => $admin_id,
                    'event' => 'update',
                    'additional_data' => $this->model->getById($admin_id),
                ];
                $this->model->toLog($logData);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $this->admins_model->getById($admin->id);

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // admin для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    // удаление админа
    public function delete()
    {
        // берем admin_id из POST или сессии
        $admin_id = $this->input->post('id');
        if (empty($admin_id))
        {
            $admin_id = $this->auth->loggedUser->id;
        }

        // берем из БД
        $admin = $this->model->getById($admin_id);
        $admin = (object)$admin;

        //только суперадмин может править суперадмина
        if ( ($admin->is_super) && (!$this->auth->loggedUser->is_super) )
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($admin))
        {
            // найден - удаляем
            $this->db->trans_begin();

            $del_res = $this->model->delete($admin_id);

            // если это текущий - выполняем выход
            if ($del_res && $this->db->trans_status() === TRUE)
            {
                if ($admin_id == $this->auth->loggedUser->id)
                {
                    // log the user out
                    $this->auth->logout();
                }

                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();

                $logData = [
                    'entity' => 'admin',
                    'entity_id' => $admin_id,
                    'event' => 'delete',
                ];
                $this->model->toLog($logData);

            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $del_res) );
        }
        else
        {
            // admin для delete не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** активация/деактивация пользователя
     *
     *  @param   int id  - ID админа. Берется из post или session
     *  @param   int $_POST['status'] == 0|1;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        // берем admin_id из POST или сессии
        $admin_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($admin_id))
        {
            $admin_id = $this->auth->loggedUser->id;
        }

        // берем из БД
        $admin = $this->model->getById($admin_id);
        $admin = (object)$admin;

        //только суперадмин может править суерадмина
        if ( ($admin->is_super) && (!$this->auth->loggedUser->is_super) )
        {
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($admin))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($admin_id, $data);

            // если это текущий - сохраняем в сессию
            if ($upd_res && $this->db->trans_status() === TRUE)
            {
                if ($admin_id == $this->auth->loggedUser->id)
                {
                    if ($new_status != 'active')
                    {
                        $this->auth->logout(); // если статуи юзера в сессии меняется на что-то кроме active - разлогинить его
                    }
                    else
                    {
                        $this->auth->loggedUser = $admin;
                    }
                }

                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // admin не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** получает список полей разрешений
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getPermissionsList()
    {
        $list = $this->permissions_model->getPermissionsList();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


}

