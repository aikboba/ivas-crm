<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Admins_admingroups
 * @property Admins_model $Admins_model
 * @property Admingroups_model  $admingroups_model
 * @property Admins_admingroups_model $admins_admingroups_model
 *
 */
class Admins_admingroups extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('admins_model');
        $this->load->model('admingroups_model');
        $this->load->model('admins_admingroups_model');

        $this->model = $this->admins_admingroups_model;
    }

    /** получает данные - запись admins_admingroups
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $admins_admingroups_id = $this->input->post('id');
        if (empty($admins_admingroups_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $admins_admingroups = $this->model->getById($admins_admingroups_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $admins_admingroups;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список админгрупп по admin_id
     *
     * @param  int $admin_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAdmingroupsListByAdminId()
    {
        $admin_id = $this->input->post('admin_id');
        if (empty($admin_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAdmingroupsByAdminId($admin_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список админов указанной админгруппы (admingroup_id)
     *
     * @param  int $admingroup_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAdminsListByAdmingroupId()
    {
        $admingroup_id = $this->input->post('admingroup_id');

        if (empty($admingroup_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAdminsByAdmingroupId($admingroup_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** удаляет запись по ее ID
     *
     *  @param   int id  - ID admins_admingroups.id. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admingroups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->delete($id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** удаляет админа admin_id из группы admingroup_id
     *
     *  @param   int admin_id  - ID клиента. Берется из post
     *  @param   int admingroup_id   - ID группы клиентов. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admingroups_presets_id
     */
    public function removeAdminFromAdmingroup()
    {
        $admin_id = $this->input->post('admin_id');
        if (empty($admin_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $admingroup_id = $this->input->post('admingroup_id');
        if (empty($admingroup_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeAdminFromAdmingroup($admin_id, $admingroup_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** создает запись - добавляет админа admin_id в группу admingroup_id
     *
     *  @param   int admin_id  - ID клиента. Берется из post
     *  @param   int admingroup_id   - ID группы клиентов. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admins_admingroups_id
     */
    public function create()
    {
        $admin_id = $this->input->post('admin_id');
        if (empty($admin_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $admingroup_id = $this->input->post('admingroup_id');
        if (empty($admingroup_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $new_id = $this->model->addAdminToAdmingroup($admin_id, $admingroup_id);

        if ( ($new_id!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID admins_admingroups. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $admins_admingroups_id = $this->input->post('id');
        $admin_id = $this->input->post('admin_id');
        $admingroup_id = $this->input->post('admingroup_id');

        if (empty($admins_admingroups_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $admins_admingroups */
        $admins_admingroups = $this->model->getById($admins_admingroups_id);

        if (!empty($admins_admingroups))
        {
            $data = [
                'admin_id' => $admin_id,
                'admingroup_id' => $admingroup_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($admins_admingroups_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($admins_admingroups_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }


            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение админгрупп админа
     *
     *  @param   int id  - ID админа. Берется из post
     *  @param   string adminadmingroups  - json_encoded массив вида $admingroups = [<admingroup_id1>, ... ,<admingroup_idN>];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function saveAdmingroups()
    {
        $admin_id = $this->input->post('id');
        if (empty($admin_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->model->clearAdmingroups($admin_id);

        $adminadmingroups = $this->input->post('adminadmingroups');

        if (!empty($adminadmingroups))
        {
            $adminadmingroups = json_decode($adminadmingroups);

            foreach ($adminadmingroups as $key => $admingroup_id)
            {
                $this->model->addAdminToAdmingroup($admin_id, $admingroup_id);
            }

        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
