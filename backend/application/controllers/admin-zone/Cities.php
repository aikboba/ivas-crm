<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Cities
 * @property Countries_model $countries_model
 * @property Cities_model $cities_model
 */
class Cities extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cities_model');
        $this->load->model('countries_model');

        $this->model = $this->cities_model;
    }

    /** получает данные об городе
     *
     *  @param   int $id  - ID города. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $city_id = intval($this->input->post_get('id'));
        if (empty($city_id))
        {
            //city_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        /** @var StdClass $option */
        $city = $this->model->getById($city_id);
        if (empty($city))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $city;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список городов по стране
     *
     * @param  int $country_id - ID страны.  берется из $_POST
     * @param  int|null $amount - макс кол-во (limit) возвращаемых записей. Берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $country_id = $this->input->post('country_id');
        if (empty($country_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $country = $this->countries_model->getById($country_id);
        if (empty($country))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $amount = $this->input->post('amount');
/*
        $params = [];
        $params[] = ['country_id', "=", $country_id];

        $list = $this->model->getList($params);
*/
        $list = $this->model->getByCountryId($country_id, $amount);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список городов отфильтрованный по названию города name
     *
     * @param string $search - искомое название
     * @param int|null $country_id - ID страны
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getFilteredList()
    {
        $search = $this->input->post('search');
        if (empty($search))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $country_id = intval($this->input->post('country_id'));

        $params   = [];
        $params[] = ['cities.name', ' like ', '%'.$search.'%'];

        if (!empty($country_id)) $params[] = ['country_id', '=', $country_id];

        $fields = [
            'cities.id as id',
            'cities.name as name',
            'cities.created',
            'cities.updated',
            'UNIX_TIMESTAMP(cities.created) as created_unix',
            'UNIX_TIMESTAMP(cities.updated) as updated_unix'
        ];

        $list = $this->model->getList($params, $fields);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }


    /** создание города
     *
     *  @param   int country_id  - ID СТРАНЫ. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $country_id = $this->input->post('country_id');
        if (empty($country_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        $data = [
            'country_id' => $country_id,
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса города
     *
     *  @param   int id  - ID города. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $city_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($city_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $city = $this->model->getById($city_id);
        if (empty($city))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // найден - изменяем status
        $data = [
            'status' => $new_status
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($city_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = $upd_res;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение опции
     *
     *  @param   int id  - ID города. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $city_id = $this->input->post('id');
        if (empty($city_id))
        {
            //$option_id нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $city = $this->model->getById($city_id);
        if (empty($city))
        {
            //$option_id нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем - можно вообще сказать
        // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
        $data = [
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($city_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = $upd_res;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаление города (через выставление статуса deleted)
     *
     *  @param   int id  - ID города. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $city_id = $this->input->post('id');
        if (empty($city_id))
        {
            //option_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $city = $this->model->gitById($city_id);
        if (empty($city))
        {
            //option_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();

        $del_res = $this->model->delete($city_id);

        if ($del_res)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $del_res) );
    }



}
