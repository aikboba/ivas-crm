<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Cli_priv
 * @property Clients_model $clients_model
 * @property Privileges_model $privileges_model
 * @property Cli_priv_model $cli_priv_model
 */
class Cli_priv extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('privileges_model');
        $this->load->model('cli_priv_model');

        $this->model = $this->cli_priv_model;
    }

    /** получает данные запись cli_priv
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $cli_priv_id = $this->input->post('id');
        if (empty($cli_priv_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $cli_priv = $this->model->getById($cli_priv_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $cli_priv;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список привилегий по client_id
     *
     * @param  int $cli_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientId()
    {
        $client_id = $this->input->post('cli_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPrivilegesByCliId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи
     *
     *  @param   int cli_id  - ID клиента. Берется из post
     *  @param   int priv_id  - ID группы привилегий. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $cli_id = $this->input->post('cli_id');
        if (empty($cli_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $priv_id = $this->input->post('priv_id');
        if (empty($priv_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $data = [
            'cli_id' => $cli_id,
            'priv_id' => $priv_id
        ];

        $valid_until = $this->input->post('valid_until');
        if (empty($valid_until)) $valid_until = 'NULL';
        $data['valid_until'] = $valid_until;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет клиента cli_id из группы привилегий priv_id
     *  (в модели удаляет связку cli-priv)
     *
     *  @param   int cli_id  - ID клиента. Берется из post
     *  @param   int priv_id   - ID группы привилегий. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removeClientFromPrivilege()
    public function removeLink()
    {
        $cli_id = $this->input->post('cli_id');
        if (empty($cli_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $priv_id = $this->input->post('priv_id');
        if (empty($priv_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeClientFromPrivilege($cli_id, $priv_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет клиента cli_id из группы привилегий priv_id
     *
     *  @param   int id  - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeClientFromPrivilege($record->cli_id, $record->priv_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение
     *
     *  @param   int id  - ID cli_priv. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $cli_priv_id = $this->input->post('id');

        if (empty($cli_priv_id))
        {
            //нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $cli_priv */
        $cli_priv = $this->model->getById($cli_priv_id);
        if (empty($cli_priv))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем - можно вообще сказать
        // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
        $data = [
            'cli_id' => $cli_priv->cli_id,
            'priv_id' => $cli_priv->priv_id,
            'valid_until' => $this->input->post('valid_until'),
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($cli_priv_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($cli_priv_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** сохранение привилегий клиента
     *
     *  @param   int id  - ID клиента. Берется из post
     *  @param   array privileges  - массив вида $privileges = [
     *                                                              <privID_1> => <valid_until_1>,
     *                                                              .........
     *                                                              <privID_n> => <valid_until_n>,
     *                                                          ];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function savePrivileges()
    {
        $client_id = $this->input->post('id');
        if (empty($client_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $clientPrivileges = $this->input->post('privileges');
        $clientPrivilegesUntil = $this->input->post('privilege_end');

        $params = [
            ['cli_id', '=', $client_id]
        ];
        $this->model->deleteByParams($params); // удаляем старые членства в группах привилегий

        foreach ($clientPrivileges as $key => $priv_id)
        {
            // создаем новые
            $uData = [
                'cli_id' => $client_id,
                'priv_id' => $priv_id,
                'valid_until' => $clientPrivilegesUntil[$key] ? : 'NULL',
            ];
            $this->model->insert($uData);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
