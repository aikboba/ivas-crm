<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients
 * @property Mailer $mailer
 * @property Clients_model $clients_model
 * @property Cli_priv_model $cli_priv_model
 * @property Clients_products_model $clients_products_model
 * @property Products_model $products_model
 * @property Options_model $options_model
 * @property Instagram_accounts_model $instagram_accounts_model
 * @property Privileges_model $privileges_model
 * @property Clients_devices_model $clients_devices_model
 * @property Clients_groups_model $clients_groups_model
 * @property Countries_model $countries_model
 * @property Cities_model $cities_model
 * @property CI_FTP $ftp
 */
class Clients extends Base_Admin_Controller
{
    public $upload_path = "";
    public $www_path = "";
    public $project_domain = "";
    public $clients_path = "";
    public $project_name = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->config('ftp', true);
        $this->upload_path = $this->config->item('ftp_upload_avatars_path', 'ftp'); // это реальная папка на FTP, куда падают загруженные файлы
        $this->www_path = $this->config->item('ftp_www_avatars_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт

        // это позднее переделать!!!
        $this->load->library('ftp');
        $conf['hostname'] = $this->config->item('ftp_host', 'ftp');
        $conf['username'] = $this->config->item('ftp_user', 'ftp');
        $conf['password'] = $this->config->item('ftp_password', 'ftp');
        $conf['port'] = $this->config->item('ftp_port', 'ftp');
        $conf['passive'] = $this->config->item('ftp_passive', 'ftp');
        //$conf['debug'] = TRUE;

        $this->ftp->connected = $this->ftp->connect($conf);

        if ($this->ftp->connected)
        {
            if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);
        }

        $this->load->model('clients_model');
        $this->model = $this->clients_model;

        $this->load->model('privileges_model');
        $this->load->model('countries_model');
        $this->load->model('cities_model');
        $this->load->model('cli_priv_model');
    }

    /** получает данные о клиенте
     *
     * @param int id  - ID клиента. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $client_id = $this->input->post('id');
        if (empty($client_id)) {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // берем из БД
        $client = $this->model->getById($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $client;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }

    /** получает список клиентов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since) {
            $params[] = ['clients.updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** получает список клиентов отфильтрованный по значениям через ИЛИ first_name, last_name, email, user_insta_nick
     *
     * @param string $search - искомое слово
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getFilteredList()
    {
        $search = $this->input->post('search');
        if (empty($search))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $params = "(first_name like '%".$search."%' 
                    OR last_name like '%".$search."%' 
                    OR user_insta_nick like '%".$search."%' 
                    OR email like '%".$search."%')";

        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** получает список клиентов в remote-mode vue DataTable формате
     *
     * @params json $server_params - json_encoded(!!!) параметры вида
     * serverParams: {
     *               columnFilters: {first_name: 'john', status: 'active'}, // пары "поле"->"значение" - параметры поиска по колонками(?)
     *               sort: [
     *                       { field: 'created', type: 'desc' },
     *                       { field: 'email', type: 'asc' },
     *               ],
     *               page: 1, // номер страницы
     *               perPage: 10 // сколько записей отображать на страницу
     *               }
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListAdv()
    {
        $params = $order = $limit = null;
        $server_params = $this->input->post('server_params'); // это "serverParams" good vue пакет оно же "filtering"
        $search_params = $this->input->post('search_params'); // это пакет "выборки"

        //  для теста!!! потом убрать ----------------------------------
        /*$server_params = ['serverParams' =>
            [
                //'columnFilters' => [ 'first_name' => 'john', 'last_name' => 'doe'], // это раскоментировать для имитации "поиска"
                'sort' => [
                    ['field' => 'email', 'type' => 'desc'],
                    ['field' => 'last_name', 'type' => 'asc'],
                ],
                'page' => 1,
                'perPage' => 10
            ]
        ];*/
        //$server_params = json_encode($server_params, JSON_PRETTY_PRINT);
        //  END для теста!!! потом убрать ----------------------------------

        if (!empty($server_params))
        {
            $server_params = json_decode($server_params);
            $prepared_params = $this->transformServerSideParams($server_params);
            $params = $prepared_params['params'];
            $order = $prepared_params['order'];
            $limit = $prepared_params['limit'];
        }

        // здесь начинается нехитрая магия!
        if (!empty($search_params))
        {
            $search_params = json_decode($search_params);
            foreach ($search_params as $arr)
            {
                $params[] = $arr;
            }
            //$params[] = ['first_name LIKE "%a%"'];
        }

        $list = $this->model->getListAdv($params, $order, $limit, []);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** создание клиента
     *
     * @param array $_POST ;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $salt = $this->utils->salt();
        $pass = $this->input->post('pass');
        $pass_enc = $this->utils->hash_password($pass, $salt);

        $data = [
            'user_insta_nick' => $this->input->post('user_insta_nick'),
            'email' => $this->input->post('email'),
            'is_bloogger' => intval(boolval($this->input->post('is_blogger'))),
            'salt' => $salt,
            'pass' => $pass_enc,
            'sex' => $this->input->post('sex'),
            'first_name' => $this->input->post('first_name'),
            'patronym_name' => $this->input->post('patronym_name'),
            'last_name' => $this->input->post('last_name'),
            'avatar' => $this->input->post('avatar'),
            'timezone' => $this->input->post('timezone'),
            'subscribed_system' => intval(boolval($this->input->post('subscribed_system'))),
            'subscribed_news' => intval(boolval($this->input->post('subscribed_system'))),
            'status' => $this->input->post('status'),
        ];

        $phone = $this->input->post('phone');
        if (!empty($phone)) $data['phone'] = $phone;

        $birthday = $this->input->post('birthday');
        if (!empty($birthday)) $data['birthday'] = $birthday;

        $country_id = $this->input->post('country_id');
        if (!empty($country_id)) $data['country_id'] = $country_id;

        $city_id = $this->input->post('city_id');
        if (!empty($city_id))
        {
            $data['city_id'] = $city_id;

            if (empty($country_id))
            {
                // определяем страну по городу
                $city = $this->model->getById($city_id);
                if (!empty($city))
                {
                    $country = $this->countries_model->getById($city->country_id);
                    if (!empty($country)) $data['country_id'] = $country->id;
                }
            }
        }

        $this->db->trans_begin();
        //$new_id = $this->model->insertOrUpdate($data);
        $new_id = $this->model->insert($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }

    /** изменение статуса клиента
     *
     * @param int id  - ID клиента. Берется из post
     * @param string $_POST ['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $client_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($client_id)) {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $client = $this->model->getById($client_id);

        if (!empty($client)) {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($client_id, $data);

            if ($upd_res !== false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($client_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        } else {
            // client не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
    }


    /** изменение клиента
     *
     * @param int id  - ID клиента. Берется из post
     * @param string $_POST ['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $client_id = $this->input->post('id');

        if (empty($client_id))
        {
            //$client_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $client = $this->model->getById($client_id);
        if (empty($client))
        {
            //$client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // найден - изменяем - можно вооще сказать
        // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
        $data = [
            'user_insta_nick' => $this->input->post('user_insta_nick'),
            'email' => $this->input->post('email'),
            'is_blogger' => intval(boolval($this->input->post('is_blogger'))),
            'first_name' => $this->input->post('first_name'),
            'patronym_name' => $this->input->post('patronym_name'),
            'last_name' => $this->input->post('last_name'),
            'subscribed_system' => intval(boolval($this->input->post('subscribed_system'))),
            'subscribed_news' => intval(boolval($this->input->post('subscribed_news'))),
            'sex' => $this->input->post('sex'),
            'status' => $this->input->post('status'),
        ];

        $phone = $this->input->post('phone');
        $data['phone'] = (!empty($phone)) ? $phone : 'NULL';

        $birthday = $this->input->post('birthday');
        $data['birthday'] = (!empty($birthday)) ? $birthday : 'NULL';

        $timezone = $this->input->post('timezone');
        if (!empty($timezone)) $data['timezone'] = $timezone;

        $pass = $this->input->post('pass');
        if (!empty($pass))
        {
            $pass_enc = $this->utils->hash_password($pass, $client->salt);
            $data['pass'] = $pass_enc;
        }
/*
        $user_insta_nick = $this->input->post('user_insta_nick');
        if( (array_key_exists('user_insta_nick', $_POST)) && (empty($user_insta_nick)) ) $data['user_insta_nick'] = 'NULL';
*/
        $country_id = $this->input->post('country_id');
        $data['country_id'] = (!empty($country_id)) ? $country_id : 'NULL';

        if (empty($country_id)) $data['city_id'] = 'NULL';
        else
        {
            $city_id = $this->input->post('city_id');
            if (!empty($city_id)) {
                $data['city_id'] = $city_id;

                if (empty($country_id)) {
                    // определяем страну по городу
                    $city = $this->cities_model->getById($city_id);
                    if (!empty($city)) {
                        $country = $this->countries_model->getById($city->country_id);
                        if (!empty($country)) $data['country_id'] = $country->id;
                        else $data['country_id'] = 'NULL';
                    }
                }
            } else $data['city_id'] = 'NULL';
        }

        $this->db->trans_begin();
        $upd_res = $this->model->update($client_id, $data);

        if ($upd_res !== false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($client_id);

        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }

    /** удаление клиента
     *
     * @param int id  - ID клиента. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $client_id = $this->input->post('id');
        if (empty($client_id))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($client_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
    }

    /** находится ли клиент, заданный через email или инста-ник или телефон хотя бы в одной из льготных групп
     * ПОПРАВКА: в более поздней версии авторизация только через email
     *
     * @param mixed $reg_param - один из рег-параметров клиента (email, user_insta_nick, phone). Берется из request
     * @param string $callback - при работе с JSONP это имя, в которое "обернуть" возвращаемый контент. Берется из request
     *
     * @return  object - JSON-объект формата envelope
     */
    public function hasPrivileges()
    {
        $reg_param = $this->input->post_get('reg_param');
        $callback = $this->input->post_get('callback');

        if (empty($reg_param))
        {
            $status = STATUS_MISSING;
            $message = "Ошибка: параметр не задан";
            $data = (object)[];

            if (empty($callback))
            {
                // обычный json
                return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
            }
            else
            {
                //jsonP
                return $this->utils->jsonpOut($this->utils->envelope($status, $message, $data), $callback);
            }
//            echo $_GET['callback'] . '(' . $this->utils->envelope($status, $message, $data) . ')';
        }

        // берем из БД
        //$client = $this->model->getByRegData($reg_param);
        $client = $this->model->getByEmail($reg_param);

        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            if (empty($callback))
            {
                // обычный json
                return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
            }
            else
            {
                //jsonP
                return $this->utils->jsonpOut($this->utils->envelope($status, $message, $data), $callback);
            }

        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->hasPrivileges($client);

        if (empty($callback))
        {
            // обычный json
            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
        else
        {
            //jsonP
            return $this->utils->jsonpOut($this->utils->envelope($status, $message, $data), $callback);
        }
    }

    /** загрузка аватара.
     *
     * @param int $client_id - ID клинта, для которого загружается аватар
     * @param string $_FILES['file'] - загружаемый файл
     *
     * @return  object - JSON-объект формата envelope
     */
    public function uploadAvatar()
    {
        if (!$this->ftp->connected)
        {
            $status = STATUS_FAIL;
            $message = "Ошибка: среда передачи недоступна.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $client_id = $this->input->post('client_id');

        /** @var StdClass $client */
        $client = $this->model->getById($client_id);

        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $old_avatar = $client->avatar;

        $parts = pathinfo($_FILES['file']['name']);
        $ext = $parts['extension'];
        $basename = $parts['filename'];

        $filename = md5(uniqid()) . "_" . $basename . "." . $ext;

        $uData = [
            'avatar' => $filename,
        ];
        $affected_rows = $this->model->update($client_id, $uData);

        if ($affected_rows !== false)
        {

            // у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------
            if( (!empty($old_avatar)) && (strpos("://",$old_avatar) === false) )
            {
                if ($this->ftp->connected)
                {
                    @$this->ftp->delete_file($client->avatar);
                }
            }

            if ($this->ftp->connected)
            {
                $this->ftp->upload($_FILES['file']['tmp_name'], $filename, 'binary');
            }
            // END у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------

            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($client_id);

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        // что-то пошло не так
        $status = STATUS_NOT_FOUND;
        $message = STATUS_NOT_FOUND_MSG;
        $data = (object)[];

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }


    /** удаление аватара
     *
     *  @param   int id  - ID клиента. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function deleteAvatar()
    {
        $client_id = intval($this->input->post('client_id'));

        if (empty($client_id))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        /** @var StdClass $client */
        $client = $this->model->getById($client_id);

        if (empty($client))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $old_avatar = $client->avatar;
        // у клиента уже был аватар и это НЕ instagram - удаляем старый файл ----------------------------
        if( (!empty($old_avatar)) && (strpos("://",$old_avatar) === false) )
        {
            if ($this->ftp->connected)
            {
                @$this->ftp->delete_file($client->avatar);
            }
            $uData = [
                'avatar' => null,
            ];
            $affected_rows = $this->model->update($client_id, $uData);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->getById($client_id);

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    public function getCourseWebHook()
    {
/*
        $f = fopen("hook_log.txt", "w+");
        ob_start();
echo "\n";
print_r($_GET);
echo "\n";
        $st = ob_get_contents();
        fputs($f, $st);
        fclose($f);
        ob_end_clean();
        return true;
*/


        $this->load->model('products_model');
        $this->load->model('options_model');
        $this->load->model('clients_products_model');
        $this->load->model('instagram_accounts_model');

        $this->config->load('cms_consts', true);

        $this->load->library('mailer');
        $this->mailer->sendType = 'sync';

        $this->project_domain = $this->config->item('BASE_URL', 'cms_consts');
        $this->clients_path = $this->project_domain.'/clients';
        $this->project_name = $this->config->item('project_name', 'cms_consts');

        $user_insta_nick = $this->input->get('user_instagram');
        $first_name = $this->input->get('first_name');
        $email = $this->input->get('email');
        $phone = $this->input->get('phone');
        $getcourse_id = $this->input->get('user_id');

        $product_name = $this->input->get('purchase_name');

        $end_dt = $this->input->get('purchase_end');


        $has_old_pass = "";
        // пытаемся определить клиента по $user_insta_nick
        $client = $this->model->getByNick($user_insta_nick);

        if (empty($client))
        {
            // пытаемся определить клиента по $email
            $client = $this->model->getByEmail($email);

            if (empty($client))
            {
                // пытаемся определить клиента по $phone
                $client = $this->model->getByPhone($phone);
            }

        }

        $salt = $this->utils->salt();
        $pass = $this->utils->generateRandomString(8);
        $pass_enc = $this->utils->hash_password($pass, $salt);

        // не нашли - создаем
        if (empty($client))
        {
            $uData = [
                'user_insta_nick' => $user_insta_nick,
                'phone' => $phone,
                'email' => $email,
                'salt' => $salt,
                'pass' => $pass_enc,
                'first_name' => $first_name,
                'getcourse_id' => $getcourse_id,
                'status' => 'active',
            ];

            $new_id = $this->model->insertOrUpdate($uData);

            if ($new_id)
            {
                $client = $this->model->getById($new_id);
                $has_old_pass="";
            }
            else return false; // не получилось создать юзера
        }
        else
        {
            // клиент уже есть в системе - проверяем, есть ли у него пароль, если нет - генерим новый
            $has_old_pass = $this->model->getPass($client->id);
            if (empty($has_old_pass))
            {
                $uData['pass'] = $pass_enc;
                $this->model->update($client->id, $uData);
            }
        }

        // клиент есть или создан. Нет старого пароля, - клиент или создан только что ли был, но пароля не было - шлем письмо
        if (empty($has_old_pass))
        {
            /*вот здесь слать письмо на client->email с $pass*/
            $mail_header_logo = $this->config->item('mail_header_logo', 'cms_consts');
            $html = $mail_header_logo .
                "<p>Поздравляем! Ты в шаге от завершения регистрации на новой платформе Риммы Карамовой!</p>" .
                "<p>Осталось лишь ввести свой логин и пароль, чтобы получить доступ к открывшимся новым возможностям!</p>" .
                "<p>Для авторизации перейди сюда: </p><strong>" . $this->clients_path . "</strong>" .
                "<p>Ваш логин: </p><strong>" . $email . "</strong>" .
                "<p>Ваш пароль: </p><strong>" . $pass . "</strong>";

            $eData = [
                'toEmail' => $email,
                'subject' => "Регистрация в " . $this->project_name,
                'body' => $html,
            ];
            $this->mailer->send($eData);
        }

        // достаем продукт по названию ----------------
        /** @var StdClass $product */
        $product = $this->products_model->getByName($product_name);

        // продукт по имени не нашли. Создаем
        if (empty($product))
        {
            $insta_acc = $this->instagram_accounts_model->getByLogin('malyshkimoi');
            $uData = [
                'instagram_account_id' => $insta_acc->id,
                'name' => $product_name,
                'product_type' => 'subscription',
                'status' => 'active'
            ];

            $new_id = $this->products_model->insertOrUpdate($uData);

            if ($new_id)
            {
                $product = $this->products_model->getById($new_id);

                // для целостности продукта для него следует создать опцию
                $uData = [
                    'product_id' => $product->id,
                    'name' => 'Подписка на 30 дней',
                    'duration' => 30,
                    'status' => 'active',
                ];
                $this->options_model->insertOrUpdate($uData);
            }
            else return false; // не получилось создать продукт

        }

        // по client_id и product_id пробуем искать связку
        $clients_products = $this->clients_products_model->getByClientIdProductId($client->id, $product->id);

        // связка не найдена - создаем
        if (empty($clients_products))
        {
            $uData = [
                'client_id' => $client->id,
                'product_id' => $product->id,
                'end_dt' => $this->utils->getcourseDateToStringDateTime($end_dt),
            ];
            $new_id = $this->clients_products_model->insertOrUpdate($uData);

            if (!$new_id) return false;
        }
        else
        {
            $uData = [
                'end_dt' => $this->utils->getcourseDateToStringDateTime($end_dt),
            ];
            $this->clients_products_model->update($clients_products->id, $uData);
            $clients_products = $this->clients_products_model->getById($clients_products->id);
        }


        return true;
    }


    /** Слияние клиентов
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ. Берется из post
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function merge()
    {
        $acceptor_id = intval($this->input->post('acceptor_id'));
        $donor_id = intval($this->input->post('donor_id'));

        if ((empty($acceptor_id)) || (empty($donor_id)) )
        {
            //$client_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $acceptor = $this->model->getById($acceptor_id);
        if (empty($acceptor))
        {
            //$client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG.". Получатель не найден.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $donor = $this->model->getById($donor_id);
        if (empty($donor))
        {
            //$client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG.". Донор не найден.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }


        $result = true;
        $this->db->trans_begin();
        $result = $this->model->merge($acceptor_id, $donor_id);

        if ($result !== false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;

            //$this->model->delete($donor_id);//РЕАЛЬНО удаляем клиента

            $this->db->trans_commit();
            $data = $this->model->getById($acceptor_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
    }


}