<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_devices
 * @property clients_model  $clients_model
 * @property clients_devices_model $clients_devices_model
 */
class Clients_devices extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('clients_devices_model');
        $this->model = $this->clients_devices_model;
    }

    /** получает данные - запись clients_devices
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_devices_id = $this->input->post('id');
        if (empty($clients_devices_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_devices = $this->model->getById($clients_devices_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_devices;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает devices по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getDevicesListByClientId()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи.
     *
     *  @param   string device  - GUID устройства. Берется из post
     *  @param   int client_id   - ID client. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $device = $this->input->post('device');
        if (empty($device))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $new_id = $this->model->addDeviceToClient($device, $client_id);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет device
     *
     * @param string device - GUID устройства. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $device = $this->input->post('device');
        if (empty($device))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getByDevice($device);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeDevice($record->device);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = $res;


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID clients_devices. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $clients_devices_id = $this->input->post('id');
        $client_id = $this->input->post('client_id');
        $device = $this->input->post('device');

        if (empty($clients_devices_id))
        {
            //нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $clients_devices */
        $clients_devices = $this->model->getById($clients_devices_id);

        if (!empty($clients_devices))
        {
            $data = [
                'client_id' => $client_id,
                'device' => $device,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($clients_devices_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}
