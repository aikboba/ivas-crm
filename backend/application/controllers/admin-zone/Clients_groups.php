<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_groups
 * @property Clients_model $clients_model
 * @property Groups_model  $groups_model
 * @property Clients_groups_model $clients_groups_model
 *
 */
class Clients_groups extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('groups_model');
        $this->load->model('clients_groups_model');

        $this->model = $this->clients_groups_model;
    }

    /** получает данные - запись Clients_groups
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_groups_id = $this->input->post('id');
        if (empty($clients_groups_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_groups = $this->model->getById($clients_groups_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_groups;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список групп по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getGroupsListByClientId()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getGroupsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список клиентов указанной группы (group_id)
     *
     * @param  int $group_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getClientsListByGroupId()
    {
        $group_id = $this->input->post('group_id');

        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getClientsByGroupId($group_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** удаляет клиента client_id из группы group_id
     *  (в модели удаляет связку groups-clients)
     *
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   int group_id   - ID группы клиентов. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removeClientFromGroup()
    public function removeLink()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $group_id = $this->input->post('group_id');
        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeClientFromGroup($client_id, $group_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет клиента client_id из группы group_id - аналогичен removeClientFromGroup
     *
     *  @param   int id  - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeClientFromGroup($record->client_id, $record->group_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** создает запись - добавляет админа client_id в группу group_id
     *
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   int group_id   - ID группы клиентов. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admins_admingroups_id
     */
    public function create()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $group_id = $this->input->post('group_id');
        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $new_id = $this->model->addClientToGroup($client_id, $group_id);

        if ( ($new_id!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID clients_groups. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $clients_groups_id = $this->input->post('id');
        $client_id = $this->input->post('client_id');
        $group_id = $this->input->post('group_id');

        if (empty($clients_groups_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $clients_groups */
        $clients_groups = $this->model->getById($clients_groups_id);

        if (!empty($clients_groups))
        {
            $data = [
                'client_id' => $client_id,
                'group_id' => $group_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($clients_groups_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($clients_groups_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение групп клиента
     *
     *  @param   int id  - ID клиента. Берется из post
     *  @param   array groups  - массив вида $groups = [<group_id1>, ... ,<group_idN>];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function saveGroups()
    {
        $client_id = $this->input->post('id');
        if (empty($client_id))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $clientGroups = $this->input->post('groups');
        foreach ($clientGroups as $key => $group_id)
        {
            $this->model->addClientToGroup($client_id, $group_id);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
