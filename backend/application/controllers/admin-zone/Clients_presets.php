<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_presets
 * @property Clients_model  $clients_model
 * @property Presets_model $presets_model
 * @property Clients_presets_model $clients_presets_model
 */
class Clients_presets extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('presets_model');
        $this->load->model('clients_presets_model');

        $this->model = $this->clients_presets_model;
    }

    /** создание записи.
     *
     *  @param   int client_id - ID клиента. Берется из post
     *  @param   int preset_id - ID пресета. Берется из post
     *  @param   string starting_dt - дата начала доступа в формате 2019-01-02 23:0:56. Берется из post
     *  @param   string expiration_dt - дата устаревания доступа в формате 2019-01-02 23:0:56. Берется из post
     *  @param   string origin - Сущность, от которой получается доступ
     *  @param   string origin_id - ID Сущности, от которой получается доступ
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $starting_dt = $this->input->post('starting_dt');
        if (empty($starting_dt)) $starting_dt = 'NULL';
        $expiration_dt = $this->input->post('expiration_dt');
        if (empty($expiration_dt)) $expiration_dt = 'NULL';
        $origin = $this->input->post('origin');
        $origin_id = $this->input->post('origin_id');

        $new_id = $this->model->addPresetToClient($preset_id, $client_id, $starting_dt, $expiration_dt, $origin, $origin_id);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные - запись clients_presets
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_presets_id = $this->input->post('id');
        if (empty($clients_presets_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_presets = $this->model->getById($clients_presets_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_presets;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет связку - забирает пресет из доступности клиента
     *  (в модели удаляет связку клиенты-presets)
     *
     *  @param   int id  - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removePresetFromClient($record->preset_id, $record->client_id, $record->origin, $record->origin_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет связку - забирает пресет из доступности клиента
     *  (в модели удаляет связку клиенты-presets)
     *
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   int preset_id   - ID пресета. Берется из post
     *  @param   int origin   - происхождение пресета. Берется из post
     *  @param   int origin_id   - ID сущности происхождения пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removePresetFromClient()
    public function removeLink()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $origin = $this->input->post('origin');
        $origin_id = $this->input->post('origin_id');

        $this->db->trans_begin();
        $res = $this->model->removePresetFromClient($preset_id, $client_id, $origin, $origin_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает пресеты по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getPresetsListByClientId()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPresetsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список клиентов, которые имеют доступ указанному preset_id
     *
     * @param  int $preset_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getClientsListByPresetId()
    {
        $preset_id = $this->input->post('preset_id');

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getClientsByPresetId($preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** изменение
     *
     *  @param   int id  - ID clients_presets. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $clients_presets_id = $this->input->post('id');
        $client_id = $this->input->post('client_id');
        $preset_id = $this->input->post('preset_id');

        $starting_dt = $this->input->post('starting_dt');
        if (empty($starting_dt)) $starting_dt = 'NULL';

        $expiration_dt = $this->input->post('expiration_dt');
        if (empty($expiration_dt)) $expiration_dt = 'NULL';

        $origin = $this->input->post('origin');
        if (empty($origin)) $origin = 'NULL';

        $origin_id = $this->input->post('origin_id');
        if (empty($origin_id)) $origin_id = 'NULL';

        if (empty($clients_presets_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $clients_presets */
        $clients_presets = $this->model->getById($clients_presets_id);

        if (!empty($clients_presets))
        {
            $data = [
                'client_id' => $client_id,
                'preset_id' => $preset_id,
                'starting_dt' => $starting_dt,
                'expiration_dt' => $expiration_dt,
                'origin' => $origin,
                'origin_id' => $origin_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($clients_presets_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($clients_presets_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }


            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение пресетов для клиента
     *
     *  @param   int id  - ID клиента. Берется из post
     *  @param   array presets  - массив вида $presets = [
     *                                                    <presetID_1> => [ 'starting_dt'=><starting_dt_1>, 'expiration_dt'=><expiration_dt_1>, 'origin'=><origin_1> , 'origin_id'=><origin_id_1> ],
     *                                                    .........
     *                                                    <presetID_n> => [ 'starting_dt'=><starting_dt_n>, 'expiration_dt'=><expiration_dt_n>, 'origin'=><origin_1> , 'origin_id'=><origin_id_n> ],
     *                                               ];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function savePresets()
    {
        $client_id = $this->input->post('id');
        if (empty($client_id))
        {
            //client_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_presets = $this->input->post('presets');
        foreach ($client_presets as $preset_id => $pVal)
        {
            $starting_dt = $pVal['starting_dt'];
            $expiration_dt = $pVal['expiration_dt'];
            $origin = $pVal['origin'];
            $origin_id = $pVal['origin_id'];
            $this->model->addPresetToClient($preset_id, $client_id, $starting_dt, $expiration_dt, $origin, $origin_id);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
