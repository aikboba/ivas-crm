<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Clients_products
 * @property clients_model  $clients_model
 * @property products_model $products_model
 * @property clients_products_model $clients_products_model
 */
class Clients_products extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->load->model('clients_model');
        $this->load->model('clients_products_model');

        $this->model = $this->clients_products_model;
    }

    /** получает данные - запись clients_products
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $clients_products_id = $this->input->post('id');
        if (empty($clients_products_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $clients_products = $this->model->getById($clients_products_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $clients_products;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает products по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getProductsListByClientId()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getProductsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список ДОСТУПНЫХ СЕЙЧАС products по client_id
     *
     * @param  int $client_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAvailableProductsListByClientId()
    {
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getAvailableProductsByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список clients, для которых есть связь с указанным product_id
     *
     * @param  int $product_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getClientsListByProductId()
    {
        $product_id = $this->input->post('product_id');

        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getClientsByProductId($product_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список clients, для которых есть связь с указанным product_id
     *
     * @param  int $product_id - берется из $_POST
     * @param  string $dt - дата, для которой строится список. берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getAvailableClientsListByProductId()
    {
        $product_id = $this->input->post('product_id');

        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        $dt = $this->input->post('dt');

        $list = $this->model->getAvailableClientsByProductId($product_id, $dt);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи.
     *
     *  @param   int client_id   - ID client. Берется из post
     *  @param   int product_id  - ID подарка. Берется из post
     *  @param   string start_dt  - Дата начала доступности. Берется из post
     *  @param   string end_dt  - Дата окончания доступности. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $start_dt = $this->input->post('start_dt');
        if (empty($start_dt)) $start_dt='NULL';
        $end_dt = $this->input->post('end_dt');
        if (empty($end_dt)) $end_dt='NULL';
        $order_id = $this->input->post('order_id');

        $origin = $this->input->post('origin');
        if (empty($origin)) $origin='manual';

        $origin_id = $this->input->post('origin_id');
        if (empty($origin_id)) $origin_id='NULL';

        $new_id = $this->model->addProductToClient($product_id, $client_id, $start_dt, $end_dt, $order_id, $origin, $origin_id);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет связку - забирает продукт из доступности клиента
     *  (в модели удаляет связку клиенты-продукты)
     *
     *  @param   int id   - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeProductFromClient($record->client_id, $record->product_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет связку - забирает продукт из доступности клиента
     *  (в модели удаляет связку клиенты-продукты)
     *
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   int product_id   - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removeProductFromClient()
    public function removeLink()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeProductFromClient($client_id, $product_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение
     *
     *  @param   int id  - ID clients_products. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $clients_products_id = $this->input->post('id');
        $client_id = $this->input->post('client_id');
        $product_id = $this->input->post('product_id');
        $start_dt = $this->input->post('start_dt');
        if (empty($start_dt)) $start_dt='NULL';
        $end_dt = $this->input->post('end_dt');
        if (empty($end_dt)) $end_dt='NULL';
        $order_id = $this->input->post('order_id');

        if (empty($clients_products_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $clients_products */
        $clients_products = $this->model->getById($clients_products_id);

        if (!empty($clients_products))
        {
            $data = [
                'client_id' => $client_id,
                'product_id' => $product_id,
                'start_dt' => $start_dt,
                'end_dt' => $end_dt,
                'order_id' => $order_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($clients_products_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($clients_products_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}
