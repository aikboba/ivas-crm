<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Countries
 * @property Countries_model $countries_model
 */
class Countries extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('countries_model');
        $this->model = $this->countries_model;
    }

    public function index()
    {

    }

    /** получает данные о стране по ID
     *
     *  @param   int id  - ID страны. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $country_id = $this->input->post('id');
        if (empty($country_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $country = $this->model->getById($country_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $country;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные о стране по iso
     *
     *  @param   string iso  - iso-код страны. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByIso()
    {
        $country_iso = $this->input->post('iso');
        if (empty($country_iso))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $country = $this->model->getByIso($country_iso);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $country;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список стран
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание страны
     *
     *  @param string iso - iso-код страны. Берется из POST;
     *  @param string name - название страны. Берется из POST;
     *  @param string status - статус страны. Берется из POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'iso' => $this->input->post('iso'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса страны
     *
     *  @param  int id  - ID страны. Берется из post
     *  @param  string status - новый статус;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $country_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($country_id))
        {
            //country_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $country = $this->model->getById($country_id);

        if (!empty($country))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($country_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($country_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // country не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение страны
     *
     *  @param   int id  - ID страны. Берется из post
     *  @param   string iso - ISO-код страны;
     *  @param   string name - название страны;
     *  @param   string status - status страны;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $country_id = $this->input->post('id');
        if (empty($country_id))
        {
            //$country_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $country = $this->model->getById($country_id);

        if (!empty($country))
        {
            // найден - изменяем
            $data = [
                'iso' => $this->input->post('iso'),
                'name' => $this->input->post('name'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($country_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($country_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // country не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление страны
     *
     *  @param   int id  - ID страны. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $country_id = $this->input->post('id');
        if (empty($country_id))
        {
            //country_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($country_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}
