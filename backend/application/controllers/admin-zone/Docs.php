<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Docs
 * @property Docs_model $docs_model
 * @property CI_FTP $ftp
 */
class Docs extends Base_Admin_Controller {

    public $upload_path = "";
    public $www_path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->config('ftp', true);
        $this->upload_path = $this->config->item('ftp_upload_docs_path', 'ftp'); // это реальная папка на FTP, куда падают загруженные файлы
        $this->www_path = $this->config->item('ftp_www_docs_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт

        // это позднее переделать!!!
        $this->load->library('ftp');
        $conf['hostname'] = $this->config->item('ftp_host', 'ftp');
        $conf['username'] = $this->config->item('ftp_user', 'ftp');
        $conf['password'] = $this->config->item('ftp_password', 'ftp');
        $conf['port']     = $this->config->item('ftp_port', 'ftp');
        $conf['passive']  = $this->config->item('ftp_passive', 'ftp');
        //$conf['debug']    = TRUE;

        $this->ftp->connected = $this->ftp->connect($conf);
        if ($this->ftp->connected)
        {
            if (!empty($this->upload_path)) $this->ftp->changedir($this->upload_path);
        }

        $this->load->model('docs_model');
        $this->model = $this->docs_model;
/*
        $source = $_SERVER['DOCUMENT_ROOT']."static/uploads/1.jpg";
        $conn = ftp_connect($conf['hostname']);
        ftp_login($conn, $conf['username'], $conf['password']);
        ftp_chdir($conn, '/www/nexus92.ru/uploads');
        $dir = ftp_pwd($conn);
        ftp_pasv($conn, true);
        ftp_put($conn, '111.jpg',$source, FTP_BINARY);
echo "<pre>";
print_r($dir);
echo "</pre>";
die();

        ftp_close($conn);
*/
    }

    public function index()
    {

    }

    /** получает данные о документе по ID
     *
     *  @param   int id  - ID документа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $doc_id = $this->input->post('id');
        if (empty($doc_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $doc = $this->model->getById($doc_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $doc;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список документов клиента
     *
     * @params int $client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $client_id = $this->input->post('client_id');

        if (empty($client_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getByClientId($client_id);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание документа с загрузкой файла
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        if (!$this->ftp->connected)
        {
            $status = STATUS_FAIL;
            $message = "Ошибка: среда передачи недоступна.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $data = [
            'client_id' => $this->input->post('client_id'),
        ];
        $title = $this->input->post('title');

        $parts = pathinfo($_FILES['file']['name']);
        $ext = $parts['extension'];
        $basename = $parts['filename'];

        $data['title'] = (empty($title)) ? $basename.".".$ext : $title;
        $data['filename'] = md5(uniqid())."_".$basename.".".$ext;
        $data['mime_type'] = $_FILES['file']['type'];
        $data['status'] = 'active';

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            if ($this->ftp->connected)
            {
                $this->ftp->upload($_FILES['file']['tmp_name'], $data['filename'], 'binary');
            }

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса документа
     *
     *  @param   int id  - ID документа. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $doc_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($doc_id))
        {
            //doc_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $doc = $this->model->getById($doc_id);

        if (!empty($doc))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($doc_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($doc_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // doc не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение документа
     *
     *  @param   int id  - ID документа. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $doc_id = $this->input->post('id');

        if (empty($doc_id))
        {
            //$doc_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $doc = $this->model->getById($doc_id);

        if (!empty($doc))
        {
            // найден - изменяем
            $data = [
                'client_id' => $this->input->post('client_id'),
                'title' => $this->input->post('title'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($doc_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($doc_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // doc не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление документа
     *
     *  @param   int id  - ID документа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        if (!$this->ftp->connected)
        {
            $status = STATUS_FAIL;
            $message = "Ошибка: среда передачи недоступна.";
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }

        $doc_id = intval($this->input->post('id'));

        if (empty($doc_id))
        {
            //doc_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            /** @var StdClass $data */
            $data = $this->model->getById($doc_id);

            $this->db->trans_begin();

            $del_res = $this->model->delete($doc_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();

                if ($this->ftp->connected)
                {
                    @$this->ftp->delete_file($data->filename);
                }
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }



}
