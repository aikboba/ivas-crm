<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Gifts
 * @property Gifts_model $gifts_model
 */
class Gifts extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('gifts_model');
        $this->model = $this->gifts_model;
    }

    public function index()
    {

    }

    /** получает данные о подарке
     *
     *  @param   int id  - ID подарка. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $gift_id = $this->input->post('id');
        if (empty($gift_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $gift = $this->model->getById($gift_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $gift;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список подарков
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание подарка
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'duration' => $this->input->post('duration'),
            'apply_if' => $this->input->post('apply_if'),
            'status' => $this->input->post('status'),
        ];

        $start_dt = $this->input->post('start_dt');
        if (!empty($start_dt)) $data['start_dt'] = $start_dt;

        $end_dt = $this->input->post('end_dt');
        if (!empty($end_dt)) $data['end_dt'] = $end_dt;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса подарка
     *
     *  @param   int id  - ID подарка. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $gift_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($gift_id))
        {
            //gift_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $gift = $this->model->getById($gift_id);

        if (!empty($gift))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($gift_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($gift_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // gift не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение подарка
     *
     *  @param   int id  - ID подарка. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $gift_id = $this->input->post('id');

        if (empty($gift_id))
        {
            //$gift_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $gift = $this->model->getById($gift_id);

        if (!empty($gift))
        {
            // найден - изменяем
            $data = [
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'duration' => intval($this->input->post('duration')),
                'apply_if' => $this->input->post('apply_if'),
                'status' => $this->input->post('status'),
            ];

            $start_dt = $this->input->post('start_dt');
            $data['start_dt'] = (!empty($start_dt)) ? $start_dt : 'NULL';

            $end_dt = $this->input->post('end_dt');
            $data['end_dt'] = (!empty($end_dt)) ? $end_dt : 'NULL';

            $this->db->trans_begin();
            $upd_res = $this->model->update($gift_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($gift_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }


            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // gift не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление подарка
     *
     *  @param   int id  - ID подарка. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $gift_id = $this->input->post('id');
        if (empty($gift_id))
        {
            //gift_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($gift_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }



}
