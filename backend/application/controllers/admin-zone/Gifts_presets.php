<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Gifts_presets
 * @property Gifts_model  $gifts_model
 * @property Presets_model $presets_model
 * @property Gifts_presets_model $gifts_presets_model
 */
class Gifts_presets extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('presets_model');
        $this->load->model('gifts_model');
        $this->load->model('gifts_presets_model');

        $this->model = $this->gifts_presets_model;
    }

    /** удаляет связку - забирает пресет из gift
     *  (в модели удаляет связку gifts-presets)
     *
     *  @param   int id  - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope.
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removePresetFromGift($record->preset_id, $record->gift_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет связку - забирает пресет из gift
     *  (в модели удаляет связку gifts-presets)
     *
     *  @param   int gift_id  - ID клиента. Берется из post
     *  @param   int preset_id   - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope.
     */
//    public function removePresetFromGift()
    public function removeLink()
    {
        $gift_id = $this->input->post('gift_id');
        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removePresetFromGift($gift_id, $preset_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные - запись gifts_presets
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $gifts_presets_id = $this->input->post('id');
        if (empty($gifts_presets_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $gifts_presets = $this->model->getById($gifts_presets_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $gifts_presets;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает presets по по gift_id
     *
     * @param  int $gift_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getPresetsListByGiftId()
    {
        $gift_id = $this->input->post('gift_id');

        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPresetsByGiftId($gift_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список gifts, в которых входит указанный preset_id
     *
     * @param  int $preset_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getGiftsListByPresetId()
    {
        $preset_id = $this->input->post('preset_id');

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getGiftsByPresetId($preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи.
     *
     *  @param   int gift_id   - ID gift. Берется из post
     *  @param   int preset_id  - ID пресета. Берется из post
     *  @param   int pos   - позиция, в которую добавляется. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $gift_id = $this->input->post('gift_id');
        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $pos = $this->input->post('pos');

        $new_id = $this->model->addPresetToGift($preset_id, $gift_id, $pos);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID gifts_presets. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $gifts_presets_id = $this->input->post('id');
        $gift_id = $this->input->post('gift_id');
        $preset_id = $this->input->post('preset_id');
        $pos = $this->input->post('pos');

        if (empty($gifts_presets_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $gifts_presets */
        $gifts_presets = $this->model->getById($gifts_presets_id);

        if (!empty($gifts_presets))
        {
            $data = [
                'gift_id' => $gift_id,
                'preset_id' => $preset_id,
                'pos' => $pos,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($gifts_presets_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($gifts_presets_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение preset для gift
     *
     *  @param   int id  - ID подарка. Берется из post
     *  @param   array presets  - json_encoded массив вида $presets = [
     *                                                    <presetID_1> => <pos_1>,
     *                                                    .........
     *                                                    <presetID_n> => <pos_n>,
     *                                               ];
     *                                               pos может и не быть, но тогда передавать 0
     *
     * @return  object - JSON-объект формата envelope
     */
    public function savePresets()
    {
        $gift_id = $this->input->post('id');
        if (empty($gift_id))
        {
            //gift_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $giftsPresets = json_decode($this->input->post('presets'));

        if (!empty($giftsPresets))
        {
            foreach ($giftsPresets as $preset_id => $pos)
            {
                $this->model->addPresetToGift($preset_id, $gift_id, $pos);
            }
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
