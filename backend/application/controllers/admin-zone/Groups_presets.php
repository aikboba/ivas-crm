<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Groups_presets
 * @property Groups_model $groups_model
 * @property Presets_model  $presets_model
 * @property Groups_presets_model $groups_presets_model
 */
class Groups_presets extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('groups_model');
        $this->load->model('presets_model');
        $this->load->model('groups_presets_model');

        $this->model = $this->groups_presets_model;
    }

    /** получает данные - запись Groups_presets
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $groups_presets_id = $this->input->post('id');
        if (empty($groups_presets_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $groups_presets = $this->model->getById($groups_presets_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $groups_presets;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список presets по group_id
     *
     * @param  int $group_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getPresetsListByGroupId()
    {
        $group_id = $this->input->post('group_id');
        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPresetsByGroupId($group_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список групп, куда входит указанный пресет
     *
     * @param  int $preset_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getGroupsListByPresetId()
    {
        $preset_id = $this->input->post('preset_id');

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getGroupsByPresetId($preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** добавляет пресет preset_id в группу group_id
     *  (в модели выстраивает связку клиенты-presets)
     *
     *  @param   int group_id   - ID группы клиентов. Берется из post
     *  @param   int preset_id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function addPresetToGroup($data = null)
    public function addLink($data = null)
    {
        if (!empty($data))
        {
            $preset_id = $data['preset_id'];
            $group_id = $data['group_id'];
        }
        else
        {
            $preset_id = $this->input->post('preset_id');
            $group_id = $this->input->post('group_id');
        }

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $new_id = $this->model->addPresetToGroup($preset_id, $group_id);

        if ( ($new_id!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет пресет preset_id из группы group_id
     *  (в модели удаляет связку группы-presets)
     *
     *  @param   int id  - ID связки. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removePresetFromGroup($record->preset_id, $record->group_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет пресет preset_id из группы group_id
     *  (в модели удаляет связку клиенты-presets)
     *
     *  @param   int group_id   - ID группы клиентов. Берется из post
     *  @param   int preset_id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removePresetFromGroup()
    public function removeLink()
    {
        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $group_id = $this->input->post('group_id');
        if (empty($group_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removePresetFromGroup($preset_id, $group_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** создание записи.
     *
     *  @param   int group_id   - ID группы клиентов. Берется из post
     *  @param   int preset_id  - ID пресета. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [];
        $data['preset_id'] = $this->input->post('preset_id');
        $data['group_id'] = $this->input->post('group_id');

        return $this->addLink($data);
    }

    /** изменение
     *
     *  @param   int id  - ID groups_presets. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $groups_presets_id = $this->input->post('id');
        $preset_id = $this->input->post('preset_id');
        $group_id = $this->input->post('group_id');

        if (empty($groups_presets_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $groups_presets */
        $groups_presets = $this->model->getById($groups_presets_id);

        if (!empty($groups_presets))
        {
            $data = [
                'preset_id' => $preset_id,
                'group_id' => $group_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($groups_presets_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($groups_presets_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение всех пресетов группы
     *
     *  @param   int id  - ID группы. Берется из post
     *  @param   array presets  - массив вида $presets = [<preset_id1>, ... ,<preset_idN>];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function savePresets()
    {
        $group_id = $this->input->post('id');
        if (empty($group_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $groupPresets = $this->input->post('presets');
        foreach ($groupPresets as $key => $preset_id)
        {
            $this->model->addPresetToGroup($preset_id, $group_id);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
