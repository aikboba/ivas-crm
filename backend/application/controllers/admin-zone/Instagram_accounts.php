<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Instagram_accounts
 * @property Instagram_accounts_model $instagram_accounts_model
 * @property Products_model $products_model
 */
class Instagram_accounts extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('instagram_accounts_model');
        $this->model = $this->instagram_accounts_model;

        $this->load->config('instagram', true);
    }

    public function index()
    {

    }

    /** получает данные об аккаунте
     *
     *  @param   int id  - ID аккаунта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $instagram_account_id = $this->input->post('id');
        if (empty($instagram_account_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $instagram_account = $this->model->getById($instagram_account_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $instagram_account;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список аккаунтов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание аккаунта
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'login' => $this->input->post('login'),
            'password' => $this->input->post('password'),
            'user_name' => $this->input->post('user_name'),
        ];

        if (!empty($this->input->post('pk'))) $data['pk'] = $this->input->post('pk');
        if (!empty($this->input->post('status'))) $data['status'] = $this->input->post('status');

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса аккаунта
     *
     *  @param   int id  - ID аккаунта. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $instagram_account_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($instagram_account_id))
        {
            //instagram_account_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $instagram_account = $this->model->getById($instagram_account_id);

        if (!empty($instagram_account))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($instagram_account_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($instagram_account_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // instagram_account не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение аккаунта
     *
     *  @param   int id  - ID продукта. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $instagram_account_id = $this->input->post('id');

        if (empty($instagram_account_id))
        {
            //$instagram_account_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $instagram_account = $this->model->getById($instagram_account_id);

        if (!empty($instagram_account))
        {
            // найден - изменяем - можно вооще сказать
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'name' => $this->input->post('name'),
                'login' => $this->input->post('login'),
                'password' => $this->input->post('password'),
                'user_name' => $this->input->post('user_name'),
            ];

            if (!empty($this->input->post('pk'))) $data['pk'] = $this->input->post('pk');
            if (!empty($this->input->post('status'))) $data['status'] = $this->input->post('status');

            $this->db->trans_begin();
            $upd_res = $this->model->update($instagram_account_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($instagram_account_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // instagram_account не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление аккаунта
     *
     *  @param   int id  - ID аккаунта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $instagram_account_id = $this->input->post('id');
        if (empty($instagram_account_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($instagram_account_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** МЕТОД БОЛЕЕ НЕ ИСПОЛЬЗУЕТСЯ!!!!
     * webhook для установки stream_url и status
     *
     *  @param   (json array) broadcast.
     *  $broadcast = json_encode([
                                    "guid": "18095102011099677", //не используется
                                    "user": {
                                            "id": 24652344165,//pk
                                            "username": "sergeykegard", //login
                                            "full_name": "sergey",//user_name
                                            "avatar_url": "https://instagram.fhrk1-1.fna.fbcdn.net/vp/221d7d48845df9b507787351d0f26a06/5E8BBDDA/t51.2885-19/s150x150/75266952_2443340879214354_1950343193119686656_n.jpg?_nc_ht=instagram.fhrk1-1.fna.fbcdn.net", //user_avatar
                                            "avatar_id": "2180846617993986416_24652344165"//не используется
                                            },
                                    "status": "inactive",//stream_status
                                    "start_at": "2019-11-23T11:30:21.000Z",//stream_created
                                    "cover_frame_url": "https://instagram.fhrk1-1.fna.fbcdn.net/v/t58.9792-15/76484823_565789730915636_7374930176943063040_n.jpg?_nc_ht=instagram.fhrk1-1.fna.fbcdn.net&oh=d3c817437ff6fe68a3ac851197b3ebf5&oe=5DDACC0C", //stream_logo
                                    "vimeo_url": "https://vimeo.com/375083161",//stream_url
                                    "message": "" //не используется
                                    }
     *                          ])
     * Берется из post
     *
     * @param string $secret_salt - md5(<secret_salt>). secret_salt - задается в /config/bots.php.  Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStream()
    {
        $secret_salt = $this->input->post('secret_salt');

        $stored_salt = $this->config->item('bots_secret_salt', 'instagram');

        if ($secret_salt != md5($stored_salt))
        {
            //неправильная соль - выходим
            $status = STATUS_DENIED;
            $message = STATUS_DENIED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $broadcast = $this->input->post('broadcast');

        if (empty($broadcast))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

/*
$broadcast = [
    "guid"=> "18095102011099677",
    "uuid"=> "18safsdfsd020110yuyjy",
    "user"=> [
        "id"=> 24652344165,
        "username"=> "peter_1979",
        "full_name"=> "Петя Нах",
        "avatar_url"=> "https://instagram.fhrk1-1.fna.fbcdn.net/vp/221d7d48845df9b507787351d0f26a06/5E8BBDDA/t51.2885-19/s150x150/75266952_2443340879214354_1950343193119686656_n.jpg?_nc_ht=instagram.fhrk1-1.fna.fbcdn.net",
        "avatar_id"=> "2180846617993986416_24652344165"
    ],
    "status"=> "active",
    "start_at"=> "2019-11-23T11:30:21.000Z",
    "cover_frame_url"=> "https://instagram.fhrk1-1.fna.fbcdn.net/v/t58.9792-15/76484823_565789730915636_7374930176943063040_n.jpg?_nc_ht=instagram.fhrk1-1.fna.fbcdn.net&oh=d3c817437ff6fe68a3ac851197b3ebf5&oe=5DDACC0C",
    "stream_url"=> "https://youtube.com/375083161",
    "vimeo_url"=> "https://vimeo.com/375083161",
    "youtube_url"=> "https://vimeo.com/375083161",
    "stream_origin"=> "youtube",
    "message"=> ""
];
$broadcast = json_encode($broadcast);
*/
        $broadcast = json_decode($broadcast);

        if (!$broadcast)
        {
            //неверный json
            $status = STATUS_INCORRECT_DATA;
            $message = STATUS_INCORRECT_DATA_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $pk = $broadcast->user->id;
        $login = $broadcast->user->username;
        $user_name = $broadcast->user->full_name;
        $user_avatar = $broadcast->user->avatar_url;

        $stream_origin = $broadcast->stream_origin;
        $stream_url = $broadcast->stream_url;
        $stream_started = $broadcast->start_at;
        $stream_logo = $broadcast->cover_frame_url;
        $stream_status = $broadcast->status;
        $stream_item_guid = $broadcast->uuid;

        $acc = $this->model->getByLogin($login);

        if (empty($acc))
        {
            //бот с таким ником не найден - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем
        $uData = [
            'pk' => $pk,
            //'login' => $login,
            'user_name' => $user_name,
            'user_avatar' => $user_avatar,
            'stream_origin' => $stream_origin,
            'stream_url' => $stream_url,
            'stream_started' => $stream_started,
            'stream_logo' => $stream_logo,
            'stream_status' => $stream_status
        ];

        $res = $this->model->setStream($acc->id, $uData);

        if ($res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );

    }


}
