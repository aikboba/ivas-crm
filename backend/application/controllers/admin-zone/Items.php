<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Items
 * @property Items_model $items_model
 * @property Vimeo $vimeo
 */
class Items extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('vimeo');

        $this->load->model('items_model');
        $this->model = $this->items_model;
    }

    public function index()
    {

    }

    /** получает данные о item
     *
     *  @param   int id  - ID item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $item_id = $this->input->post('id');
        if (empty($item_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $item = $this->model->getById($item_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $item;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные о item по его resource_string_id
     *
     *  @param string resource_string_id  - resource_string_id item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByResourceStringId()
    {
        $resource_string_id = $this->input->post('resource_string_id');
        if (empty($resource_string_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $item = $this->model->getByResourceStringId($resource_string_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $item;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные о item по его guid
     *
     *  @param string guid  - guid item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByGuid()
    {
        $guid = $this->input->post('guid');
        if (empty($guid))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $item = $this->model->getByResourceStringId($guid);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $item;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список item
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список items в remote-mode vue DataTable формате
     *
     * @params json $server_params - json_encoded(!!!) параметры вида
     * serverParams: {
     *               columnFilters: {first_name: 'john', status: 'active'}, // пары "поле"->"значение" - параметры поиска по колонками(?)
     *               sort: [
     *                       { field: 'created', type: 'desc' },
     *                       { field: 'email', type: 'asc' },
     *               ],
     *               page: 1, // номер страницы
     *               perPage: 10 // сколько записей отображать на страницу
     *               }
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListAdv()
    {
        $params = $order = $limit = null;
        $server_params = $this->input->post('server_params'); // это "serverParams" good vue пакет оно же "filtering"
        $search_params = $this->input->post('search_params'); // это пакет "выборки"
        /*
                //  для теста!!! потом убрать ----------------------------------
                $server_params = ['serverParams' =>
                    [
                        'columnFilters' => [ 'name' => 'Видео', ], // это раскоментировать для имитации "поиска"
                        'sort' => [
                            ['field' => 'name', 'type' => 'desc'],
                            //['field' => 'last_name', 'type' => 'asc'],
                        ],
                        'page' => 1,
                        'perPage' => 10
                    ]
                ];
                $server_params = json_encode($server_params, JSON_PRETTY_PRINT);
                //  END для теста!!! потом убрать ----------------------------------
        */
        if (!empty($server_params))
        {
            $server_params = json_decode($server_params);
            $prepared_params = $this->transformServerSideParams($server_params);
            $params = $prepared_params['params'];
            $order = $prepared_params['order'];
            $limit = $prepared_params['limit'];
        }

        // здесь начинается нехитрая магия!
        if (!empty($search_params))
        {
            $search_params = json_decode($search_params);
            foreach ($search_params as $arr)
            {
                $params[] = $arr;
            }
        }

        $list = $this->model->getListAdv($params, $order, $limit, []);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** создание item
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'system_name' => $this->input->post('system_name'),
            'resource_string_id' => $this->input->post('resource_string_id'),
            'description' => $this->input->post('description'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса
     *
     *  @param   int id  - ID item. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $item_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($item_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $item = $this->model->getById($item_id);

        if (!empty($item))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($item_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($item_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // item не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение item
     *
     *  @param   int id  - ID item. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $item_id = $this->input->post('id');

        if (empty($item_id))
        {
            //$item_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $item = $this->model->getById($item_id);

        if (!empty($item))
        {
            // найден - изменяем - можно вооще сказать
            $data = [
                'name' => $this->input->post('name'),
                'system_name' => $this->input->post('system_name'),
                'resource_string_id' => $this->input->post('resource_string_id'),
                'description' => $this->input->post('description'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($item_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($item_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // item не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** "удаление" item
     *
     *  @param   int id  - ID item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $item_id = $this->input->post('id');
        if (empty($item_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($item_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** "удаление" item вместо с очисткой остальных полей (видеофайл удаляется через vimeo)
     *
     *  @param   int id  - ID item. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function deleteWithContent()
    {
        $item_id = $this->input->post('id');
        if (empty($item_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->deleteWithContent($item_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** получает список элементов из удаленного сервиса Vimeo
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getVimeoItemsList()
    {
        $list = $this->vimeo->getVideos();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает информацию о конкретном элементе удаленного сервиса Vimeo, используюя его id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getVimeoItemInfo()
    {
        $resource_string_id = $this->input->post('id');
        if (empty($resource_string_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $info = $this->vimeo->getVideoInfo($resource_string_id);
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $info) );
        }
    }

    /** получает ключи для работы с Vimeo
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getVimeoCredentials()
    {
        $this->load->config('vimeo', true);
        $data = [];
        $data['vimeo_client_id'] = $this->config->item('CLIENT_ID', 'vimeo');
        $data['vimeo_client_secret'] = $this->config->item('CLIENT_SECRET', 'vimeo');
        $data['vimeo_access_token'] = $this->config->item('ACCESS_TOKEN', 'vimeo');

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** возвращает ссылку на скачивание video с Vimeo
     *
     * @param int $id - item_id медиаэлемента
     *
     * @return  object - JSON-объект формата envelope в случае неудачи
     */
    public function getVimeoDownloadLink()
    {
        $item_id = $this->input->post_get('id');
        if (empty($item_id))
        {
            //item_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $item = $this->model->getById($item_id);
        if (empty($item))
        {
            //item нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($item->item_type != 'video')
        {
            //item не видео
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = STATUS_OPER_NOT_ALLOWED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $video_id = $this->vimeo->getVideoIdByUrl($item->resource_string_id);

        if (empty($video_id))
        {
            //video_id пуст
            $status = STATUS_INCORRECT_DATA;
            $message = STATUS_INCORRECT_DATA_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $videoData = $this->vimeo->getVideoInfo($video_id);
        if (empty($videoData['body']['download'][0]['link']))
        {
            $status = STATUS_FAIL;
            $message = STATUS_FAIL_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $videoData['body']['download'][0]['link'];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
