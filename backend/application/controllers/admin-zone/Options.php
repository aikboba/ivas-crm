<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Options
 * @property Products_model $products_model
 * @property Options_model $options_model
 * @property Clients_model $clients_model
 * @property Promocodes_model $promocodes_model
 */
class Options extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('options_model');
        $this->load->model('products_model');
        $this->load->model('clients_model');
        $this->load->model('promocodes_model');

        $this->model = $this->options_model;
    }

    /** получает данные об опции
     *
     *  @param   int $id  - ID опции. Берется из post
     *  @param   int|null $client_id  - ID клиента, для которого получаем опцию (важно при применении промокода и расчете скидки).
     *  @param   string|null $promocode  - Строка-промокод. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $client = null;

        $option_id = intval($this->input->post_get('id'));
        if (empty($option_id))
        {
            //option_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = intval($this->input->post_get('client_id'));
        if (!empty($client_id))
        {
            $client = $this->clients_model->getById($client_id);
            if (empty($client))
            {
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = (object)[];

                return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
            }
        }

        $promocode = $this->input->post_get('promocode');
        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
            if (empty($promocode))
            {
                //promocode не найден- выходим
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        // берем из БД
        /** @var StdClass $option */
        $option = $this->model->getWithDiscount($option_id, $client, $promocode);
        if (empty($option))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $option;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список опций по продукту
     *
     * @param  int $product_id - берется из $_POST
     * @param   string|null $promocode  - Строка-промокод. Берется из post
     * @param   int|null $client_id  - ID клиента, для которого получаем список (важно при применении промокода и расчете скидки).
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $client = null;

        $product_id = $this->input->post_get('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = intval($this->input->post_get('client_id'));
        if (!empty($client_id))
        {
            $client = $this->clients_model->getById($client_id);
            if (empty($client))
            {
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = [];

                return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
            }
        }

        $promocode = $this->input->post_get('promocode');
        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
            if (empty($promocode))
            {
                //promocode не найден- выходим
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = [];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }
        }

        $params = [];
        $params[] = ['product_id', "=", $product_id];

        $list = $this->model->getList($params);

        if (!empty($list))
        {
            foreach ($list as $key=>$option)
            {
                $list[$key] = $this->model->getWithDiscount($option, $client, $promocode);
            }
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание опции
     *
     *  @param   int product_id  - ID продукта. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        $data = [
            'product_id' => $product_id,
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
            'description' => $this->input->post('description'),
            'is_partial_available' => $this->input->post('is_partial_available'),
            'partial_threshold' => $this->input->post('partial_threshold'),
            'partial_available_duration' => $this->input->post('partial_available_duration'),
            'status' => $this->input->post('status'),
        ];

        $duration = $this->input->post('duration');
        if (isset($duration)) $data['duration'] = intval($duration);

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса опции
     *
     *  @param   int id  - ID опции. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $option_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($option_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $option = $this->model->getById($option_id);

        if (!empty($option))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($option_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // option не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение опции
     *
     *  @param   int id  - ID опции. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $option_id = $this->input->post('id');

        if (empty($option_id))
        {
            //$option_id нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $option = $this->model->getById($option_id);

        if (!empty($option))
        {
            // найден - изменяем - можно вообще сказать
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'name' => $this->input->post('name'),
                'price' => $this->input->post('price'),
                'description' => $this->input->post('description'),
                'is_partial_available' => $this->input->post('is_partial_available'),
                'partial_threshold' => $this->input->post('partial_threshold'),
                'partial_available_duration' => $this->input->post('partial_available_duration'),
                'status' => $this->input->post('status'),
            ];

            $duration = $this->input->post('duration');
            $data['duration'] = (isset($duration)) ? intval($duration) : 'NULL';

            $this->db->trans_begin();
            $upd_res = $this->model->update($option_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // option для update не найдена
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление опции (через выставление статуса deleted)
     *
     *  @param   int id  - ID опции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $option_id = $this->input->post('id');
        if (empty($option_id))
        {
            //option_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($option_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $del_res) );
        }
    }



}
