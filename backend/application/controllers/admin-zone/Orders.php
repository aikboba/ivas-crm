<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Orders
 * @property Products_model $products_model
 * @property Options_model $options_model
 * @property Paysystems_model $paysystems_model
 * @property Orders_model $orders_model
 * @property Clients_model $clients_model
 * @property Promocodes_model $promocodes_model
 */
class Orders extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->load->model('paysystems_model');
        $this->load->model('options_model');
        $this->load->model('clients_model');
        $this->load->model('promocodes_model');
        $this->load->model('orders_model');
        $this->model = $this->orders_model;
    }

    /** получает данные о заказе
     *
     *  @param   int id  - ID заказа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $order_id = $this->input->post('id');
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $order = $this->model->getById($order_id);
        if (empty($order))
        {
            //$paysystem_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $order;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** получает список заказов без дополнительных условий
     *
     * @param  string $updated_since - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


    /** получает список заказов без дополнительных условий
     *
     * @param  string $updated_since - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getLogs()
    {
        $order_id = intval($this->input->post_get('order_id'));
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $order = $this->model->getById($order_id);
        if (empty($order))
        {
            //$paysystem_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->load->model('logs_model');
        $list = $this->logs_model->getByOrderId($order_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


    /** получает список заказов клиента
     *
     * @param  int $client_id - ID клиента
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientId()
    {
        $client_id = $this->input->post('client_id');
        $list = $this->model->getByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список заказов в remote-mode vue DataTable формате
     *
     * @params json $server_params - json_encoded(!!!) параметры вида
     * serverParams: {
     *               columnFilters: {first_name: 'john', status: 'active'}, // пары "поле"->"значение" - параметры поиска по колонками(?)
     *               sort: [
     *                       { field: 'created', type: 'desc' },
     *                       { field: 'email', type: 'asc' },
     *               ],
     *               page: 1, // номер страницы
     *               perPage: 10 // сколько записей отображать на страницу
     *               }
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListAdv()
    {
        $params = $order = $limit = null;
        $server_params = $this->input->post('server_params'); // это "serverParams" good vue пакет оно же "filtering"
        $search_params = $this->input->post('search_params'); // это пакет "выборки"

        //  для теста!!! потом убрать ----------------------------------
        /*$server_params = ['serverParams' =>
            [
                //'columnFilters' => [ 'first_name' => 'john', 'last_name' => 'doe'], // это раскоментировать для имитации "поиска"
                'sort' => [
                    ['field' => 'email', 'type' => 'desc'],
                    ['field' => 'last_name', 'type' => 'asc'],
                ],
                'page' => 1,
                'perPage' => 10
            ]
        ];*/
        //$server_params = json_encode($server_params, JSON_PRETTY_PRINT);
        //  END для теста!!! потом убрать ----------------------------------

        if (!empty($server_params))
        {
            $server_params = json_decode($server_params);
            $prepared_params = $this->transformServerSideParams($server_params);
            $params = $prepared_params['params'];
            $order = $prepared_params['order'];
            $limit = $prepared_params['limit'];
        }

        // здесь начинается нехитрая магия!
        if (!empty($search_params))
        {
            $search_params = json_decode($search_params);
            foreach ($search_params as $arr)
            {
                $params[] = $arr;
            }
        }

        $list = $this->model->getListAdv($params, $order, $limit, []);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut($this->utils->envelope($status, $message, $list));
    }

    /** создание заказа
     *
     *  @param   int option_id  - ID опции продукта. Берется из post
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   string promocode  - промокод, который ввел клиент. Берется из post
     *  @param   string $client_message  - сообщение клиента.
     *  @param   string $admin_message  - комментарий менеджера к заказу.
     *  @param   float $amount_paid  - сколько УЖЕ ОПЛАЧЕНО по данному заказу.
     *
     * @return  void - JSON-объект формата envelope
     */
    public function create()
    {
        $option_id = intval($this->input->post('option_id'));
        $client_id = intval($this->input->post('client_id'));
        $client_message = $this->input->post('client_message');
        $admin_message = $this->input->post('admin_message');
        $promocode = $this->input->post('promocode');
        $amount_paid = floatval($this->input->post('amount_paid'));

        if (empty($option_id) || empty($client_id) )
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // получаем клиента
        $client = $this->clients_model->getById($client_id);
        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // клиент неактивен или удален
        if ($client->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: клиент неактивен";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
        }

        // получаем указаную опцию
        /** @var StdClass $option */
        $option = $this->options_model->getWithDiscount($option_id, $client, $promocode);
        if (empty($option))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: опция не найдена";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // опция неактивна или удалена
        if ($option->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: опция неактивна";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // промокод есть, но применим ли он в данном случае?
        if (!empty($promocode))
        {
            if (!$this->promocodes_model->isApplicable($promocode, $option, $client))
            {
                $promocode = null;
            }
        }

        // получаем продукт по опции
        /** @var StdClass $product */
        $product = $this->products_model->getById($option->product_id);
        if (empty($product))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: продукт не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        };

        // продукт неактивен или удален
        if ($product->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: продукт неактивен";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $price = $option->price;
        $aData['client'] = $client;
        $aData['product'] = $product;
        $aData['option'] = $option;
        if (!empty($promocode)) $aData['promocode'] = $promocode;
        $data = [
            'option_id' => $option->id,
            'client_id' => $client->id,
            'price' => $price,
            'amount_paid' => $amount_paid,
            'client_message' => $client_message,
            'admin_message' => $admin_message,
            'additional_data' => json_encode($aData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
        ];
        if (!empty($promocode)) $data['promocode_id'] = $promocode->id;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса заказа
     *
     *  @param   int id  - ID заказа. Берется из post
     *  @param   string $status. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $order_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($order_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $order = $this->model->getById($order_id);

        if (!empty($order))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($order_id, $data);

            if ($upd_res!=false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($order_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // order для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение заказа
     *
     *  @param   int id  - ID заказа. Берется из post
     *  @param   int option_id  - ID опции продукта. Берется из post
     *  @param   int client_id  - ID клиента. Берется из post
     *  @param   string promocode  - промокод, который ввел клиент. Берется из post
     *  @param   string $client_message  - сообщение клиента.
     *  @param   string $admin_message  - комментарий менеджера к заказу.
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $order_id = intval($this->input->post('id'));
        $option_id = intval($this->input->post('option_id'));
        $client_id = intval($this->input->post('client_id'));
        $client_message = $this->input->post('client_message');
        $admin_message = $this->input->post('admin_message');
        $promocode = $this->input->post('promocode');
        $promocode_id = null;

        if (empty($order_id))
        {
            //$order_id нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $order */
        $order = $this->model->getById($order_id);
        if (empty($order))
        {
            //$order_id нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // получаем клиента
        $client = $this->clients_model->getById($client_id);
        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // клиент неактивен или удален
        if ($client->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: клиент неактивен";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($promocode))
        {
            $promocode = $this->promocodes_model->getByPromocode($promocode);
        }

        // получаем указаную опцию
        /** @var StdClass $option */
        $option = $this->options_model->getWithDiscount($option_id, $client, $promocode);
        if (empty($option))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: опция не найдена";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // опция неактивна или удалена
        if ($option->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: опция неактивна";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // промокод есть, но применим ли он в данном случае?
        if (!empty($promocode))
        {
            if ($this->promocodes_model->isApplicable($promocode, $option, $client, $order->id))
            {
                $promocode_id = $promocode->id;
            }
        }

        // получаем продукт по опции
        /** @var StdClass $product */
        $product = $this->products_model->getById($option->product_id);
        if (empty($product))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Ошибка: продукт не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        };

        // продукт неактивен или удален
        if ($product->status != 'active')
        {
            $status = STATUS_DISABLED;
            $message = "Ошибка: продукт неактивен";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $price = $option->price;
        $data = [
            'option_id' => $option->id,
            'client_id' => $client->id,
            'promocode_id' => (!empty($promocode_id)) ? $promocode_id : 'NULL',
            'price' => $price,
            'client_message' => $client_message,
            'admin_message' => $admin_message,
        ];

        // толко для заказов в статусе "новый" и "отмененный" возможны полноценные изменения. Иначе - только сообщение админа
        if ( ($order->status != 'new') && ($order->status != 'cancelled') )
        {
            $data = [
                'admin_message' => $admin_message,
            ];
        }


        $this->db->trans_begin();
        $upd_res = $this->model->update($order_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($order_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаление заказа (через выставление статуса deleted)
     *
     *  @param   int id  - ID заказа. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $order_id = $this->input->post('id');
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($order_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** оплата заказа
     *
     *  @param   int id  - ID заказа. Берется из post
     *  @param   float $amount - сколько оплачено
     *
     * @return  object - JSON-объект формата envelope
     */
    public function payOld()
    {
        $order_id = $this->input->post('id');
        $amount = $this->input->post('amount');

        if (empty($order_id))
        {
            //$order_id нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $order */
        $order = $this->model->getById($order_id);

        if (!empty($order))
        {
            //  если статус не new и не partially_paid - выход
            if ( ($order->status!='new') && ($order->status!='partially_paid') )
            {
                $status = STATUS_OPER_NOT_ALLOWED;
                $message = "Ошибка: заказ не может быть оплачен. Его текущий статус - ".$order->status;
                $data = (object)[];

                return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
            }

            $tot_paid = floatval($order->amount_paid + $amount);

            $data = [];
            $data['amount_paid'] = $tot_paid;

            // человек оплатил заказ до конца ------------------
            if ($tot_paid >= $order->price)
            {
                $data['status'] = 'paid';
                /** @var StdClass $option */
                $option = $this->options_model->getById($order->option_id);

                // не нашли --------------------
                if ( empty($option) )
                {
                    $status = STATUS_NOT_FOUND;
                    $message = "Ошибка: опция заказа неопределена";
                    $data = (object)[];

                    return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                }

                // статус отличаестя от active --------------------
                if ( $option->status != 'active' )
                {
                    $status = STATUS_OPER_NOT_ALLOWED;
                    $message = "Ошибка: оплачиваемая опция не активна";
                    $data = (object)[];

                    return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                }

                // у опции есть duration
                if (!empty($option->duration))
                {
                    $duration = intval($option->duration);
                    $dt = new DateTime('now');
                    $data['start_dt'] = $dt->format('Y-m-d H:i:s');

                    $dt->add(new DateInterval('P'.$duration.'D')); // добавляем $duration дней
                    $data['end_dt'] = $dt->format('Y-m-d H:i:s');
                }

            } //if ($tot_paid >= $order->price)
            else {
                $data['status'] = 'partially_paid';
            }

            $this->db->trans_begin();
            $upd_res = $this->model->update($order_id, $data);

            if ($upd_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = $upd_res;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** оплата заказа
     *
     *  @param   int id  - ID заказа. Берется из post
     *  @param   float $amount - сколько оплатить. Если параметр не задан, то произойдет доплата до полной цены
     *
     * @return  object - JSON-объект формата envelope
     */
    public function pay()
    {
        $order_id = intval($this->input->post('id'));
        $amount = floatval($this->input->post('amount'));

        if (empty($order_id))
        {
            //$order_id нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $order */
        $order = $this->model->getById($order_id);
        if (empty($order))
        {
            //$order_id нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (empty($amount)) $amount = ($order->price - $order->amount_paid); // если параметр $amount не задан, то произойдет доплата до полной цены

        /** @var object $client */
        $client = $this->clients_model->getById($order->client_id);
        // нет клиента - выходим с отказом
        if (empty($client))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Клиент не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $option */
        $option = $this->options_model->getById($order->option_id);
        // не нашли опцию - выходим с отказом
        if (empty($option))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Опция не найдена";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var object $product */
        $product = $this->products_model->getById($option->product_id);
        // не нашли продукт - выходим с отказом
        if (empty($product))
        {
            $status = STATUS_NOT_FOUND;
            $message = "Продукт не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = $this->model->isPaymentPossible($order, $option, $client, $amount);

        if ($status != STATUS_TRANS_OK)
        {
            $message = "Оплата невозможна. Код:".$status;
            $status = STATUS_OPER_NOT_ALLOWED;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = $this->model->pay($order, $option, $product, $client, $amount);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** Выборка всех заказов клиента с ипользованием промокодов
     *  Метод нужен для построения списка "какие промокоды использовал клиент"
     *
     * @param int $client_id - ID клиента, для которого производится выборка
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientIdWithPromocodesUsed()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getByClientIdPromocodeId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }


    /** Выборка всех заказов клиента с ипользованием промокода promocode_id
     *
     * @param int $client_id - ID клиента, для которого производится выборка
     * @param int|null $promocode_id - ID промокода, для которого производится выборка
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientIdPromocodeId()
    {
        $client_id = $this->input->post('client_id');
        if (empty($client_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode_id = $this->input->post('promocode_id');
        if (empty($promocode_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getByClientIdPromocodeId($client_id, $promocode_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

}
