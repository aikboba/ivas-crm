<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Orders_tags
 * @property Orders_model $orders_model
 * @property Tags_model  $tags_model
 * @property Orders_tags_model $orders_tags_model
 *
 */
class Orders_tags extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('orders_model');
        $this->load->model('tags_model');
        $this->load->model('orders_tags_model');

        $this->model = $this->orders_tags_model;
    }

    /** получает данные - запись orders_tags
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $orders_tags_id = $this->input->post('id');
        if (empty($orders_tags_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $orders_tags = $this->model->getById($orders_tags_id);

        if (empty($orders_tags))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $orders_tags;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список тегов по order_id
     *
     * @param  int $order_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getTagsListByOrderId()
    {
        $order_id = $this->input->post('order_id');
        if (empty($order_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getTagsByOrderId($order_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список админов указанной админгруппы (tag_id)
     *
     * @param  int $tag_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getOrdersListByTagId()
    {
        $tag_id = $this->input->post('tag_id');

        if (empty($tag_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getOrdersByTagId($tag_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** удаляет запись по ее ID
     *
     *  @param   int id  - ID orders_tags.id. Берется из post
     *
     * @return  object - JSON-объект формата envelope.
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->delete($id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** удаляет админа order_id из группы tag_id
     *
     *  @param   int tag_id   - ID тега. Берется из post
     *  @param   int order_id  - ID заказа. Берется из post
     *
     * @return  object - JSON-объект формата envelope.
     */
    public function removeTagFromOrder()
    {
        $order_id = $this->input->post('order_id');
        if (empty($order_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $tag_id = $this->input->post('tag_id');
        if (empty($tag_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeTagFromOrder($tag_id, $order_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** создает запись - добавляет тег tag_id в order_id
     *
     *  @param   int order_id  - ID заказа. Берется из post
     *  @param   int tag_id   - ID тега. Берется из post
     *
     * @return  object - JSON-объект формата envelope.
     */
    public function create()
    {
        $order_id = $this->input->post('order_id');
        if (empty($order_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $tag_id = $this->input->post('tag_id');
        if (empty($tag_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $new_id = $this->model->addTagToOrder($order_id, $tag_id);

        if ( ($new_id!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();

            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }


        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID orders_tags. Берется из post
     *  @param   int order_id  - ID заказа. Берется из post
     *  @param   int tag_id  - ID тега. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $orders_tags_id = intval($this->input->post('id'));
        $tag_id = intval($this->input->post('tag_id'));
        $order_id = intval($this->input->post('order_id'));

        if (empty($orders_tags_id))
        {
            //нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_TOKEN_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $orders_tags */
        $orders_tags = $this->model->getById($orders_tags_id);

        if (!empty($orders_tags))
        {
            $data = [
                'order_id' => $order_id,
                'tag_id' => $tag_id,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($orders_tags_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($orders_tags_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }


            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение тегов для заказа
     *
     *  @param   int order_id  - ID заказа. Берется из post
     *  @param   string ordertags  - json_encoded массив вида $ordertags = [<tag_id1>, ... ,<tag_idN>];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function saveTags()
    {
        $order_id = $this->input->post('order_id');
        if (empty($order_id))
        {
            //order_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->model->clearTags($order_id);

        $ordertags = $this->input->post('ordertags');

        if (!empty($ordertags))
        {
            $ordertags = json_decode($ordertags);

            foreach ($ordertags as $key => $tag_id)
            {
                $this->model->addTagToOrder($order_id, $tag_id);
            }

        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
