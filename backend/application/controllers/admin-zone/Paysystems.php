<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Paysystems
 *
 * @param Paysystems_model $paysystems_model
 */
class Paysystems extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('paysystems_model');
        $this->model = $this->paysystems_model;
    }

    public function index()
    {
/*
        $res = $this->model->getById(1);
echo "<pre>";
print_r($res);
echo "***";
echo "</pre>";
die();
*/
    }

    /** получает данные о платежной системе
     *
     *  @param   int id  - ID записи. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $paysystem_id = $this->input->post('id');
        if (empty($paysystem_id))
        {
            //paysystem_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $paysystem = $this->model->getById($paysystem_id);
        if (empty($paysystem))
        {
            //paysystem_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $paysystem;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список акков paysystems
     *
     * @param string $updated_since - datetime-строка. если задана, то возвращаются только элементы, измененные после этого момента
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание акка
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'public_key' => $this->input->post('public_key'),
            'api_secret_key' => $this->input->post('api_secret_key'),
            'additional_data' => $this->input->post('additional_data'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса акка
     *
     *  @param   int id  - ID акка. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $paysystem_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($paysystem_id))
        {
            //paysystem_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $paysystem = $this->model->getById($paysystem_id);
        if (empty($paysystem))
        {
            //paysystem нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($paysystem))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($paysystem_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($paysystem_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

    }


    /** изменение акка
     *
     *  @param   int id  - ID платежного акка. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $paysystem_id = $this->input->post('id');

        if (empty($paysystem_id))
        {
            //$paysystem_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $paysystem = $this->model->getById($paysystem_id);
        if (empty($paysystem))
        {
            //$paysystem_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if (!empty($paysystem))
        {
            // найден - изменяем - можно вооще сказать
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'name' => $this->input->post('name'),
                'public_key' => $this->input->post('public_key'),
                'api_secret_key' => $this->input->post('api_secret_key'),
                'additional_data' => $this->input->post('additional_data'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($paysystem_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($paysystem_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

    }

    /** удаление акка
     *
     *  @param   int id  - ID акка. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $paysystem_id = $this->input->post('id');
        if (empty($paysystem_id))
        {
            //$paysystem_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($paysystem_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


}
