<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Presets
 * @property Presets_model $presets_model
 * @property Presets_items_model $presets_items_model
 */
class Presets extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('presets_model');
        $this->model = $this->presets_model;
        $this->load->model('presets_items_model');
    }

    public function index()
    {

    }

    /** получает данные о пресете
     *
     *  @param   int id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $preset_id = $this->input->post('id');
        if (empty($preset_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        /** @var StdClass $preset */
        $preset = $this->model->getById($preset_id);
        if (empty($preset))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        //собираем items -----------------------------
        $items = $this->presets_items_model->getItemsByPresetId($preset->id);
        $preset->items = $items;
        //END собираем items -----------------------------

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $preset;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список тринингов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** Список по категории пресета
     *
     * @param int $presets_cat_id
     *
     * @return array of objects
     */
    public function getListByCategoryId()
    {
        $presets_cat_id = $this->input->post('presets_cat_id');

        $list = $this->model->getByCategoryId($presets_cat_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание пресета
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'preset_type' => $this->input->post('preset_type'),
            'description' => $this->input->post('description'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса
     *
     *  @param   int id  - ID пресета. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $preset_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($preset_id))
        {
            //preset_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset = $this->model->getById($preset_id);

        if (!empty($preset))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($preset_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($preset_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // preset не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение пресета
     *
     *  @param   int id  - ID пресета. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $preset_id = $this->input->post('id');

        if (empty($preset_id))
        {
            //$preset_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $preset = $this->model->getById($preset_id);

        if (!empty($preset))
        {
            // найден - изменяем - можно вооще сказать
            $data = [
                'name' => $this->input->post('name'),
                'preset_type' => $this->input->post('preset_type'),
                'description' => $this->input->post('description'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($preset_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($preset_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // preset не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление пресета
     *
     *  @param   int id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $preset_id = $this->input->post('id');
        if (empty($preset_id))
        {
            //preset_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($preset_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}
