<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Presets_cats
 * @property Presets_cats_model $presets_cats_model
 */
class Presets_cats extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('presets_cats_model');
        $this->model = $this->presets_cats_model;
    }

    public function index()
    {
    }

    /** получает данные о категориях пресетов
     *
     *  @param   int id  - ID категории. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $presets_cat_id = $this->input->post('id');
        if (empty($presets_cat_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $presets_cat = $this->model->getById($presets_cat_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $presets_cat;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список категорий пресетов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список ПРЯМЫХ потомков указанной категории пресетов
     *
     * @param   int id  - ID категории. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getChildrenList()
    {
        $id = $this->input->post('id');
        $list = $this->model->getByParentId($id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает полный список (левосторонний обход дерева)
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getTreeList()
    {
        $res =[];
        $this->model->getDescendantsListById(0,$res);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $res) );
    }

    /**
     *  Служит для рекурсивного построения списка вида [
     *                                      "categoryID_1" => "categoryName_1"
     *                                      ...
     *                                      "categoryID_n" => "categoryName_n"
     *                                    ]
     * @param int $this_id - ID 'текущей' категории
     * @param string $div - 'разделитель', служащий для визуального отделения уровней друг от друга
     * @param array &$res - массив, принимающий результат работы
     *
     * @return array | void
     */
    private function getXXX($this_id, $basename, &$res)
    {
        $children = $this->model->getByParentId($this_id);

        if (empty($children)) return;

        foreach ($children as $child)
        {
            //echo $basename.$child->name."<br>";
            //$res[] = [$child->id => $basename.$child->name];
            $res[$child->id] = $basename.$child->name;
            //$this->getXXX($child->id, $basename.$child->name." / ", $res);

            $this->getXXX($child->id, "--".$basename/*.$child->name." / "*/, $res);
        }
    }

    /**
     *  Строит иерархический список категорий пресетов , пригодный для использования при формировании выдающего списка
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getDropdownList()
    {
        $res = [];
        //return $this->getXXX(0,'',$res);
        $this->getXXX(0,'',$res);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $res) );
    }

    /** создание presets_cat
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'parent_id' => $this->input->post('parent_id'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение категории пресетов
     *
     *  @param   int id  - ID категории. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $presets_cat_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($presets_cat_id))
        {
            //presets_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $presets_cat = $this->model->getById($presets_cat_id);

        if (!empty($presets_cat))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            //TODOO: по идее, при изменении статуса родителя тот же статус должны получать и все потомки
            $upd_res = $this->model->update($presets_cat_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($presets_cat_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // presets_cat не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение категории
     *
     *  @param   int id  - ID категории. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $presets_cat_id = $this->input->post('id');

        if (empty($presets_cat_id))
        {
            //$presets_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $presets_cat = $this->model->getById($presets_cat_id);

        if (!empty($presets_cat))
        {
            // найден - изменяем - можно вообще сказать
            $data = [
                'name' => $this->input->post('name'),
                'parent_id' => $this->input->post('parent_id'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            //TODOO: по идее, при изменении статуса родителя тот же статус должны получать и все потомки
            $upd_res = $this->model->update($presets_cat_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($presets_cat_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // presets_cat не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление категории
     *
     *  @param   int id  - ID категории. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $presets_cat_id = $this->input->post('id');
        if (empty($presets_cat_id))
        {
            //presets_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            //TODOO: по идее, при удалении (изменении статуса) родителя тот же статус должны получать и все потомки
            $del_res = $this->model->delete($presets_cat_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

}
