<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Presets_Items
 * @property Presets_model $presets_model
 * @property Items_model  $items_model
 * @property Presets_items_model $presets_items_model
 */
class Presets_Items extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('presets_model');
        $this->load->model('items_model');
        $this->load->model('presets_items_model');

        $this->model = $this->presets_items_model;
    }

    /** получает данные - запись presets_items
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $presets_items_id = $this->input->post('id');
        if (empty($presets_items_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $presets_items_id = $this->model->getById($presets_items_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $presets_items_id;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает items групп по preset_id
     *
     * @param  int $preset_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getItemsListByPresetId()
    {
        $preset_id = $this->input->post('preset_id');

        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getitemsBypresetId($preset_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список presets, в которых входит указанный item_id
     *
     * @param  int $item_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getPresetsListByitemId()
    {
        $item_id = $this->input->post('item_id');

        if (empty($item_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getPresetsByitemId($item_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи.
     *
     *  @param   int preset_id  - ID пресета. Берется из post
     *  @param   int item_id   - ID item. Берется из post
     *  @param   int pos   - позиция, в которую добавляется. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $item_id = $this->input->post('item_id');
        if (empty($item_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $pos = $this->input->post('pos');

        $data = [
            'preset_id' => $preset_id,
            'item_id' => $item_id,
            'pos' => $pos,
        ];

        $new_id = $this->model->insert($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет запись по ее ID
     *
     *  @param   int id  - ID presets_items.id. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admingroups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeItemFromPreset($record->item_id, $record->preset_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет item_id из пресета preset_id
     *  (в модели удаляет связку presets-items)
     *
     *  @param   int item_id   - ID item. Берется из post
     *  @param   int preset_id  - ID пресета. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removeItemFromPreset()
    public function removeLink()
    {
        $preset_id = $this->input->post('preset_id');
        if (empty($preset_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $item_id = $this->input->post('item_id');
        if (empty($item_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeItemFromPreset($item_id, $preset_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }
    
    /** изменение
     *
     *  @param   int id  - ID presets_items. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $presets_items_id = $this->input->post('id');
        $item_id = $this->input->post('item_id');
        $preset_id = $this->input->post('preset_id');
        $pos = $this->input->post('pos');

        if (empty($presets_items_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $presets_items */
        $presets_items = $this->model->getById($presets_items_id);

        if (!empty($presets_items))
        {
            $data = [
                'preset_id' => $preset_id,
                'item_id' => $item_id,
                'pos' => $pos,
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($presets_items_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($presets_items_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** сохранение items для preset
     *
     *  @param   int id  - ID пресета. Берется из post
     *  @param   string items  - json encoded array вида $items = [
     *                                                    <itemID_1> => <pos_1>,
     *                                                    .........
     *                                                    <itemID_n> => <pos_n>,
     *                                               ];
     *                                               pos может и не быть, но тогда передавать 0
     *
     * @return  object - JSON-объект формата envelope
     */
    public function saveItems()
    {
        $preset_id = $this->input->post('id');
        if (empty($preset_id))
        {
            //preset_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->model->clearPreset($preset_id);

        $presetitems = $this->input->post('items');
        if (!empty($presetitems))
        {
            $presetitems = json_decode($presetitems);

            foreach ($presetitems as $item_id => $pos)
            {
                $this->model->addItemToPreset($item_id, $preset_id, $pos);
            }

        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
