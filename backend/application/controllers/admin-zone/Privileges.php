<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Privileges
 * @property Privileges_model $privileges_model
 */
class Privileges extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('privileges_model');
        $this->model = $this->privileges_model;
    }

    public function index()
    {
    }

    /** получает данные о группе привилегий
     *
     *  @param   int id  - ID группы. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $priv_id = $this->input->post('id');
        if (empty($priv_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $priv = $this->model->getById($priv_id);
        if (empty($priv))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $priv;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список групп
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание группы привилегий (льготы)
     *
     *  @param  array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'discount_type' => $this->input->post('discount_type'),
            'discount' => $this->input->post('discount'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса группы
     *
     *  @param   int id  - ID группы. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $priv_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($priv_id))
        {
            //priv_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $priv = $this->model->getById($priv_id);
        if (empty($priv))
        {
            //priv нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем status
        $data = [
            'status' => $new_status
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($priv_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($priv_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение льготы
     *
     *  @param   int id  - ID льготы. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $priv_id = $this->input->post('id');

        if (empty($priv_id))
        {
            //$priv_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $priv = $this->model->getById($priv_id);
        if (empty($priv))
        {
            //$priv_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'discount_type' => $this->input->post('discount_type'),
            'discount' => $this->input->post('discount'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($priv_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($priv_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаление льготы
     *
     *  @param   int id  - ID льготы. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $priv_id = $this->input->post('id');
        if (empty($priv_id))
        {
            //priv_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();

        $del_res = $this->model->delete($priv_id);

        if ($del_res)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
