<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Products
 * @property Products_model $products_model
 */
class Products extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
        $this->model = $this->products_model;
    }

    public function index()
    {

    }

    /** получает данные о продукте
     *
     *  @param   int id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $product_id = $this->input->post('id');
        if (empty($product_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $product;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список продуктов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {

        $list = $this->model->getList();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список бесплатных продуктов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListFree()
    {
        $list = $this->model->getFree();
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список продуктов указанного типа
     *
     * @param string $type
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByType()
    {
        $type = $this->input->post('type');
        $list = $this->model->getListByType($type);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список продуктов для указанного клиента
     *
     * @param int $client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientId()
    {
        $client_id = $this->input->post('client_id');
        $list = $this->model->getListByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание продукта
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'status' => $this->input->post('status'),
            'product_type' => $this->input->post('product_type'),
        ];

        $paysystem_id = $this->input->post('paysystem_id');
        if (!empty($paysystem_id)) $data['paysystem_id'] = $paysystem_id;

        $instagram_account_id = $this->input->post('instagram_account_id');
        if (!empty($instagram_account_id)) $data['instagram_account_id'] = $instagram_account_id;

        $client_id = $this->input->post('client_id');
        if (!empty($client_id)) $data['client_id'] = $client_id;

        $group_id = $this->input->post('group_id');
        if (!empty($group_id)) $data['group_id'] = $group_id;

        $preset_id = $this->input->post('preset_id');
        if (!empty($preset_id)) $data['preset_id'] = $preset_id;

        $is_free = $this->input->post('is_free');
        $data['is_free'] = intval(boolval($is_free));

        $days_available_after_stop = $this->input->post('days_available_after_stop');
        if (isset($days_available_after_stop)) $data['days_available_after_stop'] = intval($days_available_after_stop);

        $days_item_available = $this->input->post('days_item_available');
        if (isset($days_item_available)) $data['days_item_available'] = intval($days_item_available);

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $new_product = $data = $this->model->getById($new_id);

            // для продуктов-эфиров автоматически создаем пресет -----------
            if ($new_product->product_type == 'subscription')
            {
                $this->load->model('presets_model');
                $presData = [
                    'name' => $new_product->name." - эфиры",
                    'preset_type' => "playlist",
                    'status' => "active",
                ];
                $new_pres_id = $this->presets_model->insertOrUpdate($presData);

                if ($new_pres_id!==false)
                {
                    $new_preset = $this->presets_model->getById($new_pres_id);
                }
                // добавляем этот пресет в продукт
                $prodData = ['preset_id' => $new_preset->id];
                $this->model->update($new_product->id, $prodData);
            // END для продуктов-эфиров автоматически создаем пресет -----
            }
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса продукта
     *
     *  @param   int id  - ID продукта. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $product_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data =[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем status
        $data = [
            'status' => $new_status
        ];

        $this->db->trans_begin();
        $upd_res = $this->model->update($product_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($product_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** останавливает продукт (речь идет о прекращении трансляций эфирного продукта)
     * при остановке эфирного продукта не только меняется его статус, но начинается отсчет срока
     * доступности пользователям материалов связанного пресета (для сlients_presets выставляется expiration_dt
     * как NOW()+products.days_available_after_stop )
     *
     * @param   int id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function stop()
    {
        $product_id = $this->input->post('id');

        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        if ($product->status != "active")
        {
            //не тот статус
            $status = STATUS_OPER_NOT_ALLOWED;
            $message = STATUS_OPER_NOT_ALLOWED_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // найден - изменяем status
        $this->db->trans_begin();
        $res = $this->model->stop($product);

        if ($res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** устанавливает стрим по продукту и прочий обвес
     *
     *  @param   int id  - ID продукта. Берется из post
     *  @param   string $stream_origin
     *  @param   string $stream_url_data
     *  @param   string $stream_host_relay_url
     //*  @param   string $stream_guest_relay_url
     *  @param   string $stream_status
     *  @param   string $stream_item_guid
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStream()
    {
        $product_id = $this->input->post('id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $stream_status = $this->input->post('stream_status');
        if (empty($stream_status))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = "Ошибка: stream_status не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $stream_url_data = $this->input->post('stream_url_data');
        if (empty($stream_url_data))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = "Ошибка: stream_url_data не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $stream_host_relay_url = $this->input->post('stream_host_relay_url');
        //$stream_guest_relay_url = $this->input->post('stream_guest_relay_url');

        $stream_origin = $this->input->post('stream_origin');
        if (empty($stream_origin))
        {
            //product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = "Ошибка: stream_origin не найден";
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->model->getById($product_id);
        if (empty($product))
        {
            //product_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $stream_item_guid = $this->input->post('stream_item_guid');
        $uData = [
            'stream_status' => $stream_status,
            'stream_url_data' => $stream_url_data,
            'stream_host_relay_url' => (!empty($stream_host_relay_url)) ? $stream_host_relay_url : 'NULL',
            //'stream_guest_relay_url' => (!empty($stream_guest_relay_url)) ? $stream_guest_relay_url : 'NULL',
            'stream_origin' => $stream_origin,
            'stream_item_guid' => $stream_item_guid
        ];

        // найден - изменяем status
        $this->db->trans_begin();

        $upd_res = $this->model->setStream($product_id, $uData);
        //$upd_res = $this->model->update($product_id, $uData);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


    /** изменение продукта
     *
     *  @param   int id  - ID продукта. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $product_id = $this->input->post('id');

        if (empty($product_id))
        {
            //$product_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->model->getById($product_id);

        if (empty($product))
        {
            // product не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
        }
        // найден - изменяем
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'status' => $this->input->post('status'),
            'product_type' => $this->input->post('product_type'),
        ];

        $paysystem_id = $this->input->post('paysystem_id');
        $data['paysystem_id'] = !empty($paysystem_id) ? $paysystem_id : 'NULL';

        $instagram_account_id = $this->input->post('instagram_account_id');
        $data['instagram_account_id'] = !empty($instagram_account_id) ? $instagram_account_id : 'NULL';

        $client_id = $this->input->post('client_id');
        $data['client_id'] = !empty($client_id) ? $client_id : 'NULL';

        $group_id = $this->input->post('group_id');
        $data['group_id'] = !empty($group_id) ? $group_id : 'NULL';

        $preset_id = $this->input->post('preset_id');
        $data['preset_id'] = !empty($preset_id) ? $preset_id : 'NULL';

        $is_free = $this->input->post('is_free');
        $data['is_free'] = intval(boolval($is_free));

        $days_available_after_stop = $this->input->post('days_available_after_stop');
        if (isset($days_available_after_stop)) $data['days_available_after_stop'] = intval($days_available_after_stop);

        $days_item_available = $this->input->post('days_item_available');
        if (isset($days_item_available)) $data['days_item_available'] = intval($days_item_available);

        $stream_status = $this->input->post('stream_status');
        $data['stream_status'] = $stream_status;

        $stream_origin = $this->input->post('stream_origin');
        $data['stream_origin'] = $stream_origin;

        $stream_url_data = $this->input->post('stream_url_data');
        $data['stream_url_data'] = $stream_url_data;

        $stream_host_relay_url = $this->input->post('stream_host_relay_url');
        $data['stream_host_relay_url'] = $stream_host_relay_url;

        $stream_started = $this->input->post('stream_started');
        $data['stream_started'] = $stream_started;

        $stream_ended = $this->input->post('stream_ended');
        $data['stream_ended'] = $stream_ended;

        $this->db->trans_begin();
        $upd_res = $this->model->update($product_id, $data);

        if ($upd_res!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($product_id);
        }
        else
        {
            $status = STATUS_UPDATE_FAIL;
            $message = STATUS_UPDATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаление продукта
     *
     *  @param   int id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $product_id = $this->input->post('id');
        if (empty($product_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($product_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }



}
