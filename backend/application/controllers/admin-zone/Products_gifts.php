<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Products_gifts
 * @property Products_model  $products_model
 * @property Gifts_model $gifts_model
 * @property Products_gifts_model $products_gifts_model
 */
class Products_gifts extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('gifts_model');
        $this->load->model('products_model');
        $this->load->model('products_gifts_model');

        $this->model = $this->products_gifts_model;
    }

    /** получает данные - запись products_gifts
     *
     *  @param   int id  - ID. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $products_gifts_id = $this->input->post('id');
        if (empty($products_gifts_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $products_gifts_id = $this->model->getById($products_gifts_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $products_gifts_id;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает gifts по по product_id
     *
     * @param  int $product_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getGiftsListByProductId()
    {
        $product_id = $this->input->post('product_id');

        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getGiftsByProductId($product_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список products, в которых входит указанный gift_id
     *
     * @param  int $gift_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getProductsListByGiftId()
    {
        $gift_id = $this->input->post('gift_id');

        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $list = $this->model->getProductsByGiftId($gift_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание записи.
     *
     *  @param   int product_id   - ID product. Берется из post
     *  @param   int gift_id  - ID подарка. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $gift_id = $this->input->post('gift_id');
        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $data = [
            'product_id' => $product_id,
            'gift_id' => $gift_id
        ];

        $new_id = $this->model->insert($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет запись по ее ID
     *
     *  @param   int id  - ID products_gifts.id. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит admingroups_presets_id
     */
    public function delete()
    {
        $id = $this->input->post('id');
        if (empty($id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $record = $this->model->getById($id);
        if (empty($record))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeGiftFromProduct($record->gift_id, $record->product_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** удаляет gift_id из продукта product_id
     *  (в модели удаляет связку presets-items)
     *
     *  @param   int gift_id   - ID подарка. Берется из post
     *  @param   int product_id  - ID продукта. Берется из post
     *
     * @return  object - JSON-объект формата envelope. Data содержит groups_presets_id
     */
//    public function removeGiftFromProduct()
    public function removeLink()
    {
        $product_id = $this->input->post('product_id');
        if (empty($product_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $gift_id = $this->input->post('gift_id');
        if (empty($gift_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->db->trans_begin();
        $res = $this->model->removeGiftFromProduct($gift_id, $product_id);

        if ( ($res!==false) && ($this->db->trans_status() === TRUE) )
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_DEL_FAIL;
            $message = STATUS_DEL_FAIL_MSG;
            $this->db->trans_rollback();
        }
        $data = (object)[];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение
     *
     *  @param   int id  - ID products_gifts. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $products_gifts_id = $this->input->post('id');
        $product_id = $this->input->post('product_id');
        $gift_id = $this->input->post('gift_id');

        if (empty($products_gifts_id))
        {
            //нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        /** @var StdClass $products_gifts */
        $products_gifts = $this->model->getById($products_gifts_id);

        if (!empty($products_gifts))
        {
            $data = [
                'product_id' => $product_id,
                'gift_id' => $gift_id
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($products_gifts_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($products_gifts_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // для update не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** массовое добавление подарков в product
     *
     *  @param   int id  - ID продукта. Берется из post
     *  @param   array gifts  - массив вида $gifts = [<giftID_1> ... <giftID_n>];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function saveGifts()
    {
        $product_id = $this->input->post('id');
        if (empty($product_id))
        {
            //product_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $productgifts = $this->input->post('gifts');
        foreach ($productgifts as $gift_id)
        {
            $this->model->addGiftToProduct($gift_id, $product_id);
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = [];

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
