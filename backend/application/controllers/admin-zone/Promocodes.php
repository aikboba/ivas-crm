<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class promocodes
 * @property Promocodes_model $promocodes_model
 * @property Options_model $options_model
 * @property Clients_model $clients_model
 */
class Promocodes extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('promocodes_model');
        $this->model = $this->promocodes_model;
    }

    public function index()
    {

    }

    /** получает данные о промокоде
     *
     *  @param   int id  - ID промокода. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $promocode_id = $this->input->post('id');
        if (empty($promocode_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $promocode = $this->model->getById($promocode_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $promocode;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список промокодов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список промокодов указанного типа
     *
     * @param string $discount_type
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByDiscountType()
    {
        $discount_type = $this->input->post('discount_type');
        $list = $this->model->getByDiscountType($discount_type);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список промокодов для указанного клиента
     *
     * @param int $client_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByClientId()
    {
        $client_id = $this->input->post('client_id');
        $list = $this->model->getByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список промокодов по указанному product_id
     *
     * @param int $product_id
     * @param boolean $whole_product_only - если true, вернет только промокоды, относящиеся КО ВСЕМУ продукту
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByProductId()
    {
        $product_id = $this->input->post('product_id');
        $whole_product_only = $this->input->post('whole_product_only', false);
        $list = $this->model->getByProductId($product_id, $whole_product_only);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** получает список промокодов по option_id
     *
     * @param int $option_id
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getListByOptionId()
    {
        $client_id = $this->input->post('option_id');
        $list = $this->model->getByOptionId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание промокода
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'promocode' => $this->input->post('promocode'),
            'discount_type' => $this->input->post('discount_type'),
            'discount_value' => $this->input->post('discount_value'),
            'one_time_only' => $this->input->post('one_time_only'),
            'status' => $this->input->post('status'),
        ];

        $product_id = $this->input->post('product_id');
        if (!empty($product_id)) $data['product_id'] = $product_id;

        $option_id = $this->input->post('option_id');
        if (!empty($option_id)) $data['option_id'] = $option_id;

        $client_id = $this->input->post('client_id');
        if (!empty($client_id)) $data['client_id'] = $client_id;

        $start_dt = $this->input->post('start_dt');
        if (!empty($start_dt)) $data['start_dt'] = $start_dt;

        $end_dt = $this->input->post('end_dt');
        if (!empty($end_dt)) $data['end_dt'] = $end_dt;

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса
     *
     *  @param   int id  - ID промокода. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $promocode_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($promocode_id))
        {
            //promocode_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode = $this->model->getById($promocode_id);

        if (!empty($promocode))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($promocode_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // promocode не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение промокода
     *
     *  @param   int id  - ID промокода. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $promocode_id = $this->input->post('id');

        if (empty($promocode_id))
        {
            //$promocode_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $promocode = $this->model->getById($promocode_id);

        if (!empty($promocode))
        {
            // найден - изменяем - можно вооще сказать
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'name' => $this->input->post('name'),
                'promocode' => $this->input->post('promocode'),
                'discount_type' => $this->input->post('discount_type'),
                'discount_value' => $this->input->post('discount_value'),
                'one_time_only' => $this->input->post('one_time_only'),
                'status' => $this->input->post('status'),
            ];

            $product_id = $this->input->post('product_id');
            $data['product_id'] = (!empty($product_id)) ? $product_id : 'NULL';

            $option_id = $this->input->post('option_id');
            $data['option_id'] = (!empty($option_id)) ? $option_id : 'NULL';

            $client_id = $this->input->post('client_id');
            $data['client_id'] = (!empty($client_id)) ? $client_id : 'NULL';

            $start_dt = $this->input->post('start_dt');
            $data['start_dt'] = (!empty($start_dt)) ? $start_dt : 'NULL';

            $end_dt = $this->input->post('end_dt');
            $data['end_dt'] = (!empty($end_dt)) ? $end_dt : 'NULL';

            $this->db->trans_begin();
            $upd_res = $this->model->update($promocode_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($promocode_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }


            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // promocode не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление промокода
     *
     *  @param   int id  - ID промокода. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $promocode_id = $this->input->post('id');
        if (empty($promocode_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($promocode_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** Определяет, может ли быть применен указанный промокод указанным клиентом к указанному продукту/опции
     * (промокод применим, если:
     *  - текущая дата лежит между start_dt и end_dt (при запросе с фронта реализуется моделью)
     *  - он связан с клиентом + с опцией или продуктом
     *  - он связан с продуктом
     *  - он связан с опцией
     *  - если promocodу one_time_only, то он не должен фигурировать ни в одном заказе (если передан client, то ни в одном заказе КЛИЕНТА)
     * )
     *
     * @param string $promocode - сам промокод (строка)
     * @param int $option_id - option_id опции
     * @param int|null $client_id - ID клиента, для которого выполняется проерка. Необходимо для вычисления применимости персональных промокодов
     *
     * @return int
     */
    public function isApplicable()
    {
        $client = $option = null;

        $promocode = $this->input->post('promocode');
        if (empty($promocode))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $origin = $this->caller_origin;
        $this->caller_origin = 'front'; // применимость определяется в том числе и датами. имитируем запрос от фронта
        $promocode = $this->model->getByPromocode($promocode);
        $this->caller_origin = $origin;
        if (empty($promocode))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $option_id = $this->input->post('option_id');
        if (empty($option_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->load->model('options_model');
        $option = $this->options_model->getById($option_id);
        if (empty($option))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = 0;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $client_id = intval($this->input->post_get('client_id'));
        if (!empty($client_id))
        {
            $client = $this->clients_model->getById($client_id);
            if (empty($client))
            {
                $status = STATUS_NOT_FOUND;
                $message = STATUS_NOT_FOUND_MSG;
                $data = 0;

                return $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
            }
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->isApplicable($promocode, $option, $client );

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

}
