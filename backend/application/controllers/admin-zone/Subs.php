<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Subs
 * @property trans_model $trans_model
 * @property Cloudpayments $cloudpayments
 *
 */
class Subs extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('subs_model');
        $this->model = $this->subs_model;
    }

    public function index()
    {

    }

    /** получает данные о подписке по внутреннему ID
     *
     *  @param   int id  - ID подписки. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $subs_id = $this->input->post('id');
        if (empty($subs_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $subs = $this->model->getById($subs_id);
        if (empty($subs))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $subs;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список подписок по client_id
     *
     *  @param   int client_id  - ID клиента подписки. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $client_id = $this->input->post('client_id');
        if ($client_id)
        {
            $params[] = ['client_id', '=', $client_id];
        }

        $list = $this->model->getByClientId($client_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** отменяет подписку с указанным id
     *
     *  @param int id  - ID подписки. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function cancel()
    {
        $subs_id = $this->input->post('id');
        if (empty($subs_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $subs = $this->model->getById($subs_id);
        if (empty($subs))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $product = $this->products_model->getById($subs->product_id);
        if (!$product) {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $subs_string_id = $subs->subs_id;

        $this->load->library('cloudpayments');
        $this->cloudpayments->publicKey = $product->paysystem_public_key;
        $this->cloudpayments->privateKey = $product->paysystem_api_secret_key;
        $res = $this->cloudpayments->cancelSubscription($subs_string_id);

        if(!$res)
        {
            $status = STATUS_FAIL;
            $message = STATUS_FAIL_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $this->model->update($subs->id, ['status'=>'cancelled']);

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $this->model->getById($subs->id);

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }


}
