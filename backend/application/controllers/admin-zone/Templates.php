<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Templates
 * @property Templates_cats_model $templates_cats_model
 * @property Templates_model $templates_model
 */
class Templates extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('templates_model');
        $this->load->model('templates_cats_model');

        $this->model = $this->templates_model;
    }

    /** получает данные о темплейте
     *
     *  @param   int id  - ID опции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $template_id = $this->input->post('id');
        if (empty($template_id))
        {
            //admin_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $template = $this->model->getById($template_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $template;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список темплейтов по категории
     *
     * @param  int $templates_cat_id - берется из $_POST
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $templates_cat_id = $this->input->post('templates_cat_id');
        if (empty($templates_cat_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = [];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $params = [];
        $params[] = ['templates_cat_id', "=", $templates_cat_id];

        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** Список по категории темплейта
     *
     * @param int $templates_cat_id
     *
     * @return array of objects
     */
    public function getListByCategoryId()
    {
        $templates_cat_id = $this->input->post('templates_cat_id');

        $list = $this->model->getByCategoryId($templates_cat_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание темплейта
     *
     *  @param   int templates_cat_id  - ID продукта. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $templates_cat_id = $this->input->post('templates_cat_id');
        if (empty($templates_cat_id))
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        $data = [
            'templates_cat_id' => $templates_cat_id,
            'name' => $this->input->post('name'),
            'text' => $this->input->post('text'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение статуса темплейта
     *
     *  @param   int id  - ID темплейта. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $template_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($template_id))
        {
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $template = $this->model->getById($template_id);

        if (!empty($template))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($template_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // template не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** изменение темплейта
     *
     *  @param   int id  - ID темплейта. Берется из post
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $template_id = $this->input->post('id');

        if (empty($template_id))
        {
            //$template_id нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $template = $this->model->getById($template_id);

        if (!empty($template))
        {
            // найден - изменяем
            // $data = $this->input->post(); - лишнее отбросит ни этапе filterFields
            $data = [
                'templates_cat_id' => $this->input->post('templates_cat_id'),
                'name' => $this->input->post('name'),
                'text' => $this->input->post('text'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($template_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($template_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // template для update не найдена
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление темплейта (через выставление статуса deleted)
     *
     *  @param   int id  - ID темплейта. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $template_id = $this->input->post('id');
        if (empty($template_id))
        {
            //template_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($template_id);

            if ($del_res)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }



}
