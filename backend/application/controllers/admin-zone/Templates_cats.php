<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Templates_cats
 * @property Templates_cats_model $templates_cats_model
 */
class Templates_cats extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('templates_cats_model');
        $this->model = $this->templates_cats_model;
    }

    public function index()
    {

    }

    /** получает данные о категориях темплекйтов
     *
     *  @param   int id  - ID категории. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $templates_cat_id = $this->input->post('id');
        if (empty($templates_cat_id))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $templates_cat = $this->model->getById($templates_cat_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $templates_cat;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список категорий темплейтов
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $updated_since = $this->input->post('updated_since');
        if ($updated_since)
        {
            $params[] = ['updated', '>', $updated_since];
        }
        $list = $this->model->getList($params);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

    /** создание templates_cat
     *
     *  @param   array $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function create()
    {
        $data = [
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
        ];

        $this->db->trans_begin();
        $new_id = $this->model->insertOrUpdate($data);

        if ($new_id!==false)
        {
            $status = STATUS_OK;
            $message = STATUS_OK_MSG;
            $this->db->trans_commit();
            $data = $this->model->getById($new_id);
        }
        else
        {
            $status = STATUS_CREATE_FAIL;
            $message = STATUS_CREATE_FAIL_MSG;
            $this->db->trans_rollback();
            $data = (object)[];
        }

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** изменение категории темплейтов
     *
     *  @param   int id  - ID категории. Берется из post
     *  @param   string $_POST['status'];
     *
     * @return  object - JSON-объект формата envelope
     */
    public function setStatus()
    {
        $templates_cat_id = $this->input->post('id');
        $new_status = $this->input->post('status');

        if (empty($templates_cat_id))
        {
            //templates_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $templates_cat = $this->model->getById($templates_cat_id);

        if (!empty($templates_cat))
        {
            // найден - изменяем status
            $data = [
                'status' => $new_status
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($templates_cat_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // templates_cat не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }


    /** изменение категории
     *
     *  @param   int id  - ID категории. Берется из post
     *  @param   string $_POST;
     *
     * @return  object - JSON-объект формата envelope
     */
    public function update()
    {
        $templates_cat_id = $this->input->post('id');

        if (empty($templates_cat_id))
        {
            //$templates_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $templates_cat = $this->model->getById($templates_cat_id);

        if (!empty($templates_cat))
        {
            // найден - изменяем - можно вообще сказать
            $data = [
                'name' => $this->input->post('name'),
                'status' => $this->input->post('status'),
            ];

            $this->db->trans_begin();
            $upd_res = $this->model->update($templates_cat_id, $data);

            if ($upd_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
                $data = $this->model->getById($templates_cat_id);
            }
            else
            {
                $status = STATUS_UPDATE_FAIL;
                $message = STATUS_UPDATE_FAIL_MSG;
                $this->db->trans_rollback();
                $data = (object)[];
            }

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            // templates_cat не найден
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }

    /** удаление категории
     *
     *  @param   int id  - ID категории. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function delete()
    {
        $templates_cat_id = $this->input->post('id');
        if (empty($templates_cat_id))
        {
            //templates_cat_id нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
        else
        {
            $this->db->trans_begin();

            $del_res = $this->model->delete($templates_cat_id);

            if ($del_res!==false)
            {
                $status = STATUS_OK;
                $message = STATUS_OK_MSG;
                $this->db->trans_commit();
            }
            else
            {
                $status = STATUS_DEL_FAIL;
                $message = STATUS_DEL_FAIL_MSG;
                $this->db->trans_rollback();
            }
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }
    }



}
