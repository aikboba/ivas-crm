<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Trans
 * @property trans_model $trans_model
 */
class Trans extends Base_Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('trans_model');
        $this->model = $this->trans_model;
    }

    public function index()
    {

    }

    /** получает данные о транзакции по внутреннему ID
     *
     *  @param   int id  - ID транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function get()
    {
        $trans_id = $this->input->post('id');
        if (empty($trans_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $trans = $this->model->getById($trans_id);
        if (empty($trans))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }


        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $trans;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает данные о транзакции по trans_id
     *
     *  @param   int trans_id  - trans_ID транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getByTransId()
    {
        $trans_id = $this->input->post('trans_id');
        if (empty($trans_id))
        {
            //нигде нет - выходим
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        // берем из БД
        $trans = $this->model->getByTransId($trans_id);
        if (empty($trans))
        {
            //нигде нет - выходим
            $status = STATUS_NOT_FOUND;
            $message = STATUS_NOT_FOUND_MSG;
            $data = (object)[];

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $trans;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /** получает список транзакций по client_id и order_id
     *
     *  @param   int client_id  - ID клиента транзакции. Берется из post
     *  @param   int order_id  - ID заказа транзакции. Берется из post
     *
     * @return  object - JSON-объект формата envelope
     */
    public function getList()
    {
        $params = [];
        $client_id = $this->input->post('client_id');
        if ($client_id)
        {
            $params[] = ['client_id', '=', $client_id];
        }

        $order_id = $this->input->post('order_id');
        if ($order_id)
        {
            $params[] = ['order_id', '=', $order_id];
        }
        $list = $this->model->getByClientIdOrderId($client_id, $order_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $list) );
    }

}