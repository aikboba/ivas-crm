<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * DUMMY WORKER!!! - имитирует работу транскодера по "выполнению задач"
 * Class Transcoders - служит для обработки задач (tasks) из очереди tasks. Зациклен в методе demonize().
 *
 * @param Tasks_model $tasks_model
 * @param Tasks_model $tasks_completed_model
 * @param Products_model $products_model
 * @param Instagram_accounts_model $instagram_accounts_model
 */
class Transcoders extends Base_Admin_Controller
{
    private $task = null;
    private $status_code = 0;
    private $status_message = "";
    private $stream_url_data;

    private $errors = [
         0 => "OK",
        10 => "Ошибка: недостаточно данных",
        20 => "Ошибка: объект не найден",
        30 => "Ошибка: неверный тип объекта",
        40 => "Ошибка: неверное состояние объекта",
        50 => "Ошибка: операция не удалась",
       666 => "Ошибка: демон не желает останавливаться",
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('tasks_model');
        $this->tasks_model->system = 'transcoder';

        $this->load->model('tasks_completed_model');

        $this->stream_url_data = [
            'hls' => [
                'HQ' => 'hls://high_quality.url',
                'LQ' => 'hls://low_quality.url',
            ],
            'dash' => [
                'HQ' => 'dash://high_quality.url',
                'LQ' => 'dash://low_quality.url',
            ],
        ];

    }

    public function index()
    {

    }

    /** Работает в вечном цикле выборки задач. Остановить процесс можно, создав task с consumer='stop'
     *
     * @return  object - JSON-объект формата envelope
     */
    public function demonize()
    {

        while (true)
        {
            $this->task = $this->tasks_model->getNew();

            $this->db->trans_begin();
                if (!empty($this->task)) $reserv = $this->tasks_model->in_process($this->task->id);
                else $reserv = 0;
            $this->db->trans_commit();

                if (!$reserv)
                {
//echo date("Y-m-d H:i:s\n");
                    usleep(2000000); // спим 2с
                }
                else
                {
$st=date("Y-m-d H:i:s")." --- ID=".$this->task->id;
                    if (!method_exists($this, $this->task->consumer))
                    {
                        // неверный исполнитель - метод-обработчик не существует
                        $this->tasks_model->error($this->task->id, "Ошибка: обработчик '".$this->task->consumer."' не найден");
$st.=" --- Error: consumer '".$this->task->consumer."' not found\n";
                    }
                    else
                    {
                        $this->task->decoded_data_in = json_decode($this->task->data_in);

                        if (json_last_error()!= JSON_ERROR_NONE)
                        {
                            // неверный json в задаче
                            $this->tasks_model->error($this->task->id, "Ошибка: проблема при декодировании данных JSON");
$st.=" --- Error: problem while JSON decoding\n";
                        }
                        else
                        {
                            // проверки пройдены - передаем управление исполнителю
                            $this->{$this->task->consumer}();

                            if ($this->status_code)
                            {
                                // завершение с ошибкой
                                $this->tasks_model->error($this->task, $this->status_message);
                            }
                            else
                            {
                                // успешное завершение
                                $this->tasks_model->complete($this->task, $this->status_message);
                            }

//$st.=" --- ".$this->errors[$this->status_code]."\n";
$st.=" --- ".$this->status_message."\n";
                        }
                    }
echo $st;
                }

            if (!empty($this->task))
            { // был сигнал СТОП - выход
                if ($this->task->consumer == 'stop') {break;}
            }
        }
    }

    /** Останавливает выполнение демона
     *
     * @return  int
     */
    public function stop()
    {
        if ($this->tasks_model->complete($this->task->id, "ОК: завершение по STOP")) return 0;
            else return 666;
    }

    /** реакция на сигнал от внешнего сервиса о старте эфира продукта
     * В задаче должно быть:
        $product_id
        $stream_url_data
        $stream_origin
        $stream_item_guid
     * @return  void
     */
    public function transcoderStart()
    {
        $this->status_code = 0;
        $this->status_message = "OK: трансляция начата";

        $uData = [
            'stream_url_data' => (object)$this->stream_url_data,
            'stream_host_relay_url' => "rtmp://werwerwerwer.rt/rtrt",
            'stream_origin' => "vimeo",
            'stream_item_guid' => "12312312sdgsdfgdfg"
        ];
        $this->task->data_out = json_encode($uData);
        $this->tasks_model->save($this->task->id, $this->task);
        $this->tasks_model->complete($this->task);

        return;
    }

    /** реакция на сигнал от внешнего сервиса об останове эфира продукта
     * В задаче должно быть:
    $product_id
     *
     * @return  void
     */
    public function transcoderStop()
    {
        $this->status_code = 0;
        $this->status_message = "OK: трансляция завершена";

        $uData = [
            'stream_url_data' => json_encode( (object)$this->stream_url_data ),
            'stream_host_relay_url' => "rtmp://werwerwerwer.rt/rtrt",
            'stream_origin' => "vimeo",
            'stream_item_guid' => "12312312sdgsdfgdfg"
        ];
        $this->task->data_out = json_encode($uData);
        $this->tasks_model->save($this->task->id, $this->task);
        $this->tasks_model->complete($this->task);

        return $this->status_code;
    }

    /** реакция на сигнал от внешнего сервиса об PAUSE эфира продукта
     * В задаче должно быть:
    $product_id
     *
     * @return  void
     */
    public function transcoderPause()
    {
        $this->status_code = 0;
        $this->status_message = "OK: трансляция приостановлена";

        $uData = [
            'stream_url_data' => json_encode( (object)$this->stream_url_data ),
            'stream_host_relay_url' => "rtmp://werwerwerwer.rt/rtrt",
            'stream_origin' => "vimeo",
            'stream_item_guid' => "12312312sdgsdfgdfg"
        ];
        $this->task->data_out = json_encode($uData);
        $this->tasks_model->save($this->task->id, $this->task);
        $this->tasks_model->complete($this->task);

        return $this->status_code;
    }


}
