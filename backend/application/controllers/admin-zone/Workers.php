<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Workers - служит для обработки задач (tasks) из очереди tasks. Зациклен в методе demonize().
 *
 * @param Tasks_model $tasks_model
 * @param Tasks_model $tasks_completed_model
 * @param Products_model $products_model
 * @param Instagram_accounts_model $instagram_accounts_model
 * @param Taskmanager $taskmanager
 */
class Workers extends Base_Admin_Controller
{
    private $task = null;
    private $status_code = 0;
    private $status_message = "";

    private $errors = [
         0 => "OK",
        10 => "Ошибка: недостаточно данных",
        20 => "Ошибка: объект не найден",
        30 => "Ошибка: неверный тип объекта",
        40 => "Ошибка: неверное состояние объекта",
        50 => "Ошибка: операция не удалась",
       666 => "Ошибка: демон не желает останавливаться",
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('tasks_model');
        $this->tasks_model->system = 'backend';

        $this->load->model('tasks_completed_model');
    }

    public function index()
    {

    }

    /** Работает в вечном цикле выборки задач. Остановить процесс можно, создав task с consumer='stop'
     *
     * @return  object - JSON-объект формата envelope
     */
    public function demonize()
    {

        while (true)
        {
            $this->task = $this->tasks_model->getNew();

            $this->db->trans_begin();
                if (!empty($this->task)) $reserv = $this->tasks_model->in_process($this->task->id);
                    else $reserv = 0;
            $this->db->trans_commit();

                if (!$reserv)
                {
//echo date("Y-m-d H:i:s\n");
                    usleep(2000000); // спим 2с
                }
                else
                {
$st=date("Y-m-d H:i:s")." --- ID=".$this->task->id;
                    if (!method_exists($this, $this->task->consumer))
                    {
                        // неверный исполнитель - метод-обработчик не существует
                        $this->tasks_model->error($this->task, "Ошибка: обработчик '".$this->task->consumer."' не найден");
$st.=" --- Error: consumer '".$this->task->consumer."' not found\n";
                    }
                    else
                    {   // исполнитель найден - работаем на задачей
                        $this->task->decoded_data_in = json_decode($this->task->data_in);

                        if (json_last_error()!= JSON_ERROR_NONE)
                        {
                            // неверный json в задаче
                            $this->tasks_model->error($this->task, "Ошибка: проблема при декодировании данных JSON");
$st.=" --- Error: problem while JSON decoding\n";
                        }
                        else
                        {
                            // проверки пройдены - передаем управление исполнителю
                            $this->{$this->task->consumer}();

                            if ($this->status_code)
                            {
                                // завершение с ошибкой
                                $this->tasks_model->error($this->task, $this->status_message);
                            }
                            else
                            {
                                // успешное завершение
                                $this->tasks_model->complete($this->task, $this->status_message);
                            }

//$st.=" --- ".$this->errors[$this->status_code]."\n";
$st.=" --- ".$this->status_message."\n";

                        }
                    }
echo $st;
                }


            if (!empty($this->task))
            { // был сигнал СТОП - выход
                if ($this->task->consumer == 'stop') {break;}
            }
        } // while true

    }

    /** Останавливает выполнение демона
     *
     * @return  int
     */
    public function stop()
    {
        if ($this->tasks_model->complete($this->task->id, "ОК: завершение по STOP")) return 0;
            else return 666;
    }

    /** реакция на сигнал от внешнего сервиса о старте эфира продукта
     * В data_in задачи должно быть:
        $product_id
        $orientatition (необязательный)
        $width (необязательный)
        $height (необязательный)
        $fps (необязательный)
     *
     * @return void
     */
    public function startStream()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->load->config('stream', true);

        $product_id = $this->task->decoded_data_in->product_id;

        $orientation = $this->task->decoded_data_in->orientation;
        if (empty($orientation)) $orientation = $this->config->item('stream_default_orientation', 'stream');

        $width = intval($this->task->decoded_data_in->width);
        if (empty($width)) $width = $this->config->item('stream_default_width', 'stream');

        $height = intval($this->task->decoded_data_in->height);
        if (empty($height)) $height = $this->config->item('stream_default_height', 'stream');

        $fps = intval($this->task->decoded_data_in->fps);
        if (empty($fps)) $fps = $this->config->item('stream_default_fps', 'stream');


        $this->status_code = 0;
        $this->status_message = "OK";

        if (empty($product_id))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: ID продукта не найден";
//            $this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $product = $this->products_model->getById($product_id);

        if (empty($product))
        {
            //product не найден
            $this->status_code = 20;
            $this->status_message = "Ошибка: продукт не найден";
//            $this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $this->status_code = 30;
            $this->status_message = "Ошибка: продукт не эфирный";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }
/*
        if ($product->stream_status == 'active')
        {
            //стрим уже запущен. Попытка повторного запуска стрима
            $status_message = "Ошибка: эфир уже запущен";
            $err_code = 40;
            $this->tasks_model->error($this->task->id, $status_message);

            return $err_code;
        }
*/
        $task = null;

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/startStream",
            'consumer'  => "transcoderStart",
            'priority'  => 100,
            'start_dt'  => "NULL",
            'data_in'   => [
                "product_id" => $product_id,
                "orientation" => $orientation,
                "width" => $width,
                "height" => $height,
                "fps" => $fps,
            ],
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на начало стрима в очереди для транскодера

        if (!empty($new_task_id))
        {
            /** @var object $task */
            $task = $this->taskmanager->waitForTask($new_task_id, $timeout = 20); // ожидаю выполнения

            if (!empty($task))
            {
                // задача из tasks_completed выполнена
                if ($task->status == 'error')
                {
                    // выполнена с ошибкой
                    $this->status_message = "Ошибка: статус задачи - error";
                    $this->status_code = 50;
                    //$this->tasks_model->error($this->task->id, $status_message);

                    return;
                }
                else
                {
                    /* выполнена без ошибки - берем из нее данные для начала стрима
                     * ожидаем наличия
                        stream_url_data - ссылки на трансляции в разном формате и качестве
                        steram_host_relay_url - куда транслирует блоггер с мобилы
                        stream_origin
                        stream_item_guid
                    */
                    $task->data_out = json_decode($task->data_out);
                    $uData = [
                        'stream_status' => 'active',
                        'stream_url_data' => $task->data_out->stream_url_data,
                        'stream_host_relay_url' => $task->data_out->stream_host_relay_url,
                        'stream_origin' => $task->data_out->stream_origin,
                        'stream_item_guid' => $task->data_out->stream_item_guid,
                        'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                        'stream_ended' => 'NULL',
                        'stream_clients_informed' => 0,
                        'stream_health' => 'red',
                        'stream_health_last_changed_dt' => (new Datetimeex())->format("Y-m-d H:i:s"),
                    ];

                    $this->db->trans_begin();

                    $upd_res = $this->products_model->setStream($product_id, $uData);

                    if ($upd_res!==false)
                    {
                        $this->status_code = 0;
                        $this->status_message = "ОК: эфир начат";

                        if (!empty($product->instagram_account_id))
                        {
                            // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                            $iuData = [
                                'stream_origin' => $uData['stream_origin'],
                                'stream_url_data' => $uData['stream_url_data'],
                                'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                                'stream_ended' => 'NULL',
                                'stream_status' => 'active'
                            ];

                            $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                            // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                        }//if (!empty($product->instagram_account_id))

                        $this->db->trans_commit();

                        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_product';
                        $this->messinger->target = []; //all
                        $this->messinger->data = ['product_id'=>(string)$product_id];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
/*
                        // отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_products_list';
                        $this->messinger->target = []; //all
                        $this->messinger->data = [];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
*/
                        return;
                    }
                    else
                    {
                        $this->status_code = 50;
                        $this->status_message = "Ошибка: операция не удалась";
                        $this->db->trans_rollback();

                        return;
                    }

                }

            }
        }

        $this->status_message = "Ошибка: операция не удалась";
        $this->status_code = 50;

        return;
    }


    /** реакция на сигнал от внешнего сервиса об остановке эфира продукта
     *
     * @return void
     */
    public function stopStream()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->status_code = 0;
        $this->status_message = "OK: эфир остановлен";

        $product_id = $this->task->decoded_data_in->product_id;
        if (empty($product_id))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: ID продукта не найден";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $product = $this->products_model->getById($product_id);

        if (empty($product))
        {
            //product не найден
            $this->status_message = "Ошибка: продукт не найден";
            $this->status_code = 20;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $this->status_message = "Ошибка: продукт не эфирный";
            $this->status_code = 30;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->stream_status == 'inactive')
        {
            //стрим уже остановлен. Попытка повторного останова стрима
            $this->status_message = "Ошибка: эфир не запущен";
            $this->status_code = 40;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/stopStream",
            'consumer'  => "transcoderStop",
            'data_in'      => ["product_id" => $product_id],
            'priority'  => 100,
            'start_dt'  => "NULL",
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на окончание стрима в очереди для транскодера
        if (!empty($new_task_id))
        {
            $uData = [
                'stream_status' => 'inactive',
                'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
            ];

            $this->db->trans_begin();

            $upd_res = $this->products_model->setStream($product_id, $uData);

            if ($upd_res!==false)
            {
                if (!empty($product->instagram_account_id))
                {
                    // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                    $iuData = [
                        //'stream_origin' => $uData['stream_origin'],
                        //'stream_url_data' => $uData['stream_url_data'],
                        'stream_started' => (new Datetimeex())->format("Y-m-d H:i:s"),
                        'stream_status' => 'inactive',
                        'stream_ended' => (new Datetimeex())->format("Y-m-d H:i:s"),
                    ];

                    $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                    // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                }//if (!empty($product->instagram_account_id))

                $this->db->trans_commit();

                        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_product';
                        $this->messinger->target = []; //all
                        $this->messinger->data = ['product_id'=>(string)$product_id];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
/*
                // отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
                $this->load->library('messinger');
                $this->messinger->event_id = $this->utils->generateEventGuid();
                $this->messinger->action = 'system_refresh_products_list';
                $this->messinger->target = []; //all
                $this->messinger->data = [];
                $this->messinger->send();
                // END отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
*/
                $this->status_code = 0;
                $this->status_message = "OK: эфир окончен";

                return;
            }
            else
            {
                $this->db->trans_rollback();

                $this->status_code = 50;
                $this->status_message = "Ошибка: не удалось обновить сущности.";

                return;
            }

        }

        $this->status_code = 50;
        $this->status_message = "Ошибка: не удалось создать задачу для transcoder.";

        return;
    }

    /** реакция на сигнал от внешнего сервиса об PAUSE эфира продукта
     *
     * @return void
     */
    public function pauseStream()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->status_code = 0;
        $this->status_message = "OK: эфир приостановлен";

        $product_id = $this->task->decoded_data_in->product_id;
        if (empty($product_id))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: ID продукта не найден";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $product = $this->products_model->getById($product_id);

        if (empty($product))
        {
            //product не найден
            $this->status_message = "Ошибка: продукт не найден";
            $this->status_code = 20;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $this->status_message = "Ошибка: продукт не эфирный";
            $this->status_code = 30;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->stream_status == 'inactive')
        {
            //стрим уже остановлен. Попытка повторного останова стрима
            $this->status_message = "Ошибка: эфир не запущен";
            $this->status_code = 40;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $task_data_in = [
            'system'    => "transcoder",
            'publisher' => "products/pauseStream",
            'consumer'  => "transcoderPause",
            'data_in'      => ["product_id" => $product_id],
            'priority'  => 100,
            'start_dt'  => "NULL",
        ];

        $new_task_id = $this->taskmanager->createTask($task_data_in); // создаю задачу на окончание стрима в очереди для транскодера
        if (!empty($new_task_id))
        {
            $uData = [
                'stream_status' => 'paused',
            ];

            $this->db->trans_begin();

            $upd_res = $this->products_model->setStream($product_id, $uData);

            if ($upd_res!==false)
            {
                if (!empty($product->instagram_account_id))
                {
                    // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                    $iuData = [
                        //'stream_origin' => $uData['stream_origin'],
                        //'stream_url_data' => $uData['stream_url_data'],
                        'stream_status' => 'paused',
                    ];

                    $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                    // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                }//if (!empty($product->instagram_account_id))

                $this->db->trans_commit();

                // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                $this->load->library('messinger');
                $this->messinger->event_id = $this->utils->generateEventGuid();
                $this->messinger->action = 'system_refresh_product';
                $this->messinger->target = []; //all
                $this->messinger->data = ['product_id'=>$product_id];
                $this->messinger->send();
                // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
                /*
                                // отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
                                $this->load->library('messinger');
                                $this->messinger->action = 'system_refresh_products_list';
                                $this->messinger->target = []; //all
                                $this->messinger->data = [];
                                $this->messinger->send();
                                // END отправляем сообщение "обнови статуc ВСЕХ(!) продуктов" через WS ------------------------------
                */
                $this->status_code = 0;
                $this->status_message = "OK: эфир приостановлен";

                return;
            }
            else
            {
                $this->db->trans_rollback();

                $this->status_code = 50;
                $this->status_message = "Ошибка: не удалось обновить сущности.";

                return;
            }

        }

        $this->status_code = 50;
        $this->status_message = "Ошибка: не удалось создать задачу для transcoder.";

        return;
    }

    /** ПРИМЕЧАНИЕ: БОЛЕЕ НЕ ИСПОЛЬЗУЕТСЯ
     * отложенный на 5 секунд update streamData
     *
     * @return void
     */
    public function startStreamUrlDataDelay()
    {
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->status_code = 0;
        $this->status_message = "OK";

        $product_id = $this->task->decoded_data_in->product_id;
        if (empty($product_id))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: ID продукта не найден";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $product = $this->products_model->getById($product_id);

        $uData = [
            'stream_url_data' => json_encode($this->task->decoded_data_in->stream_url_data),
        ];

        $this->db->trans_begin();

        $upd_res = $this->products_model->update($product_id, $uData);

        if ($upd_res!==false)
        {
            if (!empty($product->instagram_account_id))
            {
                // изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
                $iuData = [
                    'stream_url_data' => json_encode($this->task->decoded_data_in->stream_url_data),
                ];

                $this->instagram_accounts_model->update($product->instagram_account_id, $iuData);
                // END изменение продукта успешно - отражаем изменения в instagram_account этого продукта (возможно позднее процессы надо унифицировать в одном месте)
            }//if (!empty($product->instagram_account_id))

            $this->db->trans_commit();

            // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
            $this->load->library('messinger');
            $this->messinger->event_id = $this->utils->generateEventGuid();
            $this->messinger->action = 'system_refresh_product';
            $this->messinger->target = []; //all
            $this->messinger->data = ['product_id'=>(string)$product_id];
            $this->messinger->send();
            // END отправляем сообщение "обнови статус продукта" через WS ------------------------------

            $this->status_code = 0;
            $this->status_message = "OK";

            return;
        }

        $this->status_code = 50;
        $this->status_message = "Ошибка: не удалось выполнить задачу отлоденного обновления.";

        return;
    }


    /** реакция на сигнал от внешнего сервиса об PAUSE эфира продукта
     *
     * @return void
     */
    public function streamHealthChanged()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');

        $this->status_code = 0;
        $this->status_message = "OK";

        $product_id = $this->task->decoded_data_in->product_id;
        if (empty($product_id))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: ID продукта не найден";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        // может быть строкой red|yellow|green
        $health = $this->task->decoded_data_in->health;
        if ((empty($health)) || (!in_array($health, ['red','green','none'])))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: stream_health не найден или неверен";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $product = $this->products_model->getById($product_id);

        if (empty($product))
        {
            //product не найден
            $this->status_message = "Ошибка: продукт не найден";
            $this->status_code = 20;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if ($product->product_type != 'subscription')
        {
            //продукт не эфирный
            $this->status_message = "Ошибка: продукт не эфирный";
            $this->status_code = 30;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $uData = ['stream_health'=>$health];
        $this->products_model->update($product->id, $uData);

        if ($product->stream_status == 'active')
        {
            switch ($health)
            {
                case 'red': break;
                case 'green':
                    if (!$product->stream_clients_informed)
                    {
                        // это после начала стрима
                        $uData = ['stream_clients_informed'=>1];
                        $this->products_model->update($product->id, $uData);

                        // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                        $this->load->library('messinger');
                        $this->messinger->event_id = $this->utils->generateEventGuid();
                        $this->messinger->action = 'system_refresh_product';
                        $this->messinger->target = []; //all
                        $this->messinger->data = ['product_id'=>(string)$product->id];
                        $this->messinger->send();
                        // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
                    }
                    break;
                case 'none': break;
            }
        }
        elseif ($product->stream_status == 'inactive')
        {
            switch ($health)
            {
                case 'red':break;
                case 'green':break;
                case 'none':break;
                default:
            }
        }
        elseif ($product->stream_status == 'paused')
        {
            switch ($health)
            {
                case 'red':break;
                case 'green':break;
                case 'none':break;
                default:
            }
        }

        return;
    }


    /** реакция на сигнал от внешнего сервиса о старте эфира в Instagram аккаунте
     * В data_in задачи должно быть:
    'data_in'   => json_encode(
    "instagram_account" => {
        "id": "23219474204",
        "username": "ziemannlaney2",
    },
    "stream_url_data":{
        "hls":{
            "HQ":"https://live-dev2.ivas-crm.ru:59881/a97affab-a35a-41d5-afa8-3b9f4248a726/hls/index.m3u8",
            "LQ":"https://live-dev2.ivas-crm.ru:59881/a97affab-a35a-41d5-afa8-3b9f4248a726/hls/index.m3u8"
        }
    }
    "stream_origin":"vimeo",
    "stream_item_guid":"DfgdDFGfggU",
    )
     *
     * @return void
     */
    public function startStreamInstagram()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->load->config('stream', true);

        $instagram_account = $this->task->decoded_data_in->instagram_account;
        if (empty($instagram_account))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: instagram_account отсутствует";

            return;
        }

        $instagram_account = $this->instagram_accounts_model->getByLogin($instagram_account->username);
        if (empty($instagram_account))
        {
            //product не найден
            $this->status_message = "Ошибка: Instagram аккаунт не найден";
            $this->status_code = 20;

            return;
        }

        $stream_url_data = $this->task->decoded_data_in->stream_url_data;
        if (empty($stream_url_data))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: stream_url_data отсутствует";

            return;
        }

        $stream_item_guid = $this->task->decoded_data_in->stream_item_guid;
        if (empty($stream_item_guid))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: guid отсутствует";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        if (!empty($this->task->decoded_data_in->stream_origin)) $stream_origin = $this->task->decoded_data_in->stream_origin;
        if (empty($stream_origin)) $stream_origin = $this->config->item('stream_default_origin', 'stream');

        $stream_started = (new Datetimeex())->format("Y-m-d H:i:s");

        $this->status_code = 0;
        $this->status_message = "OK";

        // устанавливаем статусы в instagram_accounts -------------------------------------------------
        $uData = [
            'stream_url_data' => json_encode($stream_url_data),
            'stream_item_guid' => $stream_item_guid,
            'stream_started' => $stream_started,
            'stream_status' => 'active'
        ];
        if (!empty($this->task->decoded_data_in->instagram_account->id)) $uData['pk'] = $this->task->decoded_data_in->instagram_account->id;
        if (!empty($instagram_account->avatar_url)) $uData['user_avatar'] = $instagram_account->avatar_url;
        if (!empty($instagram_account->full_name)) $uData['user_name'] = $instagram_account->full_name;
        if (!empty($stream_origin)) $uData['stream_origin'] = $stream_origin;

        $upd_res = $this->instagram_accounts_model->setStream($instagram_account->id, $uData);

        if ($upd_res!==false)
        {

            $this->db->trans_commit();

            $products = $this->products_model->getListByInstaAccId($instagram_account->id);
            if (!empty($products))
            {
                $this->load->library('messinger');
                foreach ($products as $prod)
                {
                    // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                    $this->messinger->event_id = $this->utils->generateEventGuid();
                    $this->messinger->action = 'system_refresh_product';
                    $this->messinger->target = []; //all
                    $this->messinger->data = ['product_id'=>(string)$prod->id];
                    $this->messinger->send();
                    // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
                }
            }

            $this->status_code = 0;
            $this->status_message = "ОК";

            return;
        }
        else
        {
            $this->db->trans_rollback();

            $this->status_code = 50;
            $this->status_message = "Ошибка: не удалось обновить instagram_account.";

            return;
        }
        //END устанавливаем статусы в instagram_accounts -------------------------------------------------

    }


    /** реакция на сигнал от внешнего сервиса об остановке эфира в Instagram аккаунте
     * В data_in задачи должно быть:
    'data_in'   => json_encode(
        "instagram_account" => {
            "id": "23219474204",
            "username": "ziemannlaney2",
        },
    )
     *
     * @return void
     */
    public function stopStreamInstagram()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('instagram_accounts_model');

        $this->load->config('stream', true);

        $instagram_account = $this->task->decoded_data_in->instagram_account;
        if (empty($instagram_account))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: instagram_account отсутствует";

            return;
        }

        $instagram_account = $this->instagram_accounts_model->getByLogin($instagram_account->username);
        if (empty($instagram_account))
        {
            //product не найден
            $this->status_message = "Ошибка: Instagram аккаунт не найден";
            $this->status_code = 20;

            return;
        }

        if (!empty($this->task->decoded_data_in->stream_origin)) $stream_origin = $this->task->decoded_data_in->stream_origin;
        if (empty($stream_origin)) $stream_origin = $this->config->item('stream_default_origin', 'stream');

        $stream_started = (new Datetimeex())->format("Y-m-d H:i:s");

        $this->status_code = 0;
        $this->status_message = "OK";

        // устанавливаем статусы в instagram_accounts -------------------------------------------------
        $uData = [
            'stream_url_data' => NULL,
            'stream_started' => $stream_started,
            'stream_status' => 'inactive'
        ];
        if (!empty($this->task->decoded_data_in->stream_item_guid)) $uData['stream_item_guid'] = $this->task->decoded_data_in->stream_item_guid;
        if (!empty($this->task->decoded_data_in->instagram_account->id)) $uData['pk'] = $this->task->decoded_data_in->instagram_account->id;
        if (!empty($instagram_account->avatar_url)) $uData['user_avatar'] = $instagram_account->avatar_url;
        if (!empty($instagram_account->full_name)) $uData['user_name'] = $instagram_account->full_name;
        if (!empty($stream_origin)) $uData['stream_origin'] = $stream_origin;

        $upd_res = $this->instagram_accounts_model->setStream($instagram_account->id, $uData);

        if ($upd_res!==false)
        {

            $this->db->trans_commit();

            $products = $this->products_model->getListByInstaAccId($instagram_account->id);
            if (!empty($products))
            {
                $this->load->library('messinger');
                foreach ($products as $prod)
                {
                    // отправляем сообщение "обнови статуc продукта" через WS ------------------------------
                    $this->messinger->event_id = $this->utils->generateEventGuid();
                    $this->messinger->action = 'system_refresh_product';
                    $this->messinger->target = []; //all
                    $this->messinger->data = ['product_id'=>(string)$prod->id];
                    $this->messinger->send();
                    // END отправляем сообщение "обнови статус продукта" через WS ------------------------------
                }
            }

            $this->status_code = 0;
            $this->status_message = "ОК";

            return;
        }
        else
        {
            $this->db->trans_rollback();

            $this->status_code = 50;
            $this->status_message = "Ошибка: не удалось обновить instagram_account.";

            return;
        }
        //END устанавливаем статусы в instagram_accounts -------------------------------------------------

    }


    /** реакция на сигнал от внешнего сервиса "изменить данные item по его guid"
     *
     * В data_in задачи должно быть:
     json_encoded -
     {
         "instagram_account": //может не быть!!!
         {
            "id": "23219474204",
            "username": "ziemannlaney2",
         },
        "item_guid": "dfgdfDcvsergjkyu456J",
        "item_url": "https://123123/dfsdfsdf/ertert.m4",
    }
     * @return void
     */
    public function itemChange()
    {
        $this->load->library('taskmanager');
        $this->load->model('products_model');
        $this->load->model('items_model');
        $this->load->model('instagram_accounts_model');

        $this->status_code = 0;
        $this->status_message = "OK";
/*
$this->task = (object)['decoded_data_in'=>[]];
$data = (object)[
    'instagram_account' => (object)[
        'id' => '1231212',
        'username' => 'malyshkimoi',
    ],
    'item_guid' => "12y",
    'item_url' => 'https://123123/dfsdfsdf/ertert.m4',
];
$this->task->decoded_data_in = $data;
*/

        $instagram_account = $this->task->decoded_data_in->instagram_account;
        if (empty($instagram_account))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: instagram_account отсутствует";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $instagram_account = $this->instagram_accounts_model->getByLogin($instagram_account->username);
        if (empty($instagram_account))
        {
            //product не найден
            $this->status_message = "Ошибка: Instagram аккаунт не найден";
            $this->status_code = 20;
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $item_guid = $this->task->decoded_data_in->item_guid;
        if (empty($item_guid))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: guid отсутствует";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $item_url = $this->task->decoded_data_in->item_url;
        if (empty($item_url))
        {
            $this->status_code = 10;
            $this->status_message = "Ошибка: url отсутствует";
            //$this->tasks_model->error($this->task->id, $status_message);

            return;
        }

        $item = $this->items_model->getByGuid($item_guid);
        if (empty($item))
        {
            $products = $this->products_model->getListByInstaAccId($instagram_account->id);

            if (count($products)>1)
            {
                $name = $system_name = $instagram_account->name . ". Instagram. Эфир от " . (new Datetimeex())->format('Y-m-d H:i:s');
            }
            else
            {
                $name = $system_name = $products[0]->name . ". Эфир от " . (new Datetimeex())->format('Y-m-d H:i:s');
            }

            $iData = [
                'name' => $name,
                'system_name' => $system_name,
                'resource_string_id' => $item_url,
                'guid' => $item_guid,
                'status' => 'new', // item всегда создается new для сохранения возможности последующего редактирования
            ];
            $new_item_id = $this->items_model->insert($iData);
            if (empty($new_item_id)) return;

            $this->load->model('presets_items_model');

            foreach ($products as $product)
            {
                $this->presets_items_model->addItemToPreset($new_item_id, $product->preset_id, 100000);
            }

            $this->status_code = 0;
            $this->status_message = "OK";
            return;
        }
        else
        {
            $uData = [
                'resource_string_id' => $item_url,
            ];

            $this->db->trans_begin();

            $upd_res = $this->items_model->update($item->id, $uData);

            if ($upd_res !== false)
            {
                $this->db->trans_commit();
                $this->status_code = 0;
                $this->status_message = "OK";

                return;
            }
            else
            {
                $this->db->trans_rollback();

                $this->status_code = 50;
                $this->status_message = "Ошибка: не удалось обновить item.";

                return;
            }
        }

    }

}
