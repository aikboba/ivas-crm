<?php
/**
 * Class Base_Admin_Controller
 * 
 * @property CI_Config $config
 * @property Utils $utils
 * @property array $data
 * @property CI_Session $session
 * @property CI_Form_validation $form_validation
 * @property CI_Lang $lang
 */
class Base_Admin_Controller extends MY_Controller {

    public $caller_origin = 'admin';

    function __construct()
    {
        parent::__construct();
/*
ob_start();
echo "\n\n";
echo "Base_Admin_Controller ---";
print_r($this->auth->token_data);
echo "\n";
$f = fopen('log.txt', 'a+');
fputs($f, ob_get_contents());
fclose($f);
ob_end_clean();
*/
    }


}