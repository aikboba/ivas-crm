<?php
/**
 * Class Base_Front_Controller
 *
 * @param Clients_model $clients_model
 */
class Base_Front_Controller extends MY_Controller {

    public $caller_origin = 'front';

    function __construct()
    {
        parent::__construct();

        if ( $this->auth->isLogged() )
        {
            $this->load->model('clients_model');

            if ( ($this->className != 'clients') && ($this->methodName != 'logout') )
            {
                $this->clients_model->touch($this->auth->loggedUser->id);
            }
        }
    }


}

