<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

/**
 * JSON Web Token implementation, based on this spec:
 * Singleton. DO NOT LOAD VIA $this->load->library()!!!
 * USAGE:
 *  JWT::encode();
 */

class JWT
{
    protected static $data = array();
    protected static $_instance = null;

    const ERROR_NO_KEY = ['error_code'=>1001, 'error_msg'=>'Error: Key may not be empty'];
    const ERROR_MALFORMED = ['error_code'=>1002, 'error_msg'=>'Error: Wrong number of JWT segments'];
    const ERROR_INVALID_HEADER_ENCODING = ['error_code'=>1003, 'error_msg'=>'Error: Invalid JWT-header encoding'];
    const ERROR_INVALID_PAYLOAD_ENCODING = ['error_code'=>1004, 'error_msg'=>'Error: Invalid JWT-payload encoding'];
    const ERROR_INVALID_SIGN_ENCODING = ['error_code'=>1005, 'error_msg'=>'Error: Invalid JWT-sign encoding'];
    const ERROR_EMPTY_ALG = ['error_code'=>1006, 'error_msg'=>'Error: Empty JWT encoding algorithm'];
    const ERROR_ALG_NOT_SUPPORTED = ['error_code'=>1007, 'error_msg'=>'Error: Algorythm not suppoerted'];
    const ERROR_SIGN_VERIFICATION_FAILED = ['error_code'=>1008, 'error_msg'=>'Error: Signature verification failed'];
    const ERROR_NOT_YET_AVAILABLE = ['error_code'=>1009, 'error_msg'=>'Error: Token is not yet available (future token)'];
    const ERROR_EXPIRED = ['error_code'=>1010, 'error_msg'=>'Error: Token is expired'];
    const ERROR_TOKEN_TYPE_NOT_SUPPORTED = ['error_code'=>1011, 'error_msg'=>'Error: Token type is not supported. Supported types: access|refresh'];
    const ERROR_TOKEN_NOT_FOUND = ['error_code'=>1012, 'error_msg'=>'Error: token not found'];

    private function __construct() {}
    private function __clone() {}
    private function __wakeup () {}

    private static $timestamp = null;
    private static $config_data = null; // хранит настройки, для изначальной инициализации (по идее - то, что лежит в конфиге)

    private static $jwt_payload = null; // jwt payload
    private static $jwt_sign = null; // jwt подпись
    private static $jwt_header = null; // jwt header
    private static $jwt_default_payload = null; // служебные поля по умолчанию. При кодировании мерджаться с переданными

    private static $jwt_access_token_ttl = null; // время жизни access токена (мин) - из конфига
    private static $jwt_refresh_token_ttl = null; // время жизни refresh токена (мин) - из конфига

    private static $jwt_token_type = 'access'; // тип генерируемого токена. Значения 'access' | 'refresh'

    private static $jwt_alg = null; // алгоритм кодирования по умолчанию
    private static $jwt_typ = null; // тип по умолчанию
    private static $jwt_key = null; // ключ из конфига

    private static $error = []; // содержит [error_code=>'<code>', error_msg=>'<msg>']
    private static $error_code = 0; // код ошибки
    private static $error_msg = ""; // сообщение об ошибке

    private static $supported_algs = array(
        'HS256' => array('hash_hmac', 'SHA256'),
        'HS512' => array('hash_hmac', 'SHA512'),
        'HS384' => array('hash_hmac', 'SHA384'),
    );

    public static function error()
    {
        return self::$error;
    }

    public static function payload()
    {
        return array_merge(self::$jwt_default_payload, self::$jwt_payload);
    }

    public static function setPayload($data)
    {
        self::$jwt_payload = $data;
    }

    public static function tokenType()
    {
        return self::$jwt_token_type;
    }

    public static function setTokenType($value)
    {
        if ( ($value == 'access') || ($value == 'refresh') ) self::$jwt_token_type = $value;
            else self::setError(self::ERROR_TOKEN_TYPE_NOT_SUPPORTED);

    }

    public static function setHeader($data)
    {
        self::$jwt_header = $data;
    }

    /**
     * Инициализирует синглетон значениями по умолчанию или из data
     */
    public static function init($data = null)
    {
        if (empty($data)) $data = self::$config_data;

        self::$jwt_alg = (empty($data['jwt_alg'])) ? 'HS256' : $data['jwt_alg'];
        self::$jwt_typ = (empty($data['jwt_typ'])) ? 'JWT' : $data['jwt_typ'];
        self::$jwt_header = ['alg'=>self::$jwt_alg, 'typ'=>self::$jwt_typ];
        self::$jwt_key = (empty($data['jwt_key'])) ? '' : $data['jwt_key'];
        self::$jwt_access_token_ttl = (empty($data['jwt_access_token_ttl'])) ? '' : $data['jwt_access_token_ttl'];
        self::$jwt_refresh_token_ttl = (empty($data['jwt_refresh_token_ttl'])) ? '' : $data['jwt_refresh_token_ttl'];

        self::$jwt_token_type = 'access';

        self::$jwt_default_payload = (empty($data['jwt_default_payload'])) ? self::$jwt_default_payload = [] : $data['jwt_default_payload'];
        if (empty(self::$jwt_payload)) self::$jwt_payload = self::$jwt_default_payload;

        self::$config_data = $data;

        self::setError(null); // сброс ошибок
    }

    /**
     * Сбрасывает значения к дефолтным
     */
    public static function reset()
    {
        self::$jwt_payload = self::$jwt_default_payload;
        self::setError(null); // сброс ошибок
    }

    private static function setError($ERR = null)
    {
        self::$error = (!empty($ERR)) ? $ERR : ['error_code'=>0, 'error_msg'=>''];
        self::$error_code = (!empty($ERR['error_code'])) ? $ERR['error_code'] : 0;
        self::$error_msg = (!empty($ERR['error_msg'])) ? $ERR['error_msg'] : '';
    }

    public static function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Декодирует JWT-строку в PHP object.
     *
     * @param string $jwt
     *
     * @return object | boolean  - JWT payload как PHP object or sets error code an returns void
     */
    public static function decode($jwt)
    {
        self::setError(null);

        if (empty($jwt))
        {
            self::setError(self::ERROR_TOKEN_NOT_FOUND);
            return false;
        }

        $timestamp = is_null(self::$timestamp) ? time() : self::$timestamp;

        if (empty(self::$jwt_key))
        {
            self::setError(self::ERROR_NO_KEY);
            return false;
        }

        $parts = explode('.', $jwt);

        if (count($parts) != 3) {
            self::setError(self::ERROR_MALFORMED);
            return false;
        }

        list($headb64, $bodyb64, $cryptob64) = $parts;

        self::$jwt_header = self::jsonDecode(self::urlsafeB64Decode($headb64));
        if (null === self::$jwt_header)
        {
            self::setError(self::ERROR_INVALID_HEADER_ENCODING);
            return false;
        }

        self::$jwt_payload = self::jsonDecode(self::urlsafeB64Decode($bodyb64));
        if (null === self::$jwt_payload)
        {
            self::setError(self::ERROR_INVALID_PAYLOAD_ENCODING);
            return false;
        }

        self::$jwt_sign = self::urlsafeB64Decode($cryptob64);
        if (false === self::$jwt_sign) {
            self::setError(self::ERROR_INVALID_SIGN_ENCODING);
            return false;
        }

        if (empty(self::$jwt_header->alg))
        {
            self::setError(self::ERROR_EMPTY_ALG);
            return false;
        }
        if (empty(self::$supported_algs[self::$jwt_header->alg])) {
            self::setError(self::ERROR_ALG_NOT_SUPPORTED);
            return false;
        }

        // Check the signature
        if (!self::verify("$headb64.$bodyb64", self::$jwt_sign, self::$jwt_key, self::$jwt_header->alg))
        {
            self::setError(self::ERROR_SIGN_VERIFICATION_FAILED);
            return false;
        }

        // Check if the nbf if it is defined. This is the time that the
        // token can actually be used. If it's not yet that time, abort.
        if (isset(self::$jwt_payload->nbf) && self::$jwt_payload->nbf > $timestamp)
        {
            self::setError(self::ERROR_NOT_YET_AVAILABLE);
            return false;
        }

        // Check that this token has been created before 'now'. This prevents
        // using tokens that have been created for later use (and haven't
        // correctly used the nbf claim).
        if (isset(self::$jwt_payload->iat) && self::$jwt_payload->iat > $timestamp)
        {
            self::setError(self::ERROR_NOT_YET_AVAILABLE);
            return false;
        }

        // Check if this token has expired.
        if (isset(self::$jwt_payload->exp) && ($timestamp) >= self::$jwt_payload->exp)
        {
            self::setError(self::ERROR_EXPIRED);
            return false;
        }
        return self::$jwt_payload;
    }

    /**
     * Возвращает сформированный JWT токен
     *
     * @param object|array $payload - объект или массив с claims (если параметр опущен, берется из self::$jwt_payload)

     * @return string - подписанный и полностью сформированный JWT
     */
    public static function encode($payload = null)
    {
        self::setError(null);

        if (empty($payload)) $payload = self::$jwt_payload;

        switch (self::$jwt_token_type)
        {
            case 'refresh':
                    $ttl = self::$jwt_refresh_token_ttl;
                break;
            case 'access':
            default:
                    $ttl = self::$jwt_access_token_ttl;
                    break;
        }

        if (is_object($payload)) $payload = (array)$payload;

        $d = new Datetimeex();
        $d->add(new DateInterval('PT'.$ttl.'M'));
        $payload['exp'] = intval($d->format("U"));
        $payload = (object)array_merge(self::$jwt_default_payload, $payload);

        $segments = [];
        $segments[] = self::urlSafeB64Encode(self::jsonEncode(self::$jwt_header));
        $segments[] = self::urlSafeB64Encode(self::jsonEncode($payload));
        $signing_input = implode('.', $segments);
        $signature = self::sign($signing_input, self::$jwt_key, self::$jwt_alg);
        $segments[] = self::urlSafeB64Encode($signature);
        return implode('.', $segments);
    }

    /**
     * Подписывает строку, использую указанный ключ и алгоритм кодирования
     *
     * @param string $msg - входная строка для подписания
     * @param string $key - секретный ключ
     * @param string $alg - алгоритм. Поддерживаются: HS256, HS384, HS512
     *
     * @return string | boolean - закодированная подпись
     */
    public static function sign($msg, $key, $alg = 'HS256')
    {
        if (empty(self::$supported_algs[$alg]))
        {
            self::setError(self::ERROR_ALG_NOT_SUPPORTED);
            return false;
        }

        list($function, $algorithm) = static::$supported_algs[$alg];

        return hash_hmac($algorithm, $msg, $key, true);
    }

    /**
     * Совпадает ли хэш данных и переданная подпись
     *
     * @param string $msg - оригинальное сообщение ($header.$body)
     * @param string $signature - оригинальная подпись (sign)
     * @param string $key - ключ
     * @param string $alg - алгоритм
     *
     * @return bool
     */
    private static function verify($msg, $signature, $key, $alg)
    {
        if (empty(self::$supported_algs[$alg]))
        {
            self::setError(self::ERROR_ALG_NOT_SUPPORTED);
            return false;
        }
        list($function, $algorithm) = self::$supported_algs[$alg];
        $hash = hash_hmac($algorithm, $msg, $key, true);

        return hash_equals($signature, $hash);
    }

    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     *
     * @return string A decoded string
     */
    public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * Encode a string with URL-safe Base64.
     *
     * @param string $input The string you want encoded
     *
     * @return string The base64 encode of what you passed in
     */
    public static function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * Decode a JSON string into a PHP object.
     *
     * @param string $input JSON string
     *
     * @return object Object representation of JSON string
     */
    public static function jsonDecode($input)
    {
        $obj = json_decode($input, false, 512, JSON_BIGINT_AS_STRING);
        return $obj;
    }

    /**
     * Encode a PHP object into a JSON string.
     *
     * @param object|array $input A PHP object or array
     *
     * @return string JSON representation of the PHP object or array
     */
    public static function jsonEncode($input)
    {
        $json = json_encode($input);

        if ( $errno = json_last_error() )
        {
            self::handleJsonError($errno);
        }

        return $json;
    }

    /**
     * Helper method to create a JSON error.
     *
     * @param int $errno An error number from json_last_error()
     *
     * @return void
     */
    private static function handleJsonError($errno)
    {
        $messages = array(
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
            JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
            JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters' //PHP >= 5.3.3
        );
        self::setError(["error_code"=>1100, "error_msg"=> isset($messages[$errno]) ? $messages[$errno]: 'Unknown JSON error: ' . $errno]);
    }

}





