<?php
/**
 * Class MY_Controller

 * @property CI_Config $config
 * @property Utils $utils
 * @property array $data
 * @property Auth $auth
 * @property CI_Session $session
 * @property CI_Lang $lang
 * @property Registry $registry
 * @property Tasks_model $tasks_model
 *
 */
class MY_Controller extends CI_Controller {

    public $data = array();
    public $model;
    public $ACL;
    public $access_params;
    protected $className;
    protected $methodName;

    function __construct()
    {
        parent::__construct();

        $this->config->load('cms_consts', true);
        //$this->config->load('jwt', true);
        $this->load->library('auth');

        $this->config->load('acl_config', true);
        $this->ACL = $this->config->item('acl_config')['ACL'];
        $dirName = $this->router->fetch_directory();
        $this->className = $this->router->fetch_class();
        $this->methodName = $this->router->fetch_method();
        $acl_key = $dirName.$this->className.'/'.$this->methodName;

        if (!empty($this->ACL[$acl_key])) $this->access_params = (object)$this->ACL[$acl_key];
        else
        {
            $acl_key = $dirName.$this->className.'/default';
            $this->access_params = (object)$this->ACL[$acl_key];
        }


        // заход в админмодуль
        if ($dirName == 'admin-zone/')
        {
            $this->auth->role = 'admin';
            //$sub = 'admin';
        }
        else
        {
            $this->auth->role = 'client';
            //$sub = 'client';
        }

/*
ob_start();
echo "\n\n";
echo "MY_Controller --- ".$acl_key;
print_r($this->auth->token_data);
echo "\n";
$f = fopen('log.txt', 'a+');
fputs($f, ob_get_contents());
fclose($f);
ob_end_clean();
*/
        // поднимаем токен
        $this->auth->tokenGet(); //так должно быть
        //$this->auth->tokenGet('access',$token); // УБРАТЬ !!!
/*
echo "<pre>";
print_r($this->auth->token);
echo "<br>";
print_r($this->auth->error);
echo "</pre>";
die();
*/
        $error = $this->auth->error;

        if ( (!empty($this->auth->token)) && ($error['error_code']==0))
        {
            $this->auth->loginFromToken(); // поднимаем юзера из токена
        }

        // если метод требует залогиненности
        if ($this->access_params->not_logged_allowed == 0)
        {

            if (!empty($error['error_code']))
            {
                //убираем юзера из auth
                $this->auth->logout();

                if ($error == STATUS_TOKEN_MISSING)
                {
                    $status = STATUS_TOKEN_MISSING;
                    $message = $error['error_msg'];
                    $data = [];

                    $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                    exit();
                }
                if ($error == STATUS_DENIED)
                {
                    $status = STATUS_DENIED;
                    $message = $error['error_msg'];
                    $data = [];

                    $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
                    exit();
                }

            } // if (!empty($error['error_code']))

            // если метод требует залогиненности определенной роли
            if ( !empty($this->access_params->role_required) )
            {
                // не тот токен. нужен role=>admin - нужен admin
                if ($this->auth->role != $this->access_params->role_required)
                {

                    //убираем юзера из auth
                    $this->auth->logout();

                    $status = STATUS_DENIED;
                    $message = STATUS_DENIED_MSG;
                    $data = [];

                    $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
                    exit();
                }
            }

            //$this->auth->loginFromToken(); // поднимаем юзера из токена

            // юзер не залогинен через токен
            if (!$this->auth->isLogged())
            {
                $status = STATUS_NOT_FOUND;
                $message = "Пользователь не поднят из токена";//STATUS_NOT_FOUND_MSG;
                $data = [];

                $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
                exit();
            }

            // это админ и нужны дополнительные разрешения
            if ( ($this->auth->role == 'admin') || ($this->access_params->role_required == 'admin') )
            {

                if ( !empty($this->access_params->permissions_required) )
                {
                    if (!$this->auth->hasPermissions($this->access_params->permissions_required))
                    {
                        $status = STATUS_DENIED;
                        $message = STATUS_DENIED_MSG;
                        $data = [];

                        $this->utils->jsonOut($this->utils->envelope($status, $message, $data));
                        exit();
                    }

                } // if ( !empty((array)$this->access_params->permissions_required) )

            }

        } //if ($this->access_params->not_logged_allowed == 0)

        $tz = null;
        if (!empty($this->auth->loggedUser->timezone)) $tz = $this->auth->loggedUser->timezone;

        $this->setTimezoneEx($tz);

    }

    public function getTimezones()
    {
        // таймзоны
        $timeZones = [];
        foreach (Datetimeex::getTimezones() as $tz) {
            $timeZones[$tz['canonical_name']] = "[".$tz['offset_string']."] - ".$tz['localized_name'];
        }

        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $timeZones;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * метод для валидации вводимых в форму значений
     * проверяет уникальность значения поля в пределах таблицы (кроме случая, когда <table>.primary_key == $excluding_id - для сохранения сущности с тем же email)
     *
     * @param string fname - имя поля в таблице - 'email'
     * @param string fvalue - проверяемое значение
     * @param mixed(scalar) excluding_id - значение primary key, запись с которым должна быть проигнорирована при проверке
     *
     * @return boolean - true - проверяемые данные в таблице отсутствуют
     *                 - false - проверяемые данные в таблице присутствуют
     */
    public function isUnique()
    {
        $fname = $this->input->post('fname');
        $fvalue = $this->input->post('fvalue');

        if (empty($fname) || empty($fvalue) )
        {
            $status = STATUS_MISSING;
            $message = STATUS_MISSING_MSG;
            $data = false;

            return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
        }

        $excluding_id = $this->input->post('excluding_id');

        $res = $this->model->isUnique($fname, $fvalue, $excluding_id);
        $status = STATUS_OK;
        $message = STATUS_OK_MSG;
        $data = $res;

        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * если токен есть - продляет его.
     * Ломиться в /prolong или в admin-zone/prolong - роуты определены
     *
     * @return object - JSON-envelope
     */
    public function prolongToken()
    {
        //ВРЕМЕННО: оставлено для совместимости с фронтом
    }
    
    public function emailvalid() {
        $email = $this->input->post('email');
//$email="sibirko@mailinator.com";
        $data = $this->utils->email_valid($email);
        
        $status = $data ? STATUS_OK : STATUS_EMAIL_NOT_VALID;
        $message = $data ? STATUS_OK_MSG : STATUS_EMAIL_NOT_VALID_MSG;
        
        return $this->utils->jsonOut( $this->utils->envelope($status, $message, $data) );
    }

    /**
     * Выставляет таймзону в системе и БД
     *
     * @param $tz_name string - имя таймзоны. Если параметр опущен, то берется $config['time_reference'] = 'Europe/Moscow';
     *
     * @return boolean
     */
    public function setTimezoneEx($tz_name = null)
    {
        if (empty($tz_name)) $tz_name = $this->config->item('time_reference');

        $res = date_default_timezone_set($tz_name);
        if ($res)
        {
            $tz = new DateTimeZone($tz_name);
            $dt = new DateTime('now', new DateTimeZone('UTC'));
            $o = $tz->getOffset($dt) . ' seconds';
            $dateOffset = clone $dt;
            $dateOffset->sub(DateInterval::createFromDateString($o));
            $dbOffset = $dateOffset->diff($dt)->format('%R%H:%I');
            $this->db->query("SET TIME_ZONE = '{$dbOffset}'");

            //$this->db->query("SET sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
            //$this->db->query("SET sql_mode = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");

            return true;
        }
        else
        {
            return false;
        }
    }

    /**Метод распарсивает переданные ServerSideParams в вид, пригодный для вызова getByParams
     *
     * @param $data
     *
     * @return array
    */
    protected function transformServerSideParams($data)
    {
        if (empty($data->serverParams)) return [];

        $serverParams = $data->serverParams;
        $params = $order = $limit = null;

        if (!empty($serverParams->columnFilters)) {
            foreach ($serverParams->columnFilters as $fname => $val) {
                if (!empty($val)) {
                    $params[] = [$fname, " LIKE ", "%" . $val . "%"];
                }

            }
        }

        if (!empty($serverParams->sort)) {
            foreach ($serverParams->sort as $sort) {
                $order[] = $sort->field . " " . $sort->type;
            }
        }

        if (!empty($serverParams->perPage)) {
            $perPage = intval($serverParams->perPage);
            $limit['limit'] = $perPage;
        }

        if ((!empty($serverParams->page)) && (!empty($perPage))) {
            $page = intval($serverParams->page) - 1;
            $limit['offset'] = $page * $perPage;
        }
        /*
                echo "<pre>params:<br>";
                print_r($params);
                echo "</pre>";
                echo "<pre>order:<br>";
                print_r($order);
                echo "</pre>";

                echo "<pre>limit:<br>";
                print_r($limit);
                echo "</pre>";
                die();
        */
        return ['params' => $params, 'order' => $order, 'limit' => $limit];
    }

}

