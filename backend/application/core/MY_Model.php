<?php use Mvc\Db;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class MY_Model
 *
 * @property CI_DB $db
 * @property Logs_model $logs_model
 */
class MY_Model extends CI_Model
{
    public $table;
    public $entity; // сущность , с которой оперирует данная модель-потмок. Нужна для верного заполнения логов
    public $primary;
    public $posfield; // string.поле, содержащее позицию, если есть
    public $delimfield; // string. поле, являющееся "ограничителем" позиции. Например: category_id
    public $error;

    public $order_by_field;
    public $order_by_dir = 'ASC';
    public $default_field_list; // это список полей по умолчанию, участвующий в формировании списков сущностей. задается в потомках

    public $fields;
    public $pref = '';

    private $delimvalue;

    protected $table_full;
    protected $primary_full;
    protected $posfield_full;
    protected $delimfield_full;

    private $fields_info;
    protected $CI;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->CI =& get_instance();

        $this->table_full = $this->db->dbprefix.$this->table;
        $this->primary_full = $this->table_full.".".$this->primary;

        if (!empty($this->posfield))
        {
            $this->posfield_full = $this->table_full.".".$this->posfield;
        }

        if (!empty($this->delimfield))
        {
            $this->delimfield_full = $this->table_full.".".$this->delimfield;
        }
    }

    /** добавлять ли дополнительные "" к $value. Для функции getByParams - в данный момент не используется
     *
     * @param array $data
     *
     * @return boolean
     */
    function needToQuote($value)
    {
        $value=strtoupper($value);
        if ( ($value=="ON")||($value=="YES")||($value=="NO")||($value=="NULL") ) return true;
        $res1 = filter_var($value, FILTER_VALIDATE_FLOAT);
        $res2 = filter_var($value, FILTER_VALIDATE_INT);
        $res3 = is_bool(filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE))===true;

        return !($res1 OR $res2 OR $res3);
    }

    /** переводит $query->field_info() в другой формат, где индексом является name;
     *
     * @param array $data
     *
     * @return array
     */
    protected function setFieldsInfo($data)
    {
        $result = [];

        foreach ($data as $field)
        {
            switch ($field->type)
            {

                case MYSQLI_TYPE_LONGLONG: // 8 = bigint
                case MYSQLI_TYPE_LONG: // 3 = int
                case MYSQLI_TYPE_TINY: // 1 = tinyint
                case MYSQLI_TYPE_SHORT: // 2 = smallint
                case MYSQLI_TYPE_INT24: // 9 = mediumint
                case MYSQLI_TYPE_YEAR: // 13 = year
                    $type =1;//'integer';
                break;

                case MYSQLI_TYPE_DECIMAL: // 0 = decimal
                case MYSQLI_TYPE_NEWDECIMAL: // 246 = decimal
                case MYSQLI_TYPE_FLOAT: // 4 = float
                case MYSQLI_TYPE_DOUBLE: // 5 = double
                    $type = 2;//'float';
                break;

                case MYSQLI_TYPE_BIT: // 16 = bit
                    $type = 3;//'boolean';
                break;

                default:
                    $type = 4;//'string';
            }

            $result[$field->name] = $type;
        }

        return $result;
    }


    /** оставляет только те пары поле=>значение, поля которых присутствуют в $this->fields
     * @param array $data
     *
     * @return array
     */
    public function filterFields($data)
    {
        $res = [];

        foreach($data as $fname => $val)
        {
            if (in_array($fname, $this->fields))
            {
                $res[$fname] = trim($val);
            }
        }

        return $res;
    }

    /** Добавляет данные
     *
     * @param array $data
     *
     * @return int id | -1 on error
     */
    public function insert($data)
    {
        $data = $this->filterFields($data);
        foreach ($data as $fname=>$fval)
        {
            $d = strtoupper($fval);
            if ( (is_null($d)) || ($d=='NULL') ) $fl = false; else $fl = true;
            $this->db->set($fname, $fval, $fl);
        }
/*
echo "<pre>";
print_r($this->db->get_compiled_insert($this->table));
echo "</pre>";
die();
*/
        $res = $this->db->insert($this->table);

        if ($res)
        {
            $id = $this->db->insert_id();

            $logData = [
                'entity' => $this->entity,
                'entity_id' => $id,
                'event' => 'create',
                'additional_data' => $this->getById($id),
            ];
            $this->toLog($logData);

            return $id;
        }

        return -1;
    }

    /** Изменяет данные
     *
     * @param int $id
     * @param array $data
     * @param boolean $escape - если true, то будет попытке escape переменную
     *
     * @return int id
     */
    public function update($id, $data, $escape = true)
    {
        $data = $this->filterFields($data);

        $this->db->where($this->primary, $id);

        foreach ($data as $fname=>$fval)
        {
            $d = strtoupper($fval);
            if ( (is_null($d)) || ($d=='NULL') || !$escape) $fl = false; else $fl = true;
            $this->db->set($fname, $fval, $fl);
        }
        $res = $this->db->update($this->table);
        //$res = $this->db->update($this->table, $data);

        if ($res)
        {
            $ret_val = $this->db->affected_rows();
        }
        else
        {
            $ret_val = false;
        }

        $logData = [
            'entity' => $this->entity,
            'entity_id' => $id,
            'event' => 'update',
            'additional_data' => $this->getById($id),
        ];
        $this->toLog($logData);

        return $ret_val;
    }

    /** Удаляет данные
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        $this->db->where($this->primary, $id);
        $res = $this->db->delete($this->table);

        if ($res) {
            $logData = [
                'entity' => $this->entity,
                'entity_id' => $id,
                'event' => 'delete',
            ];
            $this->toLog($logData);
        }

        return $res;
    }

    /** Выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function setDeleted($id)
    {
        $data = [
            'status' => 'deleted'
        ];

        return $this->update($id, $data);
    }

    /** Выставляет status в указанное значение. В потомках может быть переопределен для реализации доп логики
     *
     * @param int $id
     * @param string $status
     *
     * @return int id
     */
    public function setStatus($id, $status = 'inactive')
    {
        $data = [
            'status' => $status
        ];

        return $this->update($id, $data);
    }


    /** Производит удаление по заданным параметрам. Соединение WHERE через AND
     *
     *   @param array $params - массив массивов. $params = [
     *                                               ["admin_name","=","Василий"],
     *                                               ["admin_login","=","Vas"],
     *                                           ];
     * @return int - affected rows count
     *
     * Пример: $params = [
     *          ["admin_name","=","'Василий'"],
     *          ["admin_lastname","=","'Конь'"],
     *          ];
     */
    public function deleteByParams($params = [])
    {
        if ( empty($params) ) return -1;

        foreach ($params as $key=>$param)
        {
            $field = $param[0]; $relation = $param[1]; $value = $param[2];
            //$value = $this->db->escape($value);
            //$this->db->where($field.$relation.$value, null, false);
//            if ($this->needToQuote($value)) $value = $this->db->escape($value);
            $value = $this->db->escape($value);
            $this->db->where($field.$relation.$value, null, false);
        }

        /*
        $sql = $this->db->get_compiled_select($this->table);
        echo "<pre>";
        print_r($sql);
        echo "</pre>";
        die();
        */
        $logData = [
            'entity' => $this->entity,
            'entity_id' => -1,
            'event' => 'delete',
            'additional_data' => $params
        ];
        $this->toLog($logData);

        return $this->db->delete($this->table);
    }


    /** Добавляет или изменяет данные, в зависимости от наличия/отсутствия записи с указанным ID
     *
     * @param array $data
     *
     * @return int id
     */
    public function save($id, $data)
    {
        $rec = $this->getById($id);

        if (!empty($rec))
        {
            //запись с такии ID уже есть - update
            return $this->update($id, $data);
        }
        else
        {
            //записи с такии ID нет - insert
            return $this->insert($data);
        }

    }

    /** Выполняет InsertOrUpdate
     * NOTICE: affected_rows после "insert ... update" возвращает
     * 1 - произошел insert
     * 0 - если произошел update и НИ одно значение НЕ изменилось
     * 2 - если произошел update и ХОТЬ одно значение изменилось
     *
     * @param array $data
     *
     * @return boolean
     */
    public function insertOrUpdate($data)
    {
        $data = $this->filterFields($data);
        foreach ($data as $fname=>$fval)
        {
            $d = strtoupper($fval);
            if ( (is_null($d)) || ($d=='NULL') ) $data[$fname] = null;
        }

        $fields = array_keys($data);
        $fields_str = join(",",$fields);

        $val_tpl = $expr = "";
        foreach ($fields as $fname)
        {
            $val_tpl.="?,";
            $expr.=$fname."=VALUES(".$fname."),";
        }
        $val_tpl = substr($val_tpl, 0, -1);
        $expr .= " id=LAST_INSERT_ID(id)"; // это хак для получения id через last_insert_id для обоих случаев (insert или update)

        $sql = "INSERT INTO ".$this->table_full." ({$fields_str}) 
        VALUES ({$val_tpl})
        ON DUPLICATE KEY UPDATE {$expr}";

        $res = $this->db->query($sql, $data); // вернет true или false

        if ($res)
        {
            $id = $this->db->insert_id();

            $logData = [
                'entity' => $this->entity,
                'entity_id' => $id,
                'event' => 'create',
            ];
            $this->toLog($logData);

            return $id;
        }
        else return false;
    }

    /** Собирает join'ы для базового getByParams - метод для переопределения в потомках
     *
     * @return void
     */
    public function processJoins()
    {
    }

    /** Собирает дополнительные условия where для базового getByParams - метод для переопределения в потомках
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
    }

    /** Производит выборку по параметрам. Соединение WHERE через AND
     *
     *   @param array $params - массив массивов. $params = [
     *                                               ["admin_name","=","Василий"],
     *                                               ["admin_login","=","Vas"],
     *                                           ];
     *   @param array $order - array of string $order = [
     *                                                  "admin_name ASC",
     *                                                  "admin_login DESC",
     *                                         ];
     *   @param array $limit - массив $limit = [
     *                                           "limit" => 10,
     *                                           "offset" => 10,
     *                                          ];
     *   @param array $fields - список возвращаемых полей $fields = ['id','name'];
     *   @param boolean $returnForServerSide - возвращать ли результат в виде ['totalRows'=>XXX, 'rows'=>array of StdClass of rows ](это нужно для VUE DataTables remote mode)
     *                                         либо просто в виде - array of StdClass of rows - для любых остальных случаев
     * @return array
     *
     * Пример: $params = [
     *          ["admin_name","=","'Василий'"],
     *          ["admin_lastname","=","CONCAT('Vas',admin_lastname)"],
     *          ];
     */
    public function getByParams($params = [], $order = [], $limit = [], $fields = [], $returnForServerSide = false)
    {
        $this->db->reset_query();
        if (empty($order))
        {
            $order = [$this->order_by_field." ".$this->order_by_dir];
        }
        if (empty($fields))
        {
            $fields = $this->default_field_list;
        }

        $this->db->order_by( join(",",$order));

        if ($returnForServerSide)
        {
            $this->db->select( "SQL_CALC_FOUND_ROWS ".join(",",$fields), false);
        }
        else
        {
            $this->db->select( join(",",$fields), false );
        }

        $this->processJoins();

        if (!empty($params))
        {
            if (is_array($params)) // параметры заданы массивом
            {
                foreach ($params as $key => $param)
                {

                    if (is_array($param) && count($param)>1)
                    {
                        // параметры - это массив из 3 элементов
                        $field = $param[0];
                        $relation = $param[1];
                        $value = $param[2];

                        // если в "имени поля" нет '.', и это НЕ 'служебное слово' то добавляем к имени поля текущую таблицу-
                        // ПОМНИТЬ ОБ ЭТОМ при использовании params вида count(id) -> должно быть COUNT(id) (чтобы не сработало второе условие)
                        if ((strpos($field, ".") === false) && (strtolower($field) == $field))
                        {
                            $field = $this->table_full . "." . $field;
                        }

                        $value = $this->db->escape($value);
                        $this->db->where($field . $relation . $value, null, false);
                    }
                    else
                    {
                        // параметры - это строки
                        $this->db->where($param[0], null, false);
                    }
                }//foreach
            }
            else // параметры заданы строкой.
            {
                $this->db->where($params, null, false);
            }
        }

        $this->processAdditionalWhere();

        if (!empty($limit)) $this->db->limit($limit['limit'], $limit['offset'] );
/*
$sql = $this->db->get_compiled_select($this->table);
echo "<pre>";
print_r($sql);
echo "</pre>";
die();
*/
        $res = $this->db->get($this->table);
        $rows = $res->result();
        //$this->fields_info = $this->setFieldsInfo($res->field_data()); //НЕ УДАЛЯТЬ!!!

        $rows = $this->prepareOutput($rows);

        if ($returnForServerSide)
        {
            $totalRecords = $this->db->query("SELECT FOUND_ROWS() as rowcount")->result()[0]->rowcount;
            return ['totalRows'=>$totalRecords, 'rows'=>$rows];
        }

        return $rows;
    }

    /** Производит update по параметрам. Соединение WHERE через AND
     *
     *   @param array $params - массив массивов. $params = [
     *                                               ["admin_name","=","Василий"],
     *                                               ["admin_login","=","Vas"],
     *                                           ];
     *   @param array $data - массив массивов. $data = [
     *                                               ["admin_name","Василий"],
     *                                               ["admin_pass=md5(admin_login)"],
     *                                           ];
     * @param boolean $escape - если true, то будет попытке escape переменную
     *
     * @return array
     */
    public function updateByParams($params, $data, $escape=true)
    {
        $this->db->reset_query();

        if (!empty($params))
        {
            if (is_array($params)) // параметры заданы массивом
            {
                foreach ($params as $key => $param)
                {

                    if (is_array($param) && count($param)>1)
                    {
                        // параметры - это массив из 3 элементов
                        $field = $param[0];
                        $relation = $param[1];
                        $value = $param[2];

                        // если в "имени поля" нет '.', и это НЕ 'служебное слово' то добавляем к имени поля текущую таблицу-
                        // ПОМНИТЬ ОБ ЭТОМ при использовании params вида count(id) -> должно быть COUNT(id) (чтобы не сработало второе условие)
                        if ((strpos($field, ".") === false) && (strtolower($field) == $field))
                        {
                            $field = $this->table_full . "." . $field;
                        }

                        $value = $this->db->escape($value);
                        $this->db->where($field . $relation . $value, null, false);
                    }
                    else
                    {
                        // параметры - это строки
                        $this->db->where($param[0], null, false);
                    }
                }//foreach
            }
            else // параметры заданы строкой.
            {
                $this->db->where($params, null, false);
            }
        }

        if (!empty($data))
        {

            $data = $this->filterFields($data);

            foreach ($data as $field => $value)
            {
                // данные - это массив из 2 элементов
                $d = strtoupper($value);
                if ( (is_null($d)) || ($d=='NULL') || !$escape) $fl = false; else $fl = true;
                // если в "имени поля" нет '.', и это НЕ 'служебное слово' то добавляем к имени поля текущую таблицу-
                // ПОМНИТЬ ОБ ЭТОМ при использовании params вида count(id) -> должно быть COUNT(id) (чтобы не сработало второе условие)
                if ((strpos($field, ".") === false) && (strtolower($field) == $field))
                {
                    $field = $this->table_full . "." . $field;
                }

                $this->db->set($field, $value, $fl);
            }//foreach
        }

        $this->processAdditionalWhere();
/*
$sql = $this->db->get_compiled_update($this->table);
echo "<pre>";
print_r($sql);
echo "</pre>";
die();
*/
        $res = $this->db->update($this->table);
        if ($res)
        {
            $ret_val = $this->db->affected_rows();
        }
        else
        {
            $ret_val = false;
        }

        $additional_data = [
            'update_params' => $params,
            'update_data' => $data,
        ];
        $logData = [
            'entity' => $this->entity,
            'entity_id' => null,
            'event' => 'update',
            'additional_data' => $additional_data
        ];
        $this->toLog($logData);

        return $ret_val;
    }

    /** подготавливает вывод  - производит необходимые манипуляции в выводном множестве
     *
     * @params (array of StdClass) $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
/*
    //НЕ УДАЛЯТЬ!!! это способ получить ответ в "правильных" типах данных , если есть поля типа DECIMAL
    //в нашем случае выполнена замена типов полей DECIMAL->float
        foreach ($data as $key => &$elem)
        {
            foreach ($elem as $field => $val)
            {
                if (is_null($val)) continue;
                if (isset($this->fields_info[$field]))
                {
                    switch ($this->fields_info[$field])
                    {
                        case 1: $elem->{$field} = intval($val);
                            break;
                        case 2: $elem->{$field} = floatval($val);
                            break;
                        case 3: $elem->{$field} = boolval($val);
                            break;
                        case 4: $elem->{$field} = (string)$val;
                            break;
                        default: $elem->{$field} = (string)$val;
                    }
                }
            }
        }
*/
        return $data;
    }

    /** Выборка по ID
     *
     * @param int $id
     *
     * @return array | null
     */
    public function getById($id)
    {
        if (isset($id))
        {
            $params = [
                [$this->primary, "=", $id]
            ];
            $res = $this->getByParams($params);

            if (!empty($res)) return $res[0];
        }

        return null;
    }

    /** Выборка по списку ID
     *
     * @param array $ids
     *
     * @return array of objects | null
     */
    public function getByIds($ids)
    {
        if (is_array($ids)) $ids = join(',',$ids);

        $params = $this->table_full.".id IN (".$ids.")";
        $list = $this->getByParams($params);

        return $list;
    }
    /** Выборка АКТИВНОГО (status='active') по ID
     *
     * @param int $id
     *
     * @return array | null
     */
    public function getActiveById($id)
    {
        $params = [
            [ $this->primary, "=", $id ],
            [ 'status', "=", 'active' ]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }

    /** Выборка по name
     *
     * @param string $name
     *
     * @return object
     */
    public function getByName($name)
    {
        $params = [
            ['name', "=" , $name]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }

    /** Выборка по email
     *
     * @param string $email
     *
     * @return object
     */
    public function getByEmail($email)
    {
        $params = [
            ['email', "=" , $email]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }

    /** Формирование списка из полей
     *
     * @param array $fields = ['field1', 'field2' ...]
     * @param array $params = [
     *                          'fieldname1', '>=', 'value1'
     *                          ...
     *                        ]
     *
     * @return array of objects | null
     */
    public function getList($params = [], $fields = [])
    {
        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** проверяет, существует ли в таблице переданная пара поле-значение (кроме случая, когда $this->primary == $excluding_id)
     *  метод нужен для валидации ввода
     *
     * @param string $fname - имя поля в таблице - 'email'
     * @param string $fvalue - проверяемое значение
     * @param mixed(scalar) $excluding_id - значение primary key, запись с которым должна быть проигнорирована при проверке
     *
     * @return boolean - true - проверяемые данные в таблице отсутствуют
     *                 - false - проверяемые данные в таблице присутствуют
     */
    public function isUnique($fname, $fvalue, $excluding_id = null)
    {
        $data = [$fname=>$fvalue];
        $data = $this->filterFields($data);

        if (empty($data)) return false; // случай, когда ключ в переданном массиве не являеся одним из полей таблицы - сразу false

        $this->db->select($this->primary);
        $this->db->where($fname, $fvalue);
        if (!empty($excluding_id))
        {
            $this->db->where($this->primary."!=", $excluding_id);
        }
        $this->db->limit(1);

        $res = $this->db->get($this->table)->row();

        if (empty($res)) return true;

        return false;
    }

    /** Записывает данные в Logs
     *
     * @param array $data
     *
     * @return int | void
     */
    public function toLog($data)
    {
        if (empty($this->entity)) return; // избежать самологгирования

        // автор не задан в данных - пробуем определить ------------
        if (empty($data['author']))
        {
            // кто-то залогинен
            if ($this->auth->isLogged())
            {
                if (!empty($this->auth->role)) $data['author'] = $this->auth->role;
                if (!empty($this->auth->loggedUser->id)) $data['author_id'] = $this->auth->loggedUser->id;
            }
            else
            {
                $data['author'] = 'system';
                $data['author_id'] = 'NULL';
            }
        }
        // автор не задан в данных - пробуем определить -------------

        if (!empty($data['additional_data'])) $data['additional_data'] = json_encode($data['additional_data'], JSON_PRETTY_PRINT);

        return $this->logs_model->insert($data);
    }

    /**
     * сквозная нумерация при изменении позиции
     * private $this->delimvalue - его значение "поля-ограничителя". Например: category_id
     *
     *
     * @return Void
     */
    public function renumeratePos()
    {
        $where = !empty($this->delimfield) ? "WHERE ".$this->delimfield."=".$this->delimvalue : '';

        //echo"<br>----------$query---------<br>";
        $res = $this->db->query(
            "SELECT ".$this->primary_full." FROM ".$this->table_full." ".$where." ORDER BY ".$this->posfield_full
        );

        $res = $res->result();
        foreach ($res as $i => $item) {
            $this->db->query("UPDATE ".$this->table_full." SET ".$this->posfield_full." = ".(++$i)." WHERE ".$this->primary_full." = " . $item->{$this->primary});
        }
    }

    /**
     * добавление элемента с pos
     *
     * @param array $data
     *
     * @return int
     */
    public function insertWithPos($data)
    {
        $this->delimvalue = intval($data[$this->delimfield]);
        $pos = intval($data[$this->posfield]);

        if (empty($pos)) {
            $pos = 10000;
        }

        if (!empty($this->delimfield))
        {
            $TAIL = " AND ".$this->delimfield_full."=".$this->delimvalue;
        } else {
            $TAIL="";
        }

        $this->db->query("UPDATE ".$this->table_full." SET ".$this->posfield_full." = ".$this->posfield_full."+1 WHERE ".$this->posfield_full." >= ".$pos." ".$TAIL);

        //$result = $this->insertReal($data);
        $result = self::insert($data);

        $this->renumeratePos();

        return $result;
    }

    /**
     * изменение элемента с pos
     *
     * @param int $id
     * @param array $data
     * @param boolean $escape - если true, то будет попытке escape переменную
     *
     * @return int
     */
    public function updateWithPos($id, $data, $escape=true)
    {
        $item = $this->getById($id);

        if (!empty($data[$this->delimfield]))
        {
            $this->delimvalue = intval($data[$this->delimfield]);
        }
        else
        {
            $this->delimvalue = $item->{$this->delimfield};
        }

        $newpos = intval($data[$this->posfield]);

        if (!empty($newpos))
        {
            $oldpos = intval($item->{$this->posfield});

            if (!empty($this->delimfield)) {
                $TAIL = " AND ".$this->delimfield_full."=".$this->delimvalue;
            } else {
                $TAIL="";
            }

            if ($oldpos < $newpos)
            {
                $this->db->query("UPDATE ".$this->table_full." SET ".$this->posfield_full." = ".$this->posfield_full."-1 WHERE ".$this->posfield_full."<=".$newpos." AND ".$this->posfield_full.">".$oldpos.$TAIL);
            }

            if ($oldpos > $newpos) {
                $this->db->query("UPDATE ".$this->table_full." SET ".$this->posfield_full." = ".$this->posfield_full."+1 WHERE ".$this->posfield_full.">=".$newpos.$TAIL);
            }

        }

        $result = self::update($id, $data, $escape);

        if ((!empty($newpos)) && ($newpos != $oldpos) )
        {
            $this->renumeratePos();
        }

        return $result;
    }

    /**
     * удаление элемента с pos
     *
     * @param int $id
     *
     * @return int
     */
    public function deleteWithPos($id)
    {
        $item = $this->getById($id);
        $this->delimvalue = $item->{$this->delimfield};

        $result = self::delete($id);

        if (!empty($this->delimfield)) {
            $TAIL = " AND ".$this->delimfield."=".$this->delimvalue;
        } else {
            $TAIL="";
        }

        $this->db->query("UPDATE ".$this->table_full." SET ".$this->posfield_full." = ".$this->posfield_full."-1 WHERE ".$this->posfield_full.">".$item->{$this->posfield}.$TAIL);

        return $result;
    }


    /** Формирование списка из полей для формата VUE DataTable remote mode
     *
     * @param array $fields = ['field1', 'field2' ...]
     * @param array $params = [
     *                          'fieldname1', '>=', 'value1'
     *                          ...
     *                        ]
     *
     * @return array of objects | null
     */
    public function getListAdv($params = [], $order = [], $limit = [], $fields = [])
    {
        $res = $this->getByParams($params, $order, $limit, $fields, true);

        return $res;
    }

}
