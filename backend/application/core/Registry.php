<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

/**
 * Singleton. DO NOT LOAD VIA $this->load->library()!!!
 * USAGE:
 *  Registry::set('asd', $value);
 *  $a = Registry::get('asd');
 */

class Registry
{
    protected static $data = array();
    protected static $_instance = null;

    /*
        protected $CI;
        public function __construct() {
            $this->CI =& get_instance();
        }
    */

    private function __construct() {}
    private function __clone() {}
    private function __wakeup () {}

    static public function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /** Читает данные по ключу $key
     *
     * @param string | array $key - если ключ является массивом, то массив читается из registry по каждому ключу и возврат массива с этими ключами
     *
     * @return mixed | array of mixed
     */
    static public function get($key)
    {
        if (is_array($key))
        {
            $result = array();
            foreach ($key as $k) {
                $result[$k] = self::get($k);
            }
            return $result;
        }

        $key = (string)$key;

        if ($key != '' && array_key_exists($key, self::$data))
        {
            return self::$data[$key];
        }

        return null;
    }

    /** Возвпащает все данные Registry
     *
     * @return array
     */
    static public function get_all()
    {
        return self::$data;
    }

    /** Устанавливает данные по ключу $key
     *
     * @param string | array $key - если $key является массивом, то массив записывается в registry по каждому ключу отдельно
     *
     * @return void
     */
    static public function set($key, $value = null)
    {
        if (is_array($key))
        {
            foreach ($key as $k => $v)
            {
                self::set($k, $v);
            }
        }

        self::$data[(string)$key] = $value;
    }

    /** Проверяет наличие элемента с ключом $key
     *
     * @param string | array $key - если $key является массивом, то удаляются последовательно все элементы, соотвтетвующие ключам массивамассив записывается в registry по каждому ключу отдельно
     *
     * @return boolean
     */
    static public function has($key)
    {
        $key = (string)$key;

        return $key != '' && array_key_exists($key, self::$data);
    }

    /** Удаляет элементы с ключом $key из registry
     *
     * @param string | array $key - если $key является массивом, то удаляются последовательно все элементы, соотвтетвующие ключам массивамассив записывается в registry по каждому ключу отдельно
     *
     */
    public function delete($key)
    {
        if (is_array($key))
        {
            foreach ($key as $k)
            {
                self::delete($k);
            }
        }

        $key = (string)$key;

        if ($key != '' && array_key_exists($key, self::$data))
        {
            unset(self::$data[$key]);
        }
    }

}