<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['dashboard'] = 'Информационная доска';
$lang['admin_pages'] = 'Статические страницы';
$lang['referals_pages'] = 'Реферальные ссылки';
$lang['settings'] = 'Настройки';
$lang['menu'] = 'Меню';
$lang['widgets'] = 'Виджеты';
$lang['admin_media'] = 'Библиотека загруженных файлов';
$lang['admin_media_upload'] = 'Загрузка файлов';
$lang['new_page_title'] = 'Создание новой страницы';
$lang['new_referal_title'] = 'Создание новой рефералки';
$lang['new_widget_title'] = 'Создание нового виджета';
$lang['edit_page_title'] = 'Редактирование страницы';
$lang['edit_referal_title'] = 'Редактирование рефералки';
$lang['edit_menu_title'] = 'Редактирование меню';
$lang['edit_widget_title'] = 'Редактирование виджета';
