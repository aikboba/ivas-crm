<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Auth
 * @property tokens_model $tokens_model

 * Name: Бибилиотека со вспомогательным функционалом авторизации
*/
class Auth
{
    protected $CI;
    protected $model;

    public $role = 'admin'; // роль(тип) пользователя, которого пробовать поднять из токена
    public $error = []; // содержит [error_code=>'<code>', error_msg=>'<msg>']
    public $error_code = 0; // содержит error_code
    public $error_msg = 0; // содержит error_code
    public $token = null; // содержит строку токена
    public $token_data = []; // данные из токена

    private $_loggedUser = null; // содержит информацию о залогиненном пользователе
    private $token_header = 'auth-x'; // header для передачи токена

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('tokens_model');
    }

    public function __set($name, $value)
    {
        switch($name)
        {
            case 'loggedUser':
                    if (!empty($value->id))
                    {

                        if ($this->role == 'admin')
                        {
                            if (empty($value->permissions))
                            {
                                $value->permissions = $this->CI->permissions_model->getByAdminId($value->id);
                            }
                            if (empty($value->admingroups))
                            {
                                $value->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($value->id);
                            }

                        }
                        else
                        {
                            if (empty($value->privileges))
                            {
                                // получаем группы льгот клиента ---
                                $value->privileges = $this->CI->cli_priv_model->getPrivilegesByCliId($value->id);
                            }
                            if (empty($value->groups))
                            {
                                // получаем группы клиента ---
                                $value->groups = $this->CI->clients_groups_model->getGroupsByClientId($value->id);
                            }
                        }
                    }
                    $this->_loggedUser = $value;
                break;

        }
    }

    public function __get($name)
    {
        switch($name)
        {
            case 'loggedUser':
                return $this->_loggedUser;
                break;
        }
    }

    private function setError($ERR = null)
    {
        $this->error = (!empty($ERR)) ? $ERR : ['error_code'=>0, 'error_msg'=>''];
        $this->error_code = (!empty($ERR['error_code'])) ? $ERR['error_code'] : 0;
        $this->error_msg = (!empty($ERR['error_msg'])) ? $ERR['error_msg'] : '';
    }

    /**
     * Устанавливает токен в headers
     *
     * @param string $token - строка-токен
     *
     * @return boolean
     */
    public function tokenSet($token)
    {
        $this->token = $token;
        $this->setError( null );

        if ($this->token)
        {
            $this->CI->output->set_header($this->token_header.': '.$this->token);

            return $this->token;
        }

        return false;
    }

    /**
     * Принимает токен из headers или параметра $token
     *
     * @param string|null $token_string - строка-токен
     *
     * @return object | boolean false - токен либо ошибку(в $this->error)+boolean false
     */
    public function tokenGet($token_string = null)
    {
        $this->setError(null);

        // токен указан в строке на входе
        if ( !empty($token_string) )
        {
            $this->token = $token_string;
            $this->setError( null );

            if (!empty($this->token))
            {
                return $token_string;
            }
        }

        //поиск в хэдэрах
        $headers = $this->CI->input->request_headers();

        $headersLO = [];
        foreach($headers as $key=>$val)
        {
            $headersLO[strtolower($key)]=$val;
        }

        // токен указан Authorization header
        if ( (array_key_exists($this->token_header, $headersLO)) && (!empty($headersLO[$this->token_header])) )
        {
            $token = $headersLO[$this->token_header];

            $this->setError( null );

            if (!empty($token))
            {
                $this->token = $token;
                return $token;
            }
        }

        $this->setError( STATUS_TOKEN_MISSING );
        $this->token = null;
        return false;
    }

    /**
     * Убирает юзера из "сессии" и удаляет его текущий токен из таблицы
     */
    public function logout()
    {
        $this->setError(null);
        $this->loggedUser = null;
        $this->CI->tokens_model->removeByToken($this->token);
        $this->token = null;
    }

    /**
     * Извлекает юзера, используя $this->token. Устанавливает $this->loggedUser
     *
     * @return object | boolean false - возвращает userData либо ошибку(в $this->error)+boolean false
     */
    public function loginFromToken()
    {
        if ($this->role == 'admin')
        {
            $this->CI->load->model('admins_model');
            $this->model = $this->CI->admins_model;
            $this->CI->load->model('permissions_model');
            $this->CI->load->model('admins_admingroups_model');
        }
        else
        {
            $this->CI->load->model('clients_model');
            $this->model = $this->CI->clients_model;
            $this->CI->load->model('cli_priv_model');
            $this->CI->load->model('clients_groups_model');
        }

        $this->setError(null);

        $id = null;

        if (empty($this->token))
        {
            $this->setError( ['error_code'=>STATUS_TOKEN_MISSING, 'error_msg'=>"Ошибка: токен не найден"] );
            $this->loggedUser = null;

            return false;
        }

        // получаем по токену id
        $this->token_data = $this->CI->tokens_model->getByToken($this->token);

        if (empty($this->token_data))
        {
            $this->setError( ['error_code'=>STATUS_TOKEN_MISSING, 'error_msg'=>"Ошибка: данные по токену не найдены"] );
            $this->loggedUser = null;

            return false;
        }

        if ($this->token_data->entity != $this->role)
        {
            $this->setError( ['error_code'=>STATUS_TOKEN_WRONG, 'error_msg'=>"Ошибка: токен не соответствует роли"] );
            $this->loggedUser = null;

            return false;
        }
        // апдейтим токен -----------------------
        $uData = [
            'last_used_dt' => 'NOW()',
        ];
        $this->CI->tokens_model->update($this->token_data->id, $uData);
        // END апдейтим токен -------------------

        /** @var MY_Model $this->model*/
        $id = $this->token_data->entity_id;
        $userData = $this->model->getActiveById($id);

        if (empty($userData))
        {
            $this->setError( ['error_code'=>STATUS_NOT_FOUND, 'error_msg'=>"Ошибка: активный пользователь с id=".$id." не найден в ".$this->role] );
            $this->loggedUser = null;

            return false;
        }

        $this->loggedUser = (object)$userData;

        return $this->loggedUser;
    }

    /**
     * Проверяет, есть ли у loggedUser разрешение на операцию $permission_slug
     *
     * @param string $permission_slug
     *
     * @return int
     */
    public function isAllowed($permission_slug)
    {
        if (empty($this->loggedUser->permissions)) return 0;

        $perm = $this->loggedUser->permissions;

        if ( (empty($perm)) || (empty($perm[$permission_slug])) )
        {
            return 0;// если не залогинен, то вернет "нет прав"
        }
        else return $perm[$permission_slug];
    }

    /**
     * Возвращает залогинен ли пользователь на основании $this->loggedUser
     *
     * @return boolean
     */
    public function isLogged()
    {
        return !empty((array)$this->_loggedUser);
    }

    /**
     * Проверяет, есть ли у loggedUser ВСЕ разрешение, переданные как array of permission_slug
     *
     * @param array $permissions
     *
     * @return boolean
     */
    public function hasPermissions($permissions)
    {
        if (empty($permissions)) return 1; // на входе пусто - таки да, разрешения есть

        if (empty($this->loggedUser->permissions)) return 0; // нет логина, нет разрешений

        $perm = $this->loggedUser->permissions;
        $res = 1;
        foreach ($permissions as $permission_slug)
        {
            if (empty($perm[$permission_slug]))
            {
                $cur = 0;
            } else $cur = $perm[$permission_slug];
            $res = $res * $cur;
        }

        return (boolean)$res;
    }


}
