<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Бибилиотека со вспомогательным функционалом авторизации
*/

class Auth
{
    protected $CI;
    protected $model;

    public $role = 'admin'; // роль(тип) пользователя, которого пробовать поднять из токена
    public $error = []; // содержит [error_code=>'<code>', error_msg=>'<msg>']
    public $error_code = 0; // содержит error_code
    public $error_msg = 0; // содержит error_code
    public $token_type = 'access'; // содержит тип полученного / созданного токена (access | refresh)
    private $_token_data = null; // содержит payload дукодированного токена
    public $token_string = null; // содержит строку токена

    private $_loggedUser = null; // содержит информацию о залогиненном пользователе

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function __set($name, $value)
    {
        switch($name)
        {
            case 'loggedUser':
                    if (!empty($value->id))
                    {
                        if ($this->role == 'admin')
                        {
                            if (empty($value->permissions))
                            {
                                $value->permissions = $this->CI->permissions_model->getByAdminId($value->id);
                            }
                            if (empty($value->admingroups))
                            {
                                $value->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($value->id);
                            }

                        }
                        else
                        {
                            if (empty($value->privileges))
                            {
                                // получаем группы льгот клиента ---
                                $value->privileges = $this->CI->cli_priv_model->getPrivilegesByCliId($value->id);
                            }
                            if (empty($value->groups))
                            {
                                // получаем группы клиента ---
                                $value->groups = $this->CI->clients_groups_model->getGroupsByClientId($value->id);
                            }
                        }
                    }
                    $this->_loggedUser = $value;
                break;

            case 'token_data':
                if (!empty($value->sub))
                {
                    $this->role = $value->sub;
                }
                $this->_token_data = $value;
                break;
        }
    }

    public function __get($name)
    {
        switch($name)
        {
            case 'loggedUser':
                return $this->_loggedUser;
                break;
            case 'token_data':
                return $this->_token_data;
                break;
        }
    }

    private function setError($ERR = null)
    {
        $this->error = (!empty($ERR)) ? $ERR : ['error_code'=>0, 'error_msg'=>''];
        $this->error_code = (!empty($ERR['error_code'])) ? $ERR['error_code'] : 0;
        $this->error_msg = (!empty($ERR['error_msg'])) ? $ERR['error_msg'] : '';
    }

    /**
     * Устанавливает токен в headers
     *
     * @param object|array $payload - object | array - JWT claims
     * @param string $token_type - тип генерируемого токена (access | refresh)
     *
     * @return boolean
     */
    public function tokenSet($payload, $token_type = 'access')
    {
        switch ($token_type)
        {
            case 'refresh': $token_header = 'refresh';
                $this->token_type = 'refresh';
                break;
            case 'access':
            default: $token_header = 'authorization';
                $this->token_type = 'access';
        }

        JWT::init($this->CI->config->item('jwt'));
        JWT::setTokenType($this->token_type);
        JWT::setPayload($payload);
        $this->token_string = JWT::encode();
        $this->setError( JWT::error() );

        if ($this->token_string)
        {
            $this->CI->output->set_header($token_header.': '.$this->token_string);

            return $this->token_string;
        }

        return false;
    }

    /**
     * Принимает токен из headers или $jwt_string
     *
     * @param string $token_type - тип возвращаемого токена (access | refresh)
     *
     * @return object | boolean false - возвращает payload токена либо JWT-ошибку(в $this->error)+boolean false
     */
    public function tokenGet($token_type = 'access', $jwt_string = null)
    {
        $this->setError(null);
        $headers = $this->CI->input->request_headers();

        $headersLO = [];
        foreach($headers as $key=>$val)
        {
            $headersLO[strtolower($key)]=$val;
        }

        switch ($token_type)
        {
            case 'refresh': $token_header = 'refresh';
                            $this->token_type = 'refresh';
                break;
            case 'access':
            default: $token_header = 'authorization';
                     $this->token_type = 'access';
        }

        $decodedToken = false;
        // токен указан Authorization header
        if ( (array_key_exists($token_header, $headersLO)) && (!empty($headersLO[$token_header])) )
        {
            $token = $headersLO[$token_header];
            JWT::init($this->CI->config->item('jwt'));
            $decodedToken = JWT::decode($token);

            $this->setError( JWT::error() );
            $this->token_data = $decodedToken;

            if ($decodedToken != false)
            {
                return $decodedToken;
            }
        }

        // токен указан в строке на входе
        if ( !empty($jwt_string) )
        {
            $token = $jwt_string;
            JWT::init($this->CI->config->item('jwt'));
            $decodedToken = JWT::decode($token);

            $this->setError( JWT::error() );
            $this->token_data = $decodedToken;

            if ($decodedToken != false)
            {
                return $decodedToken;
            }
        }

        $this->setError( JWT::ERROR_TOKEN_NOT_FOUND );
        $this->token_data = $decodedToken;
        return false;
    }

    /**
     * Убирает юзера из "сессии"
     */
    public function logout()
    {
        $this->setError(null);
        $this->loggedUser = null;
    }

    /**
     * Извлекает юзера, используя token_data->id. Устанавливает $this->loggedUser
     *
     * @return object | boolean false - возвращает userData либо ошибку(в $this->error)+boolean false
     */
    public function loginFromToken()
    {
        if ($this->role == 'admin')
        {
            $this->CI->load->model('admins_model');
            $this->model = $this->CI->admins_model;
            $this->CI->load->model('permissions_model');
            $this->CI->load->model('admins_admingroups_model');
        }
        else
        {
            $this->CI->load->model('clients_model');
            $this->model = $this->CI->clients_model;
            $this->CI->load->model('cli_priv_model');
            $this->CI->load->model('clients_groups_model');
        }

        $this->setError(null);

        $id = null;
        if (!empty($this->token_data->id)) $id = $this->token_data->id;

        if (empty($id))
        {
            $this->setError( ['error_code'=>STATUS_NOT_FOUND, 'error_msg'=>"Ошибка: id не найден"] );
            $this->loggedUser = null;

            return false;
        }

        /** @var MY_Model $this->model*/
        $userData = $this->model->getActiveById($id);  /** TO DO: вот здесь прикрутить Redis*/

        if (empty($userData))
        {
            $this->setError( ['error_code'=>STATUS_NOT_FOUND, 'error_msg'=>"Ошибка: активный юзер с id=".$id." не найден в ".$this->role] );
            $this->loggedUser = null;

            return false;
        }

        $this->loggedUser = (object)$userData;

        return $this->loggedUser;
    }

    /**
     * Проверяет, есть ли у loggedUser разрешение на операцию $permission_slug
     *
     * @param string $permission_slug
     *
     * @return int
     */
    public function isAllowed($permission_slug)
    {
        if (empty($this->loggedUser->permissions)) return 0;

        $perm = $this->loggedUser->permissions;

        if ( (empty($perm)) || (empty($perm[$permission_slug])) )
        {
            return 0;// если не залогинен, то вернет "нет прав"
        }
        else return $perm[$permission_slug];
    }

    /**
     * Возвращает залогинен ли пользователь на основании $this->loggedUser
     *
     * @return boolean
     */
    public function isLogged()
    {
        return !empty((array)$this->_loggedUser);
    }

    /**
     * Проверяет, есть ли у loggedUser ВСЕ разрешение, переданные как array of permission_slug
     *
     * @param array $permissions
     *
     * @return boolean
     */
    public function hasPermissions($permissions)
    {
        if (empty($permissions)) return 1; // на входе пусто - таки да, разрешения есть

        if (empty($this->loggedUser->permissions)) return 0; // нет логина, нет разрешений

        $perm = $this->loggedUser->permissions;
        $res = 1;
        foreach ($permissions as $permission_slug)
        {
            if (empty($perm[$permission_slug]))
            {
                $cur = 0;
            } else $cur = $perm[$permission_slug];
            $res = $res * $cur;
        }

        return (boolean)$res;
    }


}
