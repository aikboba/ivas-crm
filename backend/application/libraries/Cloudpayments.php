<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Name: Библиотека для отправки запросов на API cloudpayments
 */
class Cloudpayments
{
    protected $CI;

    protected $url = 'https://api.cloudpayments.ru';

    protected $locale = 'ru-RU';

    public $publicKey;
    public $privateKey;

    public $error_msg = ''; // содержит error_msg
    public $error_code = 0; // содержит error_code

    public $error_user_msg = ''; // содержит собщение пользователю

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->config('cloudpayments', true);

        $this->locale =  $this->CI->config->item('cp_locale', 'cloudpayments');
    }

    /**
     * отправляет запросы через curl на API cloudpayments
     * @param string $endpoint - uri запроса
     * @param array $params - параметры для отправки
     * @param array $headers - дополнительные заголовки для отправки
     * @return array
     */
    protected function sendRequest($endpoint, array $params = [], array $headers = [])
    {
        $params['CultureName'] = $this->locale;
        $headers[] = 'Content-Type: application/json';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->url . $endpoint);
        curl_setopt($curl, CURLOPT_USERPWD, sprintf('%s:%s', $this->publicKey, $this->privateKey));
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        // обработка ошибки если запрос не прошел(к примеру по причине не доступности сервера cloudpayments)
        curl_close($curl);

        return (array) json_decode($result, true);
    }

    /**
     * создает транзакцию из данных пришедшых от cloudpayments
     * @param array $data
     * @return array
     */
    public function createTransactionData($data) {
        $transaction = [];

        $transaction['id'] = $data['TransactionId'];
        $transaction['amount'] = $data['Amount'];
        $transaction['currency'] = $data['Currency'];
        $transaction['currency_code'] = $data['CurrencyCode'];
        $transaction['card_first_six'] = $data['CardFirstSix'];
        $transaction['card_last_four'] = $data['CardLastFour'];
        $transaction['card_expired_month'] = explode('/', $data['CardExpDate'])[0];
        $transaction['card_expired_year'] = substr(date('Y'), 0, 2) . explode('/', $data['CardExpDate'])[1];

        if (isset($data['InvoiceId'])) {
            $transaction['invoice_id'] = $data['InvoiceId'];
        }

        if (isset($data['AccountId'])) {
            $transaction['account_id'] = $data['AccountId'];
        }

        if (isset($data['Email'])) {
            $transaction['email'] = $data['Email'];
        }

        if (isset($data['Description'])) {
            $transaction['description'] = $data['Description'];
        }

        if (isset($data['JsonData'])) {
            $transaction['json_data'] = (array)$data['JsonData'];
        }

        if (isset($data['CreatedDateIso'])) {
            $transaction['created_date_iso'] = $data['CreatedDateIso'];
        }

        if (isset($data['AuthDateIso'])) {
            $transaction['auth_date_iso'] = $data['AuthDateIso'];
        }

        if (isset($data['ConfirmDateIso'])) {
            $transaction['confirm_date_iso'] = $data['ConfirmDateIso'];
        }

        if (isset($data['AuthCode'])) {
            $transaction['auth_code'] = $data['AuthCode'];
        }

        if (isset($data['TestMode'])) {
            $transaction['test_mode'] = $data['TestMode'];
        }

        if (isset($data['IpAddress'])) {
            $transaction['ip_address'] = $data['IpAddress'];
        }

        if (isset($data['IpCountry'])) {
            $transaction['ip_country'] = $data['IpCountry'];
        }

        if (isset($data['IpCity'])) {
            $transaction['ip_city'] = $data['IpCity'];
        }

        if (isset($data['IpRegion'])) {
            $transaction['ip_region'] = $data['IpRegion'];
        }

        if (isset($data['IpDistrict'])) {
            $transaction['ip_district'] = $data['IpDistrict'];
        }

        if (isset($data['IpLatitude'])) {
            $transaction['ip_latitude'] = $data['IpLatitude'];
        }

        if (isset($data['IpLongitude'])) {
            $transaction['ip_longitude'] = $data['IpLongitude'];
        }

        if (isset($data['CardType'])) {
            $transaction['card_type'] = $data['CardType'];
        }

        if (isset($data['CardTypeCode'])) {
            $transaction['card_type_code'] = $data['CardTypeCode'];
        }

        if (isset($data['Issuer'])) {
            $transaction['issuer'] = $data['Issuer'];
        }

        if (isset($data['IssuerBankCountry'])) {
            $transaction['issuer_bank_country'] = $data['IssuerBankCountry'];
        }

        if (isset($data['Status'])) {
            $transaction['status'] = $data['Status'];
        }

        if (isset($data['StatusCode'])) {
            $transaction['status_code'] = $data['StatusCode'];
        }

        if (isset($data['Reason'])) {
            $transaction['reason'] = $data['Reason'];
        }

        if (isset($data['ReasonCode'])) {
            $transaction['reason_code'] = $data['ReasonCode'];
        }

        if (isset($data['CardHolderMessage'])) {
            $transaction['card_holder_message'] = $data['CardHolderMessage'];
        }

        if (isset($data['Name'])) {
            $transaction['card_holder_name'] = $data['Name'];
        }

        if (isset($data['Token'])) {
            $transaction['token'] = $data['Token'];
        }

        return $transaction;
    }

    /** преобразует данные пришедшие из cloudpayments
     * @param array $data
     * @return array
     */
    public function createRequired3DS($data)
    {
        $required3DS = [];
        if (isset($data['TransactionId'])) {
            $required3DS['transaction_id'] = $data['TransactionId'];
        }

        if (isset($data['PaReq'])) {
            $required3DS['token'] = $data['PaReq'];
        }

        if (isset($data['AcsUrl'])) {
            $required3DS['url'] = $data['AcsUrl'];
        }

        return $required3DS;
    }

    /**
     * проверяет ошибки в пришедших данных
     * @param $response
     * @return bool
     */
    public function getResponseError($response)
    {
        if (isset($response['Message']) && $response['Message']) {
            $this->error_msg = $response['Message'];
            $this->error_code = 1;
            return true;
        }

        if (isset($response['Model']) && isset($response['Model']['ReasonCode']) && $response['Model']['ReasonCode'] !== 0) {
            $this->error_code = $response['Model']['ReasonCode'];
            $this->error_msg = $response['Model']['Reason'];
            $this->error_user_msg = $response['Model']['CardHolderMessage'];
            return true;
        }

        return false;
    }

    /**
     * Тестовый запрос на cloudpayments
     * @return bool
     */
    public function test()
    {
        $response = $this->sendRequest('/test');
        if (isset($response['Success']) && $response['Success']) {
            return true;
        }

        return false;
    }

    /**
     * Оплата по криптограмме
     * endpoint = /payments/cards/charge - для одностадийного платежа
     * endpoint = /payments/cards/auth - для двухстадийного
     *
     * @param float $amount - Сумма платежа
     * @param string $currency - Валюта: RUB/USD/EUR/GBP и т.д.
     * @param String $ipAddress - IP адрес плательщика
     * @param String $cardHolderName - Имя держателя карты в латинице
     * @param String $cryptogram - Криптограмма платежных данных
     * @param array $params = [
     * 'InvoiceId' String - Номер счета или заказа
     * 'Description' String - Описание оплаты в свободной форме
     * 'AccountId' String - Идентификатор пользователя (Обязательный для создания подписки и получения токена)
     * 'Email' String - E-mail плательщика на который будет отправлена квитанция об оплате
     * 'JsonData' Json - Любые другие данные, которые будут связаны с транзакцией, зарезервированые параметры name, firstName, middleName, lastName, nick, phone, address, comment, birthDate
     * ] - дополнительные параметры
     * @param bool $requireConfirmation - какой платеж будет выбран true - двухстадийного, false - одностадийного
     * @return array
     */
    public function chargeCard($amount, $currency, $ipAddress, $cardHolderName, $cryptogram, $params = [], $requireConfirmation = false)
    {
        $endpoint = $requireConfirmation ? '/payments/cards/auth' : '/payments/cards/charge';
        $defaultParams = [
            'Amount' => $amount,
            'Currency' => $currency,
            'IpAddress' => $ipAddress,
            'Name' => $cardHolderName,
            'CardCryptogramPacket' => $cryptogram
        ];

        $response = $this->sendRequest($endpoint, array_merge($defaultParams, $params));

        if (isset($response['Success']) && $response['Success']) {
            return $this->createTransactionData($response['Model']);
        }

        $this->getResponseError($response);

        return isset($response['Model']) ? $this->createRequired3DS($response['Model']) : [];
    }

    /**
     * Метод для оплаты по токену
     * endpoint = /payments/tokens/auth - для двухстадийного
     * endpoint = /payments/tokens/charge - для одностадийного платежа
     *
     * @param float $amount - Сумма платежа
     * @param string $currency - Валюта: RUB/USD/EUR/GBP и т.д.
     * @param string $accountId - Идентификатор пользователя
     * @param string $token - Токен
     * @param array $params = [
     * 'InvoiceId' String - Номер счета или заказа
     * 'Description' String - Описание оплаты в свободной форме
     * 'IpAddress' String - IP адрес плательщика
     * 'Email' String - E-mail плательщика на который будет отправлена квитанция об оплате
     * 'JsonData' Json - Любые другие данные, которые будут связаны с транзакцией, зарезервированые параметры name, firstName, middleName, lastName, nick, phone, address, comment, birthDate
     * ] - дополнительные параметры
     * @param bool $requireConfirmation - какой платеж будет выбран true - двухстадийного, false - одностадийного
     * @return array
     */

    public function chargeToken($amount, $currency, $accountId, $token, $params = [], $requireConfirmation = false)
    {
        $endpoint = $requireConfirmation ? '/payments/tokens/auth' : '/payments/tokens/charge';
        $defaultParams = [
            'Amount' => $amount,
            'Currency' => $currency,
            'AccountId' => $accountId,
            'Token' => $token,
        ];

        $response = $this->sendRequest($endpoint, array_merge($defaultParams, $params));

        if (isset($response['Success']) && $response['Success']) {
            return $this->createTransactionData($response['Model']);
        }

        $this->getResponseError($response);

        return isset($response['Model']) ? $this->createRequired3DS($response['Model']) : [];
    }

    /*
     * Метод для завершения оплаты 3-D Secure
     *
     * @param Int $transactionId - Значение параметра MD(MD — параметр TransactionId из ответа сервера)
     * @param String $token - Значение одноименного параметра см. доку
     * @return array
     */
    public function confirm3DS($transactionId, $token)
    {
        $response = $this->sendRequest('/payments/cards/post3ds', [
            'TransactionId' => $transactionId,
            'PaRes' => $token
        ]);

        $this->getResponseError($response);

        return isset($response['Model']) ? $this->createTransactionData($response['Model']) : [];
    }

    /**
     * Для платежей, проведенных по двухстадийной схеме
     *
     * @param Int $transactionId - Номер транзакции в системе
     * @param float $amount - Сумма подтверждения в валюте транзакции
     * @param array $params = [
     * 'JsonData' - дополнительные данные в ввиде json
     * ] - Любые другие данные, которые будут связаны с транзакцией,
     *        в том числе инструкции для формирования онлайн-чека.
     * @return bool
     */
    public function confirmPayment($transactionId, $amount, $params = [])
    {
        $response = $this->sendRequest('/payments/confirm', array_merge([
            'TransactionId' => $transactionId,
            'Amount' => $amount
        ], $params));

        return !$this->getResponseError($response);
    }

    /**
     * Отмену оплаты
     * @param Int $transactionId - Номер транзакции в системе
     * @return bool
     */
    public function voidPayment($transactionId)
    {
        $response = $this->sendRequest('/payments/void', [
            'TransactionId' => $transactionId
        ]);

        return !$this->getResponseError($response);
    }

    /**
     * Возврат денег
     * @param int $transactionId - Номер транзакции оплаты
     * @param float $amount - Сумма возврата в валюте транзакции
     * @param array $params = [
     * 'JsonData' - дополнительные данные в ввиде json
     * ] - Любые другие данные, которые будут связаны с транзакцией,
     *        в том числе инструкции для формирования онлайн-чека.
     * @return bool
     */
    public function refundPayment($transactionId, $amount, $params = [])
    {
        $response = $this->sendRequest('/payments/refund', array_merge([
            'TransactionId' => $transactionId,
            'Amount' => $amount
        ], $params));

        return !$this->getResponseError($response);
    }

    /**
     * Проверка статуса платежа
     *
     * @param String $invoiceId - Номер заказа
     * @return array - модель транзакции
     */
    public function findPayment($invoiceId)
    {
        $response = $this->sendRequest('/payments/find', [
            'InvoiceId' => $invoiceId
        ]);

        $this->getResponseError($response);

        return isset($response['Model']) ? $this->createTransactionData($response['Model']) : [];
    }

    /**
     * Выгрузка списка транзакций
     *
     * @param string $date - Дата создания операций
     * @param string $timezone - Код временной зоны, по умолчанию — UTC
     * @return array - массив транзакций
     */
    public function listPayment($date = '', $timezone = '')
    {
        if ($date == '') {
            $date = date('Y-m-d'); //Today
        }
        $request = [
            'Date' => $date
        ];
        if($timezone) {
            $request['TimeZone'] = $timezone;
        }

        $response = $this->sendRequest('/payments/list', $request);

        $this->getResponseError($response);

        $list = [];
        if (isset($response['Model'])) {
            foreach($response['Model'] as $payment) {
                $list[$payment['TransactionId']] = $this->createTransactionData($payment);
            }
        }

        return $list;
    }

    /**
     * метод Содание чека
     *
     * @param $data
     * @param null $requestId
     * @return array|bool
     */
    public function receipt($data, $requestId = null)
    {
        $headers = [];
        if ($requestId) {
            $headers[] = "X-Request-ID: {$requestId}";
        }

        $response = $this->sendRequest('/kkt/receipt', $data, $headers);

        $isError = $this->getResponseError($response);

        return !$isError ? $response : !$isError;
    }

    /**
     * Выгрузка токенов
     * @return array - массив токенов
     */
    public function getTokensList()
    {
        $response = $this->sendRequest('/payments/tokens/list');

        $this->getResponseError($response);

        $list = [];
        if (isset($response['Model'])) {
            foreach($response['Model'] as $token) {
                $list[$token['Token']] = $token;
            }
        }
        return $list;
    }


    /**
     * Создание подписки на рекуррентные платежи
     *
     * @param String $token - Токен карты, выданный системой после первого платежа
     * @param String $accountId - Идентификатор пользователя
     * @param String $description - Назначение платежа в свободной форме
     * @param String $email - E-mail плательщика
     * @param float $amount - Сумма платежа
     * @param String $currency - Валюта: RUB/USD/EUR/GBP и т.д.
     * @param bool $requireConfirmation - Если значение true — платежи будут выполняться по двустадийной схеме
     * @param DateTime $startDate - Дата и время первого платежа по плану во временной зоне UTC
     * @param string $interval - Интервал. Возможные значения: Day, Week, Month
     * @param int $period - Период. В комбинации с интервалом, 1 Month значит раз в месяц, а 2 Week — раз в две недели
     * @param array $params = [
     * 'MaxPeriods' Int - Максимальное количество платежей в подписке
     * 'CustomerReceipt' string - Для изменения состава онлайн-чека.
     * ] - дополнительные параметры
     * @return array
     */
    public function createSubscription(
        $token, $accountId, $description, $email, $amount, $currency,
        $requireConfirmation = false, $startDate = null, $interval = 'Month', $period = 1, $params = [])
    {
        if (!$startDate) {
            $utc = new \DateTimeZone("UTC");
            $startDate = (new \DateTime( "now", $utc ))->format(\DateTime::ATOM);
        }

        $defaultParams = [
            'Token' => $token,
            'AccountId' => $accountId,
            'Description' => $description,
            'Email' => $email,
            'Amount' => $amount,
            'Currency' => $currency,
            'RequireConfirmation' => $requireConfirmation,
            'StartDate' => (string)$startDate,
            'Interval' => $interval,
            'Period' => $period,
        ];

        $response = $this->sendRequest('/subscriptions/create', array_merge($defaultParams, $params));

        $this->getResponseError($response);
        return (@$response['Model'] ?: []);
    }

    /**
     * Метод получения информации о статусе подписки.
     *
     * @param String $subscriptionId - Идентификатор подписки
     * @return array - модель подписки
     */
    public function getSubscription($subscriptionId)
    {
        $response = $this->sendRequest('/subscriptions/get', ['Id' => $subscriptionId]);

        $this->getResponseError($response);
        return (@$response['Model'] ?: []);
    }

    /**
     * Метод получения списка подписок для определенного аккаунта.
     *
     * @param string $accountId -Идентификатор подписки
     * @return array - массив всех подписок пользовтельского аккаунта
     */
    public function findSubscriptions($accountId)
    {
        $response = $this->sendRequest('/subscriptions/find', ['accountId' => $accountId]);

        $this->getResponseError($response);
        $list = [];
        if (isset($response['Model'])) {
            foreach($response['Model'] as $subscription) {
                $list[$subscription['Id']] = $subscription;
            }
        }
        return $list;
    }

    /**
     * Метод изменения ранее созданной подписки
     * @param string $subscriptionId - Идентификатор подписки
     * @param array $params = [
     * Description => string - Для изменения назначение платежа
     * Amount => float - Для изменения суммы платежа
     * Currency => String - Для изменения валюты: RUB/USD/EUR/GBP
     * RequireConfirmation => Bool - Для изменения схемы проведения платежей
     * StartDate => DateTime - Для изменения даты и времени первого (или следующего) платежа во временной зоне UTC
     * Interval => String - Для изменения интервала. Возможные значения: Week, Month
     * Period => Int - Для изменения периода. В комбинации с интервалом, 1 Month значит раз в месяц, а 2 Week — раз в две недели.
     * MaxPeriods => Int -  Для изменения максимального количества платежей в подписке
     * CustomerReceipt => String - Для изменения состава онлайн-чека.
     * ] - параметры обновления
     * @return array - измененая подписка
     */
    public function updateSubscription($subscriptionId, $params)
    {
        $defaultParams = [
            'Id' => $subscriptionId
        ];
        $response = $this->sendRequest('/subscriptions/update', array_merge($defaultParams, $params));

        $this->getResponseError($response);
        return (@$response['Model'] ?: []);
    }

    /**
     * Метод отмены подписки на рекуррентные платежи
     * https://my.cloudpayments.ru/unsubscribe - линк для отмены подписок
     *
     * @param String $subscriptionId - Идентификатор подписки
     * @return bool - true - подписка отменена, false - не вышло отменить подписку(см errors)
     */
    public function cancelSubscription($subscriptionId)
    {
        $response = $this->sendRequest('/subscriptions/cancel', ['Id' => $subscriptionId]);

        $this->getResponseError($response);
        return isset($response['Success']) ? $response['Success'] : false;
    }

    /**
     * Метод просмотра настроек уведомлений(с указанием типа уведомления)
     * example
     * запрос https://api.cloudpayments.ru/site/notifications/pay/get
     * ответ
     * "IsEnabled": true,
     * "Address": "http://example.com",
     * "HttpMethod": "GET",
     * "Encoding": "UTF8",
     * "Format": "CloudPayments"
     *
     * @param String $type - Тип уведомления: Check/Pay/Fail и т.д.
     * @return array
     */
    public function getCallbackSettings($type)
    {
        $response = $this->sendRequest("/site/notifications/$type/get");

        $this->getResponseError($response);
        return (@$response['Model'] ?: []);
    }

    /**
     * Метод изменения настроек уведомлений
     * @param string $type - Тип уведомления: Pay/Fail и т.д., кроме Check-уведомления
     * @param array $params = [
     * параметры для обновления($params)
     * IsEnabled => Bool - Если значение true — то уведомление включено. Значение по умолчанию — false.
     * Address => String - Адрес для отправки уведомлений (для HTTPS схемы необходим валидный SSL сертификат).
     * HttpMethod => String - HTTP метод для отправки уведомлений. Возможные значения: GET, POST. Значение по умолчанию — GET.
     * Encoding => String - Кодировка уведомлений. Возможные значения: UTF8, Windows1251. Значение по умолчанию — UTF8.
     * Format => String - Формат уведомлений. Возможные значения: CloudPayments, QIWI, RT. Значение по умолчанию — CloudPayments.
     * ] - параметры обновления
     * @return bool
     */
    public function updateCallbackSettings($type, $params)
    {
        $response = $this->sendRequest("/site/notifications/$type/update", $params);

        $this->getResponseError($response);

        return isset($response['Success']) ? $response['Success'] : false;
    }
}
