<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Name:  Расширение DateTime для использования временнЫх функций
 *
 * @property CI_Config $config
 */
class Datetimeex extends DateTime
{
//    protected $CI;

    public function __construct($value = null, $timezone = NULL) {

        parent::__construct($value, $timezone);

//        $this->CI =&get_instance();
    }

    /**
     * функция для кастомной сортировки эл-тов timezone
     *
     * @param $a
     * @param $b
     *
     * @return int
     */
    private static function cmpTimezone($a, $b)
    {
        
        if ($a['offset_int'] > $b['offset_int']) {
            return 1;
        }
        if ($a['offset_int'] < $b['offset_int']) {
            return -1;
        }
        if ($a['offset_int'] == $b['offset_int']) {
            if ($a['localized_name'] == $b['localized_name']) return 0;
            elseif ($a['localized_name'] > $b['localized_name']) return 1;
            else return -1;
        }
        else return 0;
    }

    /**
     * Gets array of timezones with offsets
     *
     * @return  array
     */
    public static function getTimezones()
    {
        $timezonesData = [];
        $timezonesIdentifiers = DateTimeZone::listIdentifiers();

        foreach ($timezonesIdentifiers as $timezone) {
            try {
                $formatter = new IntlDateFormatter(
                    "ru_RU",
                    IntlDateFormatter::FULL,
                    IntlDateFormatter::FULL,
                    $timezone,
                    IntlDateFormatter::GREGORIAN,
                    'ZZZZ---xxxx---VVVV---VVV'
                );

                if ($formatter) {
                    $t10n = $formatter->format(0);
                } else {
                    $t10n = null; // continue;
                }
            } catch (Throwable $e) {
                $t10n = null; // continue;
            } catch (Exception $e) {
                $t10n = null; // continue;
            }

            if (!empty($t10n)) {
                $parts = explode('---',$t10n);

//                if ($parts[2] == 'Москва') {
//                    $parts[2] = "Российская Федерация";
//                }

                $timezonesData[] = [
                    'offset' => (new DateTime(null, new DateTimeZone($timezone)))->getOffset(),
                    'offset_int' => intval($parts[1]), // иначе неверно сортирует
                    'offset_string' => $parts[0],
                    'canonical_name' => $timezone,
                    'localized_name' => /*$parts[2]."/".*/$parts[3]." (".$timezone.")",
                ];;
            }
        }

        usort($timezonesData, [__CLASS__, 'cmpTimezone']);
        array_shift($timezonesData); // какая-то странная таймзона в Либерии
        return $timezonesData;
    }

    public function isEarlier($d)
    {
        return self::diff($d)->invert === 0;
    }

    public function isLater($d)
    {
        return self::diff($d)->invert === 1;
    }

    /**
     * Метод преобразует из таймзоны в конструкторе в нужную таймзону
     *
     * @param $timeZone - таймзона в которую необходимо перевести время(даты)
     *
     * @return string - Y-m-d H:i:s
     */
    public function changeTimeZone($timeZone)
    {
        $tz = new DateTimeZone($timeZone);
        $this->setTimezone($tz);
        return $this->format('Y-m-d H:i:s');
    }
}
