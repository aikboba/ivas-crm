<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;

use \Kreait\Firebase\Exception\Messaging\ApiConnectionFailed;
use \Kreait\Firebase\Exception\Messaging\AuthenticationError;
use \Kreait\Firebase\Exception\Messaging\InvalidArgument;
use \Kreait\Firebase\Exception\Messaging\InvalidMessage;
use \Kreait\Firebase\Exception\Messaging\MessagingError;
use \Kreait\Firebase\Exception\Messaging\NotFound;
use \Kreait\Firebase\Exception\Messaging\ServerError;
use \Kreait\Firebase\Exception\Messaging\ServerUnavailable;


class Firebase_cloud_message
{
    protected $CI;
    protected $factory = null;

    protected $fcm = null;

    public $error_msg = ''; // содержит error_msg
    public $error_code = 0; // содержит error_code

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->config('firebase_cloud_message', true);


        $this->fcm = (new Factory())
            ->withServiceAccount($this->CI->config->item('file_settings', 'firebase_cloud_message'));

    }

    public function sendMessageToMultipleDevices($deviceTokens, $notification = [], $data = [])
    {
        $messageData = [];

        if ($notification) {
            $messageData['notification'] = $notification;
        }

        if ($data) {
            $messageData['data'] = $data;
        }

        $message = CloudMessage::fromArray($messageData);

        $deviceTokens = is_array($deviceTokens) ? $deviceTokens : [$deviceTokens];

        $messaging = $this->fcm->createMessaging();
        $results = [];
        try {
            if (count($deviceTokens) > 500) {
                $chunkDeviceTokens =  array_chunk($deviceTokens, 500);
                foreach($chunkDeviceTokens as $deviceTokens) {
                    $report = $this->sendMulticast($messaging, $message, $deviceTokens);
                    $results = array_merge($results, $report);
                }
            } else {
                $results = $this->sendMulticast($messaging, $message, $deviceTokens);
            }
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }

        return $results;
    }

    /**
     * @param $deviceToken
     * @param array $notification Example $notification = ['title' => $title, 'body' => $body], // optional
     * @param array $data Example $data = [/ data array /], // optional
     */
    public function sendNotificationData($deviceToken, $notification = [], $data = [])
    {
        $messageData = [
            'token' => $deviceToken,
        ];

        $response = $this->sendMessage($messageData, $notification, $data);
        return isset($response['name']) ? $response['name'] : false;
    }

    public function sendNotificationDataToTopic($topic, $notification = [], $data = [])
    {
        $messageData = [
            'topic' => $topic,
        ];

        $response = $this->sendMessage($messageData, $notification, $data);
        return isset($response['name']) ? $response['name'] : false;
    }

    public function sendMessage($messageData, $notification, $data) {
        if ($notification) {
            $messageData['notification'] = $notification;
        }
        if ($data) {
            $messageData['data'] = $data;
        }
        $messaging = $this->fcm->createMessaging();
        $message = CloudMessage::fromArray($messageData);
        try {
            $response = $messaging->send($message);
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }

        return $response;
    }

    private function sendMulticast($messaging, $message, $deviceTokens)
    {
        $report = $messaging->sendMulticast($message, $deviceTokens);
        $result = [];
        if ($report->hasFailures()) {
            foreach ($report->failures()->getItems() as $failure) {
                $result[] = $failure;
            }
        } else {
            foreach ($report->successes()->getItems() as $success) {
                $result[] = $success;
            }
        }
        return $result;
    }

    public function subscribeToTopic($deviceTokens, $topic)
    {
        $messaging = $this->fcm->createMessaging();
        $registrationTokens = is_array($deviceTokens) ? $deviceTokens : [$deviceTokens];
        try {
            $response = $messaging->subscribeToTopic($topic, $registrationTokens);
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }

        return isset($response['results']) ? $response['results'] : false;
    }
    public function unsubscribeFromTopic($deviceTokens, $topic)
    {
        $messaging = $this->fcm->createMessaging();
        $registrationTokens = is_array($deviceTokens) ? $deviceTokens : [$deviceTokens];
        try {
            $response = $messaging->unsubscribeFromTopic($topic, $registrationTokens);
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }
        return isset($response['results']) ? $response['results'] : false;
    }

    public function getAppInstance($deviceToken)
    {
        $messagig = $this->fcm->createMessaging();

        try {
            $appInstance = $messagig->getAppInstance($deviceToken);
            $instanceInfo = $appInstance->rawData();
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }
        return $instanceInfo;
    }

    public function getSubscribeTopics($deviceToken)
    {
        $messagig = $this->fcm->createMessaging();

        try {
            $appInstance = $messagig->getAppInstance($deviceToken);
            $subscriptions = $appInstance->topicSubscriptions();

            $results =[];
            foreach ($subscriptions as $subscription) {
                $results[(string)$subscription->topic()] = (string)$subscription->registrationToken();
            }
        } catch (
            ApiException | AuthApiExceptionConverter | AuthenticationError |
            ApiConnectionFailed | InvalidArgument | InvalidMessage | MessagingError | NotFound | ServerError $e
        ) {
            $this->error_msg = $e->getMessage();
            $this->error_code = $e->getCode();
        }
        return $results;
    }
}