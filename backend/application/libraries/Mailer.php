<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\SMTP;


class Mailer extends PHPMailer
{
    protected $CI;
    
    private $defaultConfig;
    private $defaultFrom;
    
    public $sendType = 'sync'; // sync | async 
    
    public $error_msg = ''; // содержит error_msg
    public $error_code = 0; // содержит error_code

    public function __construct() {
        
        $this->CI =& get_instance();
        
        $this->CI->load->config('mailer', true);
        $this->defaultConfig =  $this->CI->config->item('SMTP', 'mailer');
        $this->setConfig($this->defaultConfig);
        
        $this->defaultFrom = $this->CI->config->item('from', 'mailer');
        
        $this->CI->load->model('emails_queue_model');
        $this->CI->load->model('emails_logs_model');
                            
        parent::__construct(true);
    }
    
    public function setConfig($configData) {
        if (isset($configData['debug']) && $configData['debug']) {
            $this->SMTPDebug = SMTP::DEBUG_SERVER;                    
        }
        
        $this->isSMTP();                                           
        $this->SMTPAuth   = true;                                  
        $this->Host       = !empty($configData['host']) ? $configData['host'] : $this->defaultConfig['host'];                    
        $this->Port       = !empty($configData['port']) ? $configData['port'] : $this->defaultConfig['port']; 
        $this->Username   = !empty($configData['user']) ? $configData['user'] : $this->defaultConfig['user'];
        $this->Password   = !empty($configData['password']) ? $configData['password'] : $this->defaultConfig['password'];
        
        if (!empty($configData['encryption'])) {
            $this->SMTPSecure = $configData['encryption'] == 'ssl' ? PHPMailer::ENCRYPTION_SMTPS : PHPMailer::ENCRYPTION_STARTTLS; 
        } else {
            $this->SMTPSecure = $this->defaultConfig['encryption'] == 'ssl' ? PHPMailer::ENCRYPTION_SMTPS : PHPMailer::ENCRYPTION_STARTTLS; 
        }
        
        $this->CharSet = self::CHARSET_UTF8;
    }
    
    
    public function send($emailData = null) {
        
        $fromEmail = isset($emailData['fromEmail']) ? $emailData['fromEmail'] : $this->defaultFrom['email'];
        $fromName = isset($emailData['fromName']) ? $emailData['fromName'] : $this->defaultFrom['name'];
            
        if (!empty($emailData['toEmail'])) {
            
            $email = [
                'recipient_email' => $emailData['toEmail'],
                'recipient_name' => isset($emailData['toName']) ? $emailData['toName'] : null,
                'sender_email' => $fromEmail,
                'sender_name' => $fromName,
                'subject' => $emailData['subject'],
                'content' => $emailData['body'],
            ];
            $this->clearAttachments();
            
            if ($this->sendType == 'async')
            {
                $email['send_date'] = !empty($emailData['send_date']) ? $emailData['send_date'] : (new Datetimeex('now'))->format('Y-m-d H:i:s');
                $email['is_html'] = isset($emailData['is_html']) ? $emailData['is_html'] : 1;
                
                if (!empty($emailData['attached_files'])) {
                    $email['attached_files'] = json_encode($emailData['attached_files']);
                }
                $newId = $this->CI->emails_queue_model->insert($email);
                return $newId;
            } else {
                $this->From = $fromEmail;
                $this->FromName = $fromName;
                
                try {
                    $this->addAddress($emailData['toEmail'], isset($emailData['toName']) ? $emailData['toName'] : '') ;
                    
                    $this->Subject = $emailData['subject'];
                    if ((isset($emailData['is_html']) && $emailData['is_html']) || !isset($emailData['is_html'])) {
                        $this->isHTML(true);                               
                    } 
                    $this->Body    = $emailData['body'];
                    $this->AltBody = $emailData['body'];    
                    
                    if (!empty($emailData['attached_files'])) {
                        foreach($emailData['attached_files'] as $file) {
                            $this->addAttachment($file['path'], !empty($file['name']) ? $file['name'] : '');
                        }
                        
                        $email['attached_files'] = json_encode($emailData['attached_files']);
                    }
                    $result = parent::send();

                    $this->CI->emails_logs_model->insert($email);

                    return $result;
                } catch (Exception $e) {
                    $this->error_msg = $e->getMessage();
                    $this->error_code = $e->getCode();
                    $email['error_message'] = $this->error_msg;
                    $email['status'] = 'error';
                    $this->CI->emails_logs_model->insert($email); 
                }
            }
        } 
        return false;
    }
}