<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Библиотека для отправки сообщений через CURL на endpoint, откуда происходит их дальнейшая рассылка клиентам через WebSocket
*/

class Messinger
{
    protected $CI;

/*
    Структура POST:

                   'channel' => '<CHANNEL_ID>', //EX: mobile_app - название канала, в который будет помещено сообщение
                   'payload' => Object - передается в WS
                                {
                                   'event_id' => '<random_string>', //EX: - event_id - идентификатор отправляемого сообщения. Нужен для использования в мультиотправках (WS+push) и нереигирования 2 раза на однин и тот же event
                                   'action' => '<ACTION_NAME>', //EX: products_list_refresh - тип сообщения, ни которое будет своя логика реакции у получателя
                                   'target' => [ // пустой массив интерепретировать как "all"
                                                            {
                                                              '<entity>' => '<entity_name>', //Ex: client
                                                              '<entity_id>' => '<id_value>', //Ex: 23
                                                            }
                                                            ...
                                                            {
                                                              '<entity>' => '<entity_name>', //Ex: client
                                                              '<entity_id>' => '<id_value>', //Ex: 2343
                                                            }
                                               ],
                                   'data'   => {
                                                    '<param1>' => '<value1>',
                                                    ...
                                                    '<paramN>' => '<valueN>',
                                               }
                                 }
                    'auth' => <string> что-то для аутентификации
*/

    public $action = "";
    public $salt = "";
    public $channel = "";
    public $target = [];// в работе используeтся array. Конвертация в object в самой отправке send
    public $data = [];// в работе используeтся array. Конвертация в object в самой отправке send

    private $endpoint = "";
    public $event_id = "";

    public function __construct()
    {
        $this->CI =& get_instance();
        //$this->CI->load->library('mailer');
        $this->CI->load->config('messinger', true);
        $this->channel = $this->CI->config->item('default_channel', 'messinger');
        $this->endpoint = $this->CI->config->item('curl_url', 'messinger');
        $this->salt = $this->CI->config->item('messinger_secret_salt', 'messinger');

        if (empty($this->event_id)) $this->event_id = $this->CI->utils->generateEventGuid();

    }

    /** Отправляет данные на $this->endpoint
     *
     *  @param $data
     *
     * @return boolean
     */
    public function send()
    {
        // приведение target к массиву объектов
        $target = [];
        foreach ($this->target as $key=>$tar)
        {
            $target[] = (object) $tar;
        }

        $payload = [
            'event_id'=> $this->event_id, // рандомный идентификатор сообщения
            'action'  => $this->action, // это сообщение. типа 'system_refresh_product',
            'target'  => $target, // это цель. Но в моем понимании. для разгребания клиентом. json_encoded array of array of string
            'data'    => (object)$this->data, // это данные для задачи. json_encoded
        ];

        $payload_enc = json_encode((object)$payload);
        $sData = [
            'channel' => $this->channel, // это канал
            'payload' => $payload_enc, // то, что будет передано в WS
            'auth'    => md5($this->channel . $payload_enc . $this->salt) // соль из конфига = '2hHzmQkSRHRVxKbSqDpVTBEApJBQxs'
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
        curl_setopt($ch, CURLOPT_FAILONERROR, true); // Required for HTTP error codes to be reported via our call to curl_error($ch)
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        $data = curl_exec($ch);
        $res = (!curl_errno($ch));
/*
echo "<pre>";
        var_dump($this->endpoint);
        var_dump(curl_errno($ch));
        var_dump(curl_error($ch));
echo "</pre>";
die();
*/
        curl_close ($ch);

        return $res;
    }


}
