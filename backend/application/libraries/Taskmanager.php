<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Tasks processing library
*/

class Taskmanager
{
    protected $CI;
    
    public function __construct() {
        $this->CI =& get_instance();

        $this->CI->load->model('tasks_model');

        $this->CI->load->library('mailer');
        $this->CI->load->config('cms_consts', true);
    }

    /** создание задачи в очереди задач
     *
     *  @param array $task_data = [
    'publisher'  string - Имя источника (метода-создателя). В произвольной форме
    'consumer'   string - Имя метода-исполнителя в Worker контроллере
    'data_in'       object|array|json_encoded - Входные данные задачи в виде json_encoded
    'priority'   int    - Приоритет задания. Чем больше, тем быстрее выполнится
    'start_dt'   string - Timestamp позволенного начала выполнения задачи (Формат: 2020-01-26 23:14:34)
    'status'     string - Статус заявки в очереди ('new','in_process','ready','completed','error'),
    'status_msg' string - Сообщение к статусу заявки в очереди
    ]
     *
     * @return  boolean
     */
    public function createTask($task_data)
    {
        if (
            (!is_array($task_data)) ||
            (empty($task_data['publisher'])) ||
            (empty($task_data['consumer']))
        )
        {
            return false;
        }

        if ( (is_array($task_data['data_in'])) || is_object($task_data['data_in']) )  $task_data['data_in'] = json_encode($task_data['data_in']);
        if (json_last_error() != JSON_ERROR_NONE) { return false; }

        if (!empty($task_data['status']))
        {
            if (!in_array($task_data['status'], ['new', 'in_process', 'error'])) $task_data['status'] = 'new';
        }

        $new_id = $this->CI->tasks_model->create($task_data);

        return $new_id;
    }

    /** ожидание выполнения задачи. Выход по готовности или таймауту
     *
     *  @param int $task_id - ID задачи в очереди
     *  @param int $timeout - число секунд ожидания
     *
     * @return  boolean
     */
    public function waitForTask($task_id, $timeout = 10)
    {
        $start_dt = (new Datetimeex())->format("U");
        $end_dt = $start_dt + $timeout;

        while (true)
        {
            $now = (new Datetimeex())->format("U");

            if ($now > $end_dt) return false;

            unset($now);
            usleep(500000); // ждем 0.5с
            $task = $this->CI->tasks_model->getCompletedById($task_id);

            if( (!empty($task)) && ( ($task->status == 'completed')||($task->status == 'error') ) )
            {
                return $task;
            }
        }

        return false;
    }

}
