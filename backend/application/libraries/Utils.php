<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Custom functions library
*/

class Utils
{
    protected $CI;
    
    public function __construct() {
        $this->CI =& get_instance();
        
        $this->CI->load->library('mailer');
        $this->CI->load->config('cms_consts', true);
    }

    public function envelope($status, $message, $data, $toJSON = true)
    {
        $result = [];
        $result['status'] = $status;
        $result['message'] = $message;
        //$result['user'] = $this->CI->auth->userLogged;
        $result['data'] = $data;


        //if ($toJSON) return json_encode($result, JSON_PRETTY_PRINT);
        if ($toJSON) return json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            else return new ArrayObject($result,1|2);
    }

    public function jsonOut($json)
    {

        switch (SERVER_ENVIRONMENT)
        {
            case 'development':
            case 'testing':
                $this->CI->output->set_header("Access-Control-Allow-Origin: *");
                $this->CI->output->set_header("Access-Control-Allow-Methods: *");
                $this->CI->output->set_header("Access-Control-Allow-Headers: *");
                $this->CI->output->set_header("Access-Control-Expose-Headers: *");
            break;
        }

        $this->CI->output->set_content_type('application/json; charset=utf-8')->set_status_header(200);

        /* Продление токена, если он есть.
         * Пояснение: косвенным признаком того, что токен в полном порядке является факт залогиненности пользователя
         */
        if ($this->CI->auth->isLogged())
        {
            $this->CI->auth->tokenSet( $this->CI->auth->token );
        }

        //return $this->CI->output->set_output( $json )->_display();
        $this->CI->output->set_output( $json )->_display();

        die();
    }

    public function jsonpOut($json, $callback)
    {
        $this->CI->output->set_content_type('application/javascript; charset=utf-8')->set_status_header(200);

        switch (SERVER_ENVIRONMENT)
        {
            case 'development':
            case 'testing':
                $this->CI->output->set_header("Access-Control-Allow-Origin: *");
                $this->CI->output->set_header("Access-Control-Allow-Methods: *");
                $this->CI->output->set_header("Access-Control-Allow-Headers: *");
                $this->CI->output->set_header("Access-Control-Expose-Headers: *");
                break;
        }

        /* Продление токена, если он есть.
         * Пояснение: косвенным признаком того, что токен в полном порядке является факт залогиненности пользователя
         */
        if ($this->CI->auth->isLogged())
        {
            $this->CI->auth->tokenSet( $this->CI->auth->token );
        }

        //return $this->CI->output->set_output( $callback."(".$json.")" );
        $this->CI->output->set_output( $callback."(".$json.")" )->_display();
        die();
    }

    /**
     * Создает соль для admins и clients
     *
     * @return string
     */
    public function salt()
    {
        return md5(uniqid());
    }

    /**
     * Создает хэш пароля на основании соли пользователя и его пароля
     *
     * @param string $password - пароль
     * @param string $salt - соль.
     *
     * @return string | boolean on error
     */
    public function hash_password($password, $salt)
    {
        if (empty($password))
        {
            return FALSE;
        }

        if (empty($salt)) $salt="";

        return  sha1($password . $salt);
    }
    
    public function email_valid($email)
    {
        if(!$email) return false;

        $this->CI->load->config('email_valid', true);
        $config = $this->CI->config->item('email_valid');

        if (!$config['use'])
        {
            return true;
        }
        
        $params = [
            $config['param_api_key'] => $config['api_key'],
            $config['param_email'] => $email,
        ];
        
        if ($config['additional_params_request'])
        {
            $params = array_merge($params, $config['additional_params_request']);
        }

        $ctx = stream_context_create(array('http'=> array('timeout' => 2)));
        $response = @file_get_contents($config['url'].'?'.http_build_query($params), false, $ctx);

        $emailValidData = json_decode($response, true);

        if (isset($emailValidData['success']) && !$emailValidData['success']) 
        {
            $adminEmailAlert = $this->CI->config->item('admin_email_alert', 'cms_consts');
            
            $mail = [
                'toEmail' => $adminEmailAlert,
                'toName' => $adminEmailAlert,
                'subject' => 'Ошибка отправки на АПИ валидации Email',
                'body' => 'Код ошибки: ' . $emailValidData['error']['code'] . '<br>'. 'Описание ошибки: ' . $emailValidData['error']['info'],
            ];
            
            $this->CI->mailer->send($mail);
            
            //return false;
            return true;// в случае возникновения "системной" ошибки типа "недостаточно средств" предпочтительно пропустить невалидный email, чем отбросить валидный
        }
        
        $result = true;
        
        if ($config['check_params_response']) 
        {
            foreach($config['check_params_response'] as $key => $value) 
            {

                if (isset($emailValidData[$key]))
                {

                    if (is_array($value))
                    {
                        eval('$result = ' . $emailValidData[$key] . ' '.implode(' ', $value).';');
                    }
                    elseif ($emailValidData[$key] != $value)
                    {
                        $result = false;
                    }

                }
                else
                {
                    $result = false;
                }

                if (!$result)
                {
                    break;
                }

            }

        }

        return $result;
    }

    /** Производит замену в тексте $text всех {{{ключей}}}, присутствующих в массиве $data, на их значения.
     *  Ключи и подстановки - Case insensitive
     *
     * @param string $text
     * @param array $data
     *
     * @return string
     */
    public function substitute($text, $data)
    {
        // находим все "placeholders" - элементы вида {{{NAME}}}
        preg_match_all("/\{{3}([^\}]+)\}{3}/xim",$text,$matches);
        array_walk($matches[1], function(&$val, $key){ $val = strtoupper($val); } );
        $placeholders = array_unique($matches[1]);

        $data = array_change_key_case($data, CASE_UPPER);

        if (!empty($placeholders))
        {

            foreach ($placeholders as $pl)
            {

                if (isset($data[$pl]))
                {
                    $text = str_ireplace("{{{".$pl."}}}", $data[$pl], $text);
                }

            }

        }

        return $text;
    }

    /**
     * Getcourse date convert--Aik Boba edition (c)-- возвращает timestamp
     *
     *
     */
    public function gc_date_to_timestamp($gc_date)
    {
        $RU_MONS = array(
            'Янв' => 1,
            'Фев' => 2,
            'Мар' => 3,
            'Апр' => 4,
            'Май' => 5,
            'Июн' => 6,
            'Июл' => 7,
            'Авг' => 8,
            'Сен' => 9,
            'Окт' => 10,
            'Ноя' => 11,
            'Дек' => 12,
        );

        $date_string = false;

        if (!empty($gc_date)) {
            $gc_date_array = explode(' ', $gc_date);

            if (count($gc_date_array) >= 3) {
                $date_string = strtotime($gc_date_array[0].'.'.$RU_MONS[$gc_date_array[1]].'.'.$gc_date_array[2]);
            }
        }

        return $date_string;
    }

    /**
     * Getcourse date convert--Aik Boba edition (c)-- возвращает timestamp
     *
     *
     */
    public function getcourseDateToStringDateTime($gc_date)
    {
        $RU_MONS = array(
            'Янв' => 1,
            'Фев' => 2,
            'Мар' => 3,
            'Апр' => 4,
            'Май' => 5,
            'Июн' => 6,
            'Июл' => 7,
            'Авг' => 8,
            'Сен' => 9,
            'Окт' => 10,
            'Ноя' => 11,
            'Дек' => 12,
        );

        $date_string = false;

        if (!empty($gc_date)) {
            $arr = explode(' ', $gc_date);

            if (count($arr) >= 3) {
                $date_string = $arr[2]."-".$RU_MONS[$arr[1]]."-".$arr[0]." 00:00:00";
            }
        }

        return $date_string;
    }

    public function translit($string, $toRus = false)
    {
        // Сначала заменяем "односимвольные" малые фонемы.
        $fromAlphaStr="абвгдезийклмнопрстуфх";
        $toAlphaStr="abvgdezijklmnoprstufx";

        // создаем массив элементов для замены
        $fromToArr=Array();
        for ($i=0;$i<mb_strlen($fromAlphaStr,'utf-8');$i++)
            $fromToArr[mb_substr($fromAlphaStr,$i,1,'utf-8')]=mb_substr($toAlphaStr,$i,1,'utf-8');

        $fromAlphaStr=mb_strtoupper($fromAlphaStr,'utf-8');
        $toAlphaStr=mb_strtoupper($toAlphaStr,'utf-8');
        for ($i=0;$i<mb_strlen($fromAlphaStr,'utf-8');$i++)
            $fromToArr[mb_substr($fromAlphaStr,$i,1,'utf-8')]=mb_substr($toAlphaStr,$i,1,'utf-8');

        // Затем - "многосимвольные".
        $manySymbols = [
            "ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", 'ь' => '\'', 'ъ' => '"',
            "щ"=>"sch", "ю"=>"yu", "я"=>"ya", 'ы' => 'y\'',
            'ё' => 'yo', 'Ё' => 'YO', 'э' => 'eh', 'Э' => 'EH',
            "Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH",
            "Щ"=>"SHCH", "Ю"=>"YU", "Я"=>"YA",
        ];

        if ($toRus) {
            $filterEmpties = function($key, $value) {
                return !empty($value);
            };
            $manySymbols = array_flip($manySymbols);
            $manySymbols = array_filter($manySymbols, $filterEmpties, ARRAY_FILTER_USE_BOTH);
            $fromToArr = array_flip($fromToArr);
            $fromToArr = array_filter($fromToArr, $filterEmpties, ARRAY_FILTER_USE_BOTH);
            $converted = strtr($string, $manySymbols);
            $converted = strtr($converted, $fromToArr);
        } else {
            $converted = strtr($string, $fromToArr);
            $converted = strtr($converted, $manySymbols);
        }

        return $converted;
    }

    function detectLang($string)
    {
        $langueges = [];
        preg_match("/[a-zA-Z]+/sU", $string, $latMatch);
        if ($latMatch) {
            $langueges[] = 'lat';
        }

        preg_match("/[а-яА-Я]+/sU", $string, $rusMatch);
        if ($rusMatch) {
            $langueges[] = 'rus';
        }

        preg_match("/[1-9]+/sU", $string, $numMatch);
        if ($numMatch) {
            $langueges[] = 'num';
        }

        if (count($langueges) > 1) {
            return 'mix';
        } elseif(count($langueges) == 1) {
            return $langueges[0];
        }
        return 'unknown';
    }

    /** ВРЕМЕННО: получает данные о chat-объекте
     *
     * @param int item_id - ID item - как часть для формирования channel
     *
     * @return  object
     */
    public function getChatObject($item_id)
    {
        $result = (object)[];
        if ($this->CI->auth->isLogged())
        {
            $this->CI->load->config('chat', true);
            $salt = $this->CI->config->item('chat_secret_salt', 'chat');
            $pref = $this->CI->config->item('chat_channel_preffix', 'chat');

            $channel = $pref . $item_id;

            $chat_privileges = ["create", "get"];
            if ($this->CI->auth->loggedUser->is_blogger)
            {
                $chat_privileges[] = "feature";
            }

            $privileges = join(',', $chat_privileges);
            $user_id = $this->CI->auth->loggedUser->id;

            $auth = $channel . ":" . $user_id . ":" . $privileges;
            $auth .= ":" . md5($auth . $salt);

            $result = (object)[];

            $result->channel = $channel;
            $result->user_id = $user_id;
            $result->privileges = $privileges;
            $result->auth = $auth;
        }

        return $result;
    }

    function switcher($text, $toEng = false)
    {
        $rus = "й ц у к е н г ш щ з х ъ ф ы в а п р о л д ж э я ч с м и т ь б ю ё";
        $eng = "q w e r t y u i o p [ ] a s d f g h j k l ; ' z x c v b n m , . `";

        $rusArray = array_merge(explode(' ', mb_strtoupper($rus)), explode(' ', $rus));
        $engArray = array_merge(explode(' ', mb_strtoupper($eng)), explode(' ', $eng));

        $arrayReplace = $toEng ? array_combine($rusArray, $engArray) : array_combine($engArray, $rusArray);
        return strtr($text, $arrayReplace);
    }


    public function send_mail_gmail($html, $subject, $recipient, $sender = 'admin@rk-crm.ru')
    {
        $this->CI->load->library('email');

        $gmail_conf = $this->CI->config->item('gmail_config', 'cms_consts');

        $this->CI->email->set_newline("\r\n");
        $mail_config = array();
        $mail_config['protocol'] = 'smtp';
        $mail_config['mailtype'] = 'html';
        $mail_config['smtp_host'] = $gmail_conf['server'];
        $mail_config['smtp_user'] = $gmail_conf['username'];
        $mail_config['smtp_pass'] = $gmail_conf['password'];
        $mail_config['smtp_port'] = $gmail_conf['port'];
        $this->CI->email->initialize($mail_config);

        //Load email library
        $this->CI->email->from($sender, 'Identification');
        $this->CI->email->to($recipient);
        $this->CI->email->subject($subject);
        $this->CI->email->message($html);
        $res = $this->CI->email->send();

        return $res;
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generateEventGuid()
    {
        return $this->generateRandomString().uniqid("",true);
    }


    public function uuid()
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }


}
