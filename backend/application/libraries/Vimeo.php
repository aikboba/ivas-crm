<?php
class Vimeo
{
    const ROOT_ENDPOINT = 'https://api.vimeo.com';
    const AUTH_ENDPOINT = '/oauth/authorize';
    const VIDEOS_ENDPOINT = '/me/videos';
    const VIDEO_INFO = '/videos/%VIDEO_ID%';
    const ACCESS_TOKEN_ENDPOINT = '/oauth/access_token';
    const CLIENT_CREDENTIALS_TOKEN_ENDPOINT = '/oauth/authorize/client';
    const VERSIONS_ENDPOINT = '/versions';
    const VERSION_STRING = 'application/vnd.vimeo.*+json; version=3.4';
    const USER_AGENT = 'vimeo.php 3.0.2; (http://developer.vimeo.com/api/docs)';
    const CERTIFICATE_PATH = '/certificates/vimeo-api.pem';
 
    protected $_curl_opts = array();
  
    protected $CURL_DEFAULTS = array();

    private $_client_id = null;

    private $_client_secret = null;
 
    private $_access_token = null;

    private $core;

    public function __construct()
    {
        set_time_limit(0);

        $this->core = &get_instance();
        $this->core->load->config('vimeo', true);

        $this->_client_id = $this->core->config->item('CLIENT_ID', 'vimeo');
        $this->_client_secret = $this->core->config->item('CLIENT_SECRET', 'vimeo');
        $this->_access_token = $this->core->config->item('ACCESS_TOKEN', 'vimeo');
        
        $this->CURL_DEFAULTS = array(
            CURLOPT_HEADER => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
        );
    }

    //Получает все видео
    public function getVideos()
    {
        $result = [];
        $has_more = 1; $page = 1;

        while ($has_more)
        {
            $params=[
                'page' => $page,
                'per_page' => 100,
            ];
            $resp = $this->request(self::VIDEOS_ENDPOINT, $params);
            $data = $resp['body']['data'];
            $result = array_merge($result, $data);

            if (empty($resp['body']['paging']['next']))
            {
                $has_more = 0;
            }
            else
            {
                $page++;
            }
        }

        return $result;
    }

    public function filterByParams($data, $params = [])
    {
        $filtered = [];

        foreach ($data as $elem)
        {
            $valid = 1;
            foreach($params as $f_name=>$f_val)
            {
                if ($elem[$f_name] != $f_val)
                {
                    $valid = 0;
                    break;
                }
            }

            if ($valid) $filtered[] = $elem;
        }

        return $filtered;
    }

    public function getFilteredVideos()
    {
        $videos = $this->getVideos();

        $params = [
            'status' => 'available',
            'type' => 'video',
        ];

        return $this->filterByParams($videos, $params);
    }


    //Принимает видео айди, отдает информацию по видео
    public function getVideoInfo($video_id){
        return $this->request(str_replace('%VIDEO_ID%',$video_id,self::VIDEO_INFO));
    }

    public function request($url, $params = array(), $method = 'GET', $json_body = true, array $headers = array()): array
    {
        $headers = array_merge(array(
            'Accept' => self::VERSION_STRING,
            'User-Agent' => self::USER_AGENT,
        ), $headers);
        $method = strtoupper($method);
       
        if (!isset($headers['Authorization'])) {
            if (!empty($this->_access_token)) {
                $headers['Authorization'] = 'Bearer ' . $this->_access_token;
            } else {
                
                $headers['Authorization'] = 'Basic ' . $this->_authHeader();
            }
        }

        $curl_opts = array();
        switch ($method) {
            case 'GET':
            case 'HEAD':
                if (!empty($params)) {
                    $query_component = '?' . http_build_query($params, '', '&');
                } else {
                    $query_component = '';
                }
                $curl_url = self::ROOT_ENDPOINT . $url . $query_component;
                break;
            case 'POST':
            case 'PATCH':
            case 'PUT':
            case 'DELETE':
                if ($json_body && !empty($params)) {
                    $headers['Content-Type'] = 'application/json';
                    $body = json_encode($params);
                } else {
                    $body = http_build_query($params, '', '&');
                }
                $curl_url = self::ROOT_ENDPOINT . $url;
                $curl_opts = array(
                    CURLOPT_POST => true,
                    CURLOPT_CUSTOMREQUEST => $method,
                    CURLOPT_POSTFIELDS => $body
                );
                break;
            default:
                exit;
        }
  
        foreach ($headers as $key => $value) {
            $curl_opts[CURLOPT_HTTPHEADER][] = sprintf('%s: %s', $key, $value);
        }
        $response = $this->_request($curl_url, $curl_opts);
        $response['body'] = json_decode($response['body'], true);
        return $response;
    }

    public function getToken()
    {
        return $this->_access_token;
    }
   
    public function setToken(string $access_token)
    {
        $this->_access_token = $access_token;
    }

    public function getCURLOptions()
    {
        return $this->_curl_opts;
    }
    
    public function setCURLOptions(array $curl_opts = array())
    {
        $this->_curl_opts = $curl_opts;
    }
    

    public static function parse_headers(string $headers)
    {
        $final_headers = array();
        $list = explode("\n", trim($headers));
        $http = array_shift($list);
        foreach ($list as $header) {
            $parts = explode(':', $header, 2);
            $final_headers[trim($parts[0])] = isset($parts[1]) ? trim($parts[1]) : '';
        }
        return $final_headers;
    }
   
    public function accessToken(string $code, string $redirect_uri)
    {
        return $this->request(self::ACCESS_TOKEN_ENDPOINT, array(
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $redirect_uri
        ), "POST", false);
    }
  
    public function clientCredentials($scope = 'public')
    {
        if (is_array($scope)) {
            $scope = implode(' ', $scope);
        }
        $token_response = $this->request(self::CLIENT_CREDENTIALS_TOKEN_ENDPOINT, array(
            'grant_type' => 'client_credentials',
            'scope' => $scope
        ), "POST", false);
        return $token_response;
    }
    
    public function buildAuthorizationEndpoint($redirect_uri, $scope = 'public', $state = null)
    {
        $query = array(
            "response_type" => 'code',
            "client_id" => $this->_client_id,
            "redirect_uri" => $redirect_uri
        );
        $query['scope'] = $scope;
        if (empty($scope)) {
            $query['scope'] = 'public';
        } elseif (is_array($scope)) {
            $query['scope'] = implode(' ', $scope);
        }
        if (!empty($state)) {
            $query['state'] = $state;
        }
        return self::AUTH_ENDPOINT . '?' . http_build_query($query);
    }
  

    
  
    private function _request(string $url, $curl_opts = array())
    {
        
        $curl_opts = $this->_curl_opts + $curl_opts + $this->CURL_DEFAULTS;
      
        $curl = curl_init($url);
        curl_setopt_array($curl, $curl_opts);
        $response = curl_exec($curl);
        $curl_info = curl_getinfo($curl);
      
        curl_close($curl);
       
        $header_size = $curl_info['header_size'];
        $headers = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        
        return array(
            'body' => $body,
            'status' => $curl_info['http_code'],
            'headers' => self::parse_headers($headers)
        );
    }
    
    private function _authHeader()
    {
        return base64_encode($this->_client_id . ':' . $this->_client_secret);
    }


    /** возвращает vimeo video_id из url вида "https://vimeo.com/412223666"
     *  или "" в случае, если url не vimeo, или video_id не найдено
     *
     * @param string $text
     *
     * @return string
     */
    public function getVideoIdByUrl($url)
    {
        if (empty($url)) return "";
        if (strpos($url,"vimeo") === false) return "";

        $ar = explode('/', $url);
        if (!is_array($ar)) return "";
        $result = end($ar);

        if(empty($result)) return "";

        return $result;
    }

}