<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class YouTube {

    protected $core;

    public $client;
    public $YouTube;



    public function __construct()
    {
        $this->core = &get_instance();
        $this->client = new Google_Client();
        $this->core->load->config('youtube', true);
        if(!is_null($this->core->config->item('GOOGLE_APP_NAME', 'youtube'))) {
            $this->client->setApplicationName($this->core->config->item('GOOGLE_APP_NAME', 'youtube'));
            $this->client->setClientId($this->core->config->item('GOOGLE_ID', 'youtube'));
            $this->client->setClientSecret($this->core->config->item('GOOGLE_SECRET', 'youtube'));
            $this->client->setRedirectUri(site_url($this->core->config->item('GOOGLE_CALLBACK_URL', 'youtube')));
            $this->client->addScope(array(Google_Service_Plus::USERINFO_PROFILE, Google_Service_Plus::USERINFO_EMAIL, Google_Service_Drive::DRIVE, Google_Service_YouTube::YOUTUBE));
        }else{
            $this->client->setAuthConfig($this->core->config->item('CONFIG_GOOGLE_PATH'),'youtube');
            $this->client->addScope(array(Google_Service_Plus::USERINFO_PROFILE, Google_Service_Plus::USERINFO_EMAIL, Google_Service_Drive::DRIVE, Google_Service_YouTube::YOUTUBE));
        }
        $this->client->setAccessType('offline');
        $this->client->setApprovalPrompt('force');
        $this->Service_YouTube();
    }

    public function Service_YouTube()
    {
        $this->YouTube = new Google_Service_YouTube($this->client);
    }

    public function getVideosList($pageToken = null)
    {
        $params = [];
        if(!is_null($pageToken)) {
            $params = [
                'pageToken' => (string)$pageToken
            ];
        }
        $resp = $this->YouTube->videos->listVideos('id, snippet, contentDetails, player, statistics, status',$params);

        return $resp;
    }

    public function getVideoInfo($id)
    {
        $resp = $this->YouTube->videos->listVideos('id, snippet, contentDetails, player, statistics, status',['id'=>(string)$id]);

        return $resp;
    }
    public function getPlaylists($pageToken = null)
    {
        $params = [];
        if(!is_null($pageToken)) {
            $params = [
                'pageToken' => (string)$pageToken
            ];
        }
        $resp = $this->YouTube->playlists->listPlaylists('id, snippet, contentDetails, player, statistics, status',$params);

        return $resp;
    }

    public function getPlaylistInfo($id)
    {
        $resp = $this->YouTube->playlists->listPlaylists('id, snippet, contentDetails, player, statistics, status',['id'=>(string)$id]);

        return $resp;
    }

    public function getPlaylistsItems($pageToken = null)
    {
        $params = [];
        if(!is_null($pageToken)) {
            $params = [
                'pageToken' => (string)$pageToken
            ];
        }
        $resp = $this->YouTube->playlistItems->listPlaylistItems('id, snippet, contentDetails, player, statistics, status',$params);

        return $resp;
    }

    public function getPlaylistItemInfo($id)
    {
        $resp = $this->YouTube->playlistItems->listPlaylistItems('id, snippet, contentDetails, player, statistics, status',['playlistId'=>(string)$id]);

        return $resp;
    }

    public function getChannels($pageToken = null)
    {
        $params = [];
        if(!is_null($pageToken)) {
            $params = [
                'pageToken' => (string)$pageToken
            ];
        }
        $resp = $this->YouTube->channels->listChannels('id, snippet, contentDetails, player, statistics, status',$params);

        return $resp;
    }

    public function getChannelInfo($id)
    {
        $resp = $this->YouTube->channels->listChannels('id, snippet, contentDetails, player, statistics, status',['id'=>(string)$id]);

        return $resp;
    }
}

