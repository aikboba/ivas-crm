<?php

class YouTubeLink
{
    private $storage_dir;
    private $cookie_dir;
    private $client;
    private $video_id;

    function __construct($video_id)
    {
        $this->storage_dir = sys_get_temp_dir();
        $this->cookie_dir = sys_get_temp_dir();
        $this->client = null;
        $this->video_id = $video_id;
    }

    function setStorageDir($dir)
    {
        $this->storage_dir = $dir;
    }

    private function toHtml($html)
    {
    }

    public function curl($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public static function head($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return http_parse_headers($result);
    }

    public function getPlayerUrl($video_html)
    {
        if (strpos($video_html, 'http') === 0) {
            $video_html = $this->curl($video_html);
        }
        $player_url = null;

        if (preg_match('@<script\s*src="([^"]+player[^"]+js)@', $video_html, $matches)) {
            $player_url = $matches[1];

            if (strpos($player_url, '//') === 0) {
                $player_url = 'http://' . substr($player_url, 2);
            } elseif (strpos($player_url, '/') === 0) {

                $player_url = 'http://www.youtube.com' . $player_url;
            }
        }
        return $player_url;
    }

    public function getPlayerHtml($video_html)
    {
        $player_url = $this->getPlayerUrl($video_html);
        $cache_path = sprintf('%s/%s', $this->storage_dir, md5($player_url));
        if (file_exists($cache_path)) {
            $contents = file_get_contents($cache_path);

        }
        $contents = $this->curl($player_url);

        file_put_contents($cache_path, serialize($contents));
        return $contents;
    }

    public function getSigDecodeFunctionName($player_html)
    {
        $pattern = '@yt\.akamaized\.net\/\)\s*\|\|\s*.*?\s*c\s*&&\s*d\.set\([^,]+\s*,\s*\([^\)]+\)\(([a-zA-Z0-9$]+)@is';
        if (preg_match($pattern, $player_html, $matches)) {
            $func_name = $matches[1];
            $func_name = preg_quote($func_name);
            return $func_name;
        }
        return null;
    }

    public function getSigDecodeInstructions($player_html, $func_name)
    {

        if (preg_match('/' . $func_name . '=function\([a-z]+\){(.*?)}/', $player_html, $matches)) {
            $js_code = $matches[1];
            if (preg_match_all('/([a-z0-9]{2})\.([a-z0-9]{2})\([^,]+,(\d+)\)/i', $js_code, $matches) != false) {

                $obj_list = $matches[1];

                $func_list = $matches[2];

                preg_match_all('/(' . implode('|', $func_list) . '):function(.*?)\}/m', $player_html, $matches2, PREG_SET_ORDER);
                $functions = array();

                foreach ($matches2 as $m) {
                    if (strpos($m[2], 'splice') !== false) {
                        $functions[$m[1]] = 'splice';
                    } elseif (strpos($m[2], 'a.length') !== false) {
                        $functions[$m[1]] = 'swap';
                    } elseif (strpos($m[2], 'reverse') !== false) {
                        $functions[$m[1]] = 'reverse';
                    }
                }

                $instructions = array();
                foreach ($matches[2] as $index => $name) {
                    $instructions[] = array($functions[$name], $matches[3][$index]);
                }
                return $instructions;
            }
        }
        return null;
    }
    public function decodeSignature($signature, $video_html)
    {
        $player_html = $this->getPlayerHtml($video_html);
        $func_name = $this->getSigDecodeFunctionName($player_html);

        $instructions = (array)$this->getSigDecodeInstructions($player_html, $func_name);
        foreach ($instructions as $opt) {
            $command = $opt[0];
            $value = $opt[1];
            if ($command == 'swap') {
                $temp = $signature[0];
                $signature[0] = $signature[$value % strlen($signature)];
                $signature[$value] = $temp;
            } elseif ($command == 'splice') {
                $signature = substr($signature, $value);
            } elseif ($command == 'reverse') {
                $signature = strrev($signature);
            }
        }
        return trim($signature);
    }

    public function stream($id)
    {
        $links = $this->getDownloadLinks($id, "mp4");
        if (count($links) == 0) {
            die("no url found!");
        }

        $url = $links[0]['url'];

        $headers = array(
            'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'
        );
        if (isset($_SERVER['HTTP_RANGE'])) {
            $headers[] = 'Range: ' . $_SERVER['HTTP_RANGE'];
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $headers = '';
        $headers_sent = false;
        $success = false;
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $data) use (&$headers, &$headers_sent) {
            $headers .= $data;

            if (preg_match('@HTTP\/\d\.\d\s(\d+)@', $data, $matches)) {
                $status_code = $matches[1];

                if ($status_code == 200 || $status_code == 206) {
                    $headers_sent = true;
                    header(rtrim($data));
                }
            } else {

                $forward = array('content-type', 'content-length', 'accept-ranges', 'content-range');
                $parts = explode(':', $data, 2);
                if ($headers_sent && count($parts) == 2 && in_array(trim(strtolower($parts[0])), $forward)) {
                    header(rtrim($data));
                }
            }
            return strlen($data);
        });

        curl_setopt($ch, CURLOPT_WRITEFUNCTION, function ($curl, $data) use (&$headers_sent) {
            if ($headers_sent) {
                echo $data;
                flush();
            }
            return strlen($data);
        });
        $ret = @curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        return true;
    }

    private function selectFirst($links, $selector)
    {
        $result = array();
        $formats = preg_split('/\s*,\s*/', $selector);

        foreach ($formats as $f) {
            foreach ($links as $l) {
                if (stripos($l['format'], $f) !== false || $f == 'any') {
                    $result[] = $l;
                }
            }
        }
        return $result;
    }

    public function parseStreamMap($video_html, $video_id)
    {
        $stream_map = array();
        $result = array();

        if (preg_match('@url_encoded_fmt_stream_map["\']:\s*["\']([^"\'\s]*)@', $video_html, $matches)) {
            $stream_map = $matches[1];
        } else {
            $gvi = $this->curl("https://www.youtube.com/get_video_info?el=embedded&eurl=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D" . urlencode($video_id) . "&video_id={$video_id}");
            if (preg_match('@url_encoded_fmt_stream_map=([^\&\s]+)@', $gvi, $matches_gvi)) {
                $stream_map = urldecode($matches_gvi[1]);
            }
        }
        if ($stream_map) {
            $parts = explode(",", $stream_map);
            foreach ($parts as $p) {
                $query = str_replace('\u0026', '&', $p);
                parse_str($query, $arr);
                $result[] = $arr;
            }
            return $result;
        }

        return $result;
    }

    public function getLinks($selector = false)
    {

        $video_id = $this->video_id;
        $video_html = $this->curl("https://www.youtube.com/watch?v={$video_id}");
        $result = array();
        $url_map = $this->parseStreamMap($video_html, $video_id);
        foreach ($url_map as $arr) {
            $url = $arr['url'];
            if (isset($arr['sig'])) {
                $url = $url . '&signature=' . $arr['sig'];
            } elseif (isset($arr['signature'])) {
                $url = $url . '&signature=' . $arr['signature'];
            } elseif (isset($arr['s'])) {
                $signature = $this->decodeSignature($arr['s'], $video_html);
                $url = $url . '&signature=' . $signature;
            }

            $itag = $arr['itag'];
            $result[$itag] = array(
                'url' => $url
            );
        }

        if ($selector) {
            return $this->selectFirst($result, $selector);
        }
        return $result;
    }
}