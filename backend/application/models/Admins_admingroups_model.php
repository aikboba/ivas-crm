<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Admins_admingroups_model
*/
class Admins_admingroups_model extends MY_Model
{
    public $table = 'admins_admingroups';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'admins.email';
    public $order_by_dir = 'ASC';

    public $default_field_list = [
        'admins_admingroups.id as id',
        'admins_admingroups.id as rel_id',
        'admins_admingroups.admin_id',
        'admins_admingroups.admingroup_id',

        'admins.email',
        'admins.first_name',
        'admins.last_name',
        'admins.status as admin_status',

        'admingroups.name',
        'admingroups.status as admingroup_status',

        'admins_admingroups.created',
        'admins_admingroups.updated',
        'UNIX_TIMESTAMP(admins_admingroups.created) as created_unix',
        'UNIX_TIMESTAMP(admins_admingroups.updated) as updated_unix'
    ];



    public function __construct()
    {
        parent::__construct();

        $this->table_full = $this->db->dbprefix.$this->table;

        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('admins', 'admins_admingroups.admin_id = admins.id', 'left');
        $this->db->join('admingroups', 'admins_admingroups.admingroup_id = admingroups.id', 'left');
    }

    /** Производит выборку групп админов для определенного админа
     *
     * @param int $id - admin_ID, для которого производится выборка групп
     * @param boolean $result_by_admingroup_id - если true, то результат преобразуется в массив, где ключами будет admingroup_id (из admingroups), а не id из admins_admingroups
     *
     * @return array
     */
    public function getAdmingroupsByAdminId($id, $result_by_admingroup_id = false)
    {
        $params = [
            [$this->table_full.'.admin_id', '=', intval($id)],
        ];

        $fields = [
            'admins_admingroups.admingroup_id as id',
            'admins_admingroups.id as rel_id',
            'admingroups.name',
            'admingroups.status',
            'admingroups.created',
            'admingroups.updated',
            'UNIX_TIMESTAMP(admingroups.created) as created_unix',
            'UNIX_TIMESTAMP(admingroups.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_admingroup_id)
        {
            $data = [];
            foreach ($res as $admingroup) {
                $data[$admingroup->id] = $admingroup;
            }
        }

        return $data;
    }

    /** Производит выборку админов, состоящих в группе админов
     *
     * @param int $id - admingroup_id, для которой производится выборка пользователей
     *
     * @return array
     */
    public function getAdminsByAdmingroupId($id)
    {
        $params = [
            ['admins_admingroups.admingroup_id', '=', intval($id)],
        ];

        $fields = [
            'admins_admingroups.admin_id as id',
            'admins_admingroups.id as rel_id',
            'admins.email',
            'admins.first_name',
            'admins.last_name',
            'admins.status',
            'admins.created',
            'admins.updated',
            'UNIX_TIMESTAMP(admins.created) as created_unix',
            'UNIX_TIMESTAMP(admins.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** проверка, состоит ли админ в группе админов
     *
     * @param int $admin_id - админ
     * @param int $admingroup_id - admingroup_id, для которой производится проверка вхождения
     *
     * @return boolean
     */
    public function isAdminInAdmingroup($admin_id, $admingroup_id)
    {
        $params = [
            ['admin_id', '=', intval($admin_id)],
            ['admingroup_id', '=', intval($admingroup_id)],
        ];

        $fields = [
            'admins_admingroups.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по admin_id и admingroup_id
     *
     * @param int $admin_id - клиент
     * @param int $admingroup_id - admingroup_id, для которой производится проверка вхождения
     *
     * @return array
     */
    public function getByAdminIdAdmingroupId($admin_id, $admingroup_id)
    {
        $params = [
            ['admin_id', '=', intval($admin_id)],
            ['admingroup_id', '=', intval($admingroup_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет админа в группу. Если его там нет.
     *
     * @param int $admin_id - admin_id, который добавляетя вгруппу
     * @param int $admingroup_id - admingroup_id, в которую добавляется клиент
     *
     * @return int
     */
    public function addAdminToAdmingroup($admin_id, $admingroup_id)
    {
        $record = $this->getByAdminIdAdmingroupId($admin_id, $admingroup_id);

        // человек НЕ в группе - добавляем
        if (empty($record))
        {
            $uData = [
                'admin_id'=>intval($admin_id),
                'admingroup_id'=>intval($admingroup_id),
            ];
            $res_id = $this->insert($uData);
        }
        else
        {
            // человек уже в группе
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Удаляет запись по ID.
     * Здесь ВНИМАНИЕ: удаление по id вызывает getByAdminIdAdmingroupId который вызывает getById и в итоге вызывает parent::delete
     * Это кажется идиотизмом. Но только до тех пока удаление - простой процесс, незатрагивающий более ничего.
     * Если же будет сложная логика (например: удаление админа из группы влечет снятие привилегий), то лучше иметь 1 hub-функцию,
     * которая делает удаление
     *
     * @param int $id - admins_admingroups.id - id связки
     *
     * @return boolean
     */
    public function delete($id)
    {
        $record = $this->getById($id);

        // нет такой связки. нечего удалять - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // человек в группе - удаляем
            /*
                здесь доп логика
            */
            $id = $record->id;
            $res_id = parent::delete($id);
        }

        return $res_id;
    }

    /** Убирает админа из группы.
     *
     * @param int $admin_id - admin_id, который убирается из группы
     * @param int $admingroup_id - admingroup_id, из которой убирается клиент
     *
     * @return boolean
     */
    public function removeAdminFromAdmingroup($admin_id, $admingroup_id)
    {
        $record = $this->getByAdminIdAdmingroupId($admin_id, $admingroup_id);

        // человек НЕ в группе - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // человек в группе - удаляем
            $id = $record->id;
            $res_id = $this->delete($id);
        }

        return $res_id;
    }


    /** Исключает админа из всех групп (простое уделение без учета дополнительной логической нагрузки)
     *
     * @param int $admin_id - ID админа, для коорого очищаютя группы
     *
     * @return int
     */
    public function clearAdmingroups($admin_id)
    {
        $params = [
            ['admin_id', '=', intval($admin_id)],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }


}
