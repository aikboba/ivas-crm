<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Admin Model
* @param Admins_admingroups_model  $admins_admingroups_model
*/

class Admins_model extends MY_Model
{
    public $table = 'admins';
    public $primary = 'id';
    public $entity = 'admin';
    public $error;
    public $order_by_field = 'email';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'admins.id',
        'ip_address',
        'email',
        'first_name',
        'last_name',
        'phone',
        'timezone',
        'last_login_datetime',
        'admins.status',
        'admins.created',
        'admins.updated',
        'is_super',
        'salt',
        'GROUP_CONCAT(distinct admingroups.`id` order by admingroups.`name` SEPARATOR ",") as admingroups_list',
        'UNIX_TIMESTAMP(admins.created) as created_unix',
        'UNIX_TIMESTAMP(admins.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->CI->load->model('permissions_model');
        $this->CI->load->model('admins_admingroups_model');
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Выборка по ID. С правами
     *
     * @param int $id
     *
     * @return array of objects | null
     */
    public function getById($id)
    {
        $admin = parent::getById($id);

        if (!empty($admin))
        {
            // получаем права ---
            $admin->permissions = $this->CI->permissions_model->getByAdminId($admin->id);
            // получаем админгруппы---
            $admin->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($admin->id);
        }

        return $admin;
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('admins_admingroups', '(admins_admingroups.admin_id = admins.id)', 'left');
        $this->db->join('admingroups', '(admins_admingroups.admingroup_id = admingroups.id and admingroups.status="active")', 'left');
        $this->db->group_by('admins.id');
    }

    /** Выборка по EMAIL. С правами и учетом статуса
     *
     * @param string $email
     * @param string $status
     *
     * @return array of objects | null
     */
    public function getByEmail($email, $status=null)
    {
        $params = [
            ['email', '=', $email]
        ];

        if (!empty($status))
        {
            $params[] = ['status', '=', $status];
        }

        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $admin = [];
        if (!empty($res))
        {
            $admin = $res[0];
            // получаем права
            $admin->permissions = $this->CI->permissions_model->getByAdminId($admin->id);
            // получаем админгруппы---
            $admin->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($admin->id);
        }

        return $admin;
    }

    /** Выборка по secret_code (части процесса восстановления пароля). С правами
     *
     * @param string $code
     *
     * @return array of objects | null
     */
    public function getBySecretCode($code)
    {
        $params = [
            ['secret_code', '=', $code]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $admin = [];
        if (!empty($res))
        {
            $admin = $res[0];
            // получаем права
            $admin->permissions = $this->CI->permissions_model->getByAdminId($admin->id);
            // получаем админгруппы---
            $admin->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($admin->id);
        }

        return $admin;
    }

    /** Логин для админа
     *
     * @param mixed $reg_param - email
     * @param string $password - пароль
     *
     * @return object
     */
    public function getByLoginData($reg_param, $password)
    {
        $reg_param = trim($reg_param);
        $password = trim($password);
        $params = "admins.email='".$reg_param."'
                    AND admins.status='active'
                    AND admins.password=sha1(CONCAT('".$password."',admins.salt))";
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $admin = [];
        if (!empty($res))
        {
            $admin = $res[0];
            // получаем права
            $admin->permissions = $this->CI->permissions_model->getByAdminId($admin->id);
            // получаем админгруппы---
            $admin->admingroups = $this->CI->admins_admingroups_model->getAdmingroupsByAdminId($admin->id);
        }

        return $admin;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }


}
