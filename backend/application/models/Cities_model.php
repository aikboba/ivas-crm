<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Cities_model
 * @property Countries_model $countries_model
*/
class Cities_model extends MY_Model
{
    public $table = 'cities';
    public $entity = 'city';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'cities.id as id',
        'cities.country_id',
        'cities.name',
        'cities.status',

        'countries.name as country_name',
        'countries.iso as country_iso',
        'countries.status as country_status',

        'cities.created',
        'cities.updated',
        'UNIX_TIMESTAMP(cities.created) as created_unix',
        'UNIX_TIMESTAMP(cities.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('countries', 'countries.id = cities.country_id', 'inner');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('cities.status = "active"');
            $this->db->where('countries.status = "active"');
        }
    }

    /** Производит выборку городов для определенной страны
     *
     * @param int $country_id - ID страны, для которой производитс выборка
     *
     * @return array
     */
    public function getByCountryId($country_id, $amount = 0)
    {
        $params = [
            ['cities.country_id', '=', intval($country_id)],
        ];

        $fields = [
            'cities.id as id',
            'cities.name as name',
            'cities.created',
            'cities.updated',
            'UNIX_TIMESTAMP(cities.created) as created_unix',
            'UNIX_TIMESTAMP(cities.updated) as updated_unix'
        ];

        if (!empty($amount)) $limit = ['offset'=>0, 'limit'=>$amount];
            else $limit = [];

        $res = $this->getByParams($params, [], $limit, $fields);
        $data = $res;

        return $data;
    }

}
