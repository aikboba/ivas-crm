<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cli_priv_model extends MY_Model
{
    public $table = 'cli_priv';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'email';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'cli_priv.id as id',
        'cli_priv.id as rel_id',
        'cli_priv.cli_id as cli_id',
        'email',
        'phone',
        'first_name',
        'last_name',
        'cli_priv.priv_id as priv_id',
        'name',
        'description',
        'discount_type',
        'discount',
        'valid_until',
        'cli_priv.created as created',
        'cli_priv.updated as updated',
        'UNIX_TIMESTAMP(cli_priv.created) as created_unix',
        'UNIX_TIMESTAMP(cli_priv.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'cli_priv.cli_id = clients.id', 'left');
        $this->db->join('privileges', 'cli_priv.priv_id = privileges.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        $this->db->where('privileges.status = "active"');

        // если вызов пришел из фронтового контроллера, то выбираем только "неистекшее членство"
        if ($this->caller_origin == 'front')
        {
            $this->db->where('( valid_until >= NOW() OR valid_until IS NULL )');
        }
    }

    /** Производит выборку групп привилегий, в которых состоит данный клиент
     *
     * @param int $id - CliID, для которого производитс выборка привилегий
     * @param boolean $result_by_priv_id - если true, то результат преобразуется в массив, где ключами будет priv_id (из privileges), а не id из cli_priv
     *
     *
     * @return array
     */
    public function getPrivilegesByCliId($id, $result_by_priv_id = false)
    {
        $params = [
            ['clients.id', '=', intval($id)],
        ];

        $fields = [
            'cli_priv.priv_id as id',
            'cli_priv.id as rel_id',
            'cli_priv.valid_until as valid_until',
            'name',
            'description',
            'discount_type',
            'discount',
            'cli_priv.created as created',
            'cli_priv.updated as updated',
            'UNIX_TIMESTAMP(cli_priv.created) as created_unix',
            'UNIX_TIMESTAMP(cli_priv.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_priv_id)
        {
            $data = [];
            foreach ($res as $priv) {
                $data[$priv->priv_id] = $priv;
            }
        }

        return $data;
    }

    /** Производит выборку пользователей, состоящий в группе привилегий
     *
     * @param int $id - priv_id, для которой производится выборка пользователей
     *
     * @return array
     */
    public function getClientsByPrivId($id)
    {
        $params = [
            ['privileges.id', '=', intval($id)],
        ];

        $fields = [
            'cli_priv.id as id',
            'cli_priv.id as rel_id',
            'cli_priv.cli_id as cli_id',
            'clients.email',
            'clients.phone',
            'clients.first_name',
            'clients.last_name',
            'cli_priv.created as created',
            'cli_priv.updated as updated',
            'UNIX_TIMESTAMP(cli_priv.created) as created_unix',
            'UNIX_TIMESTAMP(cli_priv.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** Производит выборку записей определенного клиента
     *
     * @param int $cli_id - Client_ID, для которого производитс выборка
     *
     * @return array
     */
    public function getByCliId($cli_id)
    {
        $params = [
            ['cli_priv.cli_id', '=', intval($cli_id)],
        ];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }

    /** Берет запись по cli_id и priv_id
     *
     * @param int $cli_id - ID клиента
     * @param int $priv_id - ID группы привилегий
     *
     * @return array
     */
    public function getByClientIdPrivilegeId($cli_id, $priv_id)
    {
        $params = [
            ['cli_id', '=', intval($cli_id)],
            ['priv_id', '=', intval($priv_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
        else return null;
    }


    /** Убирает клиента из группы привилегий.
     *
     * @params int $client_id - ID убираемого клиента
     * @params int $privilege_id - ID группы привилегий
     *
     * @return boolean
     */
    public function removeClientFromPrivilege($cli_id, $priv_id)
    {
        $record = $this->getByClientIdPrivilegeId($cli_id, $priv_id);

        // пресет НЕ в группе - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // пресет в группе - удаляем
            $id = $record->id;
            $res_id = $this->delete($id);
        }

        return $res_id;
    }

    /** Ищет совпадающий с "ключами" элемент по параметрам в переданном массиве
     *
     * @param array of objects $data - массив для поиска
     * @param int $priv_id - ID льготной группы
     *
     * @return object | null
     */
    private function findInData($data, $priv_id)
    {
        foreach ($data as $key => $elem)
        {
            if ( ($elem->priv_id==$priv_id) ) return $elem;
        }

        return null;
    }

    /** Копирует (через insertOrUpdate) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $data_donor = $this->getByCliId($donor_id);
        $data_acceptor = $this->getByCliId($acceptor_id);

        $result = true;
        foreach ($data_donor as $elem)
        {
            $acc = $this->findInData($data_acceptor, $elem->priv_id);

            if (empty($acc))
            {
                //у акцептора еще нет этой льготы вообще
                $valid_until = $elem->valid_until;
                $created = $elem->created;
                $updated = $elem->updated;
            }
            else
            {
                if( (empty($acc->valid_until)) || (empty($elem->valid_until)) )
                {
                    $valid_until = 'NULL';
                    $created = $acc->created;
                    $updated = $acc->updated;
                }
                else
                {
                    $dt_acceptor = new Datetimeex($acc->valid_until);
                    $dt_donor = new Datetimeex($elem->valid_until);

                    if ($dt_donor->isLater($dt_acceptor))
                    {
                        //у донора предел участия в группе больше
                        $valid_until = $elem->valid_until;
                        $created = $elem->created;
                        $updated = $elem->updated;
                    }
                    else
                    {
                        //у акцептора предел участия в группе больше
                        $valid_until = $acc->valid_until;
                        $created = $acc->created;
                        $updated = $acc->updated;
                    }
                }
            }

            if (empty($valid_until)) $valid_until = 'NULL';
            $iData = [
                'cli_id'  => $acceptor_id,
                'priv_id' => $elem->priv_id,
                'valid_until' => $valid_until,
                'created' => $created,
                'updated' => $updated,
            ];

            $res_id = $this->insertOrUpdate($iData);
            $result = $result & (!empty($res_id));
        }

        return $result;
    }

}