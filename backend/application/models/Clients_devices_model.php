<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Clients_devices_model
 * @property products_model $products_model
*/
class  Clients_devices_model extends MY_Model
{
    public $table = 'clients_devices';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'clients_devices.id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'clients_devices.id as id',
        'clients_devices.device',
        'clients_devices.client_id',

        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.status as client_status',

        'clients_devices.updated',
        'UNIX_TIMESTAMP(clients_devices.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'clients_devices.client_id = clients.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('clients.status = "active"');
        }
    }

    /** Производит выборку devices для определенного client
     *
     * @param int $id - client_id, для которого производитс выборка устройств
     *
     * @return array
     */
    public function getByClientId($id)
    {
        $params = [
            ['clients_devices.client_id', '=', intval($id)],
        ];

        $fields = [
            'clients_devices.id as id',
            'clients_devices.device',
            'clients_devices.updated',
            'UNIX_TIMESTAMP(clients_devices.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;

        return $data;
    }

    /** Берет запись по device
     *
     * @param int $device - GUID устройства

     * @return array
     */
    public function getByDevice($device)
    {
        $params = [
            ['device', '=', $device],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Переодпределенный метод базовой модели. Выполняет InsertOrUpdate
     * NOTICE: affected_rows после "insert ... update" возвращает
     * 1 - произошел insert
     * 0 - если произошел update и НИ одно значение НЕ изменилось
     * 2 - если произошел update и ХОТЬ одно значение изменилось
     *
     * @param array $data
     *
     * @return boolean
     */
/*
    public function insertOrUpdate($data)
    {
        $data = $this->filterFields($data);
        foreach ($data as $fname=>$fval)
        {
            $d = strtoupper($fval);
            if ( (is_null($d)) || ($d=='NULL') ) $data[$fname] = null;
        }

        $fields = array_keys($data);
        $fields_str = join(",",$fields);

        $val_tpl = $expr = "";
        foreach ($fields as $fname)
        {
            $val_tpl.="?,";
            $expr.=$fname."=VALUES(".$fname."),";
        }
        $val_tpl = substr($val_tpl, 0, -1);
        $expr .= " id=LAST_INSERT_ID(id)"; // это лайфхак для получения id через last_insert_id для обоих случаев (insert или update)

        $sql = "INSERT INTO ".$this->table_full." ({$fields_str}) 
        VALUES ({$val_tpl})
        ON DUPLICATE KEY UPDATE {$expr}";

        $res = $this->db->query($sql, $data); // вернет true или false

        if ($res)
        {
            $id = $this->db->insert_id();

            $logData = [
                'entity' => $this->entity,
                'entity_id' => $id,
                'event' => 'create',
            ];
            $this->toLog($logData);

            return $id;
        }
        else return false;
    }
*/

    /** Добавляет (либо изменяет клиента, если device существует) клиенту запись об устройстве
     *
     * @param int $device - GUID устройства
     * @param int $client_id - клиент, из которого убирается пресет
     *
     * @return int|boolean
     */
    public function  addDeviceToClient($device, $client_id)
    {
        $iData = [
            'device' => $device,
            'client_id' => $client_id
        ];
        $res_id = $this->insert($iData);

        return $res_id;
    }

    /** Создает запись, используя insertOrUpdate
     *
     * @param array $data - массив с данными $data = ['device'=>'asd-asd-asds', 'client_id'=>12]
     *
     * @return int id
     */
    public function create($data)
    {
        return $this->insertOrUpdate($data);
    }

    /** Переопределенный метод. Всегда использует insertOrUpdate
     *
     * @param array $data - массив с данными
     *
     * @return int id
     */
    public function insert($data)
    {
        return $this->insertOrUpdate($data);
    }

    /** Убирает запись с device
     *
     * @param int $device - GUID устройства
     *
     * @return boolean|int
     */
    public function removeByDevice($device)
    {
        $params = [
            ['device', '=', $device],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }


    /** Копирует (через insertOrUpdate) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $data = $this->getByClientId($donor_id);

        $result = true;
        foreach ($data as $elem)
        {
            $iData = [
                'device'  => $elem->device,
                'client_id' => $acceptor_id,
                'updated' => $elem->updated,
            ];
            $res_id = $this->insertOrUpdate($iData);
            $result = $result & (!empty($res_id));
        }

        return $result;
    }

}
