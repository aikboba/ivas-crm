<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Clients_groups_model
 * @property Clients_groups_model $clients_groups_model
 * @property Groups_presets_model $groups_presets_model
 */
class Clients_groups_model extends MY_Model
{
    public $table = 'clients_groups';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'clients.email';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'clients_groups.id as id',
        'clients_groups.id as rel_id',
        'clients_groups.client_id',
        'clients_groups.group_id',
        'clients.email',
        'clients.phone',
        'clients.first_name',
        'clients.last_name',
        'groups.name',
        'groups.status as group_status',
        'clients_groups.created',
        'clients_groups.updated',
        'UNIX_TIMESTAMP(clients_groups.created) as created_unix',
        'UNIX_TIMESTAMP(clients_groups.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);

        $this->CI->load->model('groups_presets_model');
        $this->CI->load->model('clients_presets_model');
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'clients_groups.client_id = clients.id', 'left');
        $this->db->join('groups', 'clients_groups.group_id = groups.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('clients.status = "active"');
            $this->db->where('groups.status = "active"');
        }
    }

    /** Производит выборку записей определенного клиента
     *
     * @param int $id - Client_ID, для которого производитс выборка
     *
     * @return array
     */
    public function getByClientId($id)
    {
        $params = [
            ['clients_groups.client_id', '=', intval($id)],
        ];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }

    /** Производит выборку записей определенной группы
     *
     * @param int $id - group_id, для которой производитс выборка
     * @param boolean $result_by_group_id - если true, то результат преобразуется в массив, где ключами будет group_id (из groups), а не id из clients_groups
     *
     * @return array
     */
    public function getByGroupId($id)
    {
        $params = [
            ['clients_groups.group_id', '=', intval($id)],
        ];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }


    /** Производит выборку групп клиентов для определенного клиента
     *
     * @param int $id - Client_ID, для которого производитс выборка групп
     * @param boolean $result_by_group_id - если true, то результат преобразуется в массив, где ключами будет group_id (из groups), а не id из clients_groups
     *
     * @return array
     */
    public function getGroupsByClientId($id, $result_by_group_id = false)
    {
        $params = [
            ['clients_groups.client_id', '=', intval($id)],
        ];

        $fields = [
            'clients_groups.group_id as id',
            'clients_groups.id as rel_id',
            'clients_groups.created as rel_created',
            'groups.name',
            'groups.status',
            'groups.created',
            'groups.updated',
            'UNIX_TIMESTAMP(groups.created) as created_unix',
            'UNIX_TIMESTAMP(groups.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_group_id)
        {
            $data = [];
            foreach ($res as $group) {
                $data[$group->id] = $group;
            }
        }

        return $data;
    }

    /** Производит выборку клиентов, состоящих в группе клиентов
     *
     * @param int $id - group_id, для которой производится выборка пользователей
     *
     * @return array
     */
    public function getClientsByGroupId($id)
    {
        $params = [
            ['clients_groups.group_id', '=', intval($id)],
        ];

        $fields = [
            'clients_groups.client_id as id',
            'clients_groups.id as rel_id',
            'clients.email',
            'clients.phone',
            'clients.first_name',
            'clients.last_name',
            'clients.status',
            'clients.created',
            'clients.updated',
            'UNIX_TIMESTAMP(clients.created) as created_unix',
            'UNIX_TIMESTAMP(clients.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** проверка, состоит ли клиент в группе
     *
     * @param int $client_id - клиент
     * @param int $group_id - group_id, для которой производится проверка вхождения
     *
     * @return boolean
     */
    public function isClientInGroup($client_id, $group_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['group_id', '=', intval($group_id)],
        ];

        $fields = [
            'clients_groups.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по client_id и group_id
     *
     * @param int $client_id - клиент
     * @param int $group_id - group_id, для которой производится проверка вхождения
     *
     * @return array
     */
    public function getByClientIdGroupId($client_id, $group_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['group_id', '=', intval($group_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет клиента в группу. Если его там нет.
     *  Также устаканивает связку клиент-presets (которые являются подарочными при членстве в группе)
     *
     * @param int $client_id - client_id, который добавляетя вгруппу
     * @param int $group_id - group_id, в которую добавляется клиент
     *
     * @return int
     */
    public function addClientToGroup($client_id, $group_id)
    {
        $record = $this->getByClientIdGroupId($client_id, $group_id);

        // человек НЕ в группе - добавляем
        if (empty($record))
        {
            $uData = [
                'client_id'=>intval($client_id),
                'group_id'=>intval($group_id),
            ];
            $res_id = $this->insert($uData);

            // добавляем клиентам, входящим в группу пресеты, привязанные к этой группе -----------------------------------------
            if ($res_id)
            {
                $presets = $this->CI->groups_presets_model->getPresetsByGroupId($group_id);

                foreach ($presets as $preset)
                {
                    $starting_dt = null;
                    $expiration_dt = null;
                    $origin = 'group';
                    $this->CI->clients_presets_model->addPresetToClient($preset->id, $client_id, $starting_dt, $expiration_dt, $origin, $group_id);
                }
            }
            // END добавляем клиентам, входящим в группу пресеты, привязанные к этой группе -----------------------------------------
        }
        else
        {
            // человек уже в группе
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Убирает клиента из группы.
     *  Также устаканивает связку клиент-presets (которые являются подарочными при членстве в группе)
     *
     * @param int $client_id - client_id, который убирается из группы
     * @param int $group_id - group_id, из которой убирается клиент
     *
     * @return boolean
     */
    public function removeClientFromGroup($client_id, $group_id)
    {
        $record = $this->getByClientIdGroupId($client_id, $group_id);

        // человек НЕ в группе - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // человек в группе - удаляем
            $id = $record->id;
            $res_id = $this->delete($id);

            // синхронизируем состояние клиенты-пресеты ------------------
            if ($res_id)
            {
                $presets = $this->CI->groups_presets_model->getPresetsByGroupId($group_id);

                foreach ($presets as $preset)
                {
                    $this->CI->clients_presets_model->removePresetFromClient($preset->id, $client_id, 'group', $group_id);
                }

            }
            // END синхронизируем состояние клиенты-пресеты ------------------
        }

        return $res_id;
    }


    /** Копирует (через insertOrUpdate) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $data = $this->getByClientId($donor_id);

        $result = true;
        foreach ($data as $elem)
        {
            $iData = [
                'client_id'  => $acceptor_id,
                'group_id' => $elem->group_id,
                'created' => $elem->created,
                'updated' => $elem->updated,
            ];
            $res_id = $this->insertOrUpdate($iData);
            $result = $result & (!empty($res_id));
        }

        return $result;
    }


}
