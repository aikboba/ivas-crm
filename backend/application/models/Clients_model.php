<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends MY_Model
{
    public $table = 'clients';
    public $entity = 'client';
    public $primary = 'id';
    public $order_by_field = 'email';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'clients.id',
        'user_insta_nick',
        'user_insta_pk',
        'getcourse_id',
        'phone',
        'email',
        'sex',
        'clients.country_id as country_id',
        'countries.name as country_name',
        'clients.city_id as city_id',
        'cities.name as city_name',
        'salt',
        'first_name',
        'patronym_name',
        'last_name',
        'GROUP_CONCAT(distinct privileges.`id` order by privileges.`name` SEPARATOR ",") as priv_list',
        'GROUP_CONCAT(distinct privileges.`name` order by privileges.`name` SEPARATOR ",") as priv_names_list',
        'GROUP_CONCAT(distinct groups.`id` order by groups.`name` SEPARATOR ",") as groups_list',
        'GROUP_CONCAT(distinct groups.`name` order by groups.`name` SEPARATOR ",") as groups_names_list',
        'avatar',
        'timezone',
        'birthday',
        'is_blogger',
        'subscribed_system',
        'subscribed_news',
        'clients.status',
        'clients.last_act_dt',
        'clients.created',
        'clients.updated',
        'UNIX_TIMESTAMP(clients.created) as created_unix',
        'UNIX_TIMESTAMP(clients.updated) as updated_unix'
    ];

    public $www_path = "";

    private $online_minutes_threshold = 15; // число минут, по истечении которых с момента last_act_dt, клиент считается offline

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->CI->load->model('cli_priv_model');
        $this->CI->load->model('clients_groups_model');
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Переопределенный метод. Подготавливает вывод
     *
     * @params (array of StdClass) $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
        if (!empty($data))
        {
            // если у контроллера www_path не задан, то выбираем его из конфига
            if (empty($this->www_path))
            {
                $this->CI->load->config('ftp', true);
                $this->www_path = $this->CI->config->item('ftp_www_avatars_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт
            }

            foreach ($data as $key=>$client)
            {
                $data[ $key ]->name = $client->avatar;

                if ( (!empty($client->avatar)) && (strpos("://",$client->avatar)===false) )
                {
                    $data[ $key ]->url = $this->www_path.$client->avatar;
                }
                else
                {
                    $data[ $key ]->url = $client->avatar;
                }
            }
        }

        return  $data;
    }


    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Находит client, заданный через email или инста-ник или телефон
     *
     * @param mixed $reg_param
     * @param string $status = null - если задан, то ищет среди clients с этим статусом
     *
     * @return boolean
     */
    public function getByRegData($reg_param, $status = null)
    {
        $this->db->select($this->default_field_list);

        $this->db->group_start();
        $this->db->or_where('user_insta_nick',$reg_param)
            ->or_where('email',$reg_param)
            ->or_where('phone',$reg_param);
        $this->db->where('email', $reg_param);
        $this->db->group_end();

        if (!empty($status)) $this->db->where('clients.status',$status);
        $this->db->join('countries', 'clients.country_id = countries.id', 'left');
        $this->db->join('cities', 'clients.city_id = cities.id', 'left');
        $this->db->join('cli_priv', '(cli_priv.cli_id = clients.id)', 'left');
        $this->db->join('privileges', '(cli_priv.priv_id = privileges.id and privileges.status="active")', 'left');
        $this->db->join('clients_groups', '(clients_groups.client_id = clients.id)', 'left');
        $this->db->join('groups', '(clients_groups.group_id = groups.id and groups.status="active")', 'left');
        $this->db->group_by('clients.id');

        $this->db->limit(1);
        $res = $this->db->get($this->table);
        $client = $res->row();

        if (!empty($client))
        {
            $client = $this->extendClient($client);
        }

        return $client;
    }

    /** Логин для клиента
     *
     * @param mixed $reg_param - phone или email или user_insta_nick
     * @param string $password = пароль
     *
     * @return object
     */
    public function getByLoginData($reg_param, $password)
    {
        $reg_param = trim($reg_param);
        $password = trim($password);
        $params = "clients.email='".$reg_param."'
                   AND clients.pass=sha1(CONCAT('".$password."',clients.salt))";
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $client = [];
        if (!empty($res))
        {
            $client = $res[0];
            $client = $this->extendClient($client);
        }

        return $client;
    }

    /** Соль пользователя (для восстановления пароля)
     *
     * @param int $id - client_id пользователя
     *
     * @return string
     */
    public function getSalt($id)
    {
        $this->db->select("clients.salt");
        $this->db->where("clients.id=".$id);
        $this->db->limit( 1, 0 );
        $res = $this->db->get($this->table);
        $res = $res->row();
        if ($res)
        {
            return $res->salt;
        }

        return '';
    }

    /** Пароль пользователя
     *
     * @param int $id - client_id пользователя
     *
     * @return string
     */
    public function getPass($id)
    {
        $this->db->select("clients.pass");
        $this->db->where("clients.id=".$id);
        $this->db->limit( 1, 0 );
        $res = $this->db->get($this->table);
        $res = $res->row();
        if ($res)
        {
            return $res->pass;
        }

        return '';
    }


    /** Выборка по ID/ С привилегиями / С группами
     *
     * @param int $id
     *
     * @return object | null
     */
    public function getById($id)
    {
        $client = parent::getById($id);

        if (!empty($client))
        {
            $client = $this->extendClient($client);
        }

        return $client;
    }

    /** Выборка по списку ID БЕЗ ПРИВИЛЕГИЙ И ГРУПП
     *
     * @param int $ids
     *
     * @return array of objects | null
     */
    public function getByIds($ids)
    {
        if (is_array($ids)) $ids = join(',',$ids);

        $params = "clients.id IN (".$ids.")";
        $list = $this->getByParams($params);

        return $list;
    }

    /** Выборка по secret_code (части процесса восстановления пароля). С привилегиями
     *
     * @param string $code
     *
     * @return array of objects | null
     */
    public function getBySecretCode($code)
    {
        $caller = $this->CI->caller_origin;
        $this->CI->caller_origin = 'admin'; // этот метод будет вызываться при АКТИВАЦИИ inactive client. Т.е. с фронтА мне нужно выбрать status='inactive'

        $params = [
            ['secret_code', '=', $code]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $client = [];
        if (!empty($res))
        {
            $client = $res[0];
            $client = $this->extendClient($client);
        }

        $this->CI->caller_origin = $caller;

        return $client;
    }

    /** Выборка по phone. С привилегиями
     *
     * @param string $phone
     *
     * @return array of objects | null
     */
    public function getByPhone($phone)
    {
        $params = [
            ['phone', '=', $phone]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $client = [];
        if (!empty($res))
        {
            $client = $res[0];
            $client = $this->extendClient($client);
        }

        return $client;
    }

    /** Расширяет извлеченный объект другими необходимыми сущностями
     *
     * @param object $client
     * @return object
     */
    public function extendClient($client)
    {
        // получаем группы льготы клиента ---
        $client->privileges = $this->CI->cli_priv_model->getPrivilegesByCliId($client->id);
        // получаем группы клиента ---
        $client->groups = $this->CI->clients_groups_model->getGroupsByClientId($client->id);

        return $client;
    }

    /** Выборка по user_insta_nick. С привилегиями
     *
     * @param string $nick
     *
     * @return array of objects | null
     */
    public function getByNick($nick)
    {
        $params = [
            ['user_insta_nick', '=', $nick]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $client = [];
        if (!empty($res))
        {
            $client = $res[0];
            $client = $this->extendClient($client);
        }

        return $client;
    }


    /** Определяет, входит ли клиент (переданный как StdClass или заданный через id) хотя бы в одну группу клиентов
     *
     * @param int | object $client
     *
     * @return boolean
     */
    public function hasGroups($client)
    {

        if (!is_object($client))
        {
            // считаем, что client - это id - достаем из таблицы
            $client = $this->getById( intval($client) );
        }

        // считаем, что client - это объект
        $res = ( (!empty($client)) && (count($client->groups)!=0) );

        return $res;
    }

    /** Определяет, входит ли клиент (переданный как StdClass или заданный через id) хотя бы в одну льготную группу
     *
     * @param int | object $client
     *
     * @return boolean
     */
    public function hasPrivileges($client)
    {

        if (!is_object($client))
        {
            // считаем, что client - это id - достаем из таблицы
            $client = $this->getById( intval($client) );
        }

        // считаем, что client - это объект
        $res = ( (!empty($client)) && (count($client->privileges)!=0) );

        return $res;
    }

    /** Собирает join'ы
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('countries', 'clients.country_id = countries.id', 'left');
        $this->db->join('cities', 'clients.city_id = cities.id', 'left');
        $this->db->join('cli_priv', '(cli_priv.cli_id = clients.id)', 'left');
        $this->db->join('privileges', '(cli_priv.priv_id = privileges.id and privileges.status="active")', 'left');
        $this->db->join('clients_groups', '(clients_groups.client_id = clients.id)', 'left');
        $this->db->join('groups', '(clients_groups.group_id = groups.id and groups.status="active")', 'left');
        $this->db->group_by('clients.id');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where($this->table_full.'.status = "active"');
        }

    }


    /** Производит выборку по параметрам. Соединение WHERE через AND
     *
     *   @param array $params - массив массивов. $params = [
     *                                               ["admin_name","=","Василий"],
     *                                               ["admin_login","=","Vas"],
     *                                           ];
     *   @param array $order - array of string $order = [
     *                                                  "admin_name ASC",
     *                                                  "admin_login DESC",
     *                                         ];
     *   @param array $limit - массив $limit = [
     *                                           "limit" => 10,
     *                                           "offset" => 10,
     *                                          ];
     *   @param array $fields - список возвращаемых полей $fields = ['id','name'];
     *   @param boolean $returnForServerSide - возвращать ли результат в виде ['totalRows'=>XXX, 'rows'=>array of StdClass of rows ](это нужно для VUE DataTables remote mode)
     *      либо просто в виде - array of StdClass of rows - для любых остальных случаев
     *
     * @return array
     *
     * Пример: $params = [
     *          ["admin_name","=","'Василий'"],
     *          ["admin_lastname","=","CONCAT('Vas',admin_lastname)"],
     *          ];
     */
    public function getByParams($params = [], $order = [], $limit = [], $fields = [], $returnForServerSide = false)
    {
        if (empty($order))
        {
            $order = [$this->order_by_field." ".$this->order_by_dir];
        }
        if (empty($fields))
        {
            $fields = $this->default_field_list;
        }

        $this->db->order_by( join(",",$order));

        if ($returnForServerSide)
        {
            $this->db->select( "SQL_CALC_FOUND_ROWS ".join(",",$fields), false);
        }
        else
        {
            $this->db->select( join(",",$fields), false );
        }

        $this->processJoins();

        if (!empty($params))
        {
            if (is_array($params)) // параметры заданы массивом
            {
                foreach ($params as $key => $param)
                {
                    if (is_array($param) && count($param)>1)
                    {
                        $field = $param[0];
                        $relation = $param[1];
                        $value = $param[2];
                        //$value = $this->db->escape($value);
                        //$this->db->where($field.$relation.$value, null, false);
                        //            if ($this->needToQuote($value)) $value = $this->db->escape($value);
                        $value = $this->db->escape($value);

                        if ($field == 'priv_list')
                        {   // костыль. В MySQL нельзя использовать aliases in where
                            $this->db->having($field . $relation . $value, null, false);
                        }
                        elseif ($field == 'priv_names_list')
                        {   // костыль. В MySQL нельзя использовать aliases in where
                            $this->db->having($field . $relation . $value, null, false);
                        }
                        elseif ($field == 'groups_list')
                        {   // костыль. В MySQL нельзя использовать aliases in where
                            $this->db->having($field . $relation . $value, null, false);
                        }
                        elseif ($field == 'groups_names_list')
                        {   // костыль. В MySQL нельзя использовать aliases in where
                            $this->db->having($field . $relation . $value, null, false);
                        }
                        else
                        {
                            // если в "имени поля" нет '.', и это НЕ 'служебное слово' то добавляем к имени поля текущую таблицу-
                            // ПОМНИТЬ ОБ ЭТОМ при использовании params вида count(id) -> должно быть COUNT(id) (чтобы не сработало второе условие)
                            if ((strpos($field, ".") === false) && (strtolower($field) == $field)) {
                                $field = $this->db->dbprefix($this->table) . "." . $field;
                            }
                            $this->db->where($field . $relation . $value, null, false);
                        }
                    }
                    else
                    {
                        // параметры - это строки
                        $this->db->where($param[0], null, false);
                    }
                } // foreach
            }
            else // параметры заданы строкой.
            {
                $this->db->where($params, null, false);
            }
        }

        $this->processAdditionalWhere();

        if (!empty($limit)) $this->db->limit( $limit['limit'], $limit['offset'] );
/*
$sql = $this->db->get_compiled_select($this->table);
echo "<pre>";
print_r($sql);
echo "</pre>";
die();
*/
        $res = $this->db->get($this->table);
        $rows = $res->result();

        $rows = $this->prepareOutput($rows);

        if ($returnForServerSide)
        {
            $totalRecords = $this->db->query("SELECT FOUND_ROWS() as rowcount")->result()[0]->rowcount;
            return ['totalRows'=>$totalRecords, 'rows'=>$rows];
        }

        return $rows;
    }

    /** Обновляет значение last_act_dt на текущее
     *
     * @param int $id
     *
     * @return object | null
     */
    public function touch($id)
    {
        $this->update($id, ['last_act_dt'=>'NOW()'], false);
    }

    /** Формирование списка из полей для формата VUE DataTable remote mode
     *
     * @param array $fields = ['field1', 'field2' ...]
     * @param array $params = [
     *                          'fieldname1', '>=', 'value1'
     *                          ...
     *                        ]
     *
     * @return array of objects | null
     */
/*
    public function getListAdv($params = [], $order = [], $limit = [], $fields = [])
    {
        $res = $this->getByParams($params, $order, $limit, $fields, true);

        return $res;
    }
*/

    /** Выборка списка ONLINE клиентов. С привилегиями или без
     *  ONLINE считается клиент, у которого last_act_dt не старше 15 мин
     *
     * @param boolean $exteneded - если true, то к каждому эл-ту списка списку добавляются группы+привилегии
     *
     * @return array of objects | null
     */
    public function getOnline($extended = false)
    {
        $params = [
            ['TIMESTAMPDIFF(MINUTE,last_act_dt,NOW())<='.$this->online_minutes_threshold]
        ];

        $res = $this->getByParams($params);

        $clients = [];
        if (!empty($res))
        {
            foreach ($res as $client)
            {
                if ($extended) $client = $this->extendClient($client);
                $clients[] = $client;
            }
        }

        return $clients;
    }


    /** Слияние клиентов
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function merge($acceptor_id, $donor_id)
    {
        $this->CI->load->model('clients_devices_model');
        $this->CI->load->model('clients_groups_model');
        $this->CI->load->model('clients_products_model');
        $this->CI->load->model('clients_presets_model');
        $this->CI->load->model('docs_model');
        $this->CI->load->model('logs_model');
        $this->CI->load->model('orders_model');
        $this->CI->load->model('products_model');
        $this->CI->load->model('promocodes_model');
        $this->CI->load->model('subs_model');
        $this->CI->load->model('trans_model');

        $result = true;
        $result = $result & $this->CI->cli_priv_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->clients_devices_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->clients_groups_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->clients_products_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->clients_presets_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->docs_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->logs_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->orders_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->products_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->promocodes_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->subs_model->mergeClientsData($acceptor_id, $donor_id);
        $result = $result & $this->CI->trans_model->mergeClientsData($acceptor_id, $donor_id);

        return $result;
    }
}