<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Clients_presets_model
 * @property Products_model $products_model
*/
class  Clients_presets_model extends MY_Model
{
    public $table = 'clients_presets';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'clients_presets.id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'clients_presets.id as id',
        'clients_presets.id as rel_id', // сделано для совместимости
        'clients_presets.client_id',
        'clients_presets.preset_id',
        'clients_presets.starting_dt',
        'clients_presets.expiration_dt',
        'clients_presets.origin',
        'clients_presets.origin_id',
        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.status as client_status',
        'presets.name as preset_name',
        'presets.preset_type',
        'presets.status as preset_status',
        'clients_presets.created',
        'clients_presets.updated',
        'UNIX_TIMESTAMP(clients_presets.created) as created_unix',
        'UNIX_TIMESTAMP(clients_presets.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'clients_presets.client_id = clients.id', 'left');
        $this->db->join('presets', 'clients_presets.preset_id = presets.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('presets.status = "active"');
            $this->db->where('clients.status = "active"');
            $this->db->where('(clients_presets.starting_dt<=NOW() OR clients_presets.starting_dt IS NULL)');
            $this->db->where('(clients_presets.expiration_dt>NOW() OR clients_presets.expiration_dt IS NULL)');
        }
    }

    /** Производит выборку записей определенного клиента
     *
     * @param int $client_id - Client_ID, для которого производитс выборка
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['clients_presets.client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }


    /** Производит выборку presets для определенного client
     *
     * @param int $id - client_id, для которого производитс выборка пресетов
     * @param boolean $result_by_preset_id - если true, то результат преобразуется в массив, где ключами будет preset_id (из presets), а не id из clients_presets
     *
     * @return array
     */
    public function getPresetsByClientId($id)
    {
        $params = [
            ['clients_presets.client_id', '=', intval($id)],
        ];

        $fields = [
            'clients_presets.preset_id as id',
            'clients_presets.id as rel_id',
            'clients_presets.starting_dt',
            'clients_presets.expiration_dt',
            'clients_presets.origin',
            'clients_presets.origin_id',
            'clients_presets.event_id',
            'presets.name',
            'presets.preset_type',
            'presets.status',
            'presets.created',
            'presets.updated',
            'UNIX_TIMESTAMP(presets.created) as created_unix',
            'UNIX_TIMESTAMP(presets.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;

        return $data;
    }

    /** Производит выборку ДОСТУПНЫХ СЕЙЧАС presets для определенного client.
     * Выборка идет из связки + добавляются пресеты из бесплатных продуктов
     *
     * @param int $client_id - client_id, для которого производится выборка presets
     *
     * @return array
     */
    public function getAvailablePresetsByClientId($client_id)
    {
        $params = [
            ['clients_presets.client_id', '=', intval($client_id)],
        ];

        $fields = [
            'clients_presets.id as rel_id',
            'clients_presets.preset_id as id',
            'clients_presets.starting_dt',
            'clients_presets.expiration_dt',
            'clients_presets.origin',
            'clients_presets.origin_id',
            'presets.name',
            'presets.description',
            'presets.preset_type',
            'presets.status',
            'presets.created',
            'presets.updated',
            'UNIX_TIMESTAMP(presets.created) as created_unix',
            'UNIX_TIMESTAMP(presets.updated) as updated_unix'
        ];

        $res1 = $this->getByParams($params, [], [], $fields);

        // извлекаем все бесплатные продукты с preset_id -----------------------
        $this->CI->load->model('presets_model');
        $res2 = $this->CI->presets_model->getFree();

        foreach ($res2 as $key=>$prod)
        {
            $res2[$key]->starting_dt = null;
            $res2[$key]->expiration_dt = null;
            $res2[$key]->origin = 'free_product';
            $res2[$key]->origin_id = null;
            unset($res2[$key]->description);
        }
        // -----------------------------------------------------

        // убираем дубликаты -------------------------
        $dataAll = $data = array_merge($res1,$res2);
        $data = [];
        foreach ($dataAll as $preset)
        {
            $data[$preset->id] = $preset;
        }
        // убираем дубликаты -------------------------

        $resData = [];
        foreach ($data as $pres) $resData[] = $pres;

        return $resData;
    }

    /** Производит выборку клиентов, у которых в доступе находится preset
     *
     * @param int $id - preset_id, пресет для которого производится выборка клиентов
     *
     * @return array
     */
    public function getClientsByPresetId($id)
    {
        $params = [
            ['clients_presets.preset_id', '=', intval($id)],
        ];

        $fields = [
            'clients_presets.client_id as id',
            'clients_presets.client_id as rel_id',
            'clients_presets.starting_dt',
            'clients_presets.expiration_dt',
            'clients_presets.origin',
            'clients_presets.origin_id',
            'clients.first_name',
            'clients.last_name',
            'clients.email',
            'clients.status',
            'clients.created',
            'clients.updated',
            'UNIX_TIMESTAMP(clients.created) as created_unix',
            'UNIX_TIMESTAMP(clients.updated) as updated_unix'
        ];

        $order = ["clients.email" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** проверка, связаны ли (состоит ли) client и preset
     *
     * @param int $preset_id - preset_id, для которого производится проверка вхождения
     * @param int $client_id
     *
     * @return boolean
     */
    public function isPresetInClient($preset_id, $client_id)
    {
        $params = [
            ['preset_id', '=', intval($preset_id)],
            ['client_id', '=', intval($client_id)],
        ];

        $fields = [
            'clients_presets.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Доступен ли пресет клиенту (с учетом дат)
     *
     * @param int $preset_id - preset_id - пресет, доступность которого проверяется
     * @param int $client_id - client_id - клиент, для которого проверятся доступность
     *
     * @return boolean
     */
    public function isPresetAvailableForClient($preset_id, $client_id)
    {
        $dt = new DateTime('now');
        $now = $dt->format('Y-m-d H:i:s');
        $this->caller_origin = 'front';
        $params = [
            ['preset_id', '=', intval($preset_id)],
            ['client_id', '=', intval($client_id)],
            ['(clients_presets.starting_dt<=NOW() OR clients_presets.starting_dt IS NULL)'],
            ['(clients_presets.expiration_dt>NOW() OR clients_presets.expiration_dt IS NULL)'],
        ];

        $fields = [
            'clients_presets.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res1 = $this->getByParams($params, [], $limit, $fields);

        // не входит ли в какой-то бесплатный продукт -------------------
        $query = "SELECT products.id FROM `products` WHERE is_free=1 AND preset_id=".$preset_id." LIMIT 1";
        $res = $this->db->query($query);
        $row = $res->row();
        $res2 = $row;
        // end не входит ли в какой-то бесплатный продукт -------------------

        return ( (!empty($res1)) || (!empty($res2)) );
    }

    /** Берет запись по client_id и preset_id
     *
     * @param int $client_id -
     * @param int $preset_id - preset_id, для которого производится проверка вхождения
     * @param string $origin - "происхождение" доступа к пресету
     * @param int $origin_id - ID сущности "обеспечившей" доступ

     * @return array
     */
    public function getByClientIdPresetId($client_id, $preset_id, $origin = null, $origin_id = null)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['preset_id', '=', intval($preset_id)],
        ];

        if (!empty($origin))
        {
            $params[] = ['origin','=',$origin];
        }

        if (!empty($origin_id))
        {
            $params[] = ['origin_id','=',$origin_id];
        }

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет preset к client. Если его там нет.
     *
     * @param int $preset_id - пресет, который добавляетя к клиенту
     * @param int $client_id - клиент, к которому добавляется пресет
     * @param int $starting_dt - дата начала доступности пресета
     * @param int $expiration_dt - дата истечения доступности пресета
     * @param int $origin - источник получения доступа
     * @param int $origin_id - ID источники получения доступа
     *
     * @return int
     */
    public function addPresetToClient($preset_id, $client_id, $starting_dt, $expiration_dt, $origin, $origin_id)
    {
        /** @var StdClass $record */
        $record = $this->getByClientIdPresetId($client_id, $preset_id, $origin, $origin_id);

        // пресета нет у клиента - добавляем
        if (empty($record))
        {
            $uData = [
                'preset_id' => intval($preset_id),
                'client_id' => intval($client_id),
                'starting_dt' => (!empty($starting_dt)) ? $starting_dt : 'NULL',
                'expiration_dt' => (!empty($expiration_dt)) ? $expiration_dt : 'NULL',
                'origin' => $origin,
                'origin_id' => intval($origin_id),
            ];
            $res_id = $this->insert($uData);
        }
        else
        {
            // пресет уже у клиента - колдуем с датами
            if (!empty($expiration_dt))
            {
                //конец определен.
                if (empty($starting_dt))
                {
                    //если старт не задан, для вычисления интервала используем в качестве начала NOW
                    $s_dt = "now";
                }
                else $s_dt = $starting_dt;

                $now_dt = new Datetimeex("now");
                $new_starting_dt = new Datetimeex($s_dt);
                $new_expiration_dt = new Datetimeex($expiration_dt);
                $interval = $new_starting_dt->diff($new_expiration_dt);

                // если у существующей записи начало=NULL - не трогаем его. т.е. установлеваем start_dt в null,
                // иначе оставляем start_dt неизменным и новое значение переопределит старое
                if (empty($record->starting_dt))
                {
                    $starting_dt = null;
                }

                if (!empty($record->expiration_dt))
                {
                    // если у сеществующей записи определен конец
                    $stored_expiration_dt = new DatetimeEx($record->expiration_dt);
                    if ($stored_expiration_dt->isLater($now_dt))
                    {
                        // доступ еще актуален - прибавляем к нему интервал
                        $expiration_dt = $stored_expiration_dt->add($interval)->format("Y-m-d H:i:s");
                    }

                }
                else $expiration_dt = null; // хранится значение null - продукт УЖЕ доступен навсегда.
            }

            $uData = [
                'starting_dt' => (!empty($starting_dt)) ? $starting_dt : 'NULL',
                'expiration_dt' => (!empty($expiration_dt)) ? $expiration_dt : 'NULL',
            ];

            $this->update($record->id, $uData);
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Убирает preset из доступа клиента.
     *
     * @param int $preset_id - пресет, который убирается из клиента
     * @param int $client_id - клиент, из которого убирается пресет
     * @param string $origin - "происхождение" доступа к пресету
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return boolean
     */
    public function removePresetFromClient($preset_id, $client_id, $origin = null, $origin_id = null)
    {
        $record = $this->getByClientIdPresetId($client_id, $preset_id, $origin, $origin_id);

        // пресета НЕТ у клиента - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // пресет есть у клиента
            $id = $record->id;
            $res_id = $this->delete($id);
        }

        return $res_id;
    }

    /** Ищет совпадающий с "ключами" элемент по параметрам в переданном массиве
     *
     * @param array of objects $data - массив для поиска
     * @param int $preset_id - ID пресета
     * @param string $origin - "происхождение" записи (group, order ...)
     * @param int $origin_id - ID сущности "происхождения" записи
     *
     * @return object | null
     */
    private function findInData($data, $preset_id, $origin, $origin_id)
    {
        foreach ($data as $key => $elem)
        {
            if ( ($elem->preset_id==$preset_id) && ($elem->origin==$origin) && ($elem->origin_id==$origin_id) ) return $elem;
        }

        return null;
    }

    /** Копирует (через insertOrUpdate) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $data_donor = $this->getByClientId($donor_id);
        $data_acceptor = $this->getByClientId($acceptor_id);

        $result = true;

        /* ПРИМЕЧАНИЕ: т.к. значение Null части составного ключа не приводит к срабатыванию unique index и on duplicate key update
         * то в данном случае используется раздельный подход - если запись сО всеми эл-тами ключа есть, то производится ее update
         * если нет - то insert
        */
        foreach ($data_donor as $elem)
        {
            $acc = $this->findInData($data_acceptor, $elem->preset_id, $elem->origin, $elem->origin_id);

            if (empty($acc))
            {
                //у акцептора еще нет этого пресета вообще - делаем insert
                $starting_dt = $elem->starting_dt;
                $expiration_dt = $elem->expiration_dt;
                $created = $elem->created;
                $updated = $elem->updated;

                if (empty($starting_dt)) $starting_dt = 'NULL';
                if (empty($expiration_dt)) $expiration_dt = 'NULL';
                $iData = [
                    'client_id'  => $acceptor_id,
                    'preset_id' => $elem->preset_id,
                    'starting_dt' => $starting_dt,
                    'expiration_dt' => $expiration_dt,
                    'origin' => (!empty($elem->origin)) ? $elem->origin : 'NULL',
                    'origin_id' => (!empty($elem->origin_id)) ? $elem->origin_id : 'NULL',
                    'created' => $created,
                    'updated' => $updated,
                ];

                $res_id = $this->insert($iData);
                $result = $result & (!empty($res_id));
            }
            else
            {
                if( (empty($acc->starting_dt)) || (empty($elem->starting_dt)) )
                {
                    $starting_dt = 'NULL';
                    $created = $acc->created;
                    $updated = $acc->updated;
                }
                else
                {
                    $dt_acceptor = new Datetimeex($acc->starting_dt);
                    $dt_donor = new Datetimeex($elem->starting_dt);

                    if ($dt_donor->isLater($dt_acceptor))
                    {
                        //у донора предел доступности пресета больше
                        $starting_dt = $elem->starting_dt;
                        $created = $elem->created;
                        $updated = $elem->updated;
                    }
                    else
                    {
                        //у акцептора предел доступности пресета больше
                        $starting_dt = $acc->starting_dt;
                        $created = $acc->created;
                        $updated = $acc->updated;
                    }
                }

                if( (empty($acc->expiration_dt)) || (empty($elem->expiration_dt)) )
                {
                    $expiration_dt = 'NULL';
                    $created = $acc->created;
                    $updated = $acc->updated;
                }
                else
                {
                    $dt_acceptor = new Datetimeex($acc->expiration_dt);
                    $dt_donor = new Datetimeex($elem->expiration_dt);

                    if ($dt_donor->isLater($dt_acceptor))
                    {
                        //у донора предел доступности пресета больше
                        $expiration_dt = $elem->expiration_dt;
                        $created = $elem->created;
                        $updated = $elem->updated;
                    }
                    else
                    {
                        //у акцептора предел доступности пресета больше
                        $expiration_dt = $acc->expiration_dt;
                        $created = $acc->created;
                        $updated = $acc->updated;
                    }
                }

                if (empty($starting_dt)) $starting_dt = 'NULL';
                if (empty($expiration_dt)) $expiration_dt = 'NULL';
                $uData = [
                    'client_id'  => $acceptor_id,
                    'preset_id' => $elem->preset_id,
                    'starting_dt' => $starting_dt,
                    'expiration_dt' => $expiration_dt,
                    'origin' => (!empty($elem->origin)) ? $elem->origin : 'NULL',
                    'origin_id' => (!empty($elem->origin_id)) ? $elem->origin_id : 'NULL',
                    'created' => $created,
                    'updated' => $updated,
                ];

                $res_id = $this->update($acc->id,$uData);
                $result = $result & ($res_id!==false);
            }

        }

        return $result;
    }



}
