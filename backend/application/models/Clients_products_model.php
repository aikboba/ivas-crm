<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Clients_products_model
 * @property Products_model $products_model
 */
class  Clients_products_model extends MY_Model
{
    public $table = 'clients_products';
    public $entity = null;
    public $primary = 'id';
    public $posfield = null;
    public $delimfield = null;
    public $error;
    public $order_by_field = 'clients_products.id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'clients_products.id',
        'clients_products.id as rel_id',
        'clients_products.client_id',
        'clients_products.product_id',
        'clients_products.start_dt',
        'clients_products.end_dt',
        'clients_products.order_id',
        'clients_products.origin',
        'clients_products.origin_id',

        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.status as client_status',

        'products.name as product_name',
        'products.status as product_status',

        'clients_products.created',
        'clients_products.updated',
        'UNIX_TIMESTAMP(clients_products.created) as created_unix',
        'UNIX_TIMESTAMP(clients_products.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('products', 'clients_products.product_id = products.id', 'left');
        $this->db->join('clients', 'clients_products.client_id = clients.id', 'left');
        $this->db->join('paysystems', 'products.paysystem_id = paysystems.id', 'left');
        $this->db->join('groups', 'products.group_id = groups.id', 'left');
        $this->db->join('presets', 'products.preset_id = presets.id', 'left');
        $this->db->join('instagram_accounts', 'products.instagram_account_id = instagram_accounts.id', 'left');

    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('clients.status = "active"');
            $this->db->where('products.status = "active"');
        }
    }

    /** Производит выборку products для определенного client
     *
     * @param int $id - client_id, для которого производится выборка products
     * @param boolean $result_by_product_id - если true, то результат преобразуется в массив, где ключами будет product_id (из products), а не id из clients_products
     *
     * @return array
     */
    public function getProductsByClientId($id, $result_by_product_id = false)
    {
        $params = [
            ['clients_products.client_id', '=', intval($id)],
        ];

        $fields = [
            'clients_products.product_id as id',
            'clients_products.id as rel_id',
            'clients_products.start_dt',
            'clients_products.end_dt',
            'clients_products.order_id',
            'clients_products.origin',
            'clients_products.origin_id',

            'products.name',
            'products.product_type',
            'products.description',
            'products.is_free',
            'products.days_available_after_stop',
            'products.days_item_available',
            'products.stream_status',
            'products.stream_origin',
            'products.stream_url_data',
            'products.stream_host_relay_url',
            'products.stream_started',
            'products.stream_ended',
            'products.stream_item_id',
            'products.status',
            'products.paysystem_id',
            'products.client_id',

            'paysystems.name as paysystem_name',
            'paysystems.public_key as paysystem_public_key',
            'paysystems.api_secret_key as paysystem_api_secret_key',

            'clients.first_name as client_first_name',
            'clients.last_name as client_last_name',
            'clients.email as client_email',
            'clients.user_insta_nick as client_user_insta_nick',

            'products.instagram_account_id',
            'instagram_accounts.login as instagram_account_nick',

            'products.group_id',
            'groups.name as group_name',

            'products.preset_id',
            'presets.name as preset_name',
            'presets.status as preset_status',

            'products.created',
            'products.updated',
            'UNIX_TIMESTAMP(products.created) as created_unix',
            'UNIX_TIMESTAMP(products.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_product_id)
        {
            $data = [];
            foreach ($res as $product) {
                $data[$product->id] = $product;
            }
        }

        return $data;
    }

    /** Переопределенный метод. Подготавливает вывод
     *
     * @params (array of StdClass) $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
        // превращаем JSON stream_url_data в объект
        if (!empty($data))
        {
            foreach ($data as $key=>&$elem)
            {
                $stream_url_data = (object)[];

                if (!empty($elem->stream_url_data))
                {
                    $stream_url_data = json_decode($elem->stream_url_data);
                }

                $elem->stream_url_data = $stream_url_data;
            }
        }

        return $data;
    }

    /** Производит выборку ДОСТУПНЫХ СЕЙЧАС products для определенного client.
     * Выборка идет из связки + добавляются бесплатные продукты
     *
     * @param int $client_id - client_id, для которого производится выборка products
     * @param array $types - массив нужных типов (products.product_type)
     *
     * @return array
     */
    public function getAvailableProductsByClientId($client_id, $types = null)
    {
        $params = [
            ['clients_products.client_id', '=', intval($client_id)],
        ];
        $params[] = ['( (start_dt <= NOW()) OR (start_dt IS NULL) )'];
        $params[] = ['( (end_dt > NOW()) OR (end_dt IS NULL) )'];

        if (!empty($types))
        {
            $st = "'".join("','", $types)."'";

            $params[] = ['products.product_type IN ('.$st.')'];
        }

        $fields = [
            'clients_products.product_id as id',
            'clients_products.id as rel_id',
            'clients_products.start_dt',
            'clients_products.end_dt',
            'clients_products.order_id',
            'clients_products.origin',
            'clients_products.origin_id',

            'products.name',
            'products.product_type',
            'products.description',
            'products.is_free',
            'products.days_available_after_stop',
            'products.days_item_available',
            'products.stream_status',
            'products.stream_origin',
            'products.stream_url_data',
            'products.stream_host_relay_url',
            'products.stream_started',
            'products.stream_ended',
            'products.stream_item_id',
            'products.status',
            'products.paysystem_id',
            'products.client_id',

            'paysystems.name as paysystem_name',
            'paysystems.public_key as paysystem_public_key',
            'paysystems.api_secret_key as paysystem_api_secret_key',

            'clients.first_name as client_first_name',
            'clients.last_name as client_last_name',
            'clients.email as client_email',
            'clients.user_insta_nick as client_user_insta_nick',

            'products.instagram_account_id',
            'instagram_accounts.login as instagram_account_nick',

            'products.group_id',
            'groups.name as group_name',

            'products.preset_id',
            'presets.name as preset_name',
            'presets.status as preset_status',

            'products.created',
            'products.updated',
            'UNIX_TIMESTAMP(products.created) as created_unix',
            'UNIX_TIMESTAMP(products.updated) as updated_unix'
        ];

        $res1 = $this->getByParams($params, [], [], $fields);

        // метод плох. Можно через Юнион -----------------------
        $this->CI->load->model('products_model');
        $res2 = $this->CI->products_model->getFree();

        foreach ($res2 as $key=>$prod)
        {
            $res2[$key]->rel_id = $res2[$key]->id;
            $res2[$key]->start_dt = null;
            $res2[$key]->end_dt = null;
            $res2[$key]->order_id = null;
            $res2[$key]->origin = 'free';
            $res2[$key]->origin_id = null;
            //unset($res2[$key]->description);
            ///unset($res2[$key]->client_first_name);
            //unset($res2[$key]->client_last_name);
            //unset($res2[$key]->client_email);
            //unset($res2[$key]->client_user_insta_nick);
            //unset($res2[$key]->instagram_account_login);
            //unset($res2[$key]->group_name);
            //unset($res2[$key]->preset_name);
            //unset($res2[$key]->preset_status);
            //unset($res2[$key]->stream_item_id);
        }
        // -----------------------------------------------------

        // убираем дубликаты -------------------------
        $dataAll = $data = array_merge($res1,$res2);
        $data = [];
        foreach ($dataAll as $product)
        {
            $data[$product->id] = $product;
        }
        // убираем дубликаты -------------------------

        $resData = [];
        foreach ($data as $prod) $resData[] = $prod;

        return $resData;
    }

    /** Производит выборку clients, которые связаны с product
     *
     * @param int $id - product_ID, для которого производитс выборка подарков
     *
     * @return array
     */
    public function getClientsByProductId($id)
    {
        $params = [
            ['clients_products.product_id', '=', intval($id)],
        ];

        $fields = [
            'clients_products.client_id as id',
            'clients_products.id as rel_id',
            'clients_products.start_dt',
            'clients_products.end_dt',
            'clients_products.order_id',
            'clients_products.origin',
            'clients_products.origin_id',
            'clients.first_name',
            'clients.last_name',
            'clients.email',
            'clients.status',
            'clients.created',
            'clients.updated',
            'UNIX_TIMESTAMP(clients.created) as created_unix',
            'UNIX_TIMESTAMP(clients.updated) as updated_unix'
        ];

        $order = ["clients.last_name" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** Производит выборку clients, для которых СЕЙЧАС доступен product
     *
     * @param int $id - product_ID, для которого производитс выборка подарков
     *
     * @return array
     */
    public function getAvailableClientsByProductId($id)
    {
        $params = [
            ['clients_products.product_id', '=', intval($id)],
        ];

        $params[] = ['(clients_products.start_dt <= NOW() OR clients_products.start_dt IS NULL)'];
        $params[] = ['(clients_products.end_dt > NOW() OR clients_products.end_dt IS NULL)'];

        $fields = [
            'clients_products.client_id as id',
            'clients_products.id as rel_id',
            'clients_products.start_dt',
            'clients_products.end_dt',
            'clients_products.order_id',
            'clients_products.origin',
            'clients_products.origin_id',
            'clients.first_name',
            'clients.last_name',
            'clients.email',
            'clients.status',
            'clients.created',
            'clients.updated',
            'UNIX_TIMESTAMP(clients.created) as created_unix',
            'UNIX_TIMESTAMP(clients.updated) as updated_unix'
        ];

        $order = ["clients.last_name" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** проверка, связаны ли (состоит ли) client и product
     *
     * @param int $client_id - client_id, для которого производится проверка вхождения
     * @param int $product_id
     *
     * @return boolean
     */
    public function isProductInClient($product_id, $client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['product_id', '=', intval($product_id)],
        ];

        $fields = [
            'clients_products.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись(и) по client_id и product_id. Origin и origin_id - можно использовать, если необходимо
     *
     * @param int $client_id
     * @param int $product_id
     * @param string $origin - "происхождение" доступа к продукту
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return array
     */
    public function getByClientIdProductId($client_id, $product_id, $origin=NULL, $origin_id=NULL)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['product_id', '=', intval($product_id)],
        ];
        if (!empty($origin)) $params[] = ['origin', '=', $origin];
        if (!empty($origin_id)) $params[] = ['origin_id', '=', $origin_id];

        //$limit = ['limit'=>1, 'offset'=>0];
        //$res = $this->getByParams($params, [], $limit);
        $res = $this->getByParams($params);

        return $res;
    }

    /** Берет записи по client_id и product_id. Origin и origin_id - можно использовать, если необходимо
     *
     * @param int $client_id
     * @param int $product_id
     * @param string $origin - "происхождение" доступа к продукту
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return array
     */
    public function getAvailableByClientIdProductId($client_id, $product_id, $origin=NULL, $origin_id=NULL)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['product_id', '=', intval($product_id)],
            ['( (start_dt <= NOW()) OR (start_dt IS NULL) )']
            ['( (end_dt > NOW()) OR (end_dt IS NULL) )']
        ];
        if (!empty($origin)) $params[] = ['origin', '=', $origin];
        if (!empty($origin_id)) $params[] = ['origin_id', '=', $origin_id];

        //$limit = ['limit'=>1, 'offset'=>0];
        //$res = $this->getByParams($params, [], $limit);
        $res = $this->getByParams($params);

        return $res;
    }


    /** Добавляет product в client.
     *
     * @param int $client_id - client, которому добавляетя product
     * @param int $product_id - product, который добавляется client'у
     * @param int $order_id - если есть, id заказа, оплатив который юзер получил доступ к продукту
     * @param string $start_dt - дата начала доступности продукта (важно для продуктов-подписок)
     * @param string $end_dt - дата окончания доступности продукта
     * @param string $origin - "происхождение" доступа к продукту
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return int
     */
    public function addProductToClient($product_id, $client_id, $start_dt, $end_dt, $order_id = null, $origin='manual', $origin_id=NULL)
    {
        $uData = [
            'client_id' => intval($client_id),
            'product_id' => intval($product_id),
            'start_dt' => (!empty($start_dt)) ? $start_dt : 'NULL',
            'end_dt' => (!empty($end_dt)) ? $end_dt : 'NULL',
            'order_id' => $order_id,
            'origin' => $origin,
            'origin_id' => $origin_id,
        ];
        $res_id = $this->insert($uData);

        /*Примечание: добавление связанного preset_id в clients_presets реализовано DB триггером clients_products_after_insert*/

        return $res_id;
    }

    /** Убирает  product из client.
     *
     * @param int $product_id - product, который убирается из client
     * @param int $client_id - client, из которого убирается product
     * @param string $origin - "происхождение" доступа к продукту
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return int
     */
    public function removeProductFromClient($product_id, $client_id, $origin=NULL, $origin_id=NULL)
    {
        // если origin и/или origin_id опущены, то может вернуться несколько записей. Все из которых следует удалить
        $records = $this->getByClientIdProductId($client_id, $product_id, $origin, $origin_id);

        // client НЕ в product - выходим
        if (empty($records))
        {
            return 0;
        }
        else
        {
            foreach ($records as $record)
            {
                /*Примечание: удаление связанного preset_id из clients_presets реализовано DB триггером clients_products_before_delete*/
                $id = $record->id;
                $res = $this->delete($id);
            }

            return $res;
        }
    }


    /** Доступен ли продукт клиенту (с учетом даты)
     *
     * @param int $product_id - продукт, доступность которого проверяется
     * @param int $client_id - клиент, для которого проверятся доступность
     * @param string $origin - "происхождение" доступа к продукту
     * @param int $origin_id - ID сущности "обеспечившей" доступ
     *
     * @return boolean
     */
    public function isProductAvailableForClient($product_id, $client_id)
    {
//        $dt = new DateTime('now');
//        $exp_dt = $dt->format('Y-m-d H:i:s');
        $params = [
            ['product_id', '=', intval($product_id)],
            ['client_id', '=', intval($client_id)],
            ['( (start_dt <= NOW()) OR (start_dt IS NULL) )'],
            ['( (end_dt > NOW()) OR (end_dt IS NULL) )'],
        ];

        $fields = [
            'clients_products.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res1 = $this->getByParams($params, [], $limit, $fields);

        // не является ли продукт бесплатным ---------------------------
        $this->CI->load->model('products_model');
        $row = $this->CI->products_model->getById($product_id);
        $res2 = ($row->is_free == 1);
        // end не является ли продукт бесплатным ------------------------

        return ( (!empty($res1)) || ($res2) );
    }


    /** Производит выборку записей определенного клиента
     *
     * @param int $client_id - Client_ID, для которого производитс выборка
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['clients_products.client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }


    /** Копирует (через insertOrUpdate) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $data_donor = $this->getByClientId($donor_id);

        $result = true;

        foreach ($data_donor as $elem)
        {
            $iData = [
                'client_id'  => $acceptor_id,
                'product_id' => $elem->product_id,
                'start_dt' => (!empty($elem->start_dt)) ? $elem->start_dt : 'NULL',
                'end_dt' => (!empty($elem->end_dt)) ? $elem->end_dt : 'NULL',
                'order_id' => (!empty($elem->order_id)) ? $elem->order_id : 'NULL',
                'origin' => (!empty($elem->origin)) ? $elem->origin : 'manual',
                'origin_id' => (!empty($elem->origin_id)) ? $elem->origin_id : 'NULL',
                'created' => $elem->created,
                'updated' => $elem->updated,
            ];

            $res_id = $this->insertOrUpdate($iData);
            $result = $result & (!empty($res_id));
        }

        return $result;
    }


}
