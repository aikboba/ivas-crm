<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs_model extends MY_Model
{
    public $table = 'docs';
    public $entity = 'doc';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'title';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'client_id',
        'title',
        'filename',
        'mime_type',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';
    public $www_path = "";

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Переопределенный метод. Подготавливает вывод
     *
     * @params array of StdClass $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
        if (!empty($data))
        {
            // если у контроллера www_path не задан, то выбираем его из конфига
            if (empty($this->www_path))
            {
                $this->CI->load->config('ftp', true);
                $this->www_path = $this->CI->config->item('ftp_www_docs_path', 'ftp'); // это папка, из которой файлы доступны через веб. Используется через ftp_file_proxy скрипт
            }

            foreach ($data as $key=>$row)
            {
                $data[$key]->url = $this->www_path.$row->filename;
                $data[$key]->name = $row->title;
            }
        }

        return  $data;
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('docs.status = "active"');
        }
    }

    /** Производит выборку файлов по client_id
     *
     * @param int $client_id - client_id, для которого производится выборка файлов
     * @param string $status - вернутся только элементы с определенным статусом
     *
     * @return array
     */
    public function getByClientId($client_id, $status = null)
    {
        $params = [
            [$this->table_full.'.client_id', '=', intval($client_id)],
        ];

        if (!empty($status)) $params[] = ['status', '=', $status];

        $res = $this->getByParams($params);

        return $res;
    }


    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function clientDelete($id)
    {
        return $this->setDeleted($id);
    }

    /** Реально удаляет запись из таблицы
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        /*
         * здесь логика
         * */
        return parent::delete($id);
    }

    /** Изменяет "приписку" документов (через update) данные клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $result = true;

        $params = [
                    ['client_id', '=', $donor_id]
        ];
        $uData = ['client_id' => $acceptor_id];

        $res = $this->updateByParams($params, $uData);
        $result = !($res === false);

        return $result;
    }

}