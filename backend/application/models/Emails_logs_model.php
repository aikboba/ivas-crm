<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_logs_model extends MY_Model
{
    public $table = 'emails_logs';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'recipient_email',
        'recipient_name',
        'sender_email',
        'sender_name',
        'subject',
        'content',
        'attached_files',
        'status',
        'error_message',
        'created',
        'UNIX_TIMESTAMP(created) as created_unix'
    ];

    
    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }
}

