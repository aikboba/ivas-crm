<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_queue_model extends MY_Model
{
    public $table = 'emails_queue';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'recipient_email',
        'recipient_name',
        'sender_email',
        'sender_name',
        'send_date',
        'subject',
        'content',
        'attached_files',
        'is_html',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix',
        'UNIX_TIMESTAMP(send_date) as send_date_unix'
    ];

    
    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }
    
    
    public function getTasks($limit = 1)
    {
        //$date = $this->db->escape(date('Y-m-d H:i:s'));
        $params = [
            ['status', '=', 'new'],
            ['send_date <= NOW()']
        ];
        
        $limit = ['limit' => $limit, 'offset' => 0];
        
        return $this->getByParams($params, [], $limit);
    }
    
    

}


