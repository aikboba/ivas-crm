<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Gifts_model
 * @property Clients_presets_model $clients_presets_model
 * @property Gifts_presets_model $gifts_presets_model
 * @property Clients_model $clients_model
 * @property Products_model $products_model
 */
class Gifts_model extends MY_Model
{
    public $table = 'gifts';
    public $entity = 'gift';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'start_dt';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'name',
        'description',
        'start_dt',
        'end_dt',
        'duration',
        'apply_if',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Дарит подарок клиенту. Т.е. производит выборку presets для указанного gift и добавляет их в clients_presets на клиента clients
     *
     * @param int|object $gift - gift_id или сам объект подарка, для который дарится
     * @param int|object $client - client_id или сам объект клиента, которому добавляются подарочные пресеты
     * @param int|object|null $product - product_id, сам объект продукта либо NULL - наличие парамтра связано с
     *                                   необходимостью указывать start_dt=now в случае product_type=subscription
     *
     * @return boolean
     */
    public function sendGiftToClient($gift, $client, $product=null)
    {
        if (!is_object($gift))
        {
            $gift = $this->getById($gift);
        }
        if (empty($gift)) { return false; }

        if (!is_object($client))
        {
            $this->CI->load->model('clients_model');
            $client = $this->CI->clients_model->getById($client);
        }
        if (empty($client)) { return false; }

        if (!empty($product))
        {
            if (!is_object($product))
            {
                $this->CI->load->model('products_model');
                $product = $this->CI->products_model->getById($product);
            }
            if (empty($product)) { return false; }
        }

        // проверка, можно ли вручить клиенту подарок --------------------------------
        if (!empty($product))
        {
            if ($gift->apply_if != 'always')
            {

                $this->CI->load->model('orders_model');
                $params = [
                    ['client_id','=',$client->id],
                    ["orders.option_id IN (SELECT options.id FROM options WHERE options.product_id=".intval($product->id).")"],
                    ["orders.status IN ('paid','partially_paid')"],
                ];
                $limit = ['limit'=>1, 'offset'=>0];
                $orders = $this->CI->orders_model->getByParams($params, [], $limit);

                if ($gift->apply_if == 'in_orders')
                {
                    //вручить, только, у клиента УЖЕ ЕСТЬ заказ на этот продукт ---
                    if (empty($orders)) return false;
                }

                if ($gift->apply_if == 'not_in_orders')
                {
                    // вручить, только, у клиента ЕЩЕ НЕТ заказа на этот продукт ---
                    if (!empty($orders)) return false;
                }

            }
        }
        // END проверка, можно ли вручить клиенту подарок --------------------------------

        $this->CI->load->model('clients_presets_model');
        $this->CI->load->model('gifts_presets_model');

        $presets = $this->CI->gifts_presets_model->getPresetsByGiftId($gift->id);

        if (!empty($presets))
        {
            $duration = intval($gift->duration);
            foreach ($presets as $preset)
            {
                $dt = new DateTime('now');
                $starting_dt = $dt->format('Y-m-d H:i:s');

                $dt->add(new DateInterval('P'.$duration.'D')); // добавляем $duration дней
                $expiration_dt = $dt->format('Y-m-d H:i:s');

                if (!empty($product))
                    if ($product->product_type != 'subscription') $starting_dt = null; // только у продуктов типа subscription может быть определено значение $starting_dt для clients_presets

                $this->CI->clients_presets_model->addPresetToClient($preset->id, $client->id, $starting_dt, $expiration_dt, 'gift', $gift->id);
            }
        }

        return true;
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('gifts.status = "active"');
            $this->db->where('(gifts.start_dt<=NOW() OR gifts.start_dt IS NULL)');
            $this->db->where('(gifts.end_dt>NOW() OR gifts.end_dt IS NULL)');
        }
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

}