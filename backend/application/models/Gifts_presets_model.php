<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Gifts_presets_model extends MY_Model
{
    public $table = 'gifts_presets';
    public $entity = null;
    public $primary = 'id';
    public $posfield = "pos";
    public $delimfield = "gift_id";
    public $error;
    public $order_by_field = 'gifts_presets.pos';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'gifts_presets.id as id',
        'gifts_presets.id as rel_id',
        'gifts_presets.gift_id',
        'gifts_presets.preset_id',
        'gifts_presets.pos',
        'gifts.name as gift_name',
        'gifts.start_dt as start_dt',
        'gifts.end_dt as end_dt',
        'gifts.status as gift_status',
        'presets.name as preset_name',
        'presets.status as preset_status',
        'gifts_presets.created',
        'gifts_presets.updated',
        'UNIX_TIMESTAMP(gifts_presets.created) as created_unix',
        'UNIX_TIMESTAMP(gifts_presets.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('gifts', 'gifts_presets.gift_id = gifts.id', 'left');
        $this->db->join('presets', 'gifts_presets.preset_id = presets.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('gifts.status = "active"');
            $this->db->where('presets.status = "active"');
        }
    }

    /** Производит выборку presets для определенного gift
     *
     * @param int $id - gift_id, для которого производится выборка presets
     * @param boolean $result_by_preset_id - если true, то результат преобразуется в массив, где ключами будет preset_id (из presets), а не id из gifts_presets
     *
     * @return array
     */
    public function getPresetsByGiftId($id, $result_by_preset_id = false)
    {
        $params = [
            ['gifts_presets.gift_id', '=', intval($id)],
        ];

        $fields = [
            'gifts_presets.preset_id as id',
            'gifts_presets.id as rel_id',
            'presets.name',
            'presets.status',
            'presets.created',
            'presets.updated',
            'gifts_presets.pos',
            'UNIX_TIMESTAMP(presets.created) as created_unix',
            'UNIX_TIMESTAMP(presets.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_preset_id)
        {
            $data = [];
            foreach ($res as $preset) {
                $data[$preset->id] = $preset;
            }
        }

        return $data;
    }

    /** Производит выборку gifts, в которых состоит preset
     *
     * @param int $id - preset_ID, для которого производитс выборка подарков
     *
     * @return array
     */
    public function getGiftsByPresetId($id)
    {
        $params = [
            ['gifts_presets.preset_id', '=', intval($id)],
        ];

        $fields = [
            'gifts_presets.gift_id as id',
            'gifts_presets.id as rel_id',
            'gifts.name',
            'gifts.status as status',
            'gifts.created',
            'gifts.updated',
            'UNIX_TIMESTAMP(gifts.created) as created_unix',
            'UNIX_TIMESTAMP(gifts.updated) as updated_unix'
        ];

        $order = ["gifts.name" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** проверка, связаны ли (состоит ли) gift и preset
     *
     * @param int $gift_id - gift_id, для которого производится проверка вхождения
     * @param int $preset_id
     *
     * @return boolean
     */
    public function isPresetInGift($preset_id, $gift_id)
    {
        $params = [
            ['gift_id', '=', intval($gift_id)],
            ['preset_id', '=', intval($preset_id)],
        ];

        $fields = [
            'gifts_presets.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по gift_id и preset_id
     *
     * @param int $gift_id - gift_id, для которого производится проверка вхождения
     * @param int $preset_id -
     *
     * @return array
     */
    public function getByGiftIdPresetId($gift_id, $preset_id)
    {
        $params = [
            ['gift_id', '=', intval($gift_id)],
            ['preset_id', '=', intval($preset_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    public function insert($data)
    {
        return parent::insertWithPos($data);
    }

    public function update($id, $data, $escape=true)
    {
        return parent::updateWithPos($id, $data);
    }

    public function delete($id)
    {
        return parent::deleteWithPos($id);
    }

    /** Добавляет preset в gift. Если его там нет.
     *
     * @param int $preset_id - preset, в который добавляется gift
     * @param int $gift_id - gift, в который добавляетя preset
     * @param int $pos - позиция, в которую добавляется gift
     *
     * @return int
     */
    public function addPresetToGift($preset_id, $gift_id, $pos)
    {
        $record = $this->getByGiftIdPresetId($gift_id, $preset_id);

        // gift НЕ в preset - добавляем
        if (empty($record))
        {
            $uData = [
                'gift_id' => intval($gift_id),
                'preset_id' => intval($preset_id),
                'pos' => intval($pos),
            ];

            $res_id = $this->insert($uData);
        }
        else
        {
            // gift уже в preset
            $res_id = $record->id;
            $this->update($record->id, ['pos' => intval($pos)]);
        }

        return $res_id;
    }

    /** Убирает  preset из gift.
     *
     * @param int $preset_id - preset, который убирается из gift
     * @param int $gift_id - gift, из которого убирается пресет
     *
     * @return int
     */
    public function removePresetFromGift($preset_id, $gift_id)
    {
        $record = $this->getByGiftIdPresetId($gift_id, $preset_id);

        // gift НЕ в preset - выходим
        if (empty($record))
        {
            return 0;
        }
        else
        {
            // gift в preset
            $id = $record->id;
        }

        return parent::delete($id);
    }


}
