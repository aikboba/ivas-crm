<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Groups_presets_model
 * @param Clients_groups_model $clients_groups_model
 */
class Groups_presets_model extends MY_Model
{
    public $table = 'groups_presets';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'groups.name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'groups_presets.id as id',
        'groups_presets.id as rel_id', // для совместимости
        'groups_presets.group_id',
        'groups_presets.preset_id',
        'presets.name as preset_name',
        'presets.status as preset_status',
        'groups.name as group_name',
        'groups.status as group_status',
        'groups_presets.created',
        'groups_presets.updated',
        'UNIX_TIMESTAMP(groups_presets.created) as created_unix',
        'UNIX_TIMESTAMP(groups_presets.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);

        $this->CI->load->model("clients_groups_model");
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('groups', 'groups_presets.group_id = groups.id', 'left');
        $this->db->join('presets', 'groups_presets.preset_id = presets.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('groups.status = "active"');
            $this->db->where('presets.status = "active"');
        }
    }

    /** Производит выборку пресетов для группы
     *
     * @param int $id - group_ID, группа, для которой производится выборка
     * @param boolean $result_by_preset_id - если true, то результат преобразуется в массив, где ключами будет preset_id (из presets), а не id из groups_presets
     *
     * @return array
     */
    public function getPresetsByGroupId($group_id, $result_by_preset_id = false)
    {
        $params = [
            ['groups_presets.group_id', '=', intval($group_id)],
        ];

        $fields = [
            'groups_presets.preset_id as id',
            'groups_presets.id as rel_id',
            'presets.name',
            'presets.status',
            'presets.created',
            'presets.updated',
            'UNIX_TIMESTAMP(presets.created) as created_unix',
            'UNIX_TIMESTAMP(presets.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_preset_id)
        {
            $data = [];
            foreach ($res as $preset) {
                $data[$preset->id] = $preset;
            }
        }

        return $data;
    }

    /** Производит выборку групп, в которых есть указанный пресет
     *
     * @param int $preset_id - пресет, для которого выбирается вхождение в группы
     *
     * @return array
     */
    public function getGroupsByPresetId($preset_id)
    {
        $params = [
            ['groups_presets.preset_id', '=', intval($preset_id)],
        ];

        $fields = [
            'groups_presets.group_id as id',
            'groups_presets.id as rel_id',
            'groups.name',
            'groups.status',
            'groups.created',
            'groups.updated',
            'UNIX_TIMESTAMP(groups.created) as created_unix',
            'UNIX_TIMESTAMP(groups.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** проверка, входит ли пресет в группу
     *
     * @param int $preset_id - пресет
     * @param int $group_id - для которой производится проверка вхождения
     *
     * @return boolean
     */
    public function isPresetInGroup($preset_id, $group_id)
    {
        $params = [
            ['preset_id', '=', intval($preset_id)],
            ['group_id', '=', intval($group_id)],
        ];

        $fields = [
            'groups_presets.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по preset_id и group_id
     *
     * @param int $group_id - группа, для которой производится проверка вхождения
     * @param int $preset_id - пресет
     *
     * @return array
     */
    public function getByGroupIdPresetId($group_id, $preset_id)
    {
        $params = [
            ['group_id', '=', intval($group_id)],
            ['preset_id', '=', intval($preset_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет пресеь в группу. Если его там нет.
     *  Также устаканивает связку клиент-presets (которые являются подарочными при членстве в группе)
     *
     * @param int $preset_id - preset_id, который добавляетя в группу
     * @param int $group_id - group_id, в которую добавляется пресет
     *
     * @return int
     */
    public function addPresetToGroup($preset_id, $group_id)
    {
        $record = $this->getByGroupIdPresetId($group_id, $preset_id);

        // пресет НЕ в группе - добавляем
        if (empty($record))
        {
            $uData = [
                'preset_id'=>intval($preset_id),
                'group_id'=>intval($group_id),
            ];
            $res_id = $this->insert($uData);

            // добавляем клиентам, входящим в группу, указанный пресет  -----------------------------------------
            if ($res_id)
            {
                $clients = $this->CI->clients_groups_model->getClientsByGroupId($group_id);

                foreach ($clients as $client)
                {
                    $starting_dt = null;
                    $expiration_dt = null;
                    $origin = 'group';
                    $this->CI->clients_presets_model->addPresetToClient($preset_id, $client->id, $starting_dt, $expiration_dt, $origin, $group_id);
                }
            }
            // END добавляем клиентам, входящим в группу, указанный пресет  -----------------------------------------

        }
        else
        {
            // пресет уже в группе
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Убирает пресет из группы.
     *  Также устаканивает связку клиент-presets (которые являются подарочными при членстве в группе)
     *
     * @param int $preset_id - preset_id, который убирается из группы
     * @param int $group_id - group_id, из которой убирается клиент
     *
     * @return boolean
     */
    public function removePresetFromGroup($preset_id, $group_id)
    {
        $record = $this->getByGroupIdPresetId($group_id, $preset_id);

        // пресет НЕ в группе - выходим
        if (empty($record))
        {
            $res_id = false;
        }
        else
        {
            // пресет в группе - удаляем
            $id = $record->id;
            $res_id = $this->delete($id);

            // у всех клиентов, входящих в группу, удаляем указанный пресет  -----------------------------------------
            if ($res_id)
            {
                $clients = $this->CI->clients_groups_model->getClientsByGroupId($group_id);

                foreach ($clients as $client)
                {
                    $origin = 'group';
                    $this->CI->clients_presets_model->removePresetFromClient($preset_id, $client->id, $origin, $group_id);
                }
            }
            // END у всех клиентов, входящих в группу, удаляем указанный пресет  -----------------------------------------

        }

        return $res_id;
    }

}
