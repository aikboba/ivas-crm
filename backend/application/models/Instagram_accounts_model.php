<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instagram_accounts_model extends MY_Model
{
    public $table = 'instagram_accounts';
    public $entity = 'instagram_account';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'pk',
        'name',
        'user_name',
        'user_avatar',
        'login',
        'password',
        'status',

        'stream_origin',
        'stream_url_data',
        'stream_logo',
        'stream_started',
        'UNIX_TIMESTAMP(stream_started) as stream_started_unix',
        'stream_status',

        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('instagram_accounts.status = "active"');
        }
    }

    /** Переопределенный метод. Подготавливает вывод
     *
     * @params (array of StdClass) $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
        // превращаем JSON stream_url_data в объект
        if (!empty($data))
        {
            foreach ($data as $key=>&$elem)
            {
                $stream_url_data = (object)[];

                if (!empty($elem->stream_url_data))
                {
                    $stream_url_data = json_decode($elem->stream_url_data);
                }

                $elem->stream_url_data = $stream_url_data;
            }
        }

        return $data;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Выборка по pk
     *
     * @param string $pk
     *
     * @return object
     */
    public function getByPk($pk)
    {
        $params = [
            ['pk', "=" , $pk]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }

    /** Выборка по login
     *
     * @param string $login
     *
     * @return object
     */
    public function getByLogin($login)
    {
        $params = [
            ['login', "=" , $login]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }


    /** Устанавливает для аккаунта $id стрим в состояние, заданное полями массива stream_data
     *
     * @param int $id - id продукта
     * @param array $stream_data - данные о состоянии стрима
     *
     * @return boolean
     */
    public function setStream($id, $stream_data)
    {
        /** @var Object $instagram_account */
        $instagram_account = $this->getById($id);
        if (empty($instagram_account)) return false;

        $uData = [
            'stream_url' => $stream_data['stream_url_data'],
            'stream_started' => (!empty($stream_data['stream_started'])) ? $stream_data['stream_started'] : (new Datetimeex())->format("Y-m-d H:i:s"),
            'stream_status' => $stream_data['stream_status'],
        ];
        if (!empty($stream_data['pk'])) $uData['pk'] = $stream_data['pk'];
        if (!empty($stream_data['user_avatar'])) $uData['user_avatar'] = $stream_data['user_avatar'];
        if (!empty($stream_data['user_name'])) $uData['user_name'] = $stream_data['user_name'];
        if (!empty($stream_data['stream_origin'])) $uData['stream_origin'] = $stream_data['stream_origin'];

        $this->db->trans_begin();
        $upd_res = $this->update($instagram_account->id, $uData);

        if ($upd_res!==false)
        {
            $this->CI->load->model('products_model');
            $products = $this->CI->products_model->getListByInstaAccId($instagram_account->id);

            if (!empty($products))
            {
                $uData = [
                    'stream_status' => $stream_data['stream_status'],
                    'stream_url_data' => $stream_data['stream_url_data'],
                    'stream_host_relay_url' => (!empty($stream_data['stream_host_relay_url'])) ? $stream_data['stream_host_relay_url'] : 'NULL',
                    'stream_origin' => $stream_data['stream_origin'],
                    'stream_item_guid' => $stream_data['stream_item_guid'],
                    'stream_health' => 'none',
                    'stream_last_changed_dt' => (new Datetimeex())->format("Y-m-d H:i:s"),
                ];
                foreach ($products as $prod)
                {
                    $this->CI->products_model->setStream($prod->id, $uData);
                }
            }

            $this->db->trans_commit();

            return true;
        }
        else
        {
            $this->db->trans_rollback();

            return false;
        }


    }



}