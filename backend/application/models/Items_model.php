<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items_model extends MY_Model
{
    public $table = 'items';
    public $entity = 'item';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'name',
        'system_name',
        'description',
        'resource_string_id',
        'item_type',
        'guid',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('items.status = "active"');
        }

        $this->db->where('NOT (`items`.`status`="deleted" AND (TRIM(items.resource_string_id) = "" OR (items.`resource_string_id` IS NULL )) )');
    }

    /** Выборка по item_type
     *
     * @param string $type
     *
     * @return array of objects
     */
    public function getByType($type)
    {
        $params = [
            ['item_type', "=" , $type]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res;

        return null;
    }


    /** Выборка по resource_string_id
     *
     * @param string $resource_string_id
     *
     * @return array of objects | null
     */
    public function getByResourceStringId($resource_string_id)
    {
        $params = [
            ['resource_string_id', '=', $resource_string_id]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        if (!empty($res))
        {
            return $res[0];
        }

        return null;
    }

    /** Выборка по guid
     *
     * @param string $guid
     *
     * @return object | null
     */
    public function getByGuid($guid)
    {
        $params = [
            ['guid', '=', $guid]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        if (!empty($res))
        {
            return $res[0];
        }

        return null;
    }


    /** Переопределенный метод удаления через переопределение статуса
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Выставляет эл-ту стутус deleted и чистит resource_string_id и прочие сво-ва
     *
     * @param int $id
     *
     * @return int id
     */
    public function deleteWithContent($id)
    {
        $data = [
            'status' => 'deleted',
            'resource_string_id' => 'NULL',
            //'guid' => 'NULL',
            'available_dt' => 'NULL',
        ];

        return $this->update($id, $data);
    }


}