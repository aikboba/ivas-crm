<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs_model extends MY_Model
{
    public $table = 'logs';
    public $entity = null; // Установка пустого значения приведет к НЕсохранению "логов" таблицы logs
    public $primary = 'id';
    public $error;
    public $order_by_field = 'created';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'id',
        'entity',
        'entity_id',
        'event',
        'author',
        'author_id',

        'IF(author="admin", (SELECT first_name FROM admins WHERE id=author_id), (SELECT first_name FROM clients WHERE id=author_id) ) as first_name',
        'IF(author="admin", (SELECT last_name FROM admins WHERE id=author_id), (SELECT last_name FROM clients WHERE id=author_id) ) as last_name',
        'IF(author="admin", (SELECT email FROM admins WHERE id=author_id), (SELECT email FROM clients WHERE id=author_id) ) as email',

        'additional_data',
        'created',
        'UNIX_TIMESTAMP(created) as created_unix',
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Производит выборку записей по entity + entity_id
     *
     * @param string $entity - сущность, для которой создана запись
     * @param int $entity_id - ID этой сущности
     *
     * @return array
     */
    public function getByEntityEntityId($entity, $entity_id)
    {
        $params = [
            ['entity', '=', $entity],
            ['entity_id', '=', intval($entity_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }


    /** Производит выборку записей по конкретному заказу
     *
     * @param int $order_id - ID заказа
     *
     * @return array
     */
    public function getByOrderId($order_id)
    {
        $res = $this->getByEntityEntityId('order', $order_id);

        return $res;
    }


    /** Изменяет "приписку" логов (через update) клиента donor_id в клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $this->CI->load('clients_model');
        $new_additional_data = $this->CI->clients_model->getById($acceptor_id);

        $result = true;

        $params = [
            ['entity', '=', 'client'],
            ['entity_id', '=', $donor_id],
        ];

        $uData = [
            'client_id' => $acceptor_id,
            'additional_data' => $new_additional_data,
        ];
        $res = $this->updateByParams($params, $uData);
        $result = ($res !== false);

        return $result;
    }

}
