<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs_model_old extends CI_Model
{
    public $table = 'logs_old';
    public $primary = 'id';

	public function __construct()
	{;
		parent::__construct();
		$this->load->database();
	}

	public function get($fields = array(), $where = array(), $limit = false, $offset = 0, $count = false, $order_by = false, $array_res = false, $group_by = false)
	{
        $this->load->model('insta_accounts_model');

        $db_debug = $this->db->db_debug; $this->db->db_debug = FALSE;

        if ($count) {
            $query = $this->db->select('COUNT(' . $this->primary . ') AS data_count')
                ->where($where)
                ->get($this->table);
        } else {
            if ($limit) $this->db->limit($limit, $offset);
            if ($order_by) $this->db->order_by($order_by);
            if ($group_by) $this->db->group_by($group_by);

            $query = $this->db->select($fields)
                ->where($where)
                ->join($this->insta_accounts_model->table.' ia', 'ia.'.$this->insta_accounts_model->primary.' = '.$this->table.'.log_insta_acc', 'left')
                ->get($this->table);
        }
        if ( !empty($this->db->error()['message']) ) die(print($this->db->error()['message']));//die(print('Database query error! :('));
        $this->db->db_debug = $db_debug;

        return ($array_res ? ($limit == 1 ? $query->row_array() : $query->result_array()) : ($limit == 1 ? $query->row() : $query->result()));
	}

    public function update($id = null, $fields_array, $where = array())
	{
        $db_debug = $this->db->db_debug; $this->db->db_debug = FALSE;

        if (empty($where) && !empty($id)) {
            $this->db->where($this->primary, $id);
        } else {
            $this->db->where($where);
        }

        $this->db->update($this->table, $fields_array);

        if ( !empty($this->db->error()['message']) ) die(print('Database query error! :('));
        $this->db->db_debug = $db_debug;

        return true;
	}

    public function insert($fields_array)
	{
        $db_debug = $this->db->db_debug; $this->db->db_debug = FALSE;

        $this->db->insert($this->table, $fields_array);

        if ( !empty($this->db->error()['message']) ) die(print('Database query error! :('));
        $this->db->db_debug = $db_debug;

        return $this->db->insert_id();
	}

    public function delete($id = null)
    {
        $db_debug = $this->db->db_debug; $this->db->db_debug = FALSE;

        if (!empty($id)) $this->db->where($this->primary, $id);
        $this->db->delete($this->table);

        if ( !empty($this->db->error()['message']) ) die(print('Database query error! :('));
        $this->db->db_debug = $db_debug;

        if ( empty($this->db->error()['message']) ) return true;
            else return false;
    }
}
