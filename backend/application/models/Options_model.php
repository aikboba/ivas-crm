<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Options_model
 *
 * @property Privileges_model $privileges_model
 * @property Clients_model $clients_model
 * @property Promocodes_model $promocodes_model
 */
class Options_model extends MY_Model
{
    public $table = 'options';
    public $entity = 'option';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'product_id',
        'name',
        'description',
        'price',
        'duration',
        'is_partial_available',
        'partial_threshold',
        'partial_available_duration',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('options.status = "active"');
        }
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /**Для переданных опции и промокода для клиента возвращает цену опции с учетом всех скидок
     *
     * @param int|object $option - option_id либо объект, содержащий опцию
     * @param int|object $client - client_id либо объект, содержащий клиента
     * @param string|object $promocode - используемый промокод

     * @return bool|float
     */
    public function getDiscountPrice($option, $client, $promocode=null)
    {
        $price = $new_price = $initial_price = $option->price;

        //пересчет цены из-за льгот клиента -----------------
        if (!empty($client->privileges))
        {
            $this->load->model('privileges_model');

            foreach ($client->privileges as $priv)
            {
                $new_price = $this->privileges_model->getPriceWithDiscount($priv, $initial_price);
                if ($new_price < $price) $price = $new_price;
            }
        }
        //END пересчет цены из-за льгот клиента -----------------

        //обрабатываем промокод ---------------------------------------
        if (!empty($promocode))
        {
            $new_price = $this->promocodes_model->getPriceWithDiscount($promocode, $initial_price);
        }
        if ($new_price < $price) $price = $new_price;
        //обрабатываем промокод ---------------------------------------

        return $price;
    }

    /**Возвращает опцию с новыми полями:
     * - цена до скидки -> $option->price_before_discount
     * - цена с учетом скидки -> $option->price
     *
     * @param int|object $option - option_id либо объект, содержащий опцию
     * @param int|object $client - client_id либо объект, содержащий клиента
     * @param string|object $promocode - строка промокода либо объект, содержащий используемый промокод

     * @return bool|float
     */
    public function getWithDiscount($option, $client, $promocode=null)
    {
        // если передано int, то считаем, что это option_id, и извлекаем опцию. Иначе считаем, что в ней УЖЕ объект
        if(!is_object($option)) $option = $this->getById($option);

        if (empty($option)) return false;

        if (!empty($client))
        {
            if (!is_object($client)) $client = $this->clients_model->getById($client);
        }

        if(is_string($promocode)) $promocode = $this->promocodes_model->getByPromocode($promocode);

        if (!empty($promocode))
        {
            $promoApplicable = $this->promocodes_model->isApplicable($promocode, $option, $client);
            if (!$promoApplicable)
            {
                // промокод неприменим - удаляем промокод
                $promocode = null;
            }
        }

        $option->price_before_discount = $option->price;
        $option->price = $this->getDiscountPrice($option, $client, $promocode);

        return $option;
    }

}

