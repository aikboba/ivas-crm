<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Orders_model
 * @property Clients_model $clients_model
 * @property Clients_presets_model $clients_presets_model
 * @property Clients_products_model $clients_products_model
 * @property Clients_groups_model $clients_groups_model
 * @property Products_model $products_model
 * @property Products_gifts_model $products_gifts_model
 * @property Options_model $options_model
 * @property Gifts_model $gifts_model
 * @property Gifts_presets_model $gifts_presets_model
 * @property Subs_model $subs_model
 */
class Orders_model extends MY_Model
{
    public $table = 'orders';
    public $entity = 'order';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'created';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'orders.id',
        'orders.option_id',
        'orders.client_id',
        'orders.promocode_id',
        'subs_id',

        'products.id as product_id',
        'products.name as product_name',
        'options.name as option_name',
        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.patronym_name as client_patronym_name',

        'orders.price',
        'amount_paid',
        'amount_paid',
        '(orders.price - orders.amount_paid) as debt',
        'admin_message',
        'additional_data',
        'GROUP_CONCAT(distinct tags.`name` order by tags.`name` SEPARATOR ", ") as tags_list',
        'orders.status',
        'orders.created',
        'orders.updated',
        'UNIX_TIMESTAMP(orders.created) as created_unix',
        'UNIX_TIMESTAMP(orders.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();

        $this->CI->load->model('products_model');
        $this->CI->load->model('options_model');
        $this->CI->load->model('clients_model');
        $this->CI->load->model('promocodes_model');
        $this->CI->load->model('subs_model');
        $this->CI->load->model('paysystems_model');
        $this->CI->load->model('clients_products_model');
        $this->CI->load->model('clients_presets_model');
        $this->CI->load->model('clients_groups_model');
        $this->CI->load->model('products_gifts_model');
        $this->CI->load->model('gifts_presets_model');
        $this->CI->load->model('gifts_model');

        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы
     *
     * @return void
     */
    public function processJoins()
    {

        $this->db->join('options', 'orders.option_id = options.id', 'left');
        $this->db->join('products', 'options.product_id = products.id', 'left');
        $this->db->join('clients', 'orders.client_id = clients.id', 'left');
        $this->db->join('promocodes', 'orders.promocode_id = promocodes.id', 'left');
        $this->db->join('orders_tags', 'orders_tags.order_id = orders.id', 'left');
        $this->db->join('tags', 'orders_tags.tag_id = tags.id', 'left');
        $this->db->group_by('orders.id');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {

        // если вызов пришел из фронтового контроллера, то выбираем только доступные
        if ($this->caller_origin == 'front')
        {
            $this->db->where('NOT orders.status IN ("deleted")',null,false);
        }

    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Отменяет заказ
     *
     * @param int $id
     *
     * @return int id
     */
    public function cancel($id)
    {
        return $this->setStatus($id, 'cancelled');
    }

    /** Выполняет InsertOrUpdate
     * NOTICE: affected_rows после "insert ... update" возвращает
     * 1 - произошел insert
     * 0 - если произошел update и НИ одно значение НЕ изменилось
     * 2 - если произошел update и ХОТЬ одно значение изменилось
     *
     * @param array $data
     *
     * @return boolean
     */
/*
    public function insertOrUpdate($data)
    {
        $data = $this->filterFields($data);
        $new_data = $data;

        // формирование данных заказа -----------------------------------------------
        $aDATA = [];

        $option = $client = $product = null;
        if (!empty($data['option_id']))
        {
            $option = $this->options_model->getById( $data['option_id'] );
            $product = $this->products_model->getById( $option->product_id );
            $promocode = $this->promocodes_model->getById( $data['promocode_id'] );
            //$paysystem = $this->paysystems_model->getById( $product->paysystem_id );
        }
        if (!empty($data['client_id']))
        {
            $client = $this->clients_model->getById( $data['client_id'] );
        }

        $aDATA['product'] = $product;
        //$aDATA['paysystem'] = $paysystem;
        $aDATA['option'] = $option;
        $aDATA['client'] = $client;
        $new_data['additional_data'] = json_encode($aDATA);
        // END формирование данных заказа -----------------------------------------------

        $res = parent::insertOrUpdate($new_data);

        return $res;
    }
*/
    /** Изменяет заказ
     *
     * @param int $id
     * @param array $data
     *
     * @return int id
     */
    public function update($id, $data, $escape=true)
    {
        $data = $this->filterFields($data);

        $new_data = $data;
        /** @var StdClass $order */
        $order = $this->getById($id);

        // формирование данных заказа -----------------------------------------------
        $aDATA = [];

        $option = $client = $product = null;

        if (!empty($data['option_id'])) $option_id = $data['option_id'];
            else $option_id = $order->option_id;

        if (!empty($data['client_id'])) $client_id = $data['client_id'];
        else $client_id = $order->client_id;

        if (!empty($data['promocode_id'])) $promocode_id = $data['promocode_id'];
        else $promocode_id = $order->promocode_id;

        if (!empty($data['subs_id'])) $subs_id = $data['subs_id'];
        else $subs_id = $order->subs_id;
/*
        if (!empty($data['subs_id'])) $subs_id = $data['subs_id'];
        else $promocode_id = $order->promocode_id;
*/
        if ( (!empty($data['client_id'])) || (!empty($data['option_id'])) || (!empty($data['product_id'])) || (!empty($data['promocode_id'])) )
        {
            $option = $this->options_model->getById( $option_id );
            $product = $this->products_model->getById( $option->product_id );
            $client = $this->clients_model->getById( $client_id );
            $promocode = $this->promocodes_model->getById( $promocode_id );
            $subs = $this->subs_model->getById( $subs_id );
            //$paysystem = $this->paysystems_model->getById( $product->paysystem_id );

            $aDATA['product'] = $product;
            //$aDATA['paysystem'] = $paysystem;
            $aDATA['option'] = $option;
            $aDATA['client'] = $client;
            $aDATA['promocode'] = $promocode;
            $aDATA['subs'] = $subs;
            $new_data['additional_data'] = json_encode($aDATA);
        }
        // END формирование данных заказа -----------------------------------------------

        $res = parent::update($id, $new_data, $escape);

        return $res;
    }


    /** Производит выборку по параметрам. Соединение WHERE через AND
     *
     *   @param array $params - массив массивов. $params = [
     *                                               ["admin_name","=","Василий"],
     *                                               ["admin_login","=","Vas"],
     *                                           ];
     *   @param array $order - array of string $order = [
     *                                                  "admin_name ASC",
     *                                                  "admin_login DESC",
     *                                         ];
     *   @param array $limit - массив $limit = [
     *                                           "limit" => 10,
     *                                           "offset" => 10,
     *                                          ];
     *   @param array $fields - список возвращаемых полей $fields = ['id','name'];
     *   @param boolean $returnForServerSide - возвращать ли результат в виде ['totalRows'=>XXX, 'rows'=>array of StdClass of rows ](это нужно для VUE DataTables remote mode)
     *      либо просто в виде - array of StdClass of rows - для любых остальных случаев
     *
     * @return array
     *
     * Пример: $params = [
     *          ["admin_name","=","'Василий'"],
     *          ["admin_lastname","=","CONCAT('Vas',admin_lastname)"],
     *          ];
     */
    public function getByParams($params = [], $order = [], $limit = [], $fields = [], $returnForServerSide = false)
    {
        if (empty($order))
        {
            $order = [$this->order_by_field." ".$this->order_by_dir];
        }
        if (empty($fields))
        {
            $fields = $this->default_field_list;
        }

        $this->db->order_by( join(",",$order));

        if ($returnForServerSide)
        {
            $this->db->select( "SQL_CALC_FOUND_ROWS ".join(",",$fields), false);
        }
        else
        {
            $this->db->select( join(",",$fields), false );
        }

        $this->processJoins();

        if (!empty($params))
        {
            if (is_array($params)) // параметры заданы массивом
            {
                foreach ($params as $key => $param)
                {
                    if (is_array($param) && count($param)>1)
                    {
                        $field = $param[0];
                        $relation = $param[1];
                        $value = $param[2];
                        //$value = $this->db->escape($value);
                        //$this->db->where($field.$relation.$value, null, false);
                        //            if ($this->needToQuote($value)) $value = $this->db->escape($value);
                        $value = $this->db->escape($value);

                        if ($field == 'tags_list')
                        {   // костыль. В MySQL нельзя использовать aliases in where
                            $this->db->having($field . $relation . $value, null, false);
                        }
                        else
                        {
                            // если в "имени поля" нет '.', и это НЕ 'служебное слово' то добавляем к имени поля текущую таблицу-
                            // ПОМНИТЬ ОБ ЭТОМ при использовании params вида count(id) -> должно быть COUNT(id) (чтобы не сработало второе условие)
                            if ((strpos($field, ".") === false) && (strtolower($field) == $field)) {
                                $field = $this->db->dbprefix($this->table) . "." . $field;
                            }
                            $this->db->where($field . $relation . $value, null, false);
                        }
                    }
                    else
                    {
                        // параметры - это строки
                        $this->db->where($param[0], null, false);
                    }
                } // foreach
            }
            else // параметры заданы строкой.
            {
                $this->db->where($params, null, false);
            }
        }

        $this->processAdditionalWhere();

        if (!empty($limit)) $this->db->limit( $limit['limit'], $limit['offset'] );
/*
$sql = $this->db->get_compiled_select($this->table);
echo "<pre>";
print_r($sql);
echo "</pre>";
die();
*/
        $res = $this->db->get($this->table);
        $rows = $res->result();

        $rows = $this->prepareOutput($rows);

        if ($returnForServerSide)
        {
            $totalRecords = $this->db->query("SELECT FOUND_ROWS() as rowcount")->result()[0]->rowcount;
            return ['totalRows'=>$totalRecords, 'rows'=>$rows];
        }

        return $rows;
    }


    /** Производит выборку заказов по клиенту
     *
     * @param int $client_id - ID клиента
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку заказов по orders.subs_id - ID подписки
     *
     * @param int $subs_id - ID подписки
     *
     * @return array
     */
    public function getByOrdersSubsId($subs_id)
    {
        $params = [
            ['subs_id', '=', $subs_id],
        ];

        $res = $this->getByParams($params);

        if (!empty($res[0])) return $res[0];
        else return null;

    }

    /** Производит выборку самого нового заказа по orders.subs_id - ID подписки
     *
     * @param int $subs_id - orders.subs_ID подписки в ЦЕЛОЧИСЛЕННОМ ВИДЕ
     *
     * @return array
     */
    public function getLastByOrdersSubsId($subs_id)
    {
        $params = [
            ['subs_id', '=', intval($subs_id)],
        ];

        $limit = ['limit' => 1, 'offset' => 0];
        $order = ['orders.created DESC'];

        $res = $this->getByParams($params, $order, $limit);

        if (!empty($res[0])) return $res[0];
        else return null;
    }

    /** Производит выборку самого нового заказа по subs.subs_id - ID подписки в СТРОКОВОМ ВИДЕ
     *
     * @param string $subs_id - ID подписки в СТРОКОВОМ ВИДЕ
     *
     * @return array
     */
    public function getLastBySubsSubsId($subs_id)
    {
        $result = null;

        $subs = $this->subs_model->getBySubsId($subs_id);

        if (!empty($subs))
        {
            $params = [
                ['subs_id', '=', $subs->id],
            ];

            $limit = ['limit'=>1, 'offset'=>0];
            $order = ['orders.created DESC'];

            $res = $this->getByParams($params, $order, $limit);

            if (!empty($res[0])) $result = $res[0];
        }

        return $result;
    }

    /** Производит выборку заказов по использованному промокоду
     *
     * @param int $promocode_id - ID промокода
     *
     * @return array
     */
    public function getByPromocodeId($promocode_id)
    {
        $params = [
            ['promocode_id', '=', intval($promocode_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Функция для проверки возможности оплати текущего заказа указанным кол-вом средств указанным клиентом
     *
     * @param int|object $order - ID заказа, либо сам объект
     * @param int|object $option - ID опции, либо сам объект
     * @param int|object $client - ID клиента, либо сам объект
     * @param float $amount - количество денег
     * @param int|object $subs - ID подписки, либо сам объект
     *
     * @return boolean
     */
    public function isPaymentPossible($order, $option, $client, $amount, $subs)
    {
        if (!is_object($order)) $order = $this->getById($order);
        if (empty($order)) return STATUS_TRANS_INVALID_ORDER; // заказ не найден

        if (!is_object($option)) $option = $this->CI->options_model->getById($order->option_id);
        if (empty($option)) return STATUS_TRANS_INVALID_ORDER; // опция не найдена

        if (!is_object($client)) $client = $this->CI->clients_model->getById($client);
        if (empty($client)) return STATUS_TRANS_INVALID_CLIENT; // клиент не найден

        if (!is_object($subs)) $subs = $this->CI->subs_model->getById($subs);

        // попытка оплатить заказ с неподходящим статусом
        if ($order->status != 'new') {
            if ($order->status == 'partially_paid') {
                // если подписки нет, то это доплата текущего order
                // если есть подписка и она активна, то это рекуррентная транзакция от cloudpayments
                if (!$subs) {
                    if ($order->amount_paid + $amount > $order->price) {
                        return STATUS_TRANS_INVALID_AMOUNT;
                    }
                    if ((!$option->is_partial_available) && ($amount < $order->price)) {
                        return STATUS_TRANS_INVALID_AMOUNT;
                    }
                } elseif(!in_array($subs->status, ['active', 'pastdue'])) {
                    // у подписки нет активного статуса
                    return STATUS_TRANS_INVALID_ORDER;
                }
            } elseif($order->status == 'paid') {
                // если подписка существует, то это рекуррентная транзакция от cloudpayments
                if (!$subs) {
                    return STATUS_TRANS_INVALID_ORDER;
                }
            } else {
                return STATUS_TRANS_INVALID_ORDER;
            }
        } else {
            // попытка частично оплатить опцию, без возможности частичной оплаты
            if ((!$option->is_partial_available) && ($amount < $order->price)) return STATUS_TRANS_INVALID_AMOUNT;

            // попытка оплатить больше cтоимости или остатка стоимости
            if (($order->amount_paid + $amount) > $order->price) return STATUS_TRANS_INVALID_AMOUNT;
        }

        // client_id в заказе и транзакции не совпадают
        if ($order->client_id != $client->id) return STATUS_TRANS_INVALID_CLIENT;

        return STATUS_TRANS_OK;
    }

    /** Функция для оплаты текущего заказа указанным кол-вом средств указанным клиентом
     *
     * @param int|object $order - ID заказа, либо сам объект заказа
     * @param int|object $product - ID продукта, либо сам объект
     * @param int|object $option - ID опции, либо сам объект опции
     * @param int|object $client - ID клиента, либо сам объект
     * @param int|object $subs - ID подписки (внутр), либо сам объект подписки
     * @param float $amount - количество денег
     *
     * @return boolean
     */
    public function pay($order, $option, $product, $client, $amount, $subs)
    {
        if (!is_object($order)) $order = $this->getById($order);
        if (empty($order)) return false; // заказ не найден

        if (!is_object($client)) $client = $this->CI->clients_model->getById($client);
        if (empty($client)) return false; // клиент не найден

        if (!is_object($option)) $option = $this->CI->options_model->getById($order->option_id);
        if (empty($option)) return false; // опция не найдена

        if (!is_object($product)) $product = $this->CI->products_model->getById($option->product_id);
        if (empty($product)) return false; // продукт не найден

        if (!is_object($subs)) $subs = $this->CI->subs_model->getById($subs);

        /** @var StdClass $order */
        $tot_paid = floatval($order->amount_paid + $amount);

        $uData = [];
        $uData['amount_paid'] = $tot_paid;
        if ($tot_paid == $order->price)
        {
            // заказ оплачен полностью =====================================================
            $uData['status'] = 'paid';

            $dt = new DateTime('now');
            $start_dt = $dt->format('Y-m-d H:i:s');
            $end_dt = null;

            // у опции есть duration -------------------------------------------
            if (!empty($option->duration))
            {
                $duration = intval($option->duration);

                $dt->add(new DateInterval('P' . $duration . 'D')); // добавляем $duration дней
                $end_dt = $dt->format('Y-m-d H:i:s');
            } // end у опции есть duration ---------------------------------------

            // даем доступ клиенту к продукту------------------------
            // доступы в clients_presets добавятся DB триггером
            $this->CI->clients_products_model->addProductToClient($product->id, $client->id, $start_dt, $end_dt, $order->id, 'order', $order->id);
            // END даем доступ клиенту к продукту--------------------

            // даем доступ к подарочным пресетам ---------------------------------
            $gifts = $this->CI->products_gifts_model->getGiftsByProductId($product->id);
            if (!empty($gifts))
            {
                foreach ($gifts as $gift)
                {
                    $this->CI->gifts_model->sendGiftToClient($gift, $client, $product);
                    /*
                    $presets = $this->gifts_presets_model->getPresetsByGiftId($gift->id);
                    $gift_duration = $gift->duration;
                    foreach ($presets as $preset)
                    {
                        $dt = new DateTime('now');
                        $starting_dt = $dt->format('Y-m-d H:i:s');
                        $dt->add(new DateInterval('P' . $gift_duration . 'D')); // добавляем $gift_duration дней
                        $expiration_dt = $dt->format('Y-m-d H:i:s');

                        if ($product->product_type != 'subscription') $starting_dt = null; // только у продуктов типа subscription может быть определено значение $starting_dt для clients_presets

                        $this->clients_presets_model->addPresetToClient($preset->id, $client->id, $starting_dt, $expiration_dt, 'gift', $gift->id);
                    }
                    */
                }
            }
            // END даем доступ к подарочным пресетам -----------------------------

            // добавляем клиента в группу -----------------------
            if (!empty($product->group_id)) $this->clients_groups_model->addClientToGroup($client->id, $product->group_id);
            // END добавляем клиента в группу -------------------

        } //if ($tot_paid >= $order->price)
        // END заказ оплачен полностью =====================================================
        else {
            $uData['status'] = 'partially_paid';
            // оплата по заказу превысила порог доступности продукта - даем доступ на option->partial_available_duration дней ===========================
            if ($tot_paid >= $option->partial_threshold)
            {
                $dt = new DateTime('now');
                $start_dt = $dt->format('Y-m-d H:i:s');
                $end_dt = $dt->format('Y-m-d H:i:s');

                // у опции есть duration -------------------------------------------
                if (!empty($option->partial_available_duration))
                {
                    $duration = intval($option->partial_available_duration);
                    $dt->add(new DateInterval('P' . $duration . 'D')); // добавляем $duration дней
                    $end_dt = $dt->format('Y-m-d H:i:s');
                } // end у опции есть duration ---------------------------------------

                // даем доступ клиенту к продукту------------------------
                // доступы в clients_presets добавятся DB триггером
                $this->clients_products_model->addProductToClient($product->id, $client->id, $start_dt, $end_dt, $order->id, 'order', $order->id);
                // END даем доступ клиенту к продукту--------------------
            }
            // END оплата по заказу превысила порог доступности продукта - даем доступ на option->partial_available_duration дней =======================
        }

        if(!empty($subs))
        {
            $uData['subs_id'] = $subs->id;
        } elseif ($order->subs_id) {
            $uData['subs_id'] = $order->subs_id;
        } else {
            $uData['subs_id'] = 'NULL';
        }

        $this->db->trans_begin();
        $upd_res = $this->CI->orders_model->update($order->id, $uData);

        if ($upd_res)
        {
            $status = STATUS_TRANS_OK;
            $message = STATUS_TRANS_OK_MSG;
            $this->db->trans_commit();
        }
        else
        {
            $status = STATUS_TRANS_CANT_ACCEPT;
            $message = STATUS_TRANS_CANT_ACCEPT_MSG;
            $this->db->trans_rollback();
        }

        return $status;
    }

    /** Изменяет "приписку" заказов (через update) клиента donor_id на клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $this->CI->load->model('clients_model');
        $orders = $this->getByClientId($donor_id);

        $new_client = $this->CI->clients_model->getById($acceptor_id);

        $result = true;
        foreach($orders as $order)
        {
            $uData = [];
            $uData['client_id'] = $acceptor_id;

            if(!empty($order->additional_data))
            {
                $json = json_decode($order->additional_data);
                $additional_data = [];
                $additional_data['client'] = $new_client;
                if (!empty($json->product)) $additional_data['product'] = $json->product;
                if (!empty($json->option)) $additional_data['option'] = $json->option;
                if (!empty($json->promocode)) $additional_data['promocode'] = $json->promocode;

                $uData['additional_data'] = json_encode($additional_data);
            }

            $res = $this->update($order->id, $uData);
            $result = $result & ($res !== false);
        }

        return $result;
    }


    /** Выборка всех заказов клиента с ипользованием промокода promocode_id
     *  Если promocode_id ==  NULL - выберутся все заказы, где использован ЛЮБОЙ промокод
     *  Метод нужен для построения списка "какие промокоды использовал клиент"
     *
     * @param int $client_id - ID клиента, для которого производится выборка
     * @param int $promocode_id - ID промокода, для которого производится выборка
     *
     * @return array of objects
     */
    public function getByClientIdPromocodeId($client_id, $promocode_id = null)
    {
        $fields = [
            'orders.id',
            'orders.option_id',
            'options.name as option_name',

            'products.id as product_id',
            'products.name as product_name',

            'orders.client_id',

            'promocode_id',
            'promocodes.name as promocode_name',
            'promocodes.promocode promocode_promocode',
            'promocodes.start_dt as promocode_start_dt',
            'promocodes.end_dt as promocode_end_dt',
            'promocodes.discount_type as promocode_discount_type',
            'promocodes.discount_value as promocode_discount_value',
            'promocodes.one_time_only as promocode_one_time_only',
            'promocodes.status as promocode_status',

            'subs_id',
            'orders.price',
            'amount_paid',
            'client_message',
            'admin_message',
            'additional_data',
            'GROUP_CONCAT(distinct tags.`name` order by tags.`name` SEPARATOR ", ") as tags_list',
            'orders.status',
            'orders.created',
            'orders.updated',
            'UNIX_TIMESTAMP(orders.created) as created_unix',
            'UNIX_TIMESTAMP(orders.updated) as updated_unix'
        ];

        $params = [
            ['client_id', '=', intval($client_id)],
        ];
        if (empty($promocode_id)) $params[] = ['promocode_id IS NOT NULL'];
            else $params[] = ['promocode_id', '=', intval($promocode_id)];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }



}

