<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Orders_tags_model
*/
class Orders_tags_model extends MY_Model
{
    public $table = 'orders_tags';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'orders.id';
    public $order_by_dir = 'ASC';

    public $default_field_list = [
        'orders_tags.id as id',
        'orders_tags.id as rel_id',
        'orders_tags.order_id',
        'orders_tags.tag_id',

        'orders.status as order_status',

        'tags.name',
        'tags.status as tag_status',

        'orders_tags.created',
        'orders_tags.updated',
        'UNIX_TIMESTAMP(orders_tags.created) as created_unix',
        'UNIX_TIMESTAMP(orders_tags.updated) as updated_unix'
    ];



    public function __construct()
    {
        parent::__construct();

        $this->table_full = $this->db->dbprefix.$this->table;

        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('orders', 'orders_tags.order_id = orders.id', 'inner');
        $this->db->join('tags', 'orders_tags.tag_id = tags.id', 'inner');
    }

    /** Производит выборку тегов, привязанных к определенному заказу
     *
     * @param int $order_id - order_ID, для которого производится выборка тегов
     *
     * @return array
     */
    public function getTagsByOrderId($order_id)
    {
        $params = [
            [$this->table_full.'.order_id', '=', intval($order_id)],
        ];

        $fields = [
            'orders_tags.tag_id as id',
            'orders_tags.id as rel_id',
            'tags.name',
            'tags.description',
            'tags.status',
            'tags.created',
            'tags.updated',
            'UNIX_TIMESTAMP(tags.created) as created_unix',
            'UNIX_TIMESTAMP(tags.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** Производит выборку заказов, к которым привязан указанный тег
     *
     * @param int $tag_id - tag_id, для которой производится выборка заказов
     *
     * @return array
     */
    public function getOrdersByTagId($tag_id)
    {
        $params = [
            ['orders_tags.tag_id', '=', intval($tag_id)],
        ];

        $fields = [
            'orders_tags.order_id as id',
            'orders_tags.id as rel_id',
            'orders.option_id',
            'orders.client_id',
            'orders.promocode_id',
            'orders.subs_id',
            'orders.price',
            'orders.amount_paid',
            'orders.client_message',
            'orders.admin_message',
            'orders.additional_data',
            'orders.status',
            'orders.created',
            'orders.updated',
            'UNIX_TIMESTAMP(orders.created) as created_unix',
            'UNIX_TIMESTAMP(orders.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);

        return $res;
    }

    /** проверка, присоединен ли tag_id к заказу order_id
     *
     * @param int $order_id - заказ
     * @param int $tag_id - tag_id, для которой производится проверка присоединенности
     *
     * @return boolean
     */
    public function isTagInOrder($tag_id, $order_id)
    {
        $params = [
            ['order_id', '=', intval($order_id)],
            ['tag_id', '=', intval($tag_id)],
        ];

        $fields = [
            'orders_tags.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по order_id и tag_id
     *
     * @param int $order_id - заказ
     * @param int $tag_id - tag_id, для которого производится проверка вхождения
     *
     * @return array
     */
    public function getByOrderIdTagId($order_id, $tag_id)
    {
        $params = [
            ['order_id', '=', intval($order_id)],
            ['tag_id', '=', intval($tag_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет тег в order. Если его там нет.
     *
     * @param int $order_id - order_id, который добавляетя тег
     * @param int $tag_id - tag_id, который добавляется к заказу
     *
     * @return int
     */
    public function addTagToOrder($order_id, $tag_id)
    {
        $record = $this->getByOrderIdTagId($order_id, $tag_id);
/*
        // человек НЕ в группе - добавляем
        if (empty($record))
        {
            $uData = [
                'order_id'=>intval($order_id),
                'tag_id'=>intval($tag_id),
            ];
            $res_id = $this->insert($uData);
        }
        else
        {
            // человек уже в группе
            $res_id = $record->id;
        }

        return $res_id;
*/
        $uData = [
            'order_id'=>intval($order_id),
            'tag_id'=>intval($tag_id),
        ];
        $res_id = $this->insertOrUpdate($uData);

        return $res_id;
    }

    /** Убирает тег из заказа.
     *
     * @param int $tag_id - tag_id, который убирается из заказа
     * @param int $order_id - order_id, из которого тег
     *
     * @return int
     */
    public function removeTagFromOrder($tag_id, $order_id)
    {
        $params = [
            ['order_id', '=', intval($order_id)],
            ['tag_id', '=', intval($tag_id)],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }

    /** Очищает order от тегов
     *
     * @param int $order_id - ID заказа, для коорого очищаютя теги
     *
     * @return int
     */
    public function clearTags($order_id)
    {
        $params = [
            ['order_id', '=', intval($order_id)],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }


}
