<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissions_model extends MY_Model
{
    public $table = 'permissions';
    public $entity = null;
    public $primary = 'admin_id';
    public $error;

    public $pref = 'p_';

    private $fieldTitles =[
        'admins'      => 'Права на раздел "Сотрудники"',
        'admingroups' => 'Права на раздел "Группы сотрудников"',
        'products'    => 'Права на раздел "Продукты"',
        'paysystems'  => 'Права на раздел "Банковские счета"',
        'privileges'  => 'Права на раздел "Льготы"',
        'clients'     => 'Права на раздел "Клиенты"',
        'groups'      => 'Права на раздел "Группы клиентов"',
        'docs'        => 'Права на раздел "Документы клиентов"',
        'presets_cats'=> 'Права на раздел "Категории коллекций"',
        'presets'     => 'Права на раздел "Колллекции"',
        'items'       => 'Права на раздел "Медиафайлы"',
        'gifts'       => 'Права на раздел "Бонусы продуктов"',
        'pages'       => 'Права на раздел "Статические страницы"',
        'templates'   => 'Права на раздел "Шаблоны писем"',
        'instagram_accounts' => 'Права на раздел "Аккаунты Instagram"',
        'promocodes'  => 'Права на раздел "Промокоды"',
        'orders'      => 'Права на раздел "Заказы"',
        'tags'        => 'Права на раздел "Теги"',
        'countries_cities'  => 'Права на раздел "Страны и города"',
        'items_delete_with_content'  => 'Права на полное удаление медиафайла',
        'items_download_from_vimeo'  => 'Права на скачивание медиафайла',

    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** подготавливает ввод. Если на вход идет массив вида
    $data = [
    'admin_id' => 123,
    'admins' => 1,
    'followers' => 0,
    'reports => 1'
    ];
    он преобразуется к виду
    $data = [
    'admin_id' => 123,
    'p_admins' => 1,
    'p_followers' => 0,
    'p_reports => 1'
    ]; он преобразуется к виду
     * @param array $data
     *
     * @return array
     */
    protected function prepareInput($data)
    {
        if ($data instanceof ArrayObject) $data = $data->getArrayCopy();
        if (is_object($data)) $data = (array)$data;

        $new_data = [];

        if (!empty($data))
        {
            foreach ($data as $fname => $val) {
                if (in_array($fname, $this->fields)) {
                    $new_data[$fname] = $val;
                } else {
                    if (!preg_match("/" . $this->pref . ".*/", $fname)) $fname = $this->pref . $fname;
                    $new_data[$fname] = $val;
                }
            } // foreach
        }
        return  $new_data;
    }

    /* подготавливает вывод  - убирает префиксы с имен полей, убирает primary key*/
    protected function prepareOutput($data)
    {
        $new_data = new ArrayObject();
        $new_data->setFlags(ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);

        if (!empty($data))
        {
            foreach ($data as $fname => $val) {
                if ($fname == $this->primary) continue;
                $fname = str_replace($this->pref, '', $fname);
                $new_data->{$fname} = $val;
            }
        }

        return  $new_data;
    }

    public function getByAdminId($admin_id)
    {
        $query = $this->db
                ->where($this->primary, $admin_id)
                ->limit(1)
                ->get($this->table);
        $this->error = $this->db->error();

        if( !empty($this->error['message']) ) die($this->error['message']);

        $res = $query->row();

        return  $this->prepareOutput($res)->getArrayCopy();
    }

    /*  создает или обновляет данные о правах пользователя
     *  $data - массив данных, где одним из элементов является admin_id
     *  $full_list_update - если true, то обновляются ВСЕ права. Те, которые отсутствуют в $data становятся 0
    */
    public function save($data, $full_list_update = false)
    {
        $data = $this->prepareInput($data);
        $data = $this->filterFields($data);

        if ($full_list_update)
        {
            foreach ($this->fields as $fname)
            {
                if ($fname == $this->primary) continue;
                $data[$fname] = ( (isset($data[$fname])) && (boolval($data[$fname])) ) ? 1:0;
            }
        }

        if (empty($data[$this->primary]))
        {
            die("Ошибка сохранения: пользователь не определен");
        }

        $admin_id = $data[ $this->primary ];
        $u = $this->getByAdminId($admin_id);

        if (empty($u))
        {
            //данных нет - создаем
            $this->db->set($data)->insert($this->table);

        }
        else
        {
            //данные о пользователи уже есть - изменяем
            //unset($data[$this->primary]);
            $this->db->set($data)
                ->where($this->primary, $admin_id)
                ->update($this->table);
        }

        $this->error = $this->db->error();
        if( !empty($this->error['message']) ) die($this->error['message']);

        return $data;
    }

    /** Возвращает список полей permissions для построения формы назначения прав
     *
     * @return array
     */
    public function getPermissionsList()
    {
        return $this->fieldTitles;
    }


}