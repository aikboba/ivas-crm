<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presets_cats_model extends MY_Model
{
    public $table = 'presets_cats';
    public $entity = 'presets_cat';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'parent_id',
        'name',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('presets_cats.status = "active"');
        }
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    public function getByParentId($parent_id)
    {
        $params = [
            ['parent_id', '=', $parent_id]
        ];

        return $this->getByParams($params);
    }

    /** Возвращает "цепочку вверх" категорий от ТЕКУЩЕЙ ($this_id) до рута.
     * Используется, например, для получения "хлебных крошек"
     *
     * @param int $this_id - ID текущей категории
     * @param int $arr - массив, который после завершения рекурсии
     *
     * @return array $arr - массив результатов, где первый элемент - текущая категория, последний - первый потомок рута
     */
    public function getParentsListById($this_id,&$arr)
    {
        if($this_id == 0) return;

        /** @var StdClass $this_cat */
        $this_cat = $this->getById($this_id);

        $arr[] = $this_cat;

        if ($this_cat->parent_id == 0) return;

        $this->getParentsListById($this_cat->parent_id,$arr);
    }

    /** Возвращает полный список потомков указанной категории
     * При применении процедуры левостороннего обхода дерева к результату может быть построено визуальное отображении иерархии
     *
     * @param int $this_id - ID текущей категории
     * @param int $arr - массив, который после завершения рекурсии
     *
     * @return array $arr - массив результатов
     */
    function getDescendantsListById($this_id,&$arr)
    {
        if($this_id == -1) return;

        $res = $this->getByParentId($this_id);

        if (!empty($res))
        {

            foreach ($res as $cat)
            {
                $arr[] = $cat;
                $this->getDescendantsListById($cat->id,$arr);
            }

        }
/*
        $query="select category_code,category_name,category_parent from {$PREFFIX}_category where category_parent=$thisID order by category_pos" or die("!!!");
        $res=mysql_query($query);
        $n_r=mysql_num_rows($res);
        if (!$n_r) return;
        while (list($category_c,$category_n,$category_p)=mysql_fetch_array($res))
        {
            $arr[]=Array("code"=>$category_c,"name"=>$category_n,"parent"=>$category_p);
            GetDescendants($category_c,$arr);
        }
*/

        return -1;
    }

}