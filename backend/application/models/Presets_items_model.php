<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Presets_items_model extends MY_Model
{
    public $table = 'presets_items';
    public $entity = null;
    public $primary = 'id';
    public $posfield = "pos";
    public $delimfield = "preset_id";
    public $error;
    public $order_by_field = 'presets_items.pos';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'presets_items.id as id',
        'presets_items.id as rel_id', // для совместимости
        'presets_items.preset_id',
        'presets_items.item_id',
        'presets_items.pos',
        'presets.name as preset_name',
        'presets.status as preset_status',
        'items.name as item_name',
        'items.description as item_description',
        'items.status as item_status',
        'presets_items.created',
        'presets_items.updated',
        'UNIX_TIMESTAMP(presets_items.created) as created_unix',
        'UNIX_TIMESTAMP(presets_items.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('presets.status = "active"');
            $this->db->where('items.status = "active"');
        }
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('presets', 'presets_items.preset_id = presets.id', 'left');
        $this->db->join('items', 'presets_items.item_id = items.id', 'left');
    }

    /** Производит выборку items для определенного preset
     *
     * @param int $id - preset_ID, для которого производитс выборка групп
     * @param boolean $result_by_item_id - если true, то результат преобразуется в массив, где ключами будет item_id (из items), а не id из presets_items
     *
     * @return array
     */
    public function getItemsByPresetId($id)
    {
        $params = [
            ['presets_items.preset_id', '=', intval($id)],
        ];

        $fields = [
            'presets_items.item_id as id',
            'presets_items.id as rel_id',
            'items.name',
            'items.description',
            'items.status as status',
            'presets_items.pos',
            'items.created',
            'items.updated',
            'UNIX_TIMESTAMP(items.created) as created_unix',
            'UNIX_TIMESTAMP(items.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;

        return $data;
    }

    /** Производит выборку presets, в которых состоит item
     *
     * @param int $id - item_id, для которого производится выборка presets
     *
     * @return array
     */
    public function getPresetsByItemId($id)
    {
        $params = [
            ['presets_items.item_id', '=', intval($id)],
        ];

        $fields = [
            'presets_items.preset_id as id',
            'presets_items.id as rel_id',
            'presets.name',
            'presets.status',
            'presets.created',
            'presets.updated',
            'UNIX_TIMESTAMP(presets.created) as created_unix',
            'UNIX_TIMESTAMP(presets.updated) as updated_unix'
        ];

        $order = ["presets.name" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** проверка, связаны ли (состоит ли) item и preset
     *
     * @param int $item_id - item_id, для которого производится проверка вхождения
     * @param int $preset_id
     *
     * @return boolean
     */
    public function isItemInPreset($item_id, $preset_id)
    {
        $params = [
            ['item_id', '=', intval($item_id)],
            ['preset_id', '=', intval($preset_id)],
        ];

        $fields = [
            'presets_items.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по preset_id и item_id
     *
     * @param int $preset_id -
     * @param int $item_id - item_id, для которого производится проверка вхождения
     *
     * @return array
     */
    public function getByPresetIdItemId($preset_id, $item_id)
    {
        $params = [
            ['preset_id', '=', intval($preset_id)],
            ['item_id', '=', intval($item_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    public function insert($data)
    {
        return parent::insertWithPos($data);
    }

    public function update($id, $data, $escape=true)
    {
        return parent::updateWithPos($id, $data);
    }

    public function delete($id)
    {
        return parent::deleteWithPos($id);
    }

    /** Добавляет item в preset. Если его там нет.
     *
     * @param int $item_id - item, который добавляетя в preset
     * @param int $preset_id - preset, в который добавляется item
     * @param int $pos - позиция, в которую добавляется item
     *
     * @return int
     */
    public function addItemToPreset($item_id, $preset_id, $pos)
    {
        $record = $this->getByPresetIdItemId($preset_id, $item_id);

        // item НЕ в preset - добавляем
        if (empty($record))
        {
            $uData = [
                'item_id' => intval($item_id),
                'preset_id' => intval($preset_id),
                'pos' => intval($pos),
            ];
            $res_id = $this->insert($uData);
        }
        else
        {
            // item уже в preset
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Убирает item из preset.
     *
     * @param int $item_id - item, который убирается из пресета
     * @param int $preset_id - preset, из которого убирается item
     *
     * @return int
     */
    public function removeItemFromPreset($item_id, $preset_id)
    {
        $record = $this->getByPresetIdItemId($preset_id, $item_id);

        // item НЕ в preset - выходим
        if (empty($record))
        {
            return 0;
        }
        else
        {
            // item в preset
            $id = $record->id;
        }

        return parent::deleteWithPos($id);
    }

    /** Удаляет ВСЕ items из preset.
     *
     * @param int $preset_id - preset, из которого убираются items
     *
     * @return int
     */
    public function clearPreset($preset_id)
    {
        if (!$preset_id) return;

        $params = [
            ['preset_id', '=', $preset_id]
        ];
        $res = $this->deleteByParams($params);

        return $res;
    }

    /** Доступен ли item клиенту (с учетом даты)
     * item доступен, если находится хотя бы в одном доступном клиенту пресете
     * Если в связке client_preset присутствует starting_dt(имеем дело с пресетом "эффира"),
     * то появляется доп условие: item доступен , если он попал в пресет ПОЗДНЕЕ statring_dt
     * (т.е. видос попал в пресет после того, как этот пресет стал доступен клиенту)
     *
     * @param int $item_id - item, доступность которого проверяется
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return boolean
     */
    public function isItemAvailableForClient($item_id, $client_id)
    {
        $result = false;

        $this->CI->load->model('clients_presets_model');
        $presets = $this->CI->clients_presets_model->getAvailablePresetsByClientId($client_id);
        if (!empty($presets))
        {
            $presetsIDS = [];
            foreach ($presets as $preset)
            {
                $presetsIDS[] = $preset->id;
            }
            $presetsSTR = join(",", $presetsIDS);
            $query = "SELECT pi.id FROM `presets_items` pi 
                      LEFT JOIN `clients_presets` cp on (pi.preset_id = cp.preset_id)
                      INNER JOIN `items` i on (pi.item_id = i.id)
                      INNER JOIN `presets` pres on (pi.preset_id = pres.id)
                      WHERE pi.item_id=".$item_id." 
                      AND pi.preset_id IN (".$presetsSTR.")
                      AND ( (i.available_dt>=cp.starting_dt) OR (cp.starting_dt IS NULL) )
                      AND ( (i.available_dt<cp.expiration_dt) OR (cp.expiration_dt IS NULL) )
                      AND i.status='active'
                      AND pres.status='active'
                      LIMIT 1";
            $res = $this->db->query($query);
            $row = $res->row();
            if (!empty($row)) $result = true;
        }

        return $result;
    }

    /** Список доступных клиенту itemов из конкретного пресета(с учетом даты)
     * item доступен, если (одновременно выполняется):
     * - пресет доступен клиенту
     * - если в связке client_preset присутствует starting_dt(имеем дело с пресетом "эфира"),
     *   item попал в пресет ПОЗДНЕЕ statring_dt (т.е. видос попал в пресет после того, как этот пресет стал доступен клиенту)
     *
     * @param int $preset_id - пресет, для которого выполняется проверка
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return array
     */
    public function getAvailableItemsForClientByPreset($client_id, $preset_id)
    {
        $this->CI->load->model('clients_presets_model');
        $preset_available = $this->CI->clients_presets_model->isPresetAvailableForClient($preset_id, $client_id);

        if (!$preset_available) return []; // пресет недоступен - выходим

        //есть ли указанный пресет в связке для нахождения дат starting_dt и expiration_dt
        //$presets_link = $this->CI->clients_presets_model->getByClientIdPresetId($client_id, $preset_id);

        $fields = [
            'DISTINCT(presets_items.item_id) as id',
            'presets_items.id as rel_id',
            'items.name',
            'items.description',
            'items.status as status',
            'presets_items.pos',
            'items.created',
            'items.updated',
            'UNIX_TIMESTAMP(items.created) as created_unix',
            'UNIX_TIMESTAMP(items.updated) as updated_unix'
        ];
        $fieldsSTR = join(',',$fields);

        $query = "SELECT ".$fieldsSTR." FROM `presets_items` 
                  INNER JOIN `items` on (`presets_items`.item_id = `items`.id)
                  LEFT JOIN `clients_presets` on (`presets_items`.preset_id = `clients_presets`.preset_id)
                  WHERE `presets_items`.preset_id=".$preset_id."
                  AND ( (`items`.available_dt>=`clients_presets`.starting_dt) OR (`clients_presets`.starting_dt IS NULL) )
                  AND ( (`items`.available_dt<=`clients_presets`.expiration_dt) OR (`clients_presets`.expiration_dt IS NULL) )
                  AND `items`.status='active'
                  ORDER BY pos
                  ";

        $res = $this->db->query($query);
        $rows = $res->result();

        return $this->prepareOutput($rows);
    }

    /** Список SHOWABLE сущностей в виде [item_id, preset_id, product_id]
     * выборка идет от showable items
     * item showable, если (одновременно выполняется):
     * - не пришел срок скисания
     * - если в связке client_preset присутствует starting_dt(имеем дело с пресетом "эфира"),
     *   item попал в пресет ПОЗДНЕЕ statring_dt (т.е. видос попал в пресет после того, как этот пресет стал доступен клиенту)
     *
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return array
     */
    public function getShowableEntitiesForClient($client_id)
    {
        $result = false;

        $fields = [
            'pi.id as id',
            'pi.item_id as item_id',
            'pi.preset_id as preset_id',
            'prod.id as product_id',
        ];
        $fieldsSTR = join(',',$fields);

        $query = "SELECT ".$fieldsSTR." FROM `presets_items` pi 
                    INNER JOIN items i on (pi.item_id = i.id)
                    INNER JOIN products prod on (prod.preset_id = pi.preset_id)
                    INNER JOIN clients_presets cp on (pi.preset_id = cp.preset_id AND cp.client_id=".intval($client_id).")
                    WHERE 
                    (
                    TIMESTAMPDIFF(DAY, i.`available_dt`, NOW()) < prod.`days_item_available`
                    OR 
                    prod.`days_item_available` IS NULL
                    )
                    AND ( (i.available_dt>=cp.starting_dt) OR (cp.starting_dt IS NULL) )
                    AND ( (i.available_dt<=cp.expiration_dt) OR (cp.expiration_dt IS NULL) )
                    AND i.status='active'
                    ORDER BY pos
                  ";

        $res = $this->db->query($query);
        $rows = $res->result();

        return $this->prepareOutput($rows);
    }

    /** Список SHOWABLE (тех, для которых не пришел срок "скисания" - products.items_days_available) itemов
     * из конкретного пресета(если задан preset_id)
     * item доступен, если (одновременно выполняется):
     * - не пришел срок скисания
     * - если в связке client_preset присутствует starting_dt(имеем дело с пресетом "эфира"),
     *   item попал в пресет ПОЗДНЕЕ statring_dt (т.е. видос попал в пресет после того, как этот пресет стал доступен клиенту)
     *
     * @param int $client_id - клиент, для которого проверятся доступность
     * @param int|null $preset_id - пресет, для которого выполняется проверка
     *
     * @return array
     */
    public function getShowableItemsForClientByPreset($client_id, $preset_id)
    {
        $data = $this->getShowableEntitiesForClient($client_id);
        $IDS = $tmp = [];
        foreach ($data as $elem)
        {
            if ($elem->preset_id == $preset_id)
            {
                $id = intval($elem->id);
                $tmp[$id] =$id;
            }
        }
        foreach ($tmp as $rel_id) $IDS[]=$rel_id;

        $rows1 = $this->getByIds($IDS);
        // преодразование к общей структуре данных
        foreach ($rows1 as $key=>&$row)
        {
            $rel_id = $row->id;
            $row->id = $row->item_id;
            unset($row->item_id);
            $row->rel_id = $rel_id;
            unset($row->preset_id);
            unset($row->preset_name);
            unset($row->preset_status);
            $row->name = $row->item_name;
            unset($row->item_name);
            $row->status = $row->item_status;
            unset($row->item_status);
            $row->description = $row->item_description;
            unset($row->item_description);
        }

        // все available уже являются "нескисшими"
        //$rows2 = $this->getAvailableItemsForClientByPreset($client_id, $preset_id);
/*
        // убираем дубликаты -------------------------
        $dataAll = $data = array_merge($rows2, $rows1);
        $data = [];
        foreach ($dataAll as $item)
        {
            $data[$item->id] = $item;
        }
        // убираем дубликаты -------------------------

        $resData = [];
        foreach ($data as $item) $resData[] = $item;

        return $this->prepareOutput($resData);
*/
        return $this->prepareOutput($rows1);
    }


}
