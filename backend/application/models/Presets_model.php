<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presets_model extends MY_Model
{
    public $table = 'presets';
    public $entity = 'preset';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'name',
        'description',
        'preset_type',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('presets.status = "active"');
        }
    }

    /** Выборка по preset_type
     *
     * @param string $type
     *
     * @return array of objects
     */
    public function getByType($type)
    {
        $params = [
            ['preset_type', "=" , $type]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res;

        return null;
    }

    /** Выборка presets, назначенных на is_free продукты
     *
     * @return array of objects
     */
    public function getFree()
    {
        $params = [
            ['id IN (SELECT p.preset_id FROM `products` p WHERE p.is_free=1 AND p.status="active")']
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res;

        return false;
    }

    /** Выборка по presets_cat_id - категории пресета.
     * НЕРЕКУРСИВНА(возвращает ТОЛЬКО пресеты конкретно УКАЗАННОЙ категории, не включая пресеты потомков категории)
     *
     * @param int $presets_cat_id
     *
     * @return array of objects
     */
    public function getByCategoryId($presets_cat_id)
    {
        $params = [
            ['presets_cat_id', "=" , intval($presets_cat_id)]
        ];
        $res = $this->getByParams($params);

        if (!empty($res)) return $res;

        return null;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        if (!$this->presetSubscriptionConnected) return $this->setDeleted($id);
            else return false;
    }

    /** Возвращает true, если пресет присоединен к продукту типа subscription
     *  (такаие пресеты нельзя удалять)
     *
     * @param int $id
     *
     * @return boolean
     */
    public function presetSubscriptionConnected($id)
    {
        $this->CI->load->model('products_model');
        $params = [
            ['product_type', '=', 'subscription'],
            ['preset_id', '=', $id],
        ];
        $limit = ["limit" => 1,"offset" => 0];
        $products = $this->CI->products_model->getByParams($params, [], $limit);

        return !empty($products);
    }

    /** Список SHOWABLE presets (тех, для которых хотя бы для 1 item не пришел срок "скисания" - products.items_days_available)
     * preset showable, если в нем есть showable items
     *
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return array
     */
    public function getShowablePresetsForClient($client_id)
    {
        $this->CI->load->model('presets_items_model');
        $data = $this->presets_items_model->getShowableEntitiesForClient($client_id);

        $rows1 = [];
        if (!empty($data))
        {
            $IDS = $tmp = [];
            foreach ($data as $elem)
            {
                $id = intval($elem->preset_id);
                $tmp[$id] =$id;
            }
            foreach ($tmp as $rel_id) $IDS[]=$rel_id;

            $rows1 = $this->getByIds($IDS);
        }


        // извлекаем все бесплатные продукты с preset_id -----------------------
        $this->CI->load->model('clients_presets_model');
        $rows2 = $this->CI->clients_presets_model->getAvailablePresetsByClientId($client_id);

        foreach($rows2 as &$row)
        {
            unset($row->rel_id);
            unset($row->starting_dt);
            unset($row->expiration_dt);
            unset($row->origin);
            unset($row->origin_id);
        }

        // убираем дубликаты -------------------------
        $dataAll = $data = array_merge($rows2, $rows1);
        $data = [];
        foreach ($dataAll as $preset)
        {
            $data[$preset->id] = $preset;
        }
        // убираем дубликаты -------------------------

        $resData = [];
        foreach ($data as $pres) $resData[] = $pres;

        return $this->prepareOutput($resData);
    }


}