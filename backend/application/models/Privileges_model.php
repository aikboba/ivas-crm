<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privileges_model extends MY_Model
{
    public $table = 'privileges';
    public $entity = 'privilege';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'name',
        'description',
        'discount_type',
        'discount',
        'status',
        '(SELECT COUNT(cli_priv.id) FROM cli_priv WHERE `cli_priv`.priv_id = `privileges`.id AND (cli_priv.valid_until>=NOW() OR cli_priv.valid_until IS NULL) ) as clients_count',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('privileges.status = "active"');
        }
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Получает цену с учетом скидки, предоставляемой льготой
     * @param int|object $priv - привилегия или priv_id
     * @param float $price - цена без учета скидки
     *
     * @return float
     */
    public function getPriceWithDiscount($priv, $price)
    {
        if (!is_object($priv)) $priv = $this->getById($priv);

        switch ($priv->discount_type)
        {
            case 'percent':
                $new_price =  floatVal($price) - floatVal($price) * floatVal($priv->discount)/100;
                break;
            case 'fixed_amount':
                $new_price = floatVal($price) - floatVal($priv->discount);
                break;
            default: $new_price =  floatVal($price) - floatVal($price) * floatVal($priv->discount)/100;
        }

        if ($new_price<0) $new_price = 0;

        return $new_price;
    }

}