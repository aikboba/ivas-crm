<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Products_gifts_model extends MY_Model
{
    public $table = 'products_gifts';
    public $entity = null;
    public $primary = 'id';
    public $posfield = null;
    public $delimfield = null;
    public $error;
    public $order_by_field = 'products_gifts.id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'products_gifts.id as id',
        'products_gifts.id as rel_id', //сделано для совместимости с другими методами модели
        'products_gifts.gift_id',
        'products_gifts.product_id',
        'products.name as product_name',
        'products.status as product_status',
        'gifts.name as gift_name',
        'gifts.start_dt',
        'gifts.end_dt',
        'gifts.duration',
        'gifts.status as gift_status',
        'products_gifts.created',
        'products_gifts.updated',
        'UNIX_TIMESTAMP(products_gifts.created) as created_unix',
        'UNIX_TIMESTAMP(products_gifts.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('products', 'products_gifts.product_id = products.id', 'left');
        $this->db->join('gifts', 'products_gifts.gift_id = gifts.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('products.status = "active"');
            $this->db->where('gifts.status = "active"');
            $this->db->where('(gifts.start_dt<=NOW() OR gifts.start_dt IS NULL)');
            $this->db->where('(gifts.end_dt>NOW() OR gifts.end_dt IS NULL)');
        }
    }


    /** Производит выборку gifts для определенного product
     *
     * @param int $id - product_id, для которого производится выборка gifts
     * @param boolean $result_by_gift_id - если true, то результат преобразуется в массив, где ключами будет gift_id (из gifts), а не id из products_gifts
     *
     * @return array
     */
    public function getGiftsByProductId($id, $result_by_gift_id = false)
    {
        $params = [
            ['products_gifts.product_id', '=', intval($id)],
        ];

        $fields = [
            'products_gifts.gift_id as id',
            'products_gifts.id as rel_id',
            'gifts.name',
            'gifts.start_dt',
            'gifts.end_dt',
            'gifts.duration',
            'gifts.status',
            'gifts.created',
            'gifts.updated',
            'UNIX_TIMESTAMP(gifts.created) as created_unix',
            'UNIX_TIMESTAMP(gifts.updated) as updated_unix'
        ];

        $res = $this->getByParams($params, [], [], $fields);
        $data = $res;
        if ($result_by_gift_id)
        {
            $data = [];
            foreach ($res as $gift) {
                $data[$gift->id] = $gift;
            }
        }

        return $data;
    }

    /** Производит выборку products, в которых состоит gift
     *
     * @param int $id - gift_ID, для которого производитс выборка подарков
     *
     * @return array
     */
    public function getProductsByGiftId($id)
    {
        $params = [
            ['products_gifts.gift_id', '=', intval($id)],
        ];

        $fields = [
            'products_gifts.product_id as id',
            'products_gifts.id as rel_id',
            'products.name',
            'products.status',
            'products.created',
            'products.updated',
            'UNIX_TIMESTAMP(products.created) as created_unix',
            'UNIX_TIMESTAMP(products.updated) as updated_unix'
        ];

        $order = ["products.name" => "ASC"];

        $res = $this->getByParams($params, $order, [], $fields);

        return $res;
    }

    /** проверка, связаны ли (состоит ли) product и gift
     *
     * @param int $product_id - product_id, для которого производится проверка вхождения
     * @param int $gift_id
     *
     * @return boolean
     */
    public function isGiftInProduct($gift_id, $product_id)
    {
        $params = [
            ['product_id', '=', intval($product_id)],
            ['gift_id', '=', intval($gift_id)],
        ];

        $fields = [
            'products_gifts.id as id',
        ];
        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit, $fields);

        return !empty($res);
    }

    /** Берет запись по product_id и gift_id
     *
     * @param int $product_id
     * @param int $gift_id
     *
     * @return array
     */
    public function getByProductIdGiftId($product_id, $gift_id)
    {
        $params = [
            ['product_id', '=', intval($product_id)],
            ['gift_id', '=', intval($gift_id)],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
            else return null;
    }

    /** Добавляет gift в product. Если его там нет.
     *
     * @param int $product_id - product, в который добавляетя gift
     * @param int $gift_id - gift, который добавляется в product
     *
     * @return int
     */
    public function addGiftToProduct($gift_id, $product_id)
    {
        $record = $this->getByProductIdGiftId($product_id, $gift_id);

        // product НЕ в gift - добавляем
        if (empty($record))
        {
            $uData = [
                'product_id' => intval($product_id),
                'gift_id' => intval($gift_id),
            ];
            $res_id = $this->insert($uData);
        }
        else
        {
            // product уже в gift
            $res_id = $record->id;
        }

        return $res_id;
    }

    /** Убирает  gift из product.
     *
     * @param int $gift_id - gift, который убирается из product
     * @param int $product_id - product, из которого убирается gift
     *
     * @return int
     */
    public function removeGiftFromProduct($gift_id, $product_id)
    {
        /** @var StdClass $record */
        $record = $this->getByProductIdGiftId($product_id, $gift_id);

        // product НЕ в gift - выходим
        if (empty($record))
        {
            return 0;
        }
        else
        {
            // gift в product
            $id = $record->id;
        }

        return $this->delete($id);
    }


}
