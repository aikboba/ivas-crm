<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Products_model
 *
 * @property Presets_items_model $presets_items_model
 */
class Products_model extends MY_Model
{
    public $table = 'products';
    public $entity = 'product';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'products.id',
        'products.name',
        'product_type',
        'products.description',
        'is_free',
        'products.days_available_after_stop',
        'products.days_item_available',

        'products.stream_status',
        'products.stream_origin',
        'products.stream_url_data',
        'products.stream_host_relay_url',
        //'products.stream_guest_relay_url',
        'products.stream_started',
        'products.stream_ended',
        'products.stream_item_id',
        'products.stream_clients_informed',
        'products.stream_health',
        'products.stream_status_last_changed_dt',
        'products.stream_health_last_changed_dt',

        'products.status',
        'paysystem_id',
        'client_id',

        'paysystems.name as paysystem_name',
        'paysystems.public_key as paysystem_public_key',
        'paysystems.api_secret_key as paysystem_api_secret_key',


        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.user_insta_nick as client_user_insta_nick',

        'instagram_account_id',
        'instagram_accounts.login as instagram_account_nick',

        'group_id',
        'groups.name as group_name',

        'products.created',
        'products.updated',

        'preset_id',
        'presets.name as preset_name',
        'presets.status as preset_status',

        'UNIX_TIMESTAMP(products.created) as created_unix',
        'UNIX_TIMESTAMP(products.updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('paysystems', 'products.paysystem_id = paysystems.id', 'inner');
        $this->db->join('clients', 'products.client_id = clients.id', 'left');
        $this->db->join('groups', 'products.group_id = groups.id', 'left');
        $this->db->join('presets', 'products.preset_id = presets.id', 'left');
        $this->db->join('instagram_accounts', 'products.instagram_account_id = instagram_accounts.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('products.status IN ("active","stopped")');
            $this->db->where('( (client_id IS NULL) OR (clients.status = "active") )');
            $this->db->where('( (group_id IS NULL) OR (groups.status = "active") )');
            $this->db->where('( (preset_id IS NULL) OR (presets.status = "active") )');
            $this->db->where('( (instagram_account_id IS NULL) OR (instagram_accounts.status = "active") )');

            // персональный продукт виден только ТОМУ пользователю
            if ($this->auth->isLogged) $this->db->where('( (client_id IS NULL) OR (client_id = '.$this->auth->loggedUser->id.') )');
        }
    }

    /** Переопределенный метод. Подготавливает вывод
     *
     * @params (array of StdClass) $data - входной массив
     *
     * @return array of StdClass

     */
    protected function prepareOutput($data)
    {
        // превращаем JSON stream_url_data в объект
        if (!empty($data))
        {
            foreach ($data as $key=>&$elem)
            {
                $stream_url_data = (object)[];

                if (!empty($elem->stream_url_data))
                {
                    $stream_url_data = json_decode($elem->stream_url_data);
                }

                $elem->stream_url_data = $stream_url_data;
            }
        }

        return $data;
    }

    /** Производит выборку продуктов по типам
     *
     * @param string $type - тип продукта
     *
     * @return array
     */
    public function getListByType($type)
    {
        $params = [
            ['product_type', '=', $type],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку персональных продуктов по клиенту
     *
     * @param int $client_id - ID клиента
     *
     * @return array
     */
    public function getListByClientId($client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку продуктов по instagram_account_id
     *
     * @param int $instagram_account_id - ID клиента
     *
     * @return array
     */
    public function getListByInstaAccId($instagram_account_id)
    {
        $params = [
            ['instagram_account_id', '=', $instagram_account_id],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку продуктов блоггера (которого в настоящий момент (2020-01-27) еще не существует )
     *  пока возвращает просто активные продукты типа "эфиры"
     *
     * @return array
     */
    public function getByBlogger($blogger = null)
    {
        $params = [
            ['status', '=', 'active'],
            ['product_type', '=', 'subscription'],
        ];

        $res = $this->getByParams($params, [], [], ['products.id',
                                                    'products.name',
                                                    'products.description',
                                                    'products.stream_status',
                                                    'products.stream_status_last_changed_dt',
                                                    'products.stream_health',
                                                    'products.stream_health_last_changed_dt',
                                                    'products.stream_origin',
                                                    'products.stream_url_data',
                                                    'products.stream_host_relay_url',
                                                    //'products.stream_guest_relay_url',
                                                    'products.stream_started',
                                                    'products.stream_ended',
                                                    'products.status'
                                                    ]);

        return $res;
    }

    /** Производит выборку бесплатных продуктов
     *
     * @return array
     */
    public function getFree()
    {
        $params = [
            ['is_free', '=', 1],
        ];
        $res = $this->getByParams($params);

        return $res;
    }

    /** Выборка по name
     *
     * @param string $name
     *
     * @return array of objects | null
     */
    public function getByName($name)
    {
        $params = [
            ['name', '=', $name]
        ];
        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params,[],$limit);

        $product = [];
        if (!empty($res))
        {
            $product = $res[0];
        }

        return $product;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

    /** Производит выборку доступных ДЛЯ ПОКУПКИ products для определенного client.
     * Доступны для покупки:
     * - продукт НЕ free
     * - продукт не персонализирован или персонализирован на ЭТОГО клиента
     *
     * @param int $id - client_id, для которого производится выборка products
     *
     * @return array
     */
    public function getPurchasableProductsByClientId($id)
    {

        $params = [
            ['products.status', '!=', 'stopped'],
            ['products.is_free', '!=', 1],
            ['( (products.client_id IS NULL) OR (products.client_id='.$id.') )']
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Устанавливает для продукта $id стрим в состояние, заданное полями массива stream_data
     *
     * @param int $id - id продукта
     * @param array $stream_data - данные о состоянии стрима
     *
     * @return int | boolean
     */
    public function setStream($id, $stream_data)
    {
        $product_id = $id;
        /** @var Object $product */
        $product = $this->getById($product_id);
        if (empty($product)) return false;

        if ($product->product_type == 'subscription')
        {
            if ($stream_data['stream_status'] == 'active')
            {
                if ($product->stream_clients_informed == 0)
                {
                    // переход в статус active возможен и после паузы. в этом случае параметры не меняются
                    // запуск стрима
                    $stream_data['stream_started'] = (new Datetimeex())->format('Y-m-d H:i:s');
                    $stream_data['stream_ended'] = 'null';
                }
            }
            elseif ($stream_data['stream_status'] == 'paused')
            {
                // пауза стрима
                if (!empty($stream_data['stream_started'])) unset($stream_data['stream_started']);
                if (!empty($stream_data['stream_ended'])) unset($stream_data['stream_ended']);
                if (!empty($stream_data['stream_item_id'])) unset($stream_data['stream_item_id']);
            }
            else
            {
                // останов стрима
                if (!empty($stream_data['stream_started'])) unset($stream_data['stream_started']);
                $stream_data['stream_ended'] = (new Datetimeex())->format('Y-m-d H:i:s');
                //$stream_data['stream_item_id']   = 'null'; // пока отключил. иначе при прекращенном стриме не будет работать чат
            }

            /* если это действительно начало стрима, а не вывод из паузы, то для продукта:
            ---- создать item (если его нет. Определить по stream_data['stream_item_guid']) с именем типа "{product_name}. Эфир от DD.MM.YYYY HH:MM"
            ---- добавить этот item в preset, привязанный к указанному продукту
            */
            if ( ($stream_data['stream_status'] == 'active') && ($product->stream_clients_informed == 0) )
            {
                // переход в статус active возможен и после паузы. в этом случае параметры не меняются
                $this->CI->load->model('items_model');
                // проверяем, есть ли файл с переданный stream_item_guid
                $item_exists = false;
                if (!empty($stream_data['stream_item_guid']))
                {
                    $item = $this->CI->items_model->getByGuid($stream_data['stream_item_guid']);
                    $item_exists = !empty($item);
                }
                else
                {
                    $stream_data['stream_item_guid'] = 'NULL';
                }

                if ($item_exists)
                {
                    // item с переденным guid существует. Это продолжение старого эфира. Ничего не создаем
                    $stream_data['stream_item_id'] = $item->id;
                }
                else
                {
                    // item с переденным guid НЕ существует. Это новый эфир. Item создаем
                    $name = $system_name = $product->name . ". Эфир от " . (new Datetimeex())->format('Y-m-d H:i:s');
                    /*  В связи с этим есть один известный баг - если к одному instagram_account привязаны несколько продуктов, то item получит неверное имя.
                        Проассоциированное с последним продуктом.
                        Данное имя исправится на более правильное после вызова itemChange
                    */
                    $iData = [
                        'name' => $name,
                        'system_name' => $system_name,
                        //'resource_string_id' => $stream_data['stream_url'], //по новой политике в момент начала стрима в item не записывается resource_string_id. Делается озже по item_guid
                        'guid' => (!empty($stream_data['stream_item_guid'])) ? $stream_data['stream_item_guid'] : 'NULL',
                        'status' => 'new', // item всегда создается в статусе new для сохранения возможности последующего редактирования. При этом он не виден для клиента
                    ];
                    $new_item_id = $this->CI->items_model->insert/*OrUpdate*/($iData);
                    if (empty($new_item_id)) return false;

                    $stream_data['stream_item_id'] = $new_item_id;

                    $this->CI->load->model('presets_items_model');
                    $res_id = $this->CI->presets_items_model->addItemToPreset($new_item_id, $product->preset_id, 100000);

                    if ($res_id == false) return false;
                }
            }
            // изменяем данные в продукте ----------------------
            $upd_res = $this->update($product_id, $stream_data);
            // проблема с изменением - выходим
            //if ($upd_res == false) return $upd_res;
            //    else return($upd_res);

            return $upd_res;
        }

        // если продукт не типа "подписка", то вернет false
        return false;
    }

    /** останавливает продукт (речь идет о прекращении трансляций эфирного продукта вообще. Навсегда)
     * при остановке эфирного продукта не только меняется его статус, но начинается отсчет срока
     * доступности пользователям материалов связанного пресета (для сlients_presets выставляется expiration_dt
     * как NOW()+products.days_available_after_stop )
     *
     * @param   int | object $product  - ID продукта либо обхъект, содержащий подукт
     *
     * @return  int | boolean
     */
    public function stop($product)
    {

        if (!is_object($product)) $product = $this->getById($product); // если передан ID

        //if ($product->product_type == 'subscription')
        {
            if (!empty($product->preset_id))
            {
                $this->CI->load->model('clients_presets_model');
                $params = [
                    ['clients_presets.expiration_dt IS NULL'],
                    ['preset_id', '=', $product->preset_id],
                ];
                $uData=[
                    'expiration_dt' => "DATE_ADD(NOW(), INTERVAL ".intval($product->days_available_after_stop)." DAY)",
                ];

                $this->CI->clients_presets_model->updateByParams($params, $uData, false);
            }
        }

        $uData = [
            'status' => 'stopped',
        ];
        $res = $this->update($product->id, $uData);

        return $res;
    }

    /** Список SHOWABLE products (тех, для которых хотя бы для 1 item не пришел срок "скисания" - products.items_days_available)
     * product showable, если в его пресете есть showable items
     *
     * @param int $client_id - клиент, для которого проверятся доступность
     *
     * @return array
     */
    public function getShowableProductsForClient($client_id)
    {
        $this->CI->load->model('presets_items_model');
        $data = $this->presets_items_model->getShowableEntitiesForClient($client_id);

        $rows1 = [];
        if (!empty($data))
        {
            $IDS = $tmp = [];
            foreach ($data as $elem)
            {
                $id = intval($elem->product_id);
                $tmp[$id] =$id;
            }
            foreach ($tmp as $rel_id) $IDS[]=$rel_id;

            $rows1 = $this->getByIds($IDS);
        }

        $this->CI->load->model('clients_products_model');
        $rows2 = $this->CI->clients_products_model->getAvailableProductsByClientId($client_id);

        foreach($rows2 as &$row)
        {
            unset($row->rel_id);
            unset($row->start_dt);
            unset($row->end_dt);
            unset($row->order_id);
        }

        // убираем дубликаты -------------------------
        $dataAll = $data = array_merge($rows2, $rows1);
        $data = [];
        foreach ($dataAll as $product)
        {
            $data[$product->id] = $product;
        }
        // убираем дубликаты -------------------------

        $resData = [];
        foreach ($data as $prod) $resData[] = $prod;

        return $this->prepareOutput($resData);
    }

    /** Изменяет "приписку" персонального продукта (через update) с клиента donor_id на клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $result = true;

        $params = [
            ['client_id', '=', $donor_id],
        ];

        $uData = [
            'client_id' => $acceptor_id,
        ];
        $res = $this->updateByParams($params, $uData);
        $result = ($res !== false);

        return $result;
    }


}