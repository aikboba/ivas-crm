<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocodes_model extends MY_Model
{
    public $table = 'promocodes';
    public $entity = 'promocode';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'promocodes.id',
        'promocodes.name',
        'promocodes.promocode',
        'promocodes.start_dt',
        'promocodes.end_dt',
        'promocodes.discount_type',
        'promocodes.discount_value',
        'promocodes.status',

        'promocodes.product_id',
        'products.name as product_name',

        'promocodes.option_id',
        'options.name as option_name',

        'promocodes.client_id',
        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',

        'one_time_only',
        'promocodes.created',
        'promocodes.updated',
        'UNIX_TIMESTAMP(promocodes.created) as created_unix',
        'UNIX_TIMESTAMP(promocodes.updated) as updated_unix',
        '(SELECT COUNT(orders.id) FROM `orders` WHERE orders.status NOT IN ("cancelled", "deleted") AND orders.promocode_id=promocodes.id) as usage_count',
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'promocodes.client_id = clients.id', 'left');
        $this->db->join('products', 'promocodes.product_id = products.id', 'left');
        $this->db->join('options', 'promocodes.option_id = options.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('promocodes.status="active"');
            $this->db->where('( (promocodes.product_id IS NULL) OR (products.status = "active") )');
            $this->db->where('( (promocodes.client_id IS NULL) OR (clients.status = "active") )');
            $this->db->where('( (promocodes.option_id IS NULL) OR (options.status = "active") )');
            $this->db->where('( (promocodes.start_dt <=NOW()) OR (promocodes.start_dt IS NULL) )');
            $this->db->where('( (promocodes.end_dt >NOW()) OR (promocodes.end_dt IS NULL) )');
        }
    }

    /** Производит выборку промокодов по типам скидки
     *
     * @param string $discount_type - тип промокода
     *
     * @return array
     */
    public function getByDiscountType($discount_type)
    {
        $params = [
            ['discount_type', '=', $discount_type],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку персональных промокодов по клиенту
     *
     * @param int $client_id - ID клиента
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку промокодов по продукту
     *
     * @param int $product_id - ID продукта
     * @param boolean $whole_product_only - если true, то выбрать только промокоды , относящиеся КО ВСЕМУ продукту (без учета опций)
     *
     * @return array
     */
    public function getByProductId($product_id, $whole_product_only = false)
    {
        $params = [
            ['product_id', '=', intval($product_id)],
        ];

        if ($whole_product_only)
        {
            $params = [
                ['option_id', ' IS ', 'NULL'],
            ];
        }

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку промокодов по опции
     *
     * @param int $option_id - ID опции
     *
     * @return array
     */
    public function getByOptionId($option_id)
    {
        $params = [
            ['option_id', '=', intval($option_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Выборка по promocode
     *
     * @param string $promo
     *
     * @return object
     */
    public function getByPromocode($promo)
    {
        $params = [
            ['promocode', "=" , $promo]
        ];
        $res = $this->getByParams($params);
        if (!empty($res)) return $res[0];

        return null;
    }

    /** Получает цену с учетом скидки, предоставляемой промокодом
     *
     * @param object|int|string $promocode - object промокод|promocode_id|строка-промокод
     * @param float $price - цена без учета скидки
     *
     * @return float
     */
    public function getPriceWithDiscount($promocode, $price)
    {
        // применимость промокода с точки зрения дат, client_id,product_id, option_id проверяется ранее в utils->isPromocodeApplicable()
        if (is_int($promocode)) $promocode = $this->getById($promocode);
        elseif (is_string($promocode)) $promocode = $this->getByPromocode($promocode);

        switch ($promocode->discount_type)
        {
            case 'percent':
                $new_price =  floatVal($price) - floatVal($price) * floatVal($promocode->discount_value)/100;
                break;
            case 'fixed_amount':
                $new_price = floatVal($price) - floatVal($promocode->discount_value);
                break;
            case 'fixed_price':
                $new_price = floatVal($promocode->discount_value);
                break;
            default: $new_price =  floatVal($price) - floatVal($price) * floatVal($promocode->discount_value)/100;
        }

        if ($new_price<0) $new_price = 0;

        return $new_price;
    }

    /** Определяет, может ли быть применен указанный промокод указанным клиентом к указанному продукту/опции
     * (промокод применим, если:
     *  - текущая дата лежит между start_dt и end_dt (при запросе с фронта реализуется моделью)
     *  - он связан с клиентом + с опцией или продуктом
     *  - он связан с продуктом
     *  - он связан с опцией
     *  - если promocodу one_time_only, то он не должен фигурировать ни в одном заказе (если передан client, то ни в одном заказе КЛИЕНТА)
     * )
     *
     * @param string|int|object $promocode - сам промокод (строка), либо $promocode_id (если int), либо объект, содержащий промокод
     * @param int|object $option - option_id либо объект, содержащий опцию
     * @param int|object $client - client_id либо объект, содержащий клиента
     * @param int|null $order_id_to_ignore - усли задано это знавчение,то при проверке "не использован ли ранее этот одноразовый промокод в заказах"
     *          этот заказ игнорируется (нужно для update order)
     *
     * @return int 0 - ошибка или промокод неприменим
     *             1 - промокод применим
     *            -1 - промокод применим, но применение не имеет смысла, т.к. скидка от промокода дает меньший эффект, чем льготы
     */
    public function isApplicable($promocode, $option, $client=null, $order_id_to_ignore = null )
    {
        $this->CI->load->model('orders_model');
        $this->CI->load->model('products_model');
        $this->CI->load->model('options_model');

        if (!empty($client)) $this->CI->load->model('clients_model');;


        if (is_string($promocode))
        {
            $promocode = $this->getByPromocode($promocode);
        }
        elseif(is_int($promocode))
        {
            $promocode = $this->getById($promocode);
        }
        if (empty($promocode)) return 0;

        if(is_int($client))
        {
            $client= $this->CI->clients_model->getById($client);
        }

//            $product= $this->CI->products_model->getById($product);

        if(is_int($option))
        {
            $option= $this->CI->options_model->getById($option);
        }
        if (empty($option)) return 0;
        // -------------------------------------------------------------

        // промокод one_time_only - проверяем, не использовался ли он в заказах ЭТИ КЛИЕНТОМ
        if ($promocode->one_time_only)
        {
            $params = [];
            $params[] = ['orders.client_id', '=', $client->id];
            $params[] = ['orders.promocode_id', '=', $promocode->id];
            $params[] = ["orders.status IN ('new','paid','partially_paid')"];
            if (!empty($order_id_to_ignore)) $params[] = ["order_id", "!=", $order_id_to_ignore];

            $res = $this->CI->orders_model->getByParams($params);

            if (!empty($res)) return 0;

        }

        //client задан, client_id промокода не совпадает с переданным client->id
        if ( (!empty($client)) && (!empty($promocode->client_id)) && ($promocode->client_id!=$client->id) ) return 0;

        // у промокода задан продукт
        if (!empty($promocode->product_id))
        {
            // опция у промокода задана, но не та, что в переданной option
            if ( (!empty($promocode->option_id )) && ($promocode->option_id != $option->id) ) return 0;

            // если у промокода пуст option_id - промокод применим ко всем опциям продукта. Проверяем, является ли ДАННАЯ опция опцией продукта
            if ($promocode->product_id != $option->product_id) return 0;
        }

        // проверяем имеет ли смысл применения промокода или льготы его "перекрывают"
        if (!empty($client))
        {
            $price_no_promo = $this->CI->options_model->getDiscountPrice($option, $client);
            $price_promo = $this->CI->options_model->getDiscountPrice($option, $client, $promocode);

            if ( $price_no_promo <= $price_promo ) return -1;
        }

        return 1;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }


    /** Изменяет "приписку" персонального промокода (через update) с клиента donor_id на клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $result = true;

        $params = [
            ['client_id', '=', $donor_id],
        ];

        $uData = [
            'client_id' => $acceptor_id,
        ];
        $res = $this->updateByParams($params, $uData);
        $result = ($res !== false);

        return $result;
    }

}