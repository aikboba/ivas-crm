<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Subs_model
 *
 */

class Subs_model extends MY_Model
{
    public $table = 'subs';
    public $entity = '';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'created';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'subs.id',
        'subs.subs_id',
        'subs.client_id',
        'subs.description',
        'subs.email',
        'subs.amount',
        'subs.start_dt',
        'subs.interval_value',
        'subs.period',
        'subs.successful_transactions_number',
        'subs.failed_transactions_number',
        'subs.last_transaction_dt',
        'subs.next_transaction_dt',
        'subs.raw_data',
        'subs.status',

        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.user_insta_nick as client_user_insta_nick',

        'ANY_VALUE(options.id) as option_id',
        'ANY_VALUE(options.name) as option_name',

        'ANY_VALUE(products.id) as product_id',
        'ANY_VALUE(products.name) as product_name',

        'subs.created',
        'subs.updated',
        'UNIX_TIMESTAMP(subs.created) as created_unix',
        'UNIX_TIMESTAMP(subs.updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'subs.client_id = clients.id', 'inner');
        $this->db->join('orders', 'orders.subs_id = subs.id', 'inner');
        $this->db->join('options', 'options.id = orders.option_id', 'inner');
        $this->db->join('products', 'products.id = options.product_id', 'inner');
        $this->db->group_by('subs.id');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            // принято решение вызбирать ВСЕ подписки
            //$this->db->where($this->table.'.status="active"');

        }
    }

    /** Переводит данные о подписке из формата subscriptionData в сохраняемый нами формат
     *
     * @param array $data - данные подписки, переданные платежной системой
     *
     * @return array
     */
    public function transform($data)
    {
        if (!is_array($data)) return [];

        $data['Status'] = strtolower($data['Status']);
        if (!in_array($data['Status'], ['active','deleted','pastdue','cancelled','rejected','expired'])) $data['Status'] = 'rejected';

        $timeZoneUTC = new DateTimeZone('UTC');
        $serverTimeZone = $this->config->item('time_reference');
        $iData = [
            'subs_id' => $data['Id'],
            'client_id' => intval($data['AccountId']),
            'description' => $data['Description'],
            'email' => !empty($data['Email']) ? $data['Email'] : 'NULL',
            'amount' => floatval($data['Amount']),
            'start_dt' => !empty($data['StartDateIso']) ? (new Datetimeex($data['StartDateIso'], $timeZoneUTC))->changeTimeZone($serverTimeZone) : 'NULL',
            'interval_value' => $data['Interval'],
            'period' => $data['Period'],
            'successful_transactions_number' => $data['SuccessfulTransactionsNumber'],
            'failed_transactions_number' => $data['FailedTransactionsNumber'],
            'last_transaction_dt' => (!empty($data['LastTransactionDateIso'])) ? (new Datetimeex($data['LastTransactionDateIso'], $timeZoneUTC))->changeTimeZone($serverTimeZone) : 'NULL',
            'next_transaction_dt' => (!empty($data['NextTransactionDateIso'])) ? (new Datetimeex($data['NextTransactionDateIso'], $timeZoneUTC))->changeTimeZone($serverTimeZone) : 'NULL',
            'status'   => $data['Status'],
            'raw_data' => json_encode($data),
        ];

        return $iData;
    }

    /** Создает подписку по subsactionData
     *
     * @param array $data - данные транзакции, переданные платежной системой
     *
     * @return int
     */
    public function createFromSubsData($data)
    {
        if (!is_array($data)) return false;

        $iData = $this->transform($data);


        $res = $this->insertOrUpdate($iData);

        return $res;
    }

    /** Создает подписку по subsactionData
     *
     * @param array $data - данные подписки, переданные платежной системой
     *
     * @return int
     */
    public function createFromData($data)
    {
        if (!is_array($data)) return false;

        $res = $this->insertOrUpdate($data);

        return $res;
    }

    /** Производит выборку подписок по клиенту
     *
     * @param int $client_id - ID клиента
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку подписок по клиенту и продукту
     *
     * @param int $client_id - ID клиента
     * @param int $product_id - ID клиента
     *
     * @return array
     */
    public function getByClientIdProductId($client_id, $product_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['products.id', '=', intval($product_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку подписок по subs_id
     *
     * @param string $subs_id - ID подписки
     *
     * @return array
     */
    public function getBySubsId($subs_id)
    {
        $params = [
            ['subs_id', '=', $subs_id],
        ];

        $res = $this->getByParams($params);

        return (!empty($res[0])) ? $res[0] : null;
    }


    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function deleteCompletely($id)
    {
        return parent::delete($id);
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }


    /** Изменяет "приписку" подписки (через update) с клиента donor_id на клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $this->CI->load->library('cloudpayments');

        $this->CI->load->model('orders_model');
        $this->CI->load->model('options_model');
        $this->CI->load->model('products_model');
        $this->CI->load->model('trans_model');
        $this->CI->load->model('clients_model');


        $subs = $this->getByClientId($donor_id);

        foreach($subs as $sub)
        {
            $order = $this->CI->orders_model->getLastByOrdersSubsId($sub->id);
            if (!$order) {
                //ToDo: не существовует order, а подписка есть
                continue;
            }
            // для создания новой подписки необходим token покупки
            /*$trans = $this->CI->trans_model->getLastPayTransByOrderIdClientId($order->id, $donor_id);
            if (!$trans) {
                //ToDo: не существовует trans, а order есть(тестовая подписка?)
                continue;
            }*/
            $option = $this->CI->options_model->getById($order->option_id);
            if (!$option) {
                //ToDo: не существовует option, а order есть
                continue;
            }
            $product = $this->CI->products_model->getById($option->product_id);
            if (!$product) {
                //ToDo: не существовует product, а option есть
                continue;
            }

            /*$newClient = $this->CI->clients_model->getById($acceptor_id);
            if (!$newClient) {
                //ToDo: не существовует product, а option есть
                continue;
            }*/

            $this->CI->cloudpayments->publicKey = $product->paysystem_public_key;
            $this->CI->cloudpayments->privateKey = $product->paysystem_api_secret_key;

            $subsCloud = $this->CI->cloudpayments->getSubscription($sub->subs_id);
            if ($subsCloud) {
                $cancelResult = $this->CI->cloudpayments->cancelSubscription($subsCloud['Id']);
                if ($cancelResult) {
                    $this->deleteCompletely($sub->id);
                    /*$newSubCloud = $subsCloud;
                    $newSubCloud['AccountId'] = $acceptor_id;
                    $newSubCloud['Receipt']['Email'] = $newClient->email;

                    $transData = json_decode($trans->data, true);
                    $token = $transData['Token'];
                    $startDate = $newSubCloud['StartDateIso'];
                    //if ($subsCloud['NextTransactionDateIso']) {}

                    //var_dump($this->CI->cloudpayments->getTokensList());
                    //var_dump($this->CI->cloudpayments->test());

                    //exit;
                    $createdSubsCloud = $this->CI->cloudpayments->createSubscription(
                        $token, (string)$newSubCloud['AccountId'], $newSubCloud['Description'],
                        $newClient->email, $newSubCloud['Amount'], $newSubCloud['Currency'],
                        $newSubCloud['RequireConfirmation'], $newSubCloud['StartDateIso'], $newSubCloud['Interval'],
                        $newSubCloud['Period']
                    ); // ['CustomerReceipt' => json_encode($newSubCloud['Receipt'])]*/
                    /*if ($subsDeleted) {
                        $newSubsId = $this->createFromSubsData($createdSubsCloud);
                        $params = [
                            ['subs_id', '=', $sub->id],
                        ];

                        $uData = [
                            'subs_id' => $newSubsId,
                        ];
                        $res = $this->CI->orders_model->updateByParams($params, $uData);
                        return $res;
                    } else {
                        // ToDo: ошибка при создании подписки - запись в лог
                    }*/
                } else {
                    // ToDo: ошибка при отмене на cloudpayments - запсиь в ЛОГ
                }
                //$uData['AccountId'] = $acceptor_id;
                //$newSubs = $this->cloudpayments->updateSubscription($sub->subs_id, $uData);
            } else {
                // подписка на стороне cloudpayments отсутствует
            }
        }
        return true;

/*
// 2DO: для изменения удаленных подписок-----------------
    $this->>CI->load->model('clients_model');
    //$client = $this->CI->clients_model->getById($acceptor_id);// если понбится клиент
    $subs = $this->getByClientId($donor_id);
    foreach ($subs as $sub)
    {
        //здесь модифицируем удаленную подписку, заменяя AccountId
    }
//-------------------------------------------------------
*/


        /*$params = [
            ['client_id', '=', $donor_id],
        ];

        $uData = [
            'client_id' => $acceptor_id,
        ];
        $res = $this->updateByParams($params, $uData);
        $result = ($res !== false);

        return $result;*/
    }


}