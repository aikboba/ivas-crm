<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Tasks_completed_model
 *
 */

class Tasks_completed_model extends MY_Model
{
    public $table = 'tasks_completed';
    public $entity = '';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'priority';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'id',
        'system',
        'publisher',
        'consumer',
        'data_in',
        'data_out',
        'priority',
        'start_dt',
        'status',
        'status_msg',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
    }


}