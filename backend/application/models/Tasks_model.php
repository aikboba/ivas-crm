<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Tasks_model
 *
 */

class Tasks_model extends MY_Model
{
    public $system = 'backend'; // только задачи этого типа выбираются из очереди

    public $table = 'tasks';
    public $entity = '';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'priority';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'id',
        'system',
        'publisher',
        'consumer',
        'data_in',
        'data_out',
        'priority',
        'start_dt',
        'status',
        'status_msg',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);

        $this->CI->load->model('tasks_completed_model');
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
/*
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('tasks.status IN ("active","stopped")');
        }
*/
    }

    /** Создает задачу по массиву входных данных. Не использовать напрямую. Создавать задачи через taskmanager->createTask
     *
     * @param array $task_data - данные задачи
     *
     * @return int - id созданной задачи`
     */
    public function create($task_data)
    {
/*
        if (!is_array($task_data)) return false;
        if (empty($task_data['publisher'])) return false;
        if (empty($task_data['consumer'])) return false;

        if ( (is_array($task_data['data'])) || is_object($task_data['data']) )  $task_data['data'] = json_encode($task_data['data']);


        if (json_last_error() != JSON_ERROR_NONE) return false;

        if (!in_array($task_data['status'], ['new', 'in_process', 'ready', 'error'])) $task_data['status'] = 'new';
*/
        $res = $this->insert($task_data);

        return $res;
    }

    /** Производит выборку задач по источнику задач
     *
     * @param string|array $publisher - источник задач publisher
     *
     * @return array
     */
    public function getByPublisher($publisher)
    {
        if (is_array($publisher))
        {
            $st = "";
            foreach ($publisher as $pub)
            {
                $st.= '\''.$pub.'\',';
            }
            $st = substr($st,0,-1);
            $params = [
                ['publisher IN ('.$st.')']];
        }
        else
        {
            $params = [
                ['publisher', '=', $publisher],
            ];
        }

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку задач по обработчику
     *
     * @param string|array $consumer - обработчик задач (consumer)
     *
     * @return array
     */
    public function getByConsumer($consumer)
    {
        if (is_array($consumer))
        {
            $st = "";
            foreach ($consumer as $con)
            {
                $st.= '\''.$con.'\',';
            }
            $st = substr($st,0,-1);
            $params = [
                ['consumer IN ('.$st.')']];
        }
        else
        {
            $params = [
                ['consumer', '=', $consumer],
            ];
        }

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку задач по статусу
     *
     * @param string|array $status - статус задачи
     *
     * @return array
     */
    public function getByStatus($status)
    {
        if (is_array($status))
        {
            $st = "";
            foreach ($status as $stat)
            {
                $st.= '\''.$stat.'\',';
            }
            $st = substr($st,0,-1);
            $params = [
                ['status IN ('.$st.')']];
        }
        else
        {
            $params = [
                ['status', '=', $status],
            ];
        }

        $res = $this->getByParams($params);

        return $res;
    }

    /** Выборка следующей задачи
     *
     * @param array $additional_params - дополнительные параметры в формате params для getByParams
     *
     * @return object | null
     */
    public function getNew( $additional_params = null )
    {
        if (!empty($additional_params))
        {
            $params = $additional_params;
        }
        else $params = [];

        $params[] = ['status', '=', 'new'];
        $params[] = ['( start_dt<=NOW() OR start_dt IS NULL)'];
        $params[] = ['system', '=', $this->system];

        $limit = ['limit'=>1, 'offset'=>0];

        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
        else return null;
    }

    /** Выборка по ID !!!из таблицы ЗАВЕРШЕННЫХ задач!!!
     *
     * @param int $id
     *
     * @return array | null
     */
    public function getCompletedById($id)
    {
        if (isset($id))
        {
            $this->CI->load->model('tasks_completed_model');

            $res = $this->CI->tasks_completed_model->getById($id);

            if (!empty($res)) return $res;
        }

        return null;
    }

    /** ПЕРЕНОСИТ задачу в tasks_completed, и выставляет status = 'completed'
     *
     * @param int|object $task
     * @param string|null $msg - сообщение (запишется в status_message)
     *
     * @return boolean|int
     */
    public function complete($task, $msg = null)
    {
        if (!is_object($task))
        {
            $task = $this->getById($task);
        }

        if (empty($task)) return false;

        $task->status = 'completed';
        $task->status_msg = $msg;

        $new_id = $this->CI->tasks_completed_model->insertOrUpdate((array)$task);

        if ($new_id !== false)
        {
            $this->delete($task->id);
        }

        return $task->id;
    }

    /** Выставляет status = 'error'
     *
     * @param int|object $task - задача или ее ID
     * @param string|null $msg - сообщение (запишется в status_message)
     *
     * @return boolean
     */
    public function error($task, $msg = null)
    {
        if (!is_object($task))
        {
            $task = $this->getById($task);
        }

        if (empty($task)) return false;

        $task->status = 'error';
        $task->status_msg = $msg;

        $new_id = $this->CI->tasks_completed_model->insertOrUpdate((array)$task);

        if ($new_id !== false)
        {
            $this->delete($task->id);
        }

        return $task->id;
    }

    /** Выставляет status = 'in_process' задаче
     *
     * @param int $task_id
     *
     * @return integer
     */
    public function in_process($task_id)
    {
        $params = [
            ['id', '=', $task_id],
            ['status', '=', 'new'],
        ];
        $cnt = $this->updateByParams($params, ['status'=>'in_process']);

        return $cnt;
    }

}