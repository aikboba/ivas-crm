<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates_model extends MY_Model
{
    public $table = 'templates';
    public $entity = 'template';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'name';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'id',
        'templates_cat_id',
        'name',
        'text',
        'status',
        'created',
        'updated',
        'UNIX_TIMESTAMP(created) as created_unix',
        'UNIX_TIMESTAMP(updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
            $this->db->where('templates.status = "active"');
        }
    }

    /** Выборка по templates_cat_id - категории темплейта.
     *
     * @param int $templates_cat_id
     *
     * @return array of objects
     */
    public function getByCategoryId($templates_cat_id)
    {
        $params = [
            ['templates_cat_id', "=" , intval($templates_cat_id)]
        ];
        $res = $this->getByParams($params);

        if (!empty($res)) return $res;

        return null;
    }


    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }

}

