<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class Tokens_model
*/
class  Tokens_model extends MY_Model
{
    public $table = 'tokens';
    public $entity = null;
    public $primary = 'id';
    public $error;
    public $order_by_field = 'tokens.id';
    public $order_by_dir = 'ASC';
    public $default_field_list = [
        'tokens.id',
        'tokens.entity',
        'tokens.entity_id',
        'tokens.device',
        'tokens.token',
        'tokens.last_used_dt',
        'tokens.updated',
        'tokens.created',
        'UNIX_TIMESTAMP(tokens.created) as created_unix',
        'UNIX_TIMESTAMP(tokens.updated) as updated_unix'
    ];

    public $field_list_full = [
        'tokens.id',
        'tokens.device',
        'tokens.token',
        'tokens.entity',
        'tokens.entity_id',

        'CASE 
WHEN entity="admin" THEN (SELECT email FROM admins WHERE id=entity_id) 
WHEN entity="client" THEN (SELECT email FROM clients WHERE id=entity_id) 
/*WHEN entity="blogger" THEN (SELECT email FROM bloggers WHERE id=entity_id)*/
END as email',

        'tokens.last_used_dt',
        'tokens.updated',
        'tokens.created',
        'UNIX_TIMESTAMP(tokens.created) as created_unix',
        'UNIX_TIMESTAMP(tokens.updated) as updated_unix'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
    }

    /** Производит выборку tokens для определенной сущности
     *
     * @param string $entity - сущность
     * @param int $entity_id - id, для которого производитс выборка
     * @param string|null $device - устройство , с которым связан токен
     *
     * @return array
     */
    public function getByEntityId($entity, $entity_id, $device=null)
    {
        $params = [
            ['tokens.entity', '=', $entity],
            ['tokens.entity_id', '=', $entity_id],
        ];
        if(!empty($device)) $params[] = ['tokens.device', '=', $device];

        $res = $this->getByParams($params);
        $data = $res;

        return $data;
    }

    /** Берет запись по device
     *
     * @param int $device - GUID устройства

     * @return array
     */
    public function getByDevice($device)
    {
        $params = [
            ['device', '=', $device],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
        else return null;
    }

    /** Берет запись по token
     *
     * @param string $token - GUID устройства

     * @return array
     */
    public function getByToken($token)
    {
        $params = [
            ['token', '=', $token],
        ];

        $limit = ['limit'=>1, 'offset'=>0];
        $res = $this->getByParams($params, [], $limit);

        if (!empty($res[0])) return $res[0];
        else return null;
    }


    /** Создает запись, используя insertOrUpdate
     *
     * @param array $data - массив с данными
     *
     * @return int id
     */
    public function create($data)
    {
        return $this->insertOrUpdate($data);
    }

    /** Переопределенный метод. Всегда использует insertOrUpdate
     *
     * @param array $data - массив с данными
     *
     * @return int id
     */
    public function insert($data)
    {
        if (!empty($data['device'])) $this->removeByDevice($data['device']);
        return $this->insertOrUpdate($data);
    }


    /** Убирает запись с device
     *
     * @param int $device - GUID устройства
     *
     * @return boolean|int
     */
    public function removeByDevice($device)
    {
        $params = [
            ['device', '=', $device],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }


    /** Убирает запись с token
     *
     * @param string $token - токен
     *
     * @return boolean|int
     */
    public function removeByToken($token)
    {
        $params = [
            ['token', '=', $token],
        ];

        $res = $this->deleteByParams($params);

        return $res;
    }


    /** Обновляет значение last_used_dt на текущее
     *
     * @param int $id
     *
     * @return void
     */
    public function touch($id)
    {
        $this->update($id, ['last_used_dt'=>'NOW()'], false);
    }

}
