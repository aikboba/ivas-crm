<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Trans_model
 *
 * @property Presets_items_model $presets_items_model
 */

class Trans_model extends MY_Model
{
    public $table = 'trans';
    public $entity = '';
    public $primary = 'id';
    public $error;
    public $order_by_field = 'trans_id';
    public $order_by_dir = 'DESC';
    public $default_field_list = [
        'trans.id',
        'trans.trans_id',
        'trans.order_id',
        'trans.client_id',
        'trans.amount',
        'trans.type',
        'trans.oper',
        'trans.data',
        'trans.status',

        'clients.first_name as client_first_name',
        'clients.last_name as client_last_name',
        'clients.email as client_email',
        'clients.user_insta_nick as client_user_insta_nick',

        'trans.created',
        'trans.updated',
        'UNIX_TIMESTAMP(trans.created) as created_unix',
        'UNIX_TIMESTAMP(trans.updated) as updated_unix'
    ];

    public $pref = '';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->fields = $this->db->list_fields($this->table);
    }

    /** Собирает join'ы для базового getByParams
     *
     * @return void
     */
    public function processJoins()
    {
        $this->db->join('clients', 'trans.client_id = clients.id', 'left');
        $this->db->join('orders', 'trans.order_id = orders.id', 'left');
    }

    /** Собирает дополнительные условия where для базового getByParams
     *
     * @return void
     */
    public function processAdditionalWhere()
    {
        // если вызов пришел из фронтового контроллера, то выбираем только активных
        if ($this->caller_origin == 'front')
        {
//            $this->db->where('trans.status IN ("active","stopped")');
        }
    }

    /** Переводит данные о транзакции из формата transactionData в сохраняемый нами формат
     *
     * @param array $data - данные транзакции, переданные платежной системой
     *
     * @return array
     */
    public function transform($data)
    {
        if (!is_array($data)) return false;

        $iData = [
            'trans_id'  => intval($data['TransactionId']),
            'order_id'  => intval($data['InvoiceId']),
            'client_id' => intval($data['AccountId']),
            'amount'    => floatval($data['Amount']),
            'type'      => $data['OperationType'],
            'status'    => $data['Status'],
            'data'      => json_encode($data),
        ];

        return $iData;
    }

    /** Создает транзакцию по TransactionData
     *
     * @param array $data - данные транзакции, переданные платежной системой
     *
     * @return int
     */
    public function createFromTransData($data)
    {
        if (!is_array($data)) return false;

        $iData = $this->transform($data);

        $res = $this->insert($iData);

        return $res;
    }

    /** Создает транзакцию по TransactionData
     *
     * @param array $data - данные транзакции, переданные платежной системой
     *
     * @return int
     */
    public function createFromData($data)
    {
        if (!is_array($data)) return false;

        $res = $this->insert($data);

        return $res;
    }

    /** Производит выборку транзакций по типам
     *
     * @param string $type - тип транзакции
     *
     * @return array
     */
    public function getByType($type)
    {
        $params = [
            ['type', '=', $type],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку транзакций по клиенту
     *
     * @param int $client_id - ID клиента
     *
     * @return array
     */
    public function getByClientId($client_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку транзакций по order
     *
     * @param int $order_id - ID заказа
     *
     * @return array
     */
    public function getByOrderId($order_id)
    {
        $params = [
            ['order_id', '=', intval($order_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Производит выборку транзакций по client и order
     *
     * @param int $client_id - ID клиента
     * @param int $order_id - ID заказа
     *
     * @return array
     */
    public function getByClientIdOrderId($client_id, $order_id)
    {
        $params = [
            ['client_id', '=', intval($client_id)],
            ['order_id', '=', intval($order_id)],
        ];

        $res = $this->getByParams($params);

        return $res;
    }

    /** Выборка по trans_id
     *
     * @param int $trans_id
     *
     * @return object | null
     */
    public function getByTransId($trans_id)
    {
        $params = [
            ['trans_id', '=', $trans_id]
        ];

        $res = $this->getByParams($params);

        $trans = [];
        if (!empty($res))
        {
            $trans = $res[0];
        }

        return $trans;
    }

    /** Переопределенный метод. НЕ УДАЛЯЕТ, а выставляет status = 'deleted'
     *
     * @param int $id
     *
     * @return int id
     */
    public function delete($id)
    {
        return $this->setDeleted($id);
    }


    /** Изменяет "приписку" транзакций (через update) с клиента donor_id на клиента с acceptor_id.
     *
     * @param int $acceptor_id  - ID клиента, С КОТОРЫМ ПРОИСХОДИТ СЛИЯНИЕ
     * @param int $donor_id  - ID клиента, КОТОРОГО СЛИВАЮТ
     *
     * @return boolean
     */
    public function mergeClientsData($acceptor_id, $donor_id)
    {
        $result = true;

        $params = [
            ['client_id', '=', $donor_id],
        ];

        $uData = [
            'client_id' => $acceptor_id,
        ];
        $res = $this->updateByParams($params, $uData);
        $result = ($res !== false);

        return $result;
    }


}