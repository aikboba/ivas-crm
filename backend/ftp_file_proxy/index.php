<?php
error_reporting(-1);
ini_set('display_errors', 1);
        
$path = "alex_local";
if (isset($_ENV['CI_ENV']))
{
    $path = $_ENV['CI_ENV'];
}

include("inc/".$path."/ftp.php");

function ftp_get_contents ($conn_id, $filename) 
{
    //Create temp handler:
    $tempName = tempnam(sys_get_temp_dir(),'');
   
    if (ftp_get($conn_id, $tempName, $filename, FTP_BINARY) )
    {
        $mime = mime_content_type($tempName);
        $cont = file_get_contents($tempName);
        return ['mime_type'=>$mime, 'contents'=>$cont];
    } 
    else 
    {
    
        return false;
    }
} 

$st = $_SERVER['QUERY_STRING'];
$chunks = explode("/",$st);
$filename = array_pop($chunks);
$dir = array_pop($chunks);

$conn = ftp_connect($config['ftp_host']);
ftp_login($conn, $config['ftp_user'], $config['ftp_password']);
ftp_pasv($conn, true);
ftp_chdir($conn, $config['ftp_upload_'.$dir.'_path']);

$data = ftp_get_contents ($conn, $filename);

ftp_close($conn);

header("Content-type: ".$data['mime_type']);
echo $data['contents'];

?>