<?php
// ROOT dir
define('ROOT', realpath(__DIR__. DIRECTORY_SEPARATOR .'..').DIRECTORY_SEPARATOR);
// Path to DB\updates
define('UPDPATH', ROOT. 'DB'. DIRECTORY_SEPARATOR . "updates" . DIRECTORY_SEPARATOR);
// Path to the system directory
define('BASEPATH', ROOT. "system".DIRECTORY_SEPARATOR);
// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
// Name of the "system" directory
define('SYSDIR', basename(BASEPATH));
// Path to application directory
define('APPPATH', ROOT. "application". DIRECTORY_SEPARATOR);
// Path to config dir
define('CONFIGPATH', APPPATH ."config" );
// Path to views
define('VIEWPATH', APPPATH ."views". DIRECTORY_SEPARATOR );
// Path to the front controller (this file) directory
define('FCPATH', ROOT);

require_once(BASEPATH."core/Common.php");
require_once BASEPATH.'core/Controller.php';

if (is_cli())
{
    define('DELIM', "\n");
}
else
{
    define('DELIM', "<br>");
}

if (!empty($argv[1]))
{
    define('ENVIRONMENT', $argv[1]);
}
else
{
    define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
}

function &get_instance()
{
    return CI_Controller::get_instance();
}

$CONFIG =& load_class('Config', 'core');
$CI = new CI_Controller();

$CI->load->database();
$DB = $CI->db;

echo "Starting at ".date("Y-m-d H:i:s").DELIM;


$CI->load->library('vimeo');
$CI->load->library('utils');
$CI->load->model('items_model');

$videos = $CI->vimeo->getFilteredVideos();

if (!empty($videos))
{
    $data = $videos;
    echo "Items to process - ".count($data).DELIM.DELIM;

    $i = 0;
    foreach ($data as $video)
    {
        $i++;
        $resource_string_id = str_replace("https://vimeo.com/", "", $video['link']);
        $name = $video['name'];
        $status = $video['status'];
        echo $i.") Processing: '".$name."' - ";

        // проверяем , есть ли элемент с таким resource_string_id в таблице. Если есть - пропускаем
        $res = $CI->items_model->getByResourceStringId($resource_string_id);

        if (!empty($res))
        {
            echo "resource_string_id (".$resource_string_id.") already present - SKIPPED".DELIM;
            //такое значение есть - пропуск
            continue;
        }
        else
        {
            //такого значения нет
            $name_unique = $CI->items_model->isUnique("name", $name);
            if (!$name_unique) $name = $name . "_" . $CI->utils->salt();
            $uData = [
                'name' => $name,
                'resource_string_id' => $resource_string_id,
                'item_type' => 'video',
                'status' => 'new'
            ];
            $CI->items_model->insert($uData);
            echo "ADDED".DELIM;
        }

    }

}

echo DELIM.DELIM."Done.".DELIM;
?>