<?php
// ROOT dir
define('ROOT', realpath(__DIR__. DIRECTORY_SEPARATOR .'..').DIRECTORY_SEPARATOR);
// Path to DB\updates
define('UPDPATH', ROOT. 'DB'. DIRECTORY_SEPARATOR . "updates" . DIRECTORY_SEPARATOR);
// Path to the system directory
define('BASEPATH', ROOT. "system".DIRECTORY_SEPARATOR);
// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
// Name of the "system" directory
define('SYSDIR', basename(BASEPATH));
// Path to application directory
define('APPPATH', ROOT. "application". DIRECTORY_SEPARATOR);
// Path to config dir
define('CONFIGPATH', APPPATH ."config" );
// Path to views
define('VIEWPATH', APPPATH ."views". DIRECTORY_SEPARATOR );
// Path to the front controller (this file) directory
define('FCPATH', ROOT);

require_once(BASEPATH."core/Common.php");
require_once BASEPATH.'core/Controller.php';

if (is_cli())
{
    define('DELIM', "\n");
}
else
{
    define('DELIM', "<br>");
}

if (!empty($argv[1]))
{
    define('ENVIRONMENT', $argv[1]);
}
else
{
    define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
}

function &get_instance()
{
    return CI_Controller::get_instance();
}

$CONFIG =& load_class('Config', 'core');
$CI = new CI_Controller();

$CI->load->database();
$DB = $CI->db;

echo "Starting at ".date("Y-m-d H:i:s").DELIM;

/*
$CI->load->library('vimeo');
$CI->load->library('utils');
$CI->load->model('items_model');
*/




echo DELIM.DELIM."Done.".DELIM;
?>