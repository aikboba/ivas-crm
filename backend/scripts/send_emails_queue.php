<?php
// ROOT dir
define('ROOT', realpath(__DIR__. DIRECTORY_SEPARATOR .'..').DIRECTORY_SEPARATOR);
// Path to DB\updates
define('UPDPATH', ROOT. 'DB'. DIRECTORY_SEPARATOR . "updates" . DIRECTORY_SEPARATOR);
// Path to the system directory
define('BASEPATH', ROOT. "system".DIRECTORY_SEPARATOR);
// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
// Name of the "system" directory
define('SYSDIR', basename(BASEPATH));
// Path to application directory
define('APPPATH', ROOT. "application". DIRECTORY_SEPARATOR);
// Path to config dir
define('CONFIGPATH', APPPATH ."config" );
// Path to views
define('VIEWPATH', APPPATH ."views". DIRECTORY_SEPARATOR );
// Path to the front controller (this file) directory
define('FCPATH', ROOT);

require_once(BASEPATH."core/Common.php");
require_once BASEPATH.'core/Controller.php';

if (is_cli())
{
    define('DELIM', "\n");
}
else
{
    define('DELIM', "<br>");
}

if (!empty($argv[1]))
{
    define('ENVIRONMENT', $argv[1]);
}
else
{
    define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
}

if ($composer_autoload = config_item('composer_autoload'))
{
    if ($composer_autoload === TRUE)
    {
        file_exists(APPPATH.'vendor/autoload.php')
            ? require_once(APPPATH.'vendor/autoload.php')
            : log_message('error', '$config[\'composer_autoload\'] is set to TRUE but '.APPPATH.'vendor/autoload.php was not found.');
    }
    elseif (file_exists($composer_autoload))
    {
        require_once($composer_autoload);
    }
    else
    {
        log_message('error', 'Could not find the specified $config[\'composer_autoload\'] path: '.$composer_autoload);
    }
}

function &get_instance()
{
    return CI_Controller::get_instance();
}

$CONFIG =& load_class('Config', 'core');
$CI = new CI_Controller();

$CI->load->database();
$DB = $CI->db;

echo "Starting at ".date("Y-m-d H:i:s").DELIM;

set_time_limit(0);
$CI->load->library('mailer');
$CI->load->model('emails_queue_model');
$CI->load->model('emails_logs_model');

$limit = 1;
$sleepTime = 10;

while(true)
{
    $tasks = $CI->emails_queue_model->getTasks($limit);
    if ( empty($tasks) )
    {
        // текущих задач нет
        sleep($sleepTime);
        continue;
    }
    
    foreach($tasks as $task)
    {
        $update = $CI->emails_queue_model->update($task->id, ['status' => 'in_process']);

        if ($update != 1) 
        {
            // задача УЖЕ принята конкурирующим(?) процессом
            continue;
        }
        
        $emailSendData = [
            'toEmail' => $task->recipient_email,
            'toName' => $task->recipient_name,
            
            'fromEmail' => $task->sender_email,
            'fromName' => $task->sender_name,
            
            'subject' => $task->subject,
            'body' => $task->content,
            'is_html' => $task->is_html
        ];
        
        if (!empty($task->attached_files))
        {
            $emailSendData['attached_files'] =  json_decode($task->attached_files, true);
        }
        
        if (!$CI->mailer->send($emailSendData)) 
        {
            $CI->emails_queue_model->update($task->id, ['status' => 'error']);
            // отправка успешна -> переносим письмо в лог
            //$task->status = 'error';
            //$CI->emails_logs_model->insertOrUpdate($task);
        }
        else 
        {
            // отправка успешна -> переносим письмо в лог
            //$task->status = 'sent';
            //$CI->emails_logs_model->insertOrUpdate($task);

            // удаление из очереди
            $CI->emails_queue_model->delete($task->id);
        }
    }
}

echo DELIM.DELIM."Done.".DELIM;
?>