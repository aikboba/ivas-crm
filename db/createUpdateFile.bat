@ECHO OFF

SET hh=%time:~0,2%
IF "%time:~0,1%"==" " SET hh=0%hh:~1,1%

SET dt=%date:~6,4%%date:~3,2%%date:~0,2%%hh%%time:~3,2%%time:~6,2%

SET dirname=%~dp0%
SET filepath=%dirname%updates\
SET file=%filepath%update_%dt%.sql
(
    ECHO -- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
    ECHO -- comment: [ADD A COMMENT]
    ECHO --
) > %file%

@ECHO ON