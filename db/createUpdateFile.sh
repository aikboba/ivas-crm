#!/bin/bash
currentDate=$(date +'%Y%m%d%H%M%S')
filename="update_"${currentDate}".sql"
file=updates/${filename}

echo "
-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]
-- author: [kto]
-- comment: [ADD A COMMENT]
--" >> ${file}
