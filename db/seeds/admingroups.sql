﻿/* SQL Manager for MySQL                              5.7.2.52112 */
/* -------------------------------------------------------------- */
/* Host     : localhost                                           */
/* Port     : 3306                                                */
/* Database : ivas-crm                                                */


/* Data for the `admingroups` table  (LIMIT 0,500) */

INSERT INTO `admingroups` (`id`, `name`, `description`, `status`, `created`, `updated`) VALUES
  (1,'Главные',NULL,'active','2019-11-09 17:04:24','2019-11-09 17:04:24'),
  (2,'Не особо главные',NULL,'active','2019-11-09 17:04:37','2019-11-09 17:04:37'),
  (3,'Второстепенные',NULL,'active','2019-11-09 17:04:50','2019-11-09 17:04:50');
COMMIT;

