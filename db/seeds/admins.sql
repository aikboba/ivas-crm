/* Data for the `admins` table  (LIMIT 0,500) */

INSERT INTO `admins` (`id`, `ip_address`, `password`, `salt`, `secret_code`, `email`, `last_login_datetime`, `first_name`, `last_name`, `phone`, `timezone`, `status`, `created`, `updated`, `is_super`) VALUES
  (1,'127.0.0.1','9ba20c19b5288838f6a2197b30988c9f55d10727','5f7e41d7583b9036d272ed64b036ee76f65b',NULL,'admin@admin.com','2020-05-12 21:16:34','Иван','Грозный','+0','Europe/Moscow','active','2019-08-10 00:38:46','2020-05-12 21:16:34',1),
  (2,'127.0.0.1','45692587d8d030e0d49c8af3acd8c8834e6bd41f','0ac18d05974228c6529f61aa026050712953',NULL,'pr@manager.com','2020-02-06 01:02:24','Пиар','Менеджер','12312312345','Europe/Moscow','active','2019-08-10 00:38:46','2020-02-06 01:02:24',0),
  (7,'127.0.0.1','f0879938a6ebac5aba92e7c4e76326f4c52e2477','a0bd52ecbe0a1663bb746dc3649a2342',NULL,'admin3434@admin.com','2020-02-06 01:02:24','','231231234444','123123123443','Europe/Moscow','inactive','2019-11-09 13:13:51','2020-02-06 01:02:24',0);
COMMIT;

