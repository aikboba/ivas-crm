/* Data for the `docs` table  (LIMIT 0,500) */

INSERT INTO `docs` (`id`, `client_id`, `title`, `filename`, `mime_type`, `created`, `updated`, `status`) VALUES
  (44,3,'avatar4_darkest_dungeon.jpg','18b8b875520fa80c3c459b6f627e90c3_avatar4_darkest_dungeon.jpg','image/jpeg','2019-11-16 01:28:22','2020-03-17 10:28:07','active'),
  (45,3,'avatar3_darkest_dungeon.jpg','eecefc96a28da6fb4cbae27453361681_avatar3_darkest_dungeon.jpg','image/jpeg','2019-11-16 01:28:22','2020-03-17 10:28:07','active'),
  (46,3,'avatar2_darkest_dungeon.jpg','16a69bd2e86dc13de36e56a9714e35c1_avatar2_darkest_dungeon.jpg','image/jpeg','2019-11-16 01:28:22','2020-03-17 10:28:07','active'),
  (47,1,'forum-avatar.png','94c680cfcdf52ffcc324903d8987a684_forum-avatar.png','image/png','2020-01-07 00:11:50','2020-01-07 00:11:50','active'),
  (48,1,'Crusader_High.png','63be80788c9c71dfd8fd13060a08ed9b_Crusader_High.png','image/png','2020-01-07 00:11:50','2020-01-07 00:11:50','active'),
  (49,1,'Russian Flag.jpg','0487fa74c56c1fce52d5892f57d22d7f_Russian Flag.jpg','image/jpeg','2020-01-07 00:21:03','2020-01-07 00:21:03','active'),
  (50,1,'crusader_bigger.jpg','9f1072331b3f3e7cb7de2ddc02d2fbac_crusader_bigger.jpg','image/jpeg','2020-01-07 01:01:05','2020-01-07 01:01:05','active'),
  (52,10292,'forum-avatar.png','de94d3648d6cd61be1d799603f13a1cf_forum-avatar.png','image/png','2020-05-13 01:02:59','2020-05-13 01:02:59','active');
COMMIT;

