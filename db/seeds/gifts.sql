/* Data for the `gifts` table  (LIMIT 0,500) */

INSERT INTO `gifts` (`id`, `name`, `description`, `start_dt`, `end_dt`, `duration`, `apply_if`, `status`, `created`, `updated`) VALUES
  (1,'Подарок 1',NULL,'2019-01-01 01:00:00',NULL,20,'in_orders','active','2020-01-03 13:39:30','2020-04-12 03:02:35'),
  (2,'Подарок 2',NULL,'2020-12-12 00:00:00',NULL,100,'always','active','2020-01-03 13:40:52','2020-01-03 13:40:52'),
  (3,'Подарок 3',NULL,'2019-02-02 00:00:00','2019-12-12 00:00:00',6,'always','active','2020-01-03 13:41:31','2020-01-03 13:42:49'),
  (4,'Подарок 4',NULL,'2019-05-05 00:00:00','2020-12-13 00:00:00',5,'always','active','2020-01-03 13:42:43','2020-01-03 13:42:43');
COMMIT;

