/* Data for the `groups` table  (LIMIT 0,500) */

INSERT INTO `groups` (`id`, `name`, `description`, `status`, `created`, `updated`) VALUES
  (1,'Группа 1','Описание группы 1','active','2019-10-01 13:19:26','2019-10-01 13:19:26'),
  (2,'Группа 2','Описание группы 2','active','2019-10-01 13:19:43','2019-10-01 13:19:43'),
  (3,'Неактивная группа 3',NULL,'inactive','2019-10-01 13:19:58','2019-10-01 13:19:58');
COMMIT;

