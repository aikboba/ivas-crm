/* Data for the `permissions` table  (LIMIT 0,500) */

INSERT INTO `permissions` (`admin_id`, `p_admins`, `p_products`, `p_paysystems`, `p_privileges`, `p_clients`, `p_items`, `p_presets`, `p_groups`, `p_gifts`, `p_pages`, `p_templates`, `p_instagram_accounts`, `p_promocodes`, `p_presets_cats`, `p_admingroups`, `p_docs`, `p_bots`, `p_orders`, `p_tags`, `p_countries_cities`, `p_items_delete_with_content`, `p_items_download_from_vimeo`) VALUES
  (1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1),
  (2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0);
COMMIT;

