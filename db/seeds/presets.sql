/* Data for the `presets` table  (LIMIT 0,500) */

INSERT INTO `presets` (`id`, `presets_cat_id`, `name`, `description`, `preset_type`, `status`, `created`, `updated`) VALUES
  (1,NULL,'Сборник 1',NULL,'training','active','2019-10-05 12:47:14','2019-10-05 12:47:14'),
  (2,NULL,'Сборник 2',NULL,'training','active','2019-10-05 12:48:45','2019-10-05 12:48:45'),
  (3,NULL,'Сборник 3 неактивный',NULL,'training','inactive','2019-10-05 12:54:29','2019-10-05 12:54:29'),
  (4,NULL,'Тренинг 1','','training','active','2019-10-05 12:54:42','2019-11-20 22:43:53'),
  (6,NULL,'Мой продукт - эфиры',NULL,'playlist','active','2019-12-07 13:12:22','2019-12-07 13:12:22'),
  (7,NULL,'Мартовский кот - эфиры',NULL,'playlist','active','2019-12-09 14:29:49','2020-01-03 13:45:30'),
  (8,NULL,'Приватный аккаунт - эфиры',NULL,'playlist','active','2019-12-09 14:30:19','2020-01-03 13:45:34'),
  (9,NULL,'Тренинг 2',NULL,'training','active','2020-01-03 15:54:33','2020-01-03 15:54:33'),
  (10,NULL,'Тренинг 3',NULL,'training','active','2020-01-03 16:04:12','2020-01-03 16:04:22'),
  (11,NULL,'Просто моя коллекция','','training','inactive','2020-03-06 15:23:55','2020-03-06 15:23:55');
COMMIT;

