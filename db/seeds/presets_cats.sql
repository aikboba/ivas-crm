/* Data for the `presets_cats` table  (LIMIT 0,500) */

INSERT INTO `presets_cats` (`id`, `name`, `parent_id`, `status`, `created`, `updated`) VALUES
  (2,'Майский жук',0,'active','2019-11-05 12:37:09','2019-11-05 12:37:09'),
  (3,'Апрельский олень',0,'active','2019-11-05 12:37:43','2019-11-05 12:37:43'),
  (4,'МЖ - Избранное',2,'active','2019-11-05 12:38:10','2019-11-05 12:38:53'),
  (5,'АО - Лучшее',3,'active','2019-11-05 12:38:31','2019-11-05 17:58:46'),
  (6,'МЖ - Хиты',2,'active','2019-11-05 12:39:16','2019-11-05 12:39:16'),
  (8,'МЖ-Хиты-Избранное',6,'active','2019-11-05 22:49:40','2019-11-05 22:49:40'),
  (9,'-- ROOT --',NULL,'active','2019-11-05 12:35:57','2019-11-05 12:36:37');
COMMIT;

