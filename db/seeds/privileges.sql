/* Data for the `privileges` table  (LIMIT 0,500) */

INSERT INTO `privileges` (`id`, `name`, `description`, `discount_type`, `discount`, `status`, `created`, `updated`) VALUES
  (5,'Студенты','1','percent',30.00,'active','2019-08-30 01:36:14','2019-08-30 01:36:14'),
  (10,'Пенсионеры и пионеры','','fixed_amount',150.00,'active','2019-08-30 01:48:24','2019-12-12 16:24:34'),
  (11,'Inactive group',NULL,'percent',0.00,'inactive','2019-08-30 01:48:45','2019-08-30 01:48:45');
COMMIT;

