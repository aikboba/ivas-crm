/* Data for the `promocodes` table  (LIMIT 0,500) */

INSERT INTO `promocodes` (`id`, `name`, `promocode`, `start_dt`, `end_dt`, `discount_type`, `discount_value`, `product_id`, `option_id`, `client_id`, `one_time_only`, `status`, `created`, `updated`) VALUES
  (6,'111','111',NULL,NULL,'percent',10.00,1,NULL,NULL,0,'active','2019-12-06 11:18:19','2019-12-13 16:44:22'),
  (7,'777','777',NULL,NULL,'percent',100.00,NULL,NULL,NULL,0,'active','2019-12-14 13:31:24','2020-02-06 12:04:05'),
  (8,'Useless','000',NULL,NULL,'fixed_amount',0.00,NULL,NULL,NULL,0,'active','2020-02-01 11:46:18','2020-02-06 12:04:16'),
  (9,'555','555',NULL,NULL,'percent',50.00,NULL,NULL,NULL,0,'active','2020-03-20 00:59:56','2020-03-20 01:13:38');
COMMIT;

