/* Data for the `tags` table  (LIMIT 0,500) */

INSERT INTO `tags` (`id`, `name`, `description`, `status`, `created`, `updated`) VALUES
  (1,'Tag1',NULL,'active','2020-03-20 00:36:46','2020-03-20 00:36:46'),
  (2,'Tag 2',NULL,'active','2020-04-02 22:41:12','2020-04-02 22:41:12'),
  (3,'Tag 3',NULL,'active','2020-04-02 22:41:27','2020-04-02 22:41:27');
COMMIT;

