-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]
-- author: [kto]
-- comment: [ADD A COMMENT]
--

/* SQL Manager for MySQL                              5.7.2.52112 */
/* -------------------------------------------------------------- */
/* Host     : localhost                                           */
/* Port     : 3306                                                */
/* Database : service-rk                                          */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

/* Dropping database objects */

DROP PROCEDURE IF EXISTS `takeProductFromAllNoCursor`;
DROP PROCEDURE IF EXISTS `allowProductToAllNoCursor`;
DROP PROCEDURE IF EXISTS `allowProductToAll`;
DROP TABLE IF EXISTS `test`;
DROP TABLE IF EXISTS `trans`;
DROP TABLE IF EXISTS `tokens`;
DROP TABLE IF EXISTS `templates`;
DROP TABLE IF EXISTS `templates_cats`;
DROP TABLE IF EXISTS `tasks_completed`;
DROP TABLE IF EXISTS `tasks`;
DROP TABLE IF EXISTS `sessions`;
DROP TABLE IF EXISTS `products_gifts`;
DROP TABLE IF EXISTS `presets_items`;
DROP TABLE IF EXISTS `permissions`;
DROP TABLE IF EXISTS `paysystems`;
DROP TABLE IF EXISTS `pages`;
DROP TABLE IF EXISTS `orders_tags`;
DROP TABLE IF EXISTS `tags`;
DROP TABLE IF EXISTS `orders`;
DROP TABLE IF EXISTS `subs`;
DROP TABLE IF EXISTS `promocodes`;
DROP TABLE IF EXISTS `options`;
DROP TABLE IF EXISTS `logs`;
DROP TABLE IF EXISTS `items`;
DROP TABLE IF EXISTS `groups_presets`;
DROP TABLE IF EXISTS `gifts_presets`;
DROP TABLE IF EXISTS `gifts`;
DROP TABLE IF EXISTS `emails_queue`;
DROP TABLE IF EXISTS `emails_logs`;
DROP TABLE IF EXISTS `docs`;
DROP TABLE IF EXISTS `clients_products`;
DROP TABLE IF EXISTS `products`;
DROP TABLE IF EXISTS `instagram_accounts`;
DROP TABLE IF EXISTS `clients_presets`;
DROP TABLE IF EXISTS `presets`;
DROP TABLE IF EXISTS `presets_cats`;
DROP TABLE IF EXISTS `clients_groups`;
DROP TABLE IF EXISTS `groups`;
DROP TABLE IF EXISTS `clients_devices`;
DROP TABLE IF EXISTS `cli_priv`;
DROP TABLE IF EXISTS `privileges`;
DROP TABLE IF EXISTS `clients`;
DROP TABLE IF EXISTS `cities`;
DROP TABLE IF EXISTS `countries`;
DROP TABLE IF EXISTS `admins_admingroups`;
DROP TABLE IF EXISTS `admins`;
DROP TABLE IF EXISTS `admingroups`;

/* Structure for the `admingroups` table : */

CREATE TABLE `admingroups` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_general_ci,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`name`(50))
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `admins` table : */

CREATE TABLE `admins` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARCHAR(45) COLLATE utf8_general_ci NOT NULL,
  `password` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `salt` VARCHAR(255) COLLATE utf8_general_ci DEFAULT '',
  `secret_code` VARCHAR(40) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'для хранения кода активации, восстановления пароля и т.д.',
  `email` VARCHAR(100) COLLATE utf8_general_ci NOT NULL,
  `last_login_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'TIMESTAMP последнего логина',
  `first_name` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL,
  `last_name` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL,
  `phone` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `timezone` VARCHAR(120) COLLATE utf8_general_ci DEFAULT 'Europe/Moscow' COMMENT 'таймзона',
  `status` ENUM('active','inactive','banned','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_super` TINYINT(4) DEFAULT 0 COMMENT '\"Супер-админ\". Такому админу никто не может менять права',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `admins_u` USING BTREE (`email`),
  KEY `timezone` USING BTREE (`timezone`(20))
) ENGINE=InnoDB
AUTO_INCREMENT=8 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `admins_admingroups` table : */

CREATE TABLE `admins_admingroups` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `admingroup_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `admins_admingroups_u` USING BTREE (`admin_id`, `admingroup_id`),
  KEY `admin_id` USING BTREE (`admin_id`),
  KEY `admingroup_id` USING BTREE (`admingroup_id`),
  CONSTRAINT `admins_admingroups_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admins_admingroups_admingroup_id` FOREIGN KEY (`admingroup_id`) REFERENCES `admingroups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=3 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `countries` table : */

CREATE TABLE `countries` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `iso` CHAR(3) COLLATE utf8_general_ci DEFAULT NULL,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'active',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name` USING BTREE (`name`),
  UNIQUE KEY `iso` USING BTREE (`iso`),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=238 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `cities` table : */

CREATE TABLE `cities` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'active',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`country_id`, `name`),
  KEY `status` USING BTREE (`status`),
  KEY `country_id` USING BTREE (`country_id`),
  CONSTRAINT `fk_cities_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=5479518 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `clients` table : */

CREATE TABLE `clients` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_insta_nick` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `user_insta_pk` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Instagram_ID клиента. Используется Instagram API',
  `phone` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `email` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `pass` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `salt` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'индивидуальная соль для генерации пароля',
  `secret_code` VARCHAR(40) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'для хранения кода первичной активации пользователя, восстановления пароля и т.д.',
  `first_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `patronym_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Отчество',
  `last_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `timezone` VARCHAR(120) COLLATE utf8_general_ci DEFAULT 'Europe/Moscow' COMMENT 'таймзона',
  `birthday` DATE DEFAULT NULL COMMENT 'ДР клиента',
  `is_blogger` TINYINT(1) DEFAULT 0 COMMENT 'Является ли клиент блоггером',
  `country_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Страна клиента',
  `city_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Город клиента',
  `sex` ENUM('m','f','u') COLLATE utf8_general_ci DEFAULT 'u' COMMENT 'Пол клиента. u-undefined',
  `subscribed_system` TINYINT(1) DEFAULT 1 COMMENT 'Клиент подписан на \"Системные\" сообщения',
  `subscribed_news` TINYINT(1) DEFAULT 1 COMMENT 'Клиент подписан на \"Новостные\" сообщения',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `last_act_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Хранит время последнего действия клиента',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `getcourse_id` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `clients_email_u` USING BTREE (`email`),
  UNIQUE KEY `clients_phone_u` USING BTREE (`phone`),
  KEY `timezone` USING BTREE (`timezone`(20)),
  KEY `user_insta_nick` USING BTREE (`user_insta_nick`(30)),
  KEY `clients_first_name` USING BTREE (`first_name`(30)),
  KEY `clients_last_name` USING BTREE (`last_name`(30)),
  KEY `clients_patronym_name` USING BTREE (`patronym_name`(30)),
  KEY `clients_status` USING BTREE (`status`),
  KEY `is_blogger` USING BTREE (`is_blogger`),
  KEY `country_id` USING BTREE (`country_id`),
  KEY `city_id` USING BTREE (`city_id`),
  KEY `sex` USING BTREE (`sex`),
  CONSTRAINT `fk_clients_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_clients_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=10300 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `privileges` table : */

CREATE TABLE `privileges` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) COLLATE utf8_general_ci NOT NULL COMMENT 'Название группы привилегий',
  `description` TEXT COLLATE utf8_general_ci COMMENT 'Описание группы привилегий',
  `discount_type` ENUM('percent','fixed_amount') COLLATE utf8_general_ci DEFAULT 'percent' COMMENT 'тип предоставляемой скидки: %, фикс скидка',
  `discount` FLOAT(9,2) DEFAULT 0.00 COMMENT 'Скидка при вводе при членстве в группе',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `privileges_name_u` USING BTREE (`name`)
) ENGINE=InnoDB
AUTO_INCREMENT=12 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `cli_priv` table : */

CREATE TABLE `cli_priv` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cli_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `priv_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `valid_until` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата окончания членства в группе',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `cli_priv_u` USING BTREE (`cli_id`, `priv_id`),
  KEY `cli_id` USING BTREE (`cli_id`),
  KEY `priv_id` USING BTREE (`priv_id`),
  CONSTRAINT `fk_cli_priv_cli_id` FOREIGN KEY (`cli_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cli_priv_priv_id` FOREIGN KEY (`priv_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=14 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `clients_devices` table : */

CREATE TABLE `clients_devices` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `device` VARCHAR(250) COLLATE utf8_general_ci NOT NULL COMMENT 'GUID устройства',
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `device` USING BTREE (`device`),
  KEY `client_id` USING BTREE (`client_id`),
  CONSTRAINT `fk_clients_devices` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `groups` table : */

CREATE TABLE `groups` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_general_ci,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`name`(50))
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `clients_groups` table : */

CREATE TABLE `clients_groups` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `group_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `clients_groups_u` USING BTREE (`client_id`, `group_id`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `group_id` USING BTREE (`group_id`),
  CONSTRAINT `fk_clients_groups_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clients_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=10 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `presets_cats` table : */

CREATE TABLE `presets_cats` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `parent_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`parent_id`, `name`(150)),
  KEY `status` USING BTREE (`status`),
  KEY `parent_id` USING BTREE (`parent_id`),
  CONSTRAINT `fk_presets_cats_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `presets_cats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=9 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `presets` table : */

CREATE TABLE `presets` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `presets_cat_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID категории пресета',
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_general_ci,
  `preset_type` ENUM('playlist','training') COLLATE utf8_general_ci DEFAULT 'training' COMMENT 'Тип пресета',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`name`(50)),
  KEY `presets_cat_id` USING BTREE (`presets_cat_id`),
  CONSTRAINT `fk_presets_presets_cat_id` FOREIGN KEY (`presets_cat_id`) REFERENCES `presets_cats` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=12 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `clients_presets` table : */

CREATE TABLE `clients_presets` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `preset_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `starting_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала доступности. Если NULL - все видео будут доступны. Иначе - только те, что created после этой даты',
  `expiration_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Возможная дата окончания срока доступности',
  `origin` ENUM('group','gift','product','order','manual') COLLATE utf8_general_ci DEFAULT 'manual' COMMENT '\"Источник\" - откуда клиент получил доступ к данному пресету',
  `origin_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID сущности-источника',
  `event_id` INTEGER(11) DEFAULT NULL COMMENT 'ID \"события добавления\". В это роли может быть clients_products.id.',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `preset_id` USING BTREE (`preset_id`),
  KEY `origin` USING BTREE (`origin`),
  KEY `origin_id` USING BTREE (`origin_id`),
  CONSTRAINT `fk_clients_presets_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clients_presets_preset_id` FOREIGN KEY (`preset_id`) REFERENCES `presets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=34 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `instagram_accounts` table : */

CREATE TABLE `instagram_accounts` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pk` BIGINT(20) UNSIGNED DEFAULT NULL COMMENT 'Instagram_id',
  `name` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Название аккаунта',
  `user_name` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Имя пользователя аккаунта',
  `user_avatar` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Аватарка пользователя аккаунта',
  `login` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Instagram login аккаунта',
  `password` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'пароль для доступа к Instagram',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `stream_origin` ENUM('youtube','vimeo','rtmp','hls','other') COLLATE utf8_general_ci DEFAULT 'other',
  `stream_url_data` TEXT COLLATE utf8_general_ci COMMENT 'JSON_ENCODED object, содержащий ссылки на трансляцию в разном качестве',
  `stream_logo` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Логотип стрима - первый фрэйм',
  `stream_started` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время начала стрима',
  `stream_status` ENUM('active','inactive','paused') COLLATE utf8_general_ci DEFAULT 'inactive' COMMENT 'Состояние трансляции',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `instagram_accounts_pk` USING BTREE (`pk`),
  UNIQUE KEY `instagram_accounts_name` USING BTREE (`name`),
  UNIQUE KEY `instagram_accounts_login` USING BTREE (`login`)
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `products` table : */

CREATE TABLE `products` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `paysystem_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Связка с аккаунтами систем оплаты',
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID клиента для персональных продуктов',
  `group_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID группы, к которой добавляется клиент, купивший этот продукт',
  `preset_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID пресета, доступ к которому добавляется клиенту, купивший этот продукт',
  `instagram_account_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Instagram account, на который подпичывают клиента при покупки подписки',
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `product_type` ENUM('subscription','donation','preset') COLLATE utf8_general_ci NOT NULL DEFAULT 'preset' COMMENT 'Тип продукта - подписка, пожертвование, доступ к видосам',
  `description` TEXT COLLATE utf8_general_ci,
  `is_free` TINYINT(1) DEFAULT 0,
  `days_available_after_stop` INTEGER(11) DEFAULT 30 COMMENT 'Доступность сохраненных эфиров после окончания продукта в днях',
  `days_item_available` INTEGER(11) DEFAULT 10 COMMENT 'Число дней, в течение которых эфир(item) (после наступления своей даты доступности) будет доступен для просмотра пользователями.',
  `stream_status` ENUM('active','inactive','paused') COLLATE utf8_general_ci DEFAULT 'inactive' COMMENT 'Состояние трансляции',
  `stream_health` ENUM('red','green','none') COLLATE utf8_general_ci DEFAULT 'none' COMMENT 'Состояние трансляции с точки зрения данных транскодера',
  `stream_origin` ENUM('youtube','vimeo','rtmp','hls','other') COLLATE utf8_general_ci DEFAULT 'other',
  `stream_url_data` TEXT COLLATE utf8_general_ci COMMENT 'JSON_ENCODED object, содержащий ссылки на трансляцию в разном качестве',
  `stream_host_relay_url` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'RTMP-Ссылка для реализации стриминга со стороны блоггера',
  `stream_status_last_changed_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время последнего изменения статуса стрима',
  `stream_health_last_changed_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время последнего изменения health стрима',
  `stream_started` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время начала стрима',
  `stream_ended` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время окончания стрима',
  `stream_item_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID видео, в которое сохраняется текущий эфир',
  `stream_item_guid` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'GUID для связи с бэкап-файлом stream_item_id',
  `stream_clients_informed` TINYINT(1) DEFAULT 0 COMMENT 'Флаг проинформированномти клиентов и начале стрима',
  `status` ENUM('active','stopped','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name_u` USING BTREE (`name`(50)),
  KEY `products_paysystem_id` USING BTREE (`paysystem_id`),
  KEY `products_clients_id` USING BTREE (`client_id`),
  KEY `products_group_id` USING BTREE (`group_id`),
  KEY `instagram_account_id` USING BTREE (`instagram_account_id`),
  KEY `preset_id` USING BTREE (`preset_id`),
  CONSTRAINT `fk_products_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_products_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_products_instagram_account_id` FOREIGN KEY (`instagram_account_id`) REFERENCES `instagram_accounts` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_products_preset_id` FOREIGN KEY (`preset_id`) REFERENCES `presets` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=28 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `clients_products` table : */

CREATE TABLE `clients_products` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `product_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `start_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала доступа к продукту',
  `end_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата окончания действия',
  `order_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Заказ, оплатив который, клиент получил доступ к продукту. Может быть пуст, если доступ получен другим путем',
  `origin` ENUM('group','gift','product','order','manual') COLLATE utf8_general_ci DEFAULT 'manual' COMMENT '\"Источник\" - откуда клиент получил доступ к данному продукту',
  `origin_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID сущности-источника',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `end_dt` USING BTREE (`end_dt`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `product_id` USING BTREE (`product_id`),
  KEY `origin` USING BTREE (`origin`),
  KEY `origin_id` USING BTREE (`origin_id`),
  CONSTRAINT `fk_clients_products_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clients_products_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=15 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;


/* Structure for the `docs` table : */

CREATE TABLE `docs` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Клиент-хозяин файла',
  `title` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Пояснение (что за документ)',
  `filename` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Basename файла на сервере',
  `mime_type` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Mime-type файла на сервере',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  PRIMARY KEY USING BTREE (`id`),
  KEY `filename` USING BTREE (`filename`(10)),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `title` USING BTREE (`title`(50)),
  CONSTRAINT `docs_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=53 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `emails_logs` table : */

CREATE TABLE `emails_logs` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `recipient_email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `recipient_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `sender_email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `sender_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `subject` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `content` MEDIUMTEXT COLLATE utf8_general_ci,
  `attached_files` TEXT COLLATE utf8_general_ci,
  `status` ENUM('sent','cancelled','bounced','error') COLLATE utf8_general_ci DEFAULT 'sent',
  `error_message` TEXT COLLATE utf8_general_ci,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=44 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `emails_queue` table : */

CREATE TABLE `emails_queue` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `recipient_email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `recipient_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `sender_email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `sender_name` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `send_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subject` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `content` MEDIUMTEXT COLLATE utf8_general_ci,
  `is_html` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `attached_files` TEXT COLLATE utf8_general_ci,
  `status` ENUM('new','sent','cancelled','bounced','in_progress','error') COLLATE utf8_general_ci DEFAULT 'new',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `send_date_idx` USING BTREE (`send_date`)
) ENGINE=InnoDB
AUTO_INCREMENT=1 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `gifts` table : */

CREATE TABLE `gifts` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL,
  `description` TEXT COLLATE utf8_general_ci,
  `start_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Возможная дата начала срока срока действия подарка',
  `end_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Возможная дата окончания срока срока действия подарка',
  `duration` INTEGER(11) DEFAULT 0 COMMENT 'Длительность доступа к подарочным пресетам в днях',
  `apply_if` ENUM('in_orders','not_in_orders','always') COLLATE utf8_general_ci DEFAULT 'always' COMMENT 'Вручать подарок, если ХХХ в истории заказов: not_in_orders-нет в истории, always-неважно, in_orders-есть в истории',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `name` USING BTREE (`name`),
  KEY `start_dt` USING BTREE (`start_dt`),
  KEY `end_dt` USING BTREE (`end_dt`),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=5 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `gifts_presets` table : */

CREATE TABLE `gifts_presets` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gift_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `preset_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `pos` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `gifts_presets_u` USING BTREE (`gift_id`, `preset_id`),
  KEY `gift_id` USING BTREE (`gift_id`),
  KEY `preset_id` USING BTREE (`preset_id`),
  CONSTRAINT `fk_gifts_presets_gift_id` FOREIGN KEY (`gift_id`) REFERENCES `gifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_gifts_presets_preset_id` FOREIGN KEY (`preset_id`) REFERENCES `presets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `groups_presets` table : */

CREATE TABLE `groups_presets` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `preset_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `groups_presets_u` USING BTREE (`group_id`, `preset_id`),
  KEY `group_id` USING BTREE (`group_id`),
  KEY `preset_id` USING BTREE (`preset_id`),
  CONSTRAINT `fk_groups_presets_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_groups_presets_preset_id` FOREIGN KEY (`preset_id`) REFERENCES `presets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=3 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `items` table : */

CREATE TABLE `items` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `system_name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '\"Системное\" имя. Для хранения автогенерируемого имени при создании эфира',
  `description` TEXT COLLATE utf8_general_ci,
  `resource_string_id` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Youtube video id либо другой источник (путь к файлу, напрмиер)',
  `item_type` ENUM('video') COLLATE utf8_general_ci DEFAULT 'video' COMMENT 'тип содержимого. Видео, документ и т.д.',
  `guid` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'GUID для связи с бэкап-файлами',
  `available_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала доступности (не совпадает с created в случае с postprocessing)',
  `status` ENUM('new','active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'new',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `items_name_u` USING BTREE (`name`(150)),
  UNIQUE KEY `items_guid_u` USING BTREE (`guid`(50)),
  KEY `items_item_type` USING BTREE (`item_type`),
  KEY `items_resource_string_id` USING BTREE (`resource_string_id`)
) ENGINE=InnoDB
AUTO_INCREMENT=36 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `logs` table : */

CREATE TABLE `logs` (
  `id` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entity` ENUM('admin','client','product','option','paysystem','privilege','item','preset','group','gift','page','templates_cat','template','instagram_account','promocode','presets_cat','admingroup','doc','order','tag','country','city','other') COLLATE utf8_general_ci DEFAULT 'other' COMMENT 'сущность, с которой произведено действие',
  `entity_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID сущности',
  `event` ENUM('create','update','delete','login','logout','password_changed','other') COLLATE utf8_general_ci DEFAULT 'other' COMMENT 'произведенное действие',
  `author` ENUM('admin','client','system','other') COLLATE utf8_general_ci DEFAULT 'other' COMMENT '\"Тип\" автора изменений',
  `author_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID автора',
  `additional_data` TEXT COLLATE utf8_general_ci COMMENT 'произведенное действие',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `entity` USING BTREE (`entity`),
  KEY `entity_id` USING BTREE (`entity_id`),
  KEY `event` USING BTREE (`event`),
  KEY `created` USING BTREE (`created`)
) ENGINE=InnoDB
AUTO_INCREMENT=17 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `options` table : */

CREATE TABLE `options` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_general_ci,
  `price` FLOAT(10,2) DEFAULT 0.00 COMMENT 'Доступна ли частичная оплата',
  `duration` INTEGER(11) DEFAULT NULL COMMENT 'Длительность в днях',
  `is_partial_available` TINYINT(1) DEFAULT 0 COMMENT 'доступна ли частичная оплата',
  `partial_threshold` FLOAT(10,2) DEFAULT 0.00 COMMENT 'Пороговое значение частичной оплаты, при достижении которого продукт становится доступен покупателю',
  `partial_available_duration` INTEGER(11) DEFAULT 0 COMMENT 'Длительность доступа к продукту в днях при частичной оплате',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `options_product_id_name_u` USING BTREE (`product_id`, `name`(50)),
  KEY `options_product_id` USING BTREE (`product_id`),
  KEY `options_name` USING BTREE (`name`(50)),
  KEY `options_status` USING BTREE (`status`),
  CONSTRAINT `fk_options_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=7 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `promocodes` table : */

CREATE TABLE `promocodes` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `promocode` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Сам промокод',
  `start_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала действия',
  `end_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата окончания действия',
  `discount_type` ENUM('percent','fixed_price','fixed_amount') COLLATE utf8_general_ci DEFAULT 'percent' COMMENT 'Тип предоставляемой скидки: %, фикс цена, фикс скидка',
  `discount_value` FLOAT(10,2) DEFAULT 0.00 COMMENT 'Размер скидка при вводе промокода',
  `product_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Продукт, для которого применим промокод',
  `option_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Опция, на которую распространяется действие промокода',
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID клиента в случае персонифицированного промокода',
  `one_time_only` TINYINT(1) DEFAULT 0 COMMENT 'Является ли промокод одноразовым',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `promocode_u` USING BTREE (`promocode`),
  KEY `start_dt` USING BTREE (`start_dt`),
  KEY `end_dt` USING BTREE (`end_dt`),
  KEY `status` USING BTREE (`status`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `product_id` USING BTREE (`product_id`),
  KEY `option_id` USING BTREE (`option_id`),
  CONSTRAINT `fk_promocodes_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_promocodes_option_id` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_promocodes_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=10 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `subs` table : */

CREATE TABLE `subs` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subs_id` CHAR(35) COLLATE utf8_general_ci NOT NULL COMMENT 'Строковый идентификатор подписки',
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID клиента',
  `description` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Описание подписки',
  `email` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Email клиента',
  `amount` FLOAT(9,2) DEFAULT 0.00 COMMENT 'Размер оплаты',
  `start_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Дата начала подписки. Сохранять в формате YYYY-mm-dd hh:ii:ss',
  `interval_value` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Наименование интервала. Пример: Month',
  `period` TINYINT(4) UNSIGNED DEFAULT NULL COMMENT 'Длительность периода в единицах \'interval\'',
  `successful_transactions_number` TINYINT(4) UNSIGNED DEFAULT 0 COMMENT 'Число успешно проведенных рекуррентных платежей по подписке',
  `failed_transactions_number` TINYINT(4) UNSIGNED DEFAULT 0 COMMENT 'Число неудачно проведенных рекуррентных платежей по подписке',
  `last_transaction_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Время проведения последнего платежа',
  `next_transaction_dt` TIMESTAMP NULL DEFAULT NULL COMMENT 'Планируемое время проведения следующего платежа',
  `raw_data` TEXT COLLATE utf8_general_ci COMMENT 'json_encoded полные данные о подписке',
  `status` ENUM('active','deleted','pastdue','cancelled','rejected','expired') COLLATE utf8_general_ci NOT NULL DEFAULT 'active' COMMENT 'Cтатус подписки',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `status` USING BTREE (`status`),
  KEY `email` USING BTREE (`email`),
  KEY `last_transaction_dt` USING BTREE (`last_transaction_dt`),
  KEY `next_transaction_dt` USING BTREE (`next_transaction_dt`),
  KEY `subs_subs_id` USING BTREE (`subs_id`),
  CONSTRAINT `fk_subs_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=2 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `orders` table : */

CREATE TABLE `orders` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `price` FLOAT(10,2) DEFAULT 0.00 COMMENT 'Стоимость заказа с учетом всех скидок',
  `promocode_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'Сообщение клиента',
  `subs_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `client_message` TEXT COLLATE utf8_general_ci COMMENT 'Сообщение клиента',
  `admin_message` TEXT COLLATE utf8_general_ci COMMENT 'Примечание администратора',
  `additional_data` TEXT COLLATE utf8_general_ci COMMENT 'Предположительно - для хранения данных о продукте+опции',
  `amount_paid` FLOAT(9,2) DEFAULT 0.00 COMMENT 'Cколько реально оплачено (частичная оплата)',
  `status` ENUM('new','paid','partially_paid','cancelled','ended','deleted') COLLATE utf8_general_ci DEFAULT 'new',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `option_id` USING BTREE (`option_id`),
  KEY `cli_id` USING BTREE (`client_id`),
  KEY `status` USING BTREE (`status`),
  KEY `promocode_id` USING BTREE (`promocode_id`),
  KEY `orders_subs_id` USING BTREE (`subs_id`),
  CONSTRAINT `fk_orders_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_option_id` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_promocode_id` FOREIGN KEY (`promocode_id`) REFERENCES `promocodes` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_subs_id` FOREIGN KEY (`subs_id`) REFERENCES `subs` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=34 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `tags` table : */

CREATE TABLE `tags` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_general_ci,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name` USING BTREE (`name`),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `orders_tags` table : */

CREATE TABLE `orders_tags` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `tag_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `orders_tags_u` USING BTREE (`order_id`, `tag_id`),
  KEY `order_id` USING BTREE (`order_id`),
  KEY `tag_id` USING BTREE (`tag_id`),
  CONSTRAINT `orders_tags_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orders_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=4 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `pages` table : */

CREATE TABLE `pages` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `slug` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL,
  `short` TEXT COLLATE utf8_general_ci,
  `text` MEDIUMTEXT COLLATE utf8_general_ci,
  `meta_title` TEXT COLLATE utf8_general_ci,
  `meta_description` TEXT COLLATE utf8_general_ci,
  `meta_keywords` TEXT COLLATE utf8_general_ci,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name` USING BTREE (`name`(150)),
  UNIQUE KEY `slug` USING BTREE (`slug`),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=1 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `paysystems` table : */

CREATE TABLE `paysystems` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Имя для отображения в списках',
  `public_key` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Публичный ID (ключ) для осуществления платежей',
  `api_secret_key` VARCHAR(250) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Секретный ключ API для осуществления валидации платежей',
  `additional_data` TEXT COLLATE utf8_general_ci COMMENT 'Дополнительные данные для оплаты',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name` USING BTREE (`name`(50)),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=2 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `permissions` table : */

CREATE TABLE `permissions` (
  `admin_id` INTEGER(11) UNSIGNED NOT NULL,
  `p_admins` TINYINT(4) DEFAULT 0 COMMENT 'права на раздел \"Администраторы\"',
  `p_products` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Продуктам\"',
  `p_paysystems` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Аккаунтам платежных систем\"',
  `p_privileges` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Группам привилегий\"',
  `p_clients` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Клиенты\"',
  `p_items` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Items\"',
  `p_presets` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Пресетам\"',
  `p_groups` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Группам клиентов\"',
  `p_gifts` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Подаркам\"',
  `p_pages` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Статическим страницам\"',
  `p_templates` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Категориям темплейтов\" и \"Темплейтам\"',
  `p_instagram_accounts` TINYINT(4) DEFAULT 0 COMMENT 'Доступ к аккаунтам Instagram',
  `p_promocodes` TINYINT(4) DEFAULT 0 COMMENT 'Доступ к разделу \"Промокоды\"',
  `p_presets_cats` TINYINT(4) DEFAULT 0 COMMENT 'Права на раздел \"Категории пресетов\"',
  `p_admingroups` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Группам админов\"',
  `p_docs` TINYINT(4) DEFAULT 0 COMMENT 'Права на раздел \"Документы пользователя\"',
  `p_bots` TINYINT(4) DEFAULT 0 COMMENT 'Права на раздел \"Боты\"',
  `p_orders` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Заказам\"',
  `p_tags` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Тегам\"',
  `p_countries_cities` TINYINT(4) DEFAULT 0 COMMENT 'права на доступ к \"Страны-Города\"',
  `p_items_delete_with_content` TINYINT(4) DEFAULT 0 COMMENT 'права на полное удаление Item',
  `p_items_download_from_vimeo` TINYINT(4) DEFAULT 0 COMMENT 'права на скачивание видеофайлов с Vimeo',
  PRIMARY KEY USING BTREE (`admin_id`),
  CONSTRAINT `fk_permissions_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `presets_items` table : */

CREATE TABLE `presets_items` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `preset_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `item_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `pos` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `presets_items_u` USING BTREE (`preset_id`, `item_id`),
  KEY `preset_id` USING BTREE (`preset_id`),
  KEY `item_id` USING BTREE (`item_id`),
  CONSTRAINT `fk_presets_items_item_id` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_presets_items_preset_id` FOREIGN KEY (`preset_id`) REFERENCES `presets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=41 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `products_gifts` table : */

CREATE TABLE `products_gifts` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `gift_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `products_gifts_u` USING BTREE (`product_id`, `gift_id`),
  KEY `gift_id` USING BTREE (`gift_id`),
  KEY `product_id` USING BTREE (`product_id`),
  CONSTRAINT `fk_products_gifts_gift_id` FOREIGN KEY (`gift_id`) REFERENCES `gifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_products_gifts_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=5 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `sessions` table : */

CREATE TABLE `sessions` (
  `id` VARCHAR(40) COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` VARCHAR(45) COLLATE latin1_swedish_ci NOT NULL,
  `timestamp` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` BLOB NOT NULL,
  KEY `ci_sessions_timestamp` USING BTREE (`timestamp`)
) ENGINE=InnoDB
ROW_FORMAT=DYNAMIC CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci'
;

/* Structure for the `tasks` table : */

CREATE TABLE `tasks` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `system` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT 'backend' COMMENT 'Кому (какому сервису) предназначена задача',
  `publisher` VARCHAR(250) COLLATE utf8_general_ci NOT NULL COMMENT 'Имя источника (метода-создателя?). В произвольной форме',
  `consumer` VARCHAR(250) COLLATE utf8_general_ci NOT NULL COMMENT 'Имя метода-исполнителя в Worker контроллере',
  `data_in` TEXT COLLATE utf8_general_ci COMMENT 'Входные данные задачи в виде json_encoded',
  `data_out` TEXT COLLATE utf8_general_ci COMMENT 'Выходные данные задачи в виде json_encoded (если подразумеваются)',
  `priority` TINYINT(4) DEFAULT 0 COMMENT 'Приоритет задания. Чем больше, тем быстрее выполнится',
  `start_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp позволенного начала выполнения задачи',
  `status` ENUM('new','in_process','completed','error') COLLATE utf8_general_ci NOT NULL DEFAULT 'new' COMMENT 'Статус заявки в очереди',
  `status_msg` TEXT COLLATE utf8_general_ci COMMENT 'Сообщение к статусу заявки в очереди',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `publisher` USING BTREE (`publisher`(50)),
  KEY `cunsumer` USING BTREE (`consumer`(50)),
  KEY `status` USING BTREE (`status`),
  KEY `dest` USING BTREE (`system`),
  KEY `tasks_idx1` USING BTREE (`system`)
) ENGINE=InnoDB
AUTO_INCREMENT=15 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `tasks_completed` table : */

CREATE TABLE `tasks_completed` (
  `id` INTEGER(11) UNSIGNED NOT NULL,
  `system` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT 'backend' COMMENT 'Кому (какому сервису) предназначена задача',
  `publisher` VARCHAR(250) COLLATE utf8_general_ci NOT NULL COMMENT 'Имя источника (метода-создателя?). В произвольной форме',
  `consumer` VARCHAR(250) COLLATE utf8_general_ci NOT NULL COMMENT 'Имя метода-исполнителя в Worker контроллере',
  `data_in` TEXT COLLATE utf8_general_ci COMMENT 'Входные данные задачи в виде json_encoded',
  `data_out` TEXT COLLATE utf8_general_ci COMMENT 'Выходные данные задачи в виде json_encoded (если подразумеваются)',
  `priority` TINYINT(4) DEFAULT 0 COMMENT 'Приоритет задания. Чем больше, тем быстрее выполнится',
  `start_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp позволенного начала выполнения задачи',
  `status` ENUM('new','in_process','completed','error') COLLATE utf8_general_ci NOT NULL DEFAULT 'new' COMMENT 'Статус заявки в очереди',
  `status_msg` TEXT COLLATE utf8_general_ci COMMENT 'Сообщение к статусу заявки в очереди',
  `created` TIMESTAMP NULL DEFAULT NULL,
  `updated` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`),
  KEY `publisher` USING BTREE (`publisher`(50)),
  KEY `cunsumer` USING BTREE (`consumer`(50)),
  KEY `status` USING BTREE (`status`),
  KEY `dest` USING BTREE (`system`)
) ENGINE=InnoDB
ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `templates_cats` table : */

CREATE TABLE `templates_cats` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `name` USING BTREE (`name`(150)),
  KEY `status` USING BTREE (`status`)
) ENGINE=InnoDB
AUTO_INCREMENT=1 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `templates` table : */

CREATE TABLE `templates` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `templates_cat_id` INTEGER(11) UNSIGNED DEFAULT NULL,
  `name` VARCHAR(250) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `text` MEDIUMTEXT COLLATE utf8_general_ci,
  `status` ENUM('active','inactive','deleted') COLLATE utf8_general_ci DEFAULT 'inactive',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `templates_templates_cat_id_name_u` USING BTREE (`templates_cat_id`, `name`(100)),
  KEY `templates_cat_id` USING BTREE (`templates_cat_id`),
  KEY `name` USING BTREE (`name`(100)),
  KEY `status` USING BTREE (`status`),
  CONSTRAINT `fk_templates_templates_cat_id` FOREIGN KEY (`templates_cat_id`) REFERENCES `templates_cats` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=1 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `tokens` table : */

CREATE TABLE `tokens` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entity` ENUM('admin','client','blogger') COLLATE utf8_general_ci DEFAULT 'client' COMMENT 'сущность, для которой хранится токен',
  `entity_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID сущности',
  `device` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'GUID устройства',
  `token` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'токен',
  `last_used_dt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время последнего использования',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `tokens_token_u` USING BTREE (`token`),
  UNIQUE KEY `tokens_device_u` USING BTREE (`device`)
) ENGINE=InnoDB
AUTO_INCREMENT=5 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `trans` table : */

CREATE TABLE `trans` (
  `id` INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trans_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID транзакции в системе оплаты',
  `order_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID заказа',
  `client_id` INTEGER(11) UNSIGNED DEFAULT NULL COMMENT 'ID клиента',
  `amount` FLOAT(10,2) DEFAULT NULL COMMENT 'Сумма оплаты из параметров платежа',
  `type` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Тип операции: Payment/Refund/CardPayout',
  `oper` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Хранение типа операции для trans (check/pay/ и .д.)',
  `status` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Статус платежа',
  `data` TEXT COLLATE utf8_general_ci COMMENT 'Данные транзакции в JSON',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY USING BTREE (`id`),
  KEY `type` USING BTREE (`type`),
  KEY `status` USING BTREE (`status`),
  KEY `order_id` USING BTREE (`order_id`),
  KEY `client_id` USING BTREE (`client_id`),
  KEY `trans_id` USING BTREE (`trans_id`),
  CONSTRAINT `fk_trans_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_trans_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=37 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Structure for the `test` table : */

CREATE TABLE `test` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `prods` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=2 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;