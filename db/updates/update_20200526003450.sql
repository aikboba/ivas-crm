-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE PROCEDURE `allowProductToAll`(
        IN `product_id_param` INTEGER(11)
    )
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
--  declare my_variable int;
  DECLARE done int default 0;
  DECLARE p_id int default NULL;
  DECLARE p_is_free tinyint(1) default NULL;
  DECLARE _client_id int default NULL;

-- параметризованный курсор
--  declare clients_cursor cursor for select id from clients where not id in ()filterfield = my_variable;
  
  DECLARE clients_cursor cursor FOR select id from clients;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  SELECT id, is_free INTO p_id, p_is_free FROM `products` WHERE id=product_id_param;
  
  IF p_is_free=1 THEN

    BEGIN

      OPEN clients_cursor;
  
    WHILE done!=1 DO
      fetch clients_cursor into _client_id;
      INSERT INTO `clients_products` (`client_id`, `product_id`) VALUES (_client_id, product_id_param) ON DUPLICATE KEY UPDATE `end_dt`=null;
    END WHILE ;
  
      CLOSE clients_cursor;

    
    END;
  
  END IF;

--  set my_variable = 333;

--  set my_variable = 555;
--  open my_cursor;
--  ...
--  close clients_cursor; 
END;
