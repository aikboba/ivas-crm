-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE PROCEDURE `allowProductToAllNoCursor`(
        IN `product_id_param` INTEGER(11)
    )
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
--  declare my_variable int;
  DECLARE p_id int default NULL;
  DECLARE p_is_free tinyint(1) default NULL;

  SELECT id, is_free INTO p_id, p_is_free FROM `products` WHERE id=product_id_param;
  
  IF p_is_free=1 THEN

    BEGIN

        INSERT INTO `clients_products` (`client_id`, `product_id`) 
            SELECT `clients`.`id` AS `client_id`, `products`.`id` AS `product_id` FROM `clients`
            LEFT JOIN `products` ON `products`.`id`=product_id_param
            ON DUPLICATE KEY UPDATE `end_dt`=null;
    
    END;
  
  END IF;


END;