-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE PROCEDURE `takeProductFromAllNoCursor`(
        IN `product_id_param` INTEGER(11)
    )
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  DECLARE p_id int default NULL;
  DECLARE p_is_free tinyint(1) default NULL;

  SELECT id, is_free INTO p_id, p_is_free FROM `products` WHERE id=product_id_param;
  
  IF p_is_free!=1 THEN

    BEGIN

        DELETE FROM `clients_products` WHERE `product_id`=product_id_param AND `order_id` IS NULL;
    
    END;
  
  END IF;


END;
