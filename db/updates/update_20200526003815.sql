-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE TRIGGER `clients_products_after_insert` AFTER INSERT ON `clients_products`
  FOR EACH ROW
BEGIN
    DECLARE cli_id int default NULL;
    DECLARE prod_id int default NULL;
    DECLARE prod_start_dt TIMESTAMP default NULL;
    DECLARE prod_end_dt TIMESTAMP default NULL;
    DECLARE prod_is_free TINYINT(1) default NULL;
    DECLARE prod_type enum('subscription','donation','preset') default 'preset';

    DECLARE pres_id int default NULL;
    DECLARE pres_start_dt TIMESTAMP default NULL;
    DECLARE pres_end_dt TIMESTAMP default NULL;
    
  DECLARE v_origin VARCHAR(50) default NULL;
    DECLARE v_origin_id int(11) default NULL;
    DECLARE v_event_id int(11) default NULL;

    SET cli_id = NEW.client_id;
    SET prod_id = NEW.product_id;
    SET prod_start_dt = NEW.start_dt;
    SET prod_end_dt = NEW.end_dt;
    SET pres_end_dt = NEW.end_dt;
    SET v_origin = NEW.origin;
    SET v_origin_id = NEW.origin_id;
    SET v_event_id = NEW.id;
    
    SELECT preset_id, is_free, product_type INTO pres_id,prod_is_free, prod_type  FROM `products` WHERE id=prod_id;
    
    IF prod_type='subscription' THEN SET pres_start_dt = prod_start_dt;
    ELSE SET pres_start_dt = NULL;
    END IF;
    
    IF (pres_id IS NOT NULL) AND (prod_is_free=0) THEN
        BEGIN
            INSERT INTO `clients_presets` (`client_id`, `preset_id`, `starting_dt`, `expiration_dt`, `origin`, `origin_id`, `event_id`) VALUES (cli_id, pres_id, pres_start_dt, pres_end_dt, v_origin, v_origin_id, v_event_id);
        END;
    END IF;

END;
