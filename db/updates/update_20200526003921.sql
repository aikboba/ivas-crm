-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE TRIGGER `clients_products_before_delete` BEFORE DELETE ON `clients_products`
  FOR EACH ROW
BEGIN
    DECLARE cli_id int default NULL;
    DECLARE prod_id int default NULL;
    DECLARE pres_id int default NULL;
    DECLARE end_dt TIMESTAMP default NULL;
    DECLARE prod_is_free TINYINT(1) default NULL;
    DECLARE v_event_id int(11) default NULL;

    SET v_event_id = OLD.id;
    
    SET cli_id = OLD.client_id;
    SET prod_id = OLD.product_id;
    
    SELECT preset_id, is_free INTO pres_id, prod_is_free FROM `products` WHERE id=prod_id;
    
    IF (pres_id IS NOT NULL) THEN
        BEGIN
            DELETE FROM `clients_presets` WHERE `event_id`=v_event_id;
        END;
    END IF;

END;
