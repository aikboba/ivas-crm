-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE TRIGGER `items_before_insert` BEFORE INSERT ON `items`
  FOR EACH ROW
BEGIN
  IF NEW.status='active' THEN SET NEW.available_dt=NOW();
    END IF;
END;
