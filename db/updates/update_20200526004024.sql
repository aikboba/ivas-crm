-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--

CREATE TRIGGER `items_before_update` BEFORE UPDATE ON `items`
  FOR EACH ROW
BEGIN
  IF (NEW.status='active' AND OLD.status!='active' AND ISNULL(OLD.available_dt) ) THEN SET NEW.available_dt=NOW();
    END IF;
END;
