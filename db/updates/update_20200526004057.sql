-- ID: [TASKID]-[UPDATE INCREMENT NUMBER]-[UPDATE AUTHOR]
-- comment: [ADD A COMMENT]
--


CREATE TRIGGER `products_before_update` BEFORE UPDATE ON `products`
  FOR EACH ROW
BEGIN
  IF NEW.stream_status!=OLD.stream_status THEN SET NEW.stream_status_last_changed_dt=NOW();
    END IF;

  IF NEW.stream_health!=OLD.stream_health THEN SET NEW.stream_health_last_changed_dt=NOW();
    END IF;
  
END;