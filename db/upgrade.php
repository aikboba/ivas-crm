<?php
/*
 * Производит накат DB updates из папки /DB/updates/ .
 * Накатанные апдейты сохраняет в БД в таблицу вида
    CREATE TABLE `db_updates` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `update_name` varchar(50) NOT NULL,
      `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `update_name` (`update_name`)
    ) ENGINE=InnoDB;
 *
 * Для првильного фунционирования должна быть доступна и выставлена переменная окружения CI_ENV
 *
 * Значение берется из окружения переменной окружения CI_ENV (сессии терминала при запуске через CLI)
 * либо из коммандной строки php -f upgrade.php "<your_settings_set_name>"
 *
 * В случае запуска через WEB берется из SetEnv CI_ENV <your_settings_set_name>
 *
 * ВНИМАНИЕ: при запуске на пустой БД (первом запуске) выдаст ошибку, но продолжит работу
 * */

// ROOT dir

/*
define('ROOT', realpath( __DIR__ . DIRECTORY_SEPARATOR .'..') . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR );
// Path to DB\updates
define('UPDPATH', realpath( __DIR__ . DIRECTORY_SEPARATOR .'..') . DIRECTORY_SEPARATOR . 'DB'. DIRECTORY_SEPARATOR . "updates" . DIRECTORY_SEPARATOR);
// Path to application directory
define('APPPATH', ROOT. "application". DIRECTORY_SEPARATOR);
// Path to the system directory
define('BASEPATH', APPPATH. "system".DIRECTORY_SEPARATOR);
*/

// Path to DB\updates
define('UPDPATH', __DIR__ . DIRECTORY_SEPARATOR . "updates" . DIRECTORY_SEPARATOR);
// Path to application directory
define('APPPATH', realpath( __DIR__ . DIRECTORY_SEPARATOR .'..') . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR . "application". DIRECTORY_SEPARATOR);

define('BASEPATH', realpath( __DIR__ . DIRECTORY_SEPARATOR .'..') . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR);

if ( ! function_exists('is_cli'))
{

    /**
     * Is CLI?
     *
     * Test to see if a request was made from the command line.
     *
     * @return     bool
     */
    function is_cli()
    {
        return (PHP_SAPI === 'cli' OR defined('STDIN'));
    }
}

if (is_cli())
{
    define('DELIM', "\n");
}
else
{
    define('DELIM', "<br>");
}

if (!empty($argv[1]))
{
    define('ENVIRONMENT', $argv[1]);
}
else
{
    define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
}

// load default DB settings
if ( file_exists($file_path = APPPATH.'config/database.php')) include($file_path);
// Is the config file in the environment folder?
if ( file_exists($file_path = APPPATH.'config/'.ENVIRONMENT.'/database.php')) include($file_path);

$dsn = $db[$active_group]['dsn'];
$user = $db[$active_group]['username'];
$pass = $db[$active_group]['password'];
$pref = $db[$active_group]['dbprefix'];

//$DB = new PDO($dsn, $user, $pass);
$DB = new PDO($dsn, $user, $pass);
$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//$data = $DB->query("select * from ".$pref.'admins');
//chdir(__DIR__);

$installedUpdates = [];
$updates = scandir(UPDPATH);
sort($updates, SORT_STRING);

$query = "SELECT update_name FROM `db_updates` ORDER BY `update_name`";
try {
    $stmt = $DB->query($query);
    $stmt->setFetchMode(PDO::FETCH_COLUMN,0);
    $installedUpdates = $stmt->fetchAll();
}
catch (PDOException $e)
{
    //die("ОШИБКА:". $e->getMessage());
    echo $e->getMessage();
}

echo DELIM."Starting DB update process ...".DELIM.DELIM;

$DB->exec("SET NAMES utf8");
$DB->exec("SET collation_connection = utf8_general_ci");
$DB->beginTransaction();

foreach ($updates as $update)
{
    if ($update == '.' || $update == '..') {
        continue;
    }

    $updateFileName = pathinfo($update, PATHINFO_FILENAME);
    $total_err = $err = 0;
    if (!in_array($updateFileName, $installedUpdates))
    {
            echo DELIM;
            echo "Running update: {$update} ... ";
            $sql = file_get_contents(UPDPATH.$update);
            //$sql = "select * from admins";
            try {
                $stmt = $DB->prepare($sql);
                $stmt->execute();
                $i = 0;
                do {
                    $i++;
                } while ($stmt->nextRowset());
            }
            catch (PDOException $e)
            {
                $total_err++;
                $err = "ОШИБКА:". $e->getMessage();
            }

            if (!$err)
            {
                $sql = "INSERT INTO `db_updates` (`update_name`) values('".$updateFileName."')";
                $DB->exec($sql);
                echo "OK".DELIM;
            }
            else
            {
                echo "[exception] update file: {$update} ".DELIM;
                print_r($err);
                echo DELIM;
                break;
            }

    } else {
        echo "Skipping update: ".$update.DELIM;
    }
}

if ( $total_err )
{
    $DB->rollback();
    echo DELIM;echo DELIM;
    echo "ERROR WHILE PROCESSING UPDATES. TRANSACTION ROLLBACK".DELIM;
    exit("Unable to execute migrations");
}
else
{
    $DB->commit();
    echo DELIM;echo DELIM;
    echo "Process completed.".DELIM;
}

echo "Done.".DELIM;
?>