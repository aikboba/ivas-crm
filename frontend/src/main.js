import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbvue/lib/css/mdb.min.css'
import 'element-ui/lib/theme-chalk/index.css'
import 'vue-good-table/dist/vue-good-table.css'

import Vue from 'vue'
import Vuex from 'vuex'
import commonStore from "./store/common"
import chatStore from "./store/chat"
import router from './router'
import VueMq from 'vue-mq'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import axios from 'axios/dist/axios'
import VueAxios from 'vue-axios/dist/vue-axios.min'
import VueGoodTablePlugin from 'vue-good-table'
import VuePlyr from 'vue-plyr'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueHead from 'vue-head'
import VueTheMask from 'vue-the-mask'

import App from './App.vue'

Vue.config.productionTip = false
Vue.use(Vuex);
const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    commonStore,
    chatStore
  },
});

switch (window.location.pathname.split("/")[1]) {
  case process.env.VUE_APP_ADMINURL:
    store.commit('commonStore/updateConfig', {
      URLPRE: '/' + process.env.VUE_APP_ADMINURL,
      APP: 'appAdmin',
      ACCESSTOKEN: 'YWAjrT2iZXNzX3Rva2Vu'
    });
    break;

  default:
    store.commit('commonStore/updateConfig', {
      URLPRE: '',
      APP: 'appMember',
      ACCESSTOKEN: 'FmMjZXNlOz45X3Rva2Vu'
    });
}

if (localStorage.getItem(store.state.commonStore.config.ACCESSTOKEN)) {
  store.commit('commonStore/updateAccessTokenData', {
    [store.state.commonStore.config.ACCESSTOKEN]: localStorage.getItem(store.state.commonStore.config.ACCESSTOKEN)
  });
}

Vue.use(VueMq, {
  breakpoints: {
    xs: 576,
    sm: 768,
    md: 992,
    lg: 1200,
    xl: Infinity,
  }
});

Vue.use(Element, { locale });
Vue.use(VueAxios, axios);
Vue.use(VueGoodTablePlugin);
Vue.use(VuePlyr);
Vue.use(VueHead, {
  separator: '-',
  complement: process.env.VUE_APP_APPTITLE
});
Vue.use(VueMoment, {
  moment,
});
Vue.use(VueTheMask);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
