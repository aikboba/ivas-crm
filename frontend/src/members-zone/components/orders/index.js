import Vue from 'vue';
import {mapActions, mapGetters, mapState} from "vuex";

export default Vue.extend({

  data() {
    return {
      controller: 'orders',
      baseUrl: '/orders',
      status: {
        new: {text: 'Новый', textPl: 'Новые', className: ''},
        paid: {text: 'Оплаченные', textPl: 'Оплаченные', className: 'grey-text'},
        partially_paid: {text: 'Частично оплаченный', textPl: 'Частично оплаченные', className: 'red-text'},
        cancelled: {text: 'Отмененный', textPl: 'Отмененные', className: 'orange-text'},
        ended: {text: 'Завершенный', textPl: 'Завершенные', className: 'orange-text'},
        deleted: {text: 'Удаленный', textPl: 'Удаленные', className: 'orange-text'},
      },
    }
  },
});