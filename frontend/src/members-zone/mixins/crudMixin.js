import Vue from 'vue';
import { mapState, mapGetters, mapActions } from 'vuex';
import { eventBus } from "@/store/eventBus";

export default Vue.extend({

  data() {
    return {
      dataObj: null,
    }
  },
  computed: {
    ...mapState('commonStore', ['config']),
    ...mapGetters('commonStore', ['getDataLoading', 'getAPIURL', 'token']),
  },
  methods: {
    ...mapActions('commonStore', ['updateDataLoading']),
    getData(param, callback) {
      const loadingKey = this.controller + '_getData_m';

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});
        let _this = this;
        let bodyFormData = new FormData();
        bodyFormData.append('id', param);

        this.axios({
          method: 'post',
          url: this.getAPIURL + '/' + this.controller + '/get',
          data: bodyFormData,
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              _this.dataObj = {...response.data.data};

              if (callback) callback();
            } else {
              if (response.data.status >= 90 && response.data.status < 110) {
                eventBus.$emit('kill_user');
              } else {
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
              }
            }
          })
          .catch(function (response) {
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
  },
});