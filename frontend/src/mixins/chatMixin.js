import Vue from 'vue';
import { mapState, mapGetters, mapActions } from 'vuex';
const NchanSubscriber = require("nchan");

export default Vue.extend({

  computed: {
    ...mapState('commonStore', ['member', 'config']),
    ...mapState('chatStore', ['chatConfig', 'chats']),
    ...mapGetters('chatStore', ['getDataLoading']),
  },
  methods: {
    ...mapActions('chatStore', ['updateDataLoading', 'setChat', 'deleteChat', 'updateChat', 'updateChatMessages', 'receiveChatMessage']),
    initConnection(chatId, callback) {
      let _this = this;
      let url = "wss://" + this.chatConfig.CHAT_DOMAIN + "/" + chatId;

      //this.connection.socket = new WebSocket(url);
      this.setChat({
        id: chatId,
        url: url,
        connectionSocket: new NchanSubscriber(url, {
          subscriber: "websocket",
          reconnect: "persist"
        }),
        connectionStatus: null,
      });

      this.chats[chatId].connectionSocket.on('connect', function() {
        _this.updateChat({
          id: chatId,
          data: { connectionStatus: 'connected' }
        });
        _this.chats[chatId].connectionSocket.on("message", _this.onMessageHandler);

        if (callback) callback();
      });

      this.chats[chatId].connectionSocket.on('error', function(code, message) {
        _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Ошибка загрузки сообщений: ' + message});
      });

      this.chats[chatId].connectionSocket.start(); // begin (or resume) subscribing
      //this.chats[chatId].connectionSocket.stop(); // stop subscriber. do not reconnect.
    },
    dropConnection(chatId, callback) {
      if (this.chats && this.chats.hasOwnProperty(chatId)) {
        this.chats[chatId].connectionSocket.stop();
        this.deleteChat(chatId);
      }

      if (callback) callback();
    },
    onMessageHandler(message) {
      this.receiveChatMessage(JSON.parse(message));
    },
    loadMessages(chatId, params) {
      let loadingKey = 'chatMsgLoad_' + chatId;

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});

        let page = 0;
        let limit = this.chatConfig.CHAT_LOAD_MSGS_LIMIT;

        if (params) {
          if ('page' in params && !isNaN(params.page)) params.page = 0;
          if ('limit' in params && !isNaN(params.limit)) params.limit = this.chatConfig.CHAT_LOAD_MSGS_LIMIT;
        }

        let _this = this;
        let messageRequest = {
          channel: chatId,
          data: {
            user_id: this.chatParams.user.id,
            user_name: this.chatParams.user.name,
            page: page,
            limit: limit
          }
        };

        this.axios({
          method: 'post',
          url: 'https://' + this.chatConfig.API_DOMAIN + '/chat/load_messages',
          data: messageRequest
        })
          .then(function (response) {
            if (response.data.success === true) {
              _this.updateChatMessages({
                chatId: chatId,
                messages: response.data.messages.sort((a, b) => a.data.i - b.data.i)
              });
            } else {
              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Ошибка загрузки сообщений: ' + response.data.statusText});
            }
          })
          .catch(function (response) {
            console.log(response);
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
    sendMessage(message, chatId) {
      let loadingKey = 'chatMsgSend_' + chatId;

      if (message && !this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});

        let _this = this;
        let messageRequest = {
          channel: chatId,
          data: {
            user_id: this.chatParams.user.id,
            user_name: this.chatParams.user.name,
            message: message
          }
        };

        this.axios({
          method: 'post',
          url: 'https://' + this.chatConfig.API_DOMAIN + '/chat/create_message',
          data: messageRequest,
          headers: {
            'content-type': 'multipart/form-data',
          }
        })
          .then(function (response) {
            if (response.data.success !== true) {
              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Ошибка загрузки сообщений: ' + response.data.statusText});
            }
          })
          .catch(function (response) {
            console.log(response);
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    }
  },
});