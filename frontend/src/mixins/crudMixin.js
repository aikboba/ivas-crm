import Vue from 'vue';
import { mapState, mapGetters, mapActions } from 'vuex';
import {eventBus} from "@/store/eventBus";

export default Vue.extend({

  data() {
    return {
      dataObj: null,
      dataClone: null,
    }
  },
  computed: {
    ...mapState('commonStore', ['config']),
    ...mapGetters('commonStore', ['getDataLoading', 'token']),
  },
  methods: {
    ...mapActions('commonStore', ['updateDataLoading']),
    getData(param, callback) {
      const loadingKey = this.controller + '_getData';

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});
        let _this = this;
        let bodyFormData = new FormData();
        bodyFormData.append('id', param);

        this.axios({
          method: 'post',
          url: this.getAPIURL + '/' + this.controller + '/get',
          data: bodyFormData,
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              _this.dataObj = {...response.data.data};
              _this.dataClone = {...response.data.data};

              if (callback) callback();
            } else {
              if (response.data.status >= 90 && response.data.status < 110) {
                eventBus.$emit('kill_user');
              } else {
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
              }
            }
          })
          .catch(function (response) {
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
    updateData(formName, callback) {
      const loadingKey = this.controller + '_updateData';

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});

        let form = document.getElementById(formName);
        let bodyFormData = new FormData(form);
        let formAction = '/' + this.controller + (this.isNew ? '/create' : '/update');
        let _this = this;

        this.axios({
          method: 'post',
          url: this.getAPIURL + formAction,
          data: bodyFormData,
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              _this.dataClone = {..._this.dataObj};

              if (!_this.isNew) {
                _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: 'Данные успешно обновлены! :)'});
              } else {
                _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: 'Запись успешно создана! :)'});
              }

              if (callback) callback();
            } else {
              if (response.data.status >= 90 && response.data.status < 110) {
                eventBus.$emit('kill_user');
              } else {
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
              }
            }
          })
          .catch(function (response) {
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
    validateUnique(rule, value, callback) {
      if (value.toString().trim() != '' && value != this.dataClone[rule.field]) {
        let _this = this;
        let bodyFormData = new FormData();
        bodyFormData.append('fname', rule.field);
        bodyFormData.append('fvalue', value);

        this.axios({
          method: 'post',
          url: this.getAPIURL+'/' + this.controller+'/isUnique',
          data: bodyFormData,
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              if (response.data.data) {
                callback();
              } else {
                callback(new Error('Такая запись уже существует!'));
              }
            } else {
              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
            }
          })
          .catch(function (response) {
            console.log(response);
            return false;
          });
      } else {
        callback();
      }
    },
  },
});