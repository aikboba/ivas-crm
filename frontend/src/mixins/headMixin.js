import Vue from 'vue';
import { mapState } from 'vuex';

export default Vue.extend({
  computed: {
    ...mapState('commonStore', ['meta']),
  },
  head: {
    title: function () {
      return {
        inner: this.meta.title
      }
    }
  }
});