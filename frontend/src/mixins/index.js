export default {
  methods: {
    getTimezones: function(tzSource) {
      let _this = this;

      this.axios({
        method: 'post',
        url: tzSource,
        headers: {
          'content-type': 'multipart/form-data',
          'auth-x': this.token,
        }
      })
        .then(function (response) {
          if (!response.data.status) {
            _this.timeZones = response.data.data;
          } else {
            _this.timeZones = [];
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        })
        .catch(function (response) {
          console.log(response);
          return false;
        });
    },
  }
}