import Vue from 'vue';
import { mapState, mapGetters, mapActions } from 'vuex';
import { eventBus } from '@/store/eventBus';

export default Vue.extend({

  data() {
    return {
      data: null,
    }
  },
  computed: {
    ...mapState('commonStore', ['config']),
    ...mapGetters('commonStore', ['getDataLoading', 'getAPIURL', 'token']),
  },
  methods: {
    ...mapActions('commonStore', ['updateDataLoading']),
    getList() {
      const loadingKey = this.controller + '_getList';

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});
        let _this = this;

        this.axios({
          method: 'post',
          url: this.getAPIURL + '/' + this.controller + '/getList',
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              _this.data = response.data.data;

              _this.getListCallback();
            } else {
              if (response.data.status >= 90 && response.data.status < 110) {
                eventBus.$emit('kill_user');
              } else {
                _this.data = null;
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
              }
            }
          })
          .catch(function () {
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
    getListCallback() {

    },
    openRow() {

    },
    deleteRow(id) {
      let callback = function() {
        this.updateDataLoading({[this.controller + '_deleteRow_' + id]: false});
        this.$notify.success({title: this.config.NOTIFY.successTitle, message: 'Запись успешно удалена!'});
      };
      this.updateDataLoading({[this.controller + '_deleteRow_' + id]: true});
      this.setStatus(id, 'deleted', callback.bind(this));
    },
    restoreRow(id) {
      let callback = function() {
        this.updateDataLoading({[this.controller + '_restoreRow_' + id]: false});
        this.$notify.success({title: this.config.NOTIFY.successTitle, message: 'Запись успешно восстановлена!'});
      };
      this.updateDataLoading({[this.controller + '_restoreRow_' + id]: true});
      this.setStatus(id, 'active', callback.bind(this));
    },
    activateRow(id) {
      let callback = function() {
        this.updateDataLoading({[this.controller + '_activateRow_' + id]: false});
        this.$notify.success({title: this.config.NOTIFY.successTitle, message: 'Запись успешно активирована!'});
      };
      this.updateDataLoading({[this.controller + '_activateRow_' + id]: true});
      this.setStatus(id, 'active', callback.bind(this));
    },
    setStatus(id, status, callback) {
      const loadingKey = this.controller + '_rowStatus_' + status + '_' + id;

      if (!this.getDataLoading(loadingKey)) {
        this.updateDataLoading({[loadingKey]: true});
        let _this = this;
        let bodyFormData = new FormData();
        bodyFormData.append('id', id);
        bodyFormData.append('status', status);

        this.axios({
          method: 'post',
          url: this.getAPIURL+'/' + this.controller+'/setStatus',
          data: bodyFormData,
          headers: {
            'content-type': 'multipart/form-data',
            'auth-x': this.token,
          }
        })
          .then(function (response) {
            if (!response.data.status) {
              _this.getList();
              
              if (callback) callback();
            } else {
              if (response.data.status >= 90 && response.data.status < 110) {
                eventBus.$emit('kill_user');
              } else {
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
              }
            }
          })
          .catch(function (response) {
            console.log(response);
            return false;
          })
          .finally(function () {
            _this.updateDataLoading({[loadingKey]: false});
          });
      }
    },
  },
  created() {
    this.getList();

    const _this = this;

    eventBus.$on(this.controller + '_open-row-handle', _this.openRow);
    eventBus.$on(this.controller + '_activate-row-handle', _this.activateRow);
    eventBus.$on(this.controller + '_restore-row-handle', _this.restoreRow);
    eventBus.$on(this.controller + '_delete-row-handle', _this.deleteRow);
  },
  destroyed() {
    eventBus.$off([
      this.controller + '_open-row-handle',
      this.controller + '_activate-row-handle',
      this.controller + '_restore-row-handle',
      this.controller + '_delete-row-handle'
    ]);
  }
});