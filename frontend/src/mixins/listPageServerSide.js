import Vue from 'vue';

export default Vue.extend({
  methods: {
    updateParams(newProps) {
      this.tabs[this.currentTab].serverSideObj.serverParams = {...this.tabs[this.currentTab].serverSideObj.serverParams, ...newProps};
    },
    onPageChange(params) {
      this.updateParams({page: params.currentPage});
      this.loadItems();
    },
    onPerPageChange(params) {
      this.updateParams({perPage: params.currentPerPage});
      this.loadItems();
    },
    onSortChange(params) {
      this.updateParams({
        sort: [{
          type: params[0].type,
          field: params[0].field,
        }],
      });
      this.loadItems();
    },
    onColumnFilter(params) {
      this.updateParams(params);
      this.loadItems();
    }
  }
});