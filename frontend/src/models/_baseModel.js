import { eventBus } from '@/store/eventBus';

function getGlobal(args) {
  const getMethod = args.hasOwnProperty('getMethod') ? args.getMethod : 'get';
  const loadingKey = args.controller + '_getData' + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');
  const dataVar = args.hasOwnProperty('dataVar') ?
    args.dataVar : (this.$data.hasOwnProperty('dataObj') ? 'dataObj' : null);

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let _this = this;
    let bodyFormData = new FormData();

    if (args.hasOwnProperty('id')) bodyFormData.append('id', args.id);
    if (args.hasOwnProperty('fields')) {
      Object.keys(args.fields).forEach(key => bodyFormData.append(key, args.fields[key]));
    }

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + args.controller + '/' + getMethod,
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        _this.updateDataLoading({[loadingKey]: false});

        if (!response.data.status) {

          if (dataVar) {
            switch (typeof response.data.data) {
              case "string":
                _this.$data[dataVar] = response.data.data;
                break;

              default:
                _this.$data[dataVar] = {..._this.$data[dataVar], ...response.data.data};
            }
          }

          if (_this.$data.hasOwnProperty(dataVar + 'Clone'))
            _this.$data[dataVar + 'Clone'] = {..._this.$data[dataVar]};

          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (args.hasOwnProperty('failback')) args.failback();
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            if (args.hasOwnProperty('failMsg')) {
              if (args.failMsg !== false)
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: args.failMsg});
            } else {
              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
            }

          }
        }
      })
      .catch(function () {
        _this.updateDataLoading({[loadingKey]: false});
        return false;
      });
  }
}

function getListGlobal(args) {
  const getListMethod = args.hasOwnProperty('getListMethod') ? args.getListMethod : 'getList';
  const loadingKey = args.controller + '_' + getListMethod + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');
  const dataVar = args.hasOwnProperty('dataVar') ?
    args.dataVar : (this.$data.hasOwnProperty('data') ? 'data' : null);

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let bodyFormData = new FormData();
    let _this = this;

    if (args.hasOwnProperty('fields')) {
      Object.keys(args.fields).forEach(key => bodyFormData.append(key, args.fields[key]));
    }

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + args.controller + '/' + getListMethod,
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (dataVar)
            _this.$data[dataVar] = Array.isArray(response.data.data) ? [...response.data.data] : {...response.data.data};

          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            if (dataVar) _this.$data[dataVar] = null;

            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

function updateGlobal(args) {
  const dataVar = args.hasOwnProperty('dataVar') ? args.dataVar : 'dataObj';

  if (args.action === 'update' && this.$data[dataVar].hasOwnProperty('status') && this.$data[dataVar + 'Clone']
    && this.$data[dataVar].status !== this.$data[dataVar + 'Clone'].status
      && this.$data[dataVar].status === 'deleted')
  {
    let callbackFn = function() {
      updateFn.call(this, args);
    };
    let failbackFn = function() {
      this.$notify.error({title: this.config.NOTIFY.failTitle, message: 'Невозможно произвести удаление'});
      this.$data[dataVar] = {...this.$data[dataVar + 'Clone']};
    };

    deleteGlobal.call(this, {
      controller: args.controller,
      id: this.$data[dataVar].id,
      successMsg: false,
      callback: callbackFn.bind(this),
      failback: failbackFn.bind(this)
    });
  } else {
    updateFn.call(this, args);
  }
}

function updateFn(args) {
  const loadingKey = args.controller + '_updateData' + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');
  const dataVar = args.hasOwnProperty('dataVar') ?
    args.dataVar : (this.$data.hasOwnProperty('dataObj') ? 'dataObj' : null);
  const successMsg = args.hasOwnProperty('successMsg') ?
                        args.successMsg : args.action === 'create' ?
                          'Запись успешно создана! :)' : 'Данные успешно обновлены! :)';

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});

    let bodyFormData = args.hasOwnProperty('formName') ? new FormData(document.getElementById(args.formName)) : new FormData();
    let formAction = '/' + args.controller + '/' + args.action;
    let _this = this;

    if (args.hasOwnProperty('fields')) {
      Object.keys(args.fields).forEach(key => bodyFormData.append(key, args.fields[key]));
    }

    this.axios({
      method: 'post',
      url: this.getAPIURL + formAction,
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (dataVar) _this.$data[dataVar] = {..._this.$data[dataVar], ...response.data.data};
          if (_this.$data.hasOwnProperty(dataVar + 'Clone'))
            _this.$data[dataVar + 'Clone'] = {..._this.$data[dataVar]};

          if (successMsg !== false)
            _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: successMsg});

          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

function deleteGlobal(args) {
  const deleteMethod = args.hasOwnProperty('deleteMethod') ? args.deleteMethod : 'delete';
  const loadingIdx = args.hasOwnProperty('loadingIdx') ? args.loadingIdx : 'deleteData';
  const loadingKey = args.controller + '_' + loadingIdx + '_' + args.id + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');
  const successMsg = args.hasOwnProperty('successMsg') ? args.successMsg : 'Данные успешно удалены из БД!';

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let _this = this;
    let bodyFormData = new FormData();
    bodyFormData.append('id', args.id);

    if (args.hasOwnProperty('fields')) {
      Object.keys(args.fields).forEach(key => bodyFormData.append(key, args.fields[key]));
    }

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + args.controller + '/' + deleteMethod,
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }

          if (successMsg !== false)
            _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: successMsg});
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            if (args.hasOwnProperty('failback')) args.failback();
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function (response) {
        console.log(response);
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

function setStatusGlobal(args) {
  const loadingKey = args.controller + '_changeStatusData_' + args.id + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let _this = this;
    let bodyFormData = new FormData();
    bodyFormData.append('id', args.id);
    bodyFormData.append('status', args.status);

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + args.controller + '/setStatus',
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (args.hasOwnProperty('statusList')) {
            _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: 'Состояние успешно изменено: ' + args.statusList[args.status].text});
          }

          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function (response) {
        console.log(response);
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

function authGlobal(args) {
  const loadingKey = args.controller + '_auth_' + args.action + (args.hasOwnProperty('loadPstfx') ? '_' + args.loadPstfx : '');
  const dataVar = args.hasOwnProperty('dataVar') ? args.dataVar : null;
  const successMsg = args.hasOwnProperty('successMsg') ? args.successMsg : null;

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});

    let bodyFormData = new FormData();
    let formAction = '/' + args.controller + '/' + args.action;
    let headers = {
      'content-type': 'multipart/form-data',
    };
    let _this = this;

    if (args.hasOwnProperty('fields')) {
      Object.keys(args.fields).forEach(key => bodyFormData.append(key, args.fields[key]));
    }

    if (this.token)
      headers['auth-x'] = this.token;

    this.axios({
      method: 'post',
      url: this.getAPIURL + formAction,
      data: bodyFormData,
      headers: headers
    })
      .then(function (response) {
        if (!response.data.status) {
          if (dataVar) _this.$data[dataVar] = {...response};

          switch (args.action) {
            case 'login':
              if ('auth-x' in response.headers) {
                localStorage.setItem(_this.config.ACCESSTOKEN, response.headers['auth-x']);
                _this.updateAccessTokenData({ [_this.config.ACCESSTOKEN]: response.headers['auth-x'] });
                eventBus.$emit(args.controller + '-access-token-received');
                _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: 'Авторизация прошла успешно!'});
              } else {
                _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Токен не получен'});
              }

              break;

            case 'logout':
              localStorage.removeItem(_this.config.ACCESSTOKEN);
              _this.updateAccessTokenData({ [_this.config.ACCESSTOKEN]: null });

              break;

            default:
          }

          if (successMsg)
            _this.$notify.success({title: _this.config.NOTIFY.successTitle, message: successMsg});

          if (args.hasOwnProperty('callback')) args.callback();
          if (args.hasOwnProperty('callbacks')) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (response.data.status >= 90 && response.data.status < 110 && args.action !== 'logout') {
            eventBus.$emit('kill_user');
          } else {
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

function validateStringGlobal(rule, value, callback) {
  if (isNaN(parseInt(value))) callback();
  else callback(new Error('Не может быть числом!'));
}

function validateBlankGlobal(rule, value, callback) {
  if (value && value.match(/^.+\s.+$/g)) callback(new Error('Не содержать пробел(ы)!'));
  else callback();
}

function validateUniqueGlobal(args, rule, value, callback) {
  if (value && (this.isNew || (args.oldDataObj && value !== args.oldDataObj[rule.field]))) {
    let _this = this;
    let bodyFormData = new FormData();
    bodyFormData.append('fname', rule.field);
    bodyFormData.append('fvalue', value);

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + args.controller + '/isUnique',
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': localStorage.getItem(this.config.ACCESSTOKEN),
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (response.data.data) {
            callback();
          } else {
            callback(new Error('Такая запись уже существует!'));
          }
        } else {
          _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
        }
      })
      .catch(function (response) {
        console.log(response);
        return false;
      });
  } else {
    callback();
  }
}

export {
  getGlobal,
  getListGlobal,
  updateGlobal,
  deleteGlobal,
  setStatusGlobal,
  validateStringGlobal,
  validateBlankGlobal,
  validateUniqueGlobal,
  authGlobal
}