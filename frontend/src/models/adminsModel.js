import
{
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateUniqueGlobal,
  authGlobal
}
  from './_baseModel';

let data = {
  controller: 'admins',
  baseUrl: '/admins',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text'},
    banned: {text: 'Заблокирован', textPl: 'Заблокированные', className: 'orange-text'},
  },
};

let dataInit = {
    id: null,
    password: '',
    password_new: '',
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    phoneMasked: '',
    status: 'inactive',
    timezone: 'Europe/Moscow',
    permissions: {},
    admingroups: [],
  };

function get(args) {
  if (!args.hasOwnProperty('callbacks')) args.callbacks = [];

  args.callbacks.push(function () {
    const dataVar = args.hasOwnProperty('dataVar') ?
      args.dataVar : (this.$data.hasOwnProperty('dataObj') ? 'dataObj' : null);

    if (dataVar) this[dataVar].phoneMasked = this[dataVar].phone;
  }.bind(this));
  
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function auth(args) {
  authGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateUnique,
  auth
}