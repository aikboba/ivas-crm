import {
  getGlobal,
  getListGlobal,
  updateGlobal,
  deleteGlobal,
  setStatusGlobal,
  validateUniqueGlobal, validateStringGlobal,
  authGlobal
} from './_baseModel';

let data = {
  controller: 'clients',
  baseUrl: '/clients',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: '', params: [['status', '=', 'active']]},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text', params: [['status', '=', 'inactive']]},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text', params: [['status', '=', 'deleted']]}
  },
  sex: {
    m: {text: 'Мальчик', textPl: 'Мальчики', className: ''},
    f: {text: 'Девочка', textPl: 'Девочки', className: ''},
    u: {text: 'Не определился', textPl: 'Не определились', className: ''}
  },
};

let dataInit = {
    id: null,
    first_name: '',
    last_name: '',
    patronym_name: '',
    user_insta_nick: '',
    url: '',
    phone: '',
    phoneMasked: '',
    email: '',
    pass: '',
    pass_new: '',
    birthday: '',
    country_id: '',
    city_id: '',
    sex: 'u',
    status: 'inactive',
    timezone: 'Europe/Moscow',
    privileges: [],
  };

function get(args) {
  if (!args.hasOwnProperty('callbacks')) args.callbacks = [];

  args.callbacks.push(function () {
    const dataVar = args.hasOwnProperty('dataVar') ?
      args.dataVar : (this.$data.hasOwnProperty('dataObj') ? 'dataObj' : null);

    if (dataVar) this[dataVar].phoneMasked = this[dataVar].phone;
  }.bind(this));

  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function auth(args) {
  authGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateUnique,
  auth
}