import { getListGlobal, updateGlobal } from './_baseModel';

let data = {
  controller: 'docs',
  baseUrl: '/docs',
};

let dataInit = {
    fileList: [],
  };

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

export {
  data,
  dataInit,
  getList,
  update,
}