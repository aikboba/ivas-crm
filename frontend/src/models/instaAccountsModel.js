import
{
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateUniqueGlobal
}
  from './_baseModel';

let data = {
  controller: 'instagram_accounts',
  baseUrl: '/instagram-accounts',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text'},
  },
  streamStatuses: {
    active: {text: 'Идет трансляция эфира', className: 'green-text'},
    inactive: {text: 'Эфир не запущен', className: 'grey-text'},
  },
};

let dataInit = {
    id: null,
    pk: null,
    name: '',
    user_name: '',
    user_avatar: '',
    login: '',
    password: '',
    description: '',
    status: 'inactive',
  };

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateUnique
}