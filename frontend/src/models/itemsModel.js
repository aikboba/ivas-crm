import {
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateUniqueGlobal
}
from './_baseModel';

let data = {
  controller: 'items',
  baseUrl: '/items',
  status: {
    new: {text: 'Новый', textPl: 'Новые', className: 'green-text', params: [['status', '=', 'new']]},
    active: {text: 'Активный', textPl: 'Активные', className: '', params: [['status', '=', 'active']]},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text', params: [['status', '=', 'inactive']]},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text', params: [['status', '=', 'deleted']]},
  },
};

let dataInit = {
    id: null,
    file: null,
    name: '',
    system_name: '',
    description: '',
    resource_string_id: '',
    item_type: 'video',
    status: 'new',
  };

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteSeriously(args) {
  args = {...args, ...{controller: data.controller}};


  if (args.hasOwnProperty('id')) {
    const getVideoId = require('get-video-id');
    const loadingKey = args.controller + '_deleteSeriously_' + args.id;
    this.$data['tmp_item'] = null;
    this.$data['tmp_videoObj'] = null;
    this.$data['tmp_vimeo'] = null;

    if (!this.getDataLoading(loadingKey)) {
      this.updateDataLoading({[loadingKey]: true});

      //Try to get Item
      getGlobal.call(this, {
        id: args.id,
        controller: data.controller,
        dataVar: 'tmp_item',
        callback: function () {
          if (this.$data.tmp_item && this.$data.tmp_item.resource_string_id) {
            this.$data.tmp_videoObj = getVideoId(this.$data.tmp_item.resource_string_id);

            if (this.$data.tmp_videoObj && this.$data.tmp_videoObj.service === 'vimeo' && this.$data.tmp_videoObj.id) {
              //Try to get Vimeo Credo
              getGlobal.call(this, {
                controller: data.controller,
                getMethod: 'getVimeoCredentials',
                dataVar: 'tmp_vimeo',
                callback: function () {
                  if (this.$data.tmp_vimeo && this.$data.tmp_vimeo.hasOwnProperty('vimeo_access_token')) {
                    //Try delete from Vimeo
                    this.updateDataLoading({[loadingKey]: false});
                    let endpoint = 'https://api.vimeo.com/videos';
                    let _this = this;

                    fetch(`${endpoint}/${this.$data.tmp_videoObj.id}`, {
                      method: 'delete',
                      cache: 'no-cache',
                      mode: 'cors',
                      headers: {
                        'auth-x': 'bearer ' + this.$data.tmp_vimeo.vimeo_access_token
                      }
                    })
                      .then(response => {
                        if (response) {
                          switch (response.status) {
                            case 204:
                              _this.$notify.success({title: 'Vimeo', message: 'Видео успешно удалено из Vimeo!'});
                              //And then... delete data from DB
                              deleteGlobal.call(_this, {
                                id: args.id,
                                controller: data.controller,
                                deleteMethod: 'deleteWithContent',
                                loadingIdx: 'deleteSeriously',
                                callback: args.callback
                              });
                              break;

                            case 403:
                              if (args.hasOwnProperty('failback')) args.failback();
                              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Ваш Access token не уполномочен удалять видео с Vimeo!'});
                              break;

                            case 404:
                              if (args.hasOwnProperty('failback')) args.failback();
                              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Файл уже удален или перемещен!'});
                              break;

                            default:
                              if (args.hasOwnProperty('failback')) args.failback();
                              _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: 'Непонятная ошибка удаления видео с Vimeo!'});
                          }
                        }

                        return response;
                      });

                    this.$data.tmp_vimeo = null;//Clear Vimeo Credo
                  } else {
                    if (args.hasOwnProperty('failback')) args.failback();
                    this.updateDataLoading({[loadingKey]: false});
                  }
                }.bind(this),
                failback: function () {
                  if (args.hasOwnProperty('failback')) args.failback();
                  this.updateDataLoading({[loadingKey]: false});
                }.bind(this),
              });
            } else {
              if (args.hasOwnProperty('failback')) args.failback();
              this.updateDataLoading({[loadingKey]: false});
            }
          } else {
            if (args.hasOwnProperty('failback')) args.failback();
            this.updateDataLoading({[loadingKey]: false});
          }
        }.bind(this),
        failback: function () {
          if (args.hasOwnProperty('failback')) args.failback();
          this.updateDataLoading({[loadingKey]: false});
        }.bind(this),
      });
    }
  }
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  deleteSeriously,
  setStatus,
  validateString,
  validateUnique
}