import { getListGlobal  } from './_baseModel';

let data = {
  controller: 'trans',
  baseUrl: '/trans',
};

let dataInit = {
    id: null,
    trans_id: '',
    order_id: '',
    client_id: '',
    amount: '',
    type: '',
    oper: '',
    status: '',
    data: '',
  };

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

export {
  data,
  dataInit,
  getList,
}