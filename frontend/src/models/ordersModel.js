import { getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal  } from './_baseModel';
import { eventBus } from "@/store/eventBus";

let data = {
  controller: 'orders',
  baseUrl: '/orders',
  status: {
    new: {text: 'Новый', textPl: 'Новые', className: '', params: [['status', '=', 'new']]},
    paid: {text: 'Оплаченный', textPl: 'Оплаченные', className: 'green-text', params: [['status', '=', 'paid']]},
    partially_paid: {text: 'Частично оплаченный', textPl: 'Частично оплаченные', className: 'red-text', params: [['status', '=', 'partially_paid']]},
    cancelled: {text: 'Отмененный', textPl: 'Отмененные', className: 'grey-text', params: [['status', '=', 'cancelled']]},
    ended: {text: 'Завершенный', textPl: 'Завершенные', className: 'grey-text', params: [['status', '=', 'ended']]},
    deleted: {text: 'Удаленный', textPl: 'Удаленные', className: 'grey-text', params: [['status', '=', 'deleted']]},
  },
};

let dataInit = {
    id: null,
    product_id: '',
    option_id: '',
    client_id: '',
    price: 0,
    promocode: '',
    promocodeTest: '',
    client_message: '',
    admin_message: '',
    additional_data: '',
    amount_paid: 0,
    pay_amount: 0,
    status: 'new',
  };

function get(args) {
  if (!args.hasOwnProperty('callbacks')) args.callbacks = [];

  let productIdCallback = function() {
    let dataVar = args && 'dataVar' in args ? args.dataVar : 'dataObj';
    let productId = this.orderDataObj.additional_data ? JSON.parse(this.orderDataObj.additional_data).product.id : '';
    let promocode = this.orderDataObj.additional_data ?
      (JSON.parse(this.orderDataObj.additional_data).hasOwnProperty('promocode') ?
        JSON.parse(this.orderDataObj.additional_data).promocode.promocode : '') : '';

    this.$set(this[dataVar], 'product_id', productId);
    this[dataVar].promocode = this[dataVar].promocodeTest = promocode;
    this[dataVar].pay_amount = this[dataVar].price - this[dataVar].amount_paid;
  };
  args.callbacks.push(productIdCallback.bind(this));

  getGlobal.call(this, {
    ...args,
    ...{ controller: data.controller }
  });
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateAmount(rule, value, callback) {
  if (this.productOptionDataObj && (this.orderDataObj.status === 'new' || this.orderDataObj.status === 'paid' || this.orderDataObj.status === 'partially_paid')) {
    if (!this.productOptionDataObj.partial_threshold && value < this.orderDataObj.price - this.orderDataObj.amount_paid) {
      callback(new Error('Сумма долга: ' + (this.orderDataObj.price - this.orderDataObj.amount_paid) + ' р.!'));
    } else if (value <= 0) {
      callback(new Error('Минимальное значение - 1!'));
    } else if (value > this.orderDataObj.price - this.orderDataObj.amount_paid) {
      callback(new Error('Сумма превышает долг!'));
    } else {
      callback();
    }
  } else {
    callback();
  }
}

function pay(args) {
  const loadingKey = data.controller + '_orderPay_' + args.id;

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let _this = this;
    let bodyFormData = new FormData();
    bodyFormData.append('id', args.id);
    bodyFormData.append('amount', args.amount);

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + data.controller + '/pay',
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': localStorage.getItem(this.config.ACCESSTOKEN),
      }
    })
      .then(function (response) {
        if (!response.data.status) {
          if (args.hasOwnProperty('callback')) args.callback();
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateAmount,
  pay
}