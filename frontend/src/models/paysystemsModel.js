import {
  getGlobal, getListGlobal, updateGlobal, deleteGlobal,
  setStatusGlobal, validateUniqueGlobal
}
from "@/models/_baseModel";

let data = {
  controller: 'paysystems',
  baseUrl: '/paysystems',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text'},
  },
};

let dataInit = {
    id: null,
    name: '',
    public_key: '',
    api_secret_key: '',
    additional_data: '',
    status: 'inactive',
  };

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateUnique
}