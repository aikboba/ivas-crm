import {
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateUniqueGlobal
}
from './_baseModel';

let data = {
  controller: 'presets',
  baseUrl: '/presets',
  status: {
    active: {text: 'Активная', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивная', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удалена', textPl: 'Удаленные', className: 'red-text'},
  },
  presetTypes: {
    playlist: {text: 'Корзинка', textPl: 'Корзинки', className: 'grey-text'},
    training: {text: 'Тренинг', textPl: 'Тренинги', className: 'grey-text'}
  },
};

let dataInit = {
    id: null,
    presets_cat_id: '',
    name: '',
    description: '',
    preset_type: 'training',
    status: 'inactive',
  };

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateUnique
}