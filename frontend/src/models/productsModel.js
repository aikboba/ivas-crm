import
{
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateUniqueGlobal
}
  from './_baseModel';

let data = {
  controller: 'products',
  baseUrl: '/products',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text'},
  },
  type: {
    preset: {text: 'Плейлист', textPl: 'Плейлисты', className: 'grey-text'},
    subscription: {text: 'Доступ к эфиру', textPl: 'Доступы к эфиру', className: 'grey-text'},
    donation: {text: 'Пожертвование', textPl: 'Пожертвования', className: 'grey-text'},
  }
};

let dataInit = {
    id: null,
    paysystem_id: '',
    client_id: '',
    group_id: '',
    instagram_account_id: '',
    preset_id: '',
    days_item_available: 0,
    days_available_after_stop: 0,
    product_type: 'preset',
    is_free: false,
    name: '',
    description: '',
    status: 'inactive',
  };

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateUnique
}