import {
  getGlobal, getListGlobal, updateGlobal, deleteGlobal, setStatusGlobal,
  validateStringGlobal, validateBlankGlobal, validateUniqueGlobal
}
  from './_baseModel';
import { eventBus } from "@/store/eventBus";

let data = {
  controller: 'promocodes',
  baseUrl: '/promocodes',
  status: {
    active: {text: 'Активный', textPl: 'Активные', className: ''},
    inactive: {text: 'Неактивный', textPl: 'Неактивные', className: 'grey-text'},
    deleted: {text: 'Удален', textPl: 'Удаленные', className: 'red-text'},
  },
  promocodeDiscountTypes: {
    percent: {text: 'Процент', textPl: 'Проценты', className: 'grey-text'},
    fixed_amount: {text: 'Фикс. значение', textPl: 'Фикс. значения', className: 'grey-text'},
    fixed_price: {text: 'Фикс. цена', textPl: 'Фикс. цены', className: 'grey-text'},
  },
};

let dataInit = {
    id: null,
    name: '',
    promocode: '',
    start_dt: '',
    end_dt: '',
    client_id: '',
    product_id: '',
    option_id: '',
    discount_value: 0,
    discount_type: 'percent',
    one_time_only: false,
    status: 'inactive',
  };

function validatePromo (args, rule, value, callback) {
  if (value.toString().trim() !== '') {
    let _this = this;
    let bodyFormData = new FormData();
    bodyFormData.append('option_id', this.orderDataObj.option_id);
    bodyFormData.append('promocode', value);

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + data.controller + '/isApplicable',
      data: bodyFormData,
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': localStorage.getItem(this.config.ACCESSTOKEN),
      }
    })
      .then(function (response) {
        if (args && args.hasOwnProperty('callback')) args.callback(response.data.data);
        if (response.data.data !== 0) {
          callback();
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            callback(new Error('Промокод не действителен!'));
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.dataLoading.getData = false;
      });

  } else {
    if (args && args.hasOwnProperty('callback')) args.callback(null);
    callback();
  }
}

function get(args) {
  getGlobal.call(this, {...args, ...{controller: data.controller}});
}

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function update(args) {
  updateGlobal.call(this, {...args, ...{controller: data.controller}});
}

function deleteFn(args) {
  deleteGlobal.call(this, {...args, ...{controller: data.controller}});
}

function setStatus(args) {
  setStatusGlobal.call(this, {...args, ...{controller: data.controller}});
}

function validateString(rule, value, callback) {
  validateStringGlobal.call(this, rule, value, callback);
}

function validateBlank(rule, value, callback) {
  validateBlankGlobal.call(this, rule, value, callback);
}

function validateUnique(args, rule, value, callback) {
  validateUniqueGlobal.call(this, {...args, ...{controller: data.controller}}, rule, value, callback);
}

export {
  data,
  dataInit,
  get,
  getList,
  update,
  deleteFn,
  setStatus,
  validateString,
  validateBlank,
  validateUnique,
  validatePromo
}