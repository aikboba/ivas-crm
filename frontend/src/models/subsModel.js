import { getListGlobal, updateGlobal } from './_baseModel';

let data = {
  controller: 'subs',
  baseUrl: '/subs',
  status: {
    active: {text: 'Активная', textPl: 'Активные', className: 'green-text'},
    pastdue: {text: 'Просрочена', textPl: 'Просроченные', className: 'green-text'},
    cancelled: {text: 'Отменена', textPl: 'Отмененные', className: 'brown-text'},
    rejected: {text: 'Отклонена', textPl: 'Отклоненные', className: 'orange-text'},
    expired: {text: 'Завершена', textPl: 'Просроченные', className: 'grey-text'},
    deleted: {text: 'Удалена', textPl: 'Удаленные', className: 'red-text'},
  },
};

let dataInit = {
    id: null,
    amount: 0,
    product_id: '',
    product_name: '',
    option_id: '',
    option_name: '',
    description: '',
    created: '',
    next_transaction_dt: '',
    status: 'active',
  };

function getList(args) {
  getListGlobal.call(this, {...args, ...{controller: data.controller}});
}

function cancel(args) {
  updateGlobal.call(this, {
    ...args,
    ...{
      controller: data.controller,
      action: 'cancel',
    }
  });
}

export {
  data,
  dataInit,
  getList,
  cancel
}