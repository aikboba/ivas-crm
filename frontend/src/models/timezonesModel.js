import { eventBus } from "@/store/eventBus";

let data = {
  controller: 'index',
  baseUrl: '/timezones ',
};

function getTimezones(args) {
  const loadingKey = args.controller + '_getTimezones';

  if (!this.getDataLoading(loadingKey)) {
    this.updateDataLoading({[loadingKey]: true});
    let _this = this;

    this.axios({
      method: 'post',
      url: this.getAPIURL + '/' + data.controller + '/getTimezones',
      headers: {
        'content-type': 'multipart/form-data',
        'auth-x': this.token,
      }
    })
      .then(function (response) {
        let dataVar = args.hasOwnProperty('dataVar') ? args.dataVar : 'data';

        if (!response.data.status) {
          _this.$data[dataVar] = response.data.data;

          if (args && args.callback) args.callback();
          if (args && 'callbacks' in args) {
            args.callbacks.forEach(cb => cb());
          }
        } else {
          if (response.data.status >= 90 && response.data.status < 110) {
            eventBus.$emit('kill_user');
          } else {
            _this.$data[dataVar] = null;
            _this.$notify.error({title: _this.config.NOTIFY.failTitle, message: response.data.message});
          }
        }
      })
      .catch(function () {
        return false;
      })
      .finally(function () {
        _this.updateDataLoading({[loadingKey]: false});
      });
  }
}

export {
  data,
  getTimezones
}