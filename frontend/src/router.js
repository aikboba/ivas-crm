import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from '@/pages/admin-zone/Dashboard';
import adminsIndexPage from '@/pages/admin-zone/admins/adminsIndexPage';
import admingroupsListPage from '@/pages/admin-zone/admingroups/admingroupsListPage';
import clientsIndexPage from '@/pages/admin-zone/clients/clientsIndexPage';
import clientgroupsListPage from '@/pages/admin-zone/clientgroups/clientgroupsListPage';
import productsIndexPage from '@/pages/admin-zone/products/productsIndexPage';
import ordersIndexPage from '@/pages/admin-zone/orders/ordersIndexPage';
import orderTransListPage from '@/pages/admin-zone/orderTrans/orderTransListPage';
import paysystemsIndexPage from '@/pages/admin-zone/paysystems/paysystemsIndexPage';
import giftsIndexPage from "@/pages/admin-zone/gifts/giftsIndexPage";
import instaAccountsIndexPage from '@/pages/admin-zone/instaAccounts/instaAccountsIndexPage';
import privilegesIndexPage from '@/pages/admin-zone/privileges/privilegesIndexPage';
import presetsIndexPage from '@/pages/admin-zone/presets/presetsIndexPage';
import promocodesIndexPage from '@/pages/admin-zone/promocodes/promocodesIndexPage';
import itemsIndexPage from '@/pages/admin-zone/items/itemsIndexPage';
import NotFound from '@/pages/admin-zone/NotFound';

import Dashboard_member from '@/pages/members-zone/Dashboard'
import Profile_member from '@/pages/members-zone/profile';
import Pay_member from '@/pages/members-zone/pay';
import ordersIndexPage_member from '@/pages/members-zone/orders/ordersIndexPage';
import streamSinglePage from '@/pages/members-zone/stream/streamSinglePage';
import video_member from '@/pages/members-zone/video';
import playlist_member from '@/pages/members-zone/playlist';
import trainings_member from '@/pages/members-zone/trainings';
import NotFound_member from '@/pages/members-zone/NotFound';

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {//------------- ADMIN-ZONE ROUTES ---------------
      path: '/' + process.env.VUE_APP_ADMINURL,
      name: 'Dashboard',
      component: Dashboard,
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/admins',
      component: adminsIndexPage,
      props: true,
      children: [
        {
          path: ':adminId',
          name: 'adminsSinglePage',
          component: adminsIndexPage,
        },
        {
          path: '',
          name: 'adminsListPage',
          component: adminsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/admingroups',
      name: 'admingroupsListPage',
      component: admingroupsListPage,
      props: true
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/clients',
      component: clientsIndexPage,
      props: true,
      children: [
        {
          path: ':clientId',
          name: 'clientsSinglePage',
          component: clientsIndexPage,
        },
        {
          path: '',
          name: 'clientsListPage',
          component: clientsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/clientgroups',
      name: 'clientgroupsListPage',
      component: clientgroupsListPage,
      props: true
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/paysystems',
      component: paysystemsIndexPage,
      props: true,
      children: [
        {
          path: ':paysystemId',
          name: 'paysystemsSinglePage',
          component: paysystemsIndexPage,
        },
        {
          path: '',
          name: 'paysystemsListPage',
          component: paysystemsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/products',
      component: productsIndexPage,
      props: true,
      children: [
        {
          path: ':productId',
          name: 'productsSinglePage',
          component: productsIndexPage,
        },
        {
          path: '',
          name: 'productsListPage',
          component: productsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/orders',
      component: ordersIndexPage,
      props: true,
      children: [
        {
          path: ':orderId',
          name: 'ordersSinglePage',
          component: ordersIndexPage,
        },
        {
          path: '',
          name: 'orders',
          component: ordersIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/order-trans/:clientId/:orderId',
      name: 'orderTransListPage',
      component: orderTransListPage,
      props: true
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/gifts',
      component: giftsIndexPage,
      props: true,
      children: [
        {
          path: ':giftId',
          name: 'giftsSinglePage',
          component: giftsIndexPage,
        },
        {
          path: '',
          name: 'giftsListPage',
          component: giftsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/instagram-accounts',
      component: instaAccountsIndexPage,
      props: true,
      children: [
        {
          path: ':instaAccId',
          name: 'instaAccountsSinglePage',
          component: instaAccountsIndexPage,
        },
        {
          path: '',
          name: 'instaAccountsListPage',
          component: instaAccountsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/privileges',
      component: privilegesIndexPage,
      props: true,
      children: [
        {
          path: ':privilegeId',
          name: 'privilegesSinglePage',
          component: privilegesIndexPage,
        },
        {
          path: '',
          name: 'privilegesListPage',
          component: privilegesIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/presets',
      component: presetsIndexPage,
      props: true,
      children: [
        {
          path: ':presetId',
          name: 'presetsSinglePage',
          component: presetsIndexPage,
        },
        {
          path: '',
          name: 'presetsListPage',
          component: presetsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/promocodes',
      component: promocodesIndexPage,
      props: true,
      children: [
        {
          path: ':promoId',
          name: 'promocodesSinglePage',
          component: promocodesIndexPage,
        },
        {
          path: '',
          name: 'promocodesListPage',
          component: promocodesIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/items',
      component: itemsIndexPage,
      props: true,
      children: [
        {
          path: ':itemId',
          name: 'itemsSinglePage',
          component: itemsIndexPage,
        },
        {
          path: '',
          name: 'itemsListPage',
          component: itemsIndexPage,
        },
      ],
    },
    {
      path: '/' + process.env.VUE_APP_ADMINURL + '/*',
      name: 'NotFound',
      component: NotFound,
      props: true,
    },
    {//----------------- MEMBERS ROUTES ------------------
      path: process.env.VUE_APP_MEMBERURL + '/my-profile',
      name: 'Profile_member',
      component: Profile_member,
      props: true
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/pay/:productId',
      component: Pay_member,
      name: 'Pay_member',
      props: true,
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/orders',
      component: ordersIndexPage_member,
      props: true,
      children: [
        {
          path: ':orderId',
          name: 'ordersSinglePage_member',
          component: ordersIndexPage_member,
          props: true
        },
        {
          path: '',
          name: 'orders_member',
          component: ordersIndexPage_member,
          props: true
        },
      ],
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/stream',
      component: streamSinglePage,
      props: true,
      children: [
        {
          path: ':streamId',
          name: 'streamSingle',
          component: streamSinglePage,
        },
      ],
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/video',
      component: video_member,
      props: true,
      children: [
        {
          path: ':videoId',
          name: 'video_member',
          component: video_member,
        },
      ],
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/playlist',
      component: playlist_member,
      props: true,
      children: [
        {
          path: ':presetId',
          name: 'playlist_member',
          component: playlist_member,
        },
      ],
    },
    {
      path: process.env.VUE_APP_MEMBERURL + '/training',
      component: trainings_member,
      props: true,
      children: [
        {
          path: ':presetId',
          name: 'trainings_member',
          component: trainings_member,
        },
      ],
    },
    {
      path: '/',
      name: 'Dashboard_member',
      component: Dashboard_member,
      props: true,
    },
    {
      path: '*',
      name: 'NotFound_member',
      component: NotFound_member,
      props: true,
    },
  ]
})
