import Vue from 'vue';

const state = {
  chatConfig: {
    CHAT_AUTH_SAULT: process.env.VUE_APP_CHAT_AUTH_SAULT,
    CHAT_DOMAIN: process.env.VUE_APP_CHAT_DOMAIN,
    API_DOMAIN: process.env.VUE_APP_API_DOMAIN,
    CHAT_LOAD_MSGS_LIMIT: process.env.VUE_APP_CHAT_LOAD_MSGS_LIMIT,
  },
  chats: {},
  dataLoading: {},
};
const getters = {
  getDataLoading: (state) => (id) => id in state.dataLoading && state.dataLoading[id] === true,
};
const mutations = {
  setChatConfig (state, config) {
    state.chatConfig = config;
  },
  setChat (state, chatData) {
    Vue.set(state.chats, chatData.id, {
      url: chatData.url,
      connectionSocket: chatData.connectionSocket,
      connectionStatus: chatData.connectionStatus,
      messages: [],
    });
  },
  deleteChat (state, chatId) {
    Vue.delete(state.chats, chatId);
  },
  updateChat (state, chatData) {
    state.chats[chatData.id] = {...state.chats[chatData.id], ...chatData.data};
  },
  updateChatConfig (state, configData) {
    state.chatConfig = {...state.chatConfig, ...configData};
  },
  updateChatMessages (state, messageObj) {
    state.chats[messageObj.chatId].messages = [
      ...new Map([...state.chats[messageObj.chatId].messages, ...messageObj.messages].map( o => [o.data.i, o] )).values()
    ];
  },
  receiveChatMessage (state, message) {
    let mesExists = state.chats[message.data.c].messages.find(m => m.data.i === message.data.i);
    if (!mesExists)
      state.chats[message.data.c].messages.push(message);
  },
  setDataLoading (state, dataLoading) {
    state.dataLoading = dataLoading;
  },
  updateDataLoading (state, dataLoading) {
    state.dataLoading = {...state.dataLoading, ...dataLoading};
  },
};
const actions = {
  setChatConfig({commit}, data) {
    commit('setChatConfig', data);
  },
  setChat({commit}, data) {
    commit('setChat', data);
  },
  deleteChat({commit}, data) {
    commit('deleteChat', data);
  },
  updateChat({commit}, data) {
    commit('updateChat', data);
  },
  updateChatMessages({commit}, data) {
    commit('updateChatMessages', data);
  },
  receiveChatMessage({commit}, data) {
    commit('receiveChatMessage', data);
  },
  updateChatConfig({commit}, data) {
    commit('updateChatConfig', data);
  },
  setDataLoading({commit}, data) {
    commit('setDataLoading', data);
  },
  updateDataLoading({commit}, data) {
    commit('updateDataLoading', data);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};