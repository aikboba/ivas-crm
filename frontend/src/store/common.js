const state = {
  admin: null,
  adminToolsMenu: [],
  member: null,
  memberToolsMenu: [],
  accessTokenData: {},
  config: {
    APP: null,
    SERVERURL: process.env.VUE_APP_SERVERURL,
    URLPRE: '',
    APPTITLE: process.env.VUE_APP_APPTITLE,
    ACCESSTOKEN: null,
    APPMEMBERTITLE: process.env.VUE_APP_APPMEMBERTITLE,
    REACTIVEDATA: {
      admin_profile: false,
      products: false,
      paysystems: false,
      privileges: false,
      clients: false,
      admins_list: false,
      activeInstaStream: true,
    },
    REACTIVEINTERVAL: {
      get_self_profile: 3000,
      get_admins: 3000,
      get_clients: 3000,
      get_products: 3000,
      get_privileges: 3000,
      get_paysystems: 3000,
      get_activeStream: 3000,
    },
    CLIENTS_FILES: {
      sizeLimit: 10,//MB
      docTypes: 'image/*,.pdf,.txt,.doc,.docx,.xls,.xlsx,.ppt,.pptx',
      avaTypes: 'image/*',
    },
    NOTIFY: {
      successTitle: null,
      failTitle: 'Ошибка!',
      warningTitle: 'Внимание!',
    },
  },
  meta: {
    title: '',
    description: '',
  },
  dataLoading: {},
};
const getters = {
  token: () => state.accessTokenData[state.config.ACCESSTOKEN],
  getDataLoading: (state) => (id) => id in state.dataLoading && state.dataLoading[id] === true,
  getAPIURL: (state) => state.config.SERVERURL + state.config.URLPRE,
  getAdminToolsMenu() {
    let objType = {};

    if (state.adminToolsMenu) {
      for (let value of state.adminToolsMenu) {
        if ('children' in value) {
          let catName = 'cat_' + value.name;

          if (!objType.hasOwnProperty(catName)) objType[catName] = {name: value.name};
          if (!objType[catName].hasOwnProperty('children')) objType[catName].children = [];

          objType[catName].children = [...objType[catName].children, ...value.children];
        } else {
          objType[value.name] = value;
        }
      }
    }

    return objType;
  },
  getMemberToolsMenu() {
    let objType = {};

    if (state.memberToolsMenu) {
      for (let value of state.memberToolsMenu) {
        if ('children' in value) {
          let catName = 'cat_' + value.name;

          if (!objType.hasOwnProperty(catName)) objType[catName] = {name: value.name};
          if (!objType[catName].hasOwnProperty('children')) objType[catName].children = [];

          objType[catName].children = [...objType[catName].children, ...value.children];
        } else {
          objType[value.name] = value;
        }
      }
    }

    return objType;
  }
};
const mutations = {
  updateAccessTokenData (state, tokenData) {
    state.accessTokenData = {...state.accessTokenData, ...tokenData};
  },
  updateConfig (state, configData) {
    state.config = {...state.config, ...configData};
  },
  setAdmin (state, admin) {
    state.admin = admin;
  },
  updateAdmin (state, adminData) {
    state.admin = {...state.admin, ...adminData};
  },
  setAdminToolsMenu (state, adminToolsMenu) {
    state.adminToolsMenu = new Set([...state.adminToolsMenu, ...adminToolsMenu]);
  },
  deleteAdminToolsMenu (state, adminToolsMenu) {
    let tmp = new Set([...state.adminToolsMenu]);
    adminToolsMenu.forEach((item) => tmp.delete(item));
    state.adminToolsMenu = new Set([...tmp]);
  },
  setMember (state, member) {
    state.member = member;
  },
  updateMember (state, memberData) {
    state.member = {...state.member, ...memberData};
  },
  updateMeta (state, metaData) {
    state.meta = {...state.meta, ...metaData};
    document.title = state.meta.title;
  },
  setMemberToolsMenu (state, memberToolsMenu) {
    state.memberToolsMenu = new Set([...state.memberToolsMenu, ...memberToolsMenu]);
  },
  deleteMemberToolsMenu (state, memberToolsMenu) {
    let tmp = new Set([...state.memberToolsMenu]);
    memberToolsMenu.forEach((item) => tmp.delete(item));
    state.memberToolsMenu = new Set([...tmp]);
  },
  setDataLoading (state, dataLoading) {
    state.dataLoading = dataLoading;
  },
  updateDataLoading (state, dataLoading) {
    state.dataLoading = {...state.dataLoading, ...dataLoading};
  },
};
const actions = {
  updateAccessTokenData({commit}, data) {
    commit('updateAccessTokenData', data);
  },
  updateConfig({commit}, data) {
    commit('updateConfig', data);
  },
  setDataLoading({commit}, data) {
    commit('setDataLoading', data);
  },
  updateDataLoading({commit}, data) {
    commit('updateDataLoading', data);
  },
  setAdmin({commit}, data) {
    commit('setAdmin', data);
  },
  updateAdmin({commit}, data) {
    commit('updateAdmin', data);
  },
  updateMeta({commit}, data) {
    commit('updateMeta', data);
  },
  setAdminToolsMenu({commit}, data) {
    commit('setAdminToolsMenu', data);
  },
  deleteAdminToolsMenu({commit}, data) {
    commit('deleteAdminToolsMenu', data);
  },
  setMember({commit}, data) {
    commit('setMember', data);
  },
  updateMember({commit}, data) {
    commit('updateMember', data);
  },
  setMemberToolsMenu({commit}, data) {
    commit('setMemberToolsMenu', data);
  },
  deleteMemberToolsMenu({commit}, data) {
    commit('deleteMemberToolsMenu', data);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};