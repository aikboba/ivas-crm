require "digest"
require 'rubygems'
require_relative 'config/environment'
require 'sinatra'
require "sinatra/reloader"
require "sinatra/json"
require "nchan_tools"
require "nchan_tools/pubsub"

AUTH_SALT = ENV["WEBSOCKET_AUTH_SALT"]


configure { 
  set :server, :puma
  set :port, 3000
  set :bind, "0.0.0.0"
  set :allow_origin, :any
  set :allow_methods, [:get, :post, :options]  
}

not_found do
  "Page Not Found"
end

before do
  headers 'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']  
end

options "*" do
  response.headers["Access-Control-Allow-Origin"] = "*"
  response.headers["Access-Control-Allow-Headers"] = "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  response.headers["Access-Control-Allow-Methods"] = ['OPTIONS', 'GET', 'POST']
  halt 200
end

post "/v1/chat/create" do 
  validate_chat_request
  message = Messages::Create.new(@request_validator.channel, @request_validator.user_id, @request_validator.data).call
  if message.errors.any?
    return json({
      success: false,
      errors: message.errors.map { |e| "wrong_data_#{e}" }
    })  
  else
    push = { 
      action: "chat_message",
      data: Messages::Pack.new(message.message).call,
    }.to_json
    connection = publisher(message.message.channel)
    connection.submit(push, :POST, :'application/json')    
    return json({ success: true })  
  end
end

post "/v1/chat/get" do 
  validate_chat_request
  validator = Messages::Validator::Pagination.new(@request_validator.channel, @request_validator.data).call
  if validator.errors.any?
    return json({
      success: false,
      errors: validator.errors.map { |e| "wrong_data_#{e}" }
    })  
  else
    messages = Messages::Load.new(validator.params).call
    featured_message = nil
    if (feature = Features::Find.new(@request_validator.channel).call)
      featured_message = Messages::Pack.new(feature.message).call
    end
    return json({
      success: true,
      messages: messages.map { |m| Messages::Pack.new(m).call },
      featured_message: featured_message
    })    
  end
end


post "/v1/chat/feature" do 

  validate_chat_request
  feature = Features::Create.new(@request_validator.channel, @request_validator.data).call
  if feature.errors.any?
    return json({
      success: false,
      errors: validator.errors.map { |e| "wrong_data_#{e}" }
    })  
  else
    push = { 
      action: "chat_feature_message",
      data: Messages::Pack.new(feature.feature.message).call,
    }.to_json
    connection = publisher(@request_validator.channel)
    connection.submit(push, :POST, :'application/json')    
  end  
end


post "/v1/chat/unfeature" do 

  validate_chat_request
  feature = Features::Delete.new(@request_validator.channel).call
  Features::Delete.new(@request_validator.channel).call
  push = { 
    action: "chat_unfeature_message",
    data: [ @request_validator.channel ],
  }.to_json
  connection = publisher(@request_validator.channel)
  connection.submit(push, :POST, :'application/json')
  return json({ success: true })  
end


post "/broadcast" do
  headers 'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']    
p params
  unless verify_broadcast_auth(params)
    p "wrong_params: #{params.inspect}"
    return json({success: false, error_code: "wrong_params"})
  end  
  channel = params["channel"]  
  unless channel.blank?
    connection = publisher(channel)  
    push = params["payload"].to_s
    connection.submit(push, :POST, :'application/json')
  end
  return json({success: true})  
end

def publisher channel
  NchanTools::Publisher.new("http://nginx_pub/pub/" + channel)
end

def verify_broadcast_auth params
  auth = (params["channel"].to_s + params["payload"].to_s + AUTH_SALT)
  Digest::MD5.hexdigest(auth) == params["auth"].to_s
end


def validate_chat_request
  @request_validator = RequestValidator::Chat.new(request).call
  if @request_validator.errors.any?
    halt json({
      success: false,
      errors: @request_validator.errors
    })  
  end
end

# def create_chat_message params
#   Message.create({
#     channel: params["channel"],
#     user_name: params["data"]["user_name"],
#     user_id: params["data"]["user_id"],
#     message: params["data"]["message"].strip
#   })
# end

# def chat_message_serrialize message
#   {
#     action: "chat",
#     data: {
#       i: message.id,
#       m: CGI::escapeHTML(message.message),
#       u: message.user_name,
#       ui: message.user_id,
#       d: message.created_at.iso8601,
#       c: message.channel
#     }
#     # i: message.id,
#     # m: CGI::escapeHTML(message.message),
#     # u: message.user_name,
#     # ui: message.user_id,
#     # d: message.created_at.to_s(:db)
#   }
# end

