class Feature < ActiveRecord::Base
  belongs_to :message
  scope :unremoved, -> { where(removed: false ) }
  scope :removed, -> { where(removed: true ) }
end