module Features

  class Create

    attr_accessor :errors, :feature

    def initialize channel, params
      @channel = channel      
      @params = params
      @errors = []
    end

    def call
      validator = Features::Validator::Create.new(@channel, @params).call
      if validator.errors.any?
        @errors = validator.errors
      else
        @feature = Feature.create(validator.params)
      end
      self

      # feature = nil
      # if Message.where(id: @params[:message_id], channel: @params[:channel]).first
      #   Feature.unremoved.where(channel: @params[:channel]).update_all(removed: true)
      #   feature = Feature.create({
      #     channel: @params[:channel],
      #     message_id: @params[:message_id],
      #     removed: false
      #   })
      # end
      # feature
    end
  end
end