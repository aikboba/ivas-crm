module Features

  class Delete

    def initialize channel
      @channel = channel
    end

    def call
      Feature.where(channel: @channel).update_all(removed: true)
    end
  end
end