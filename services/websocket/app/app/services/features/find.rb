module Features

  class Find

    def initialize channel
      @channel = channel
    end

    def call
      Feature.unremoved.where(channel: @channel).first
    end
  end
end