module Features::Validator

  class Create

    attr_accessor :params, :errors

    def initialize channel, params
      @input_params = params.dup
      @params = {}
      @errors = []
      @input_params["channel"] = channel
    end

    def call
      ["message_id", "channel"].each do |field|
        @params[field] = @input_params[field]
        if @params[field].to_s.blank?
          @errors << field.to_s
        end
      end
      self
    end
  end
end