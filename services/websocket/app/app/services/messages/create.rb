module Messages

  class Create

    attr_accessor :errors, :message

    def initialize channel, user_id, data
      @params = data.dup
      @params["channel"] = channel
      @params["user_id"] = user_id
      @message = nil
      @errors = []
    end

    def call
      validator = Messages::Validator::Create.new(@params).call
      if validator.errors.any?
        @errors = validator.errors
      else
        @message = Message.create(validator.params)
      end
      self
    end
  end
end