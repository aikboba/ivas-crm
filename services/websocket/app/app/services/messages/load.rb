module Messages

  class Load

    def initialize params
      @params = params
    end

    def call
      rows = []
      statement = Message.where(channel: @params[:channel], removed: false)
      statement = statement.limit(@params[:limit])
      statement = statement.offset(@params[:start])
      if @params[:dir] == "before"
        statement = statement.where("created_at <= ?", @params[:datetime])
        statement = statement.order(id: :desc)
        rows = statement.all.to_a
      else
        statement = statement.where("created_at >= ?", @params[:datetime])
        statement = statement.order(id: :asc)        
        rows = statement.all.to_a.reverse
      end
      rows
    end
  end
end