module Messages

  class Pack

    def initialize message
      @message = message
    end

    def call
      [
        @message.channel, 
        @message.id,
        @message.user_id,
        @message.user_name,
        CGI::escapeHTML(@message.message),
        @message.created_at.iso8601
      ]
    end
  end
end