module Messages::Validator

  class Create

    attr_accessor :params, :errors

    def initialize params
      @input_params = params.dup
      @params = {}      
      @errors = []
    end

    def call
      ["user_name", "message", "channel", "user_id"].each do |field|
        @params[field] = @input_params[field]
        if @params[field].to_s.blank?
          @errors << field.to_s
        end
      end
      self
    end
  end
end