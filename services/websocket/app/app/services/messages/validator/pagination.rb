module Messages::Validator

  class Pagination

    attr_accessor :data, :params, :errors

    def initialize channel, data
      @channel = channel
      @data = data
      @params = {}
      @errors = []
    end

    def call
      begin 
        # ISO 8601
        @params[:datetime] = DateTime.parse(@data["datetime"].to_s)
      rescue Exception => e
        puts e.message
        @errors << "datetime"
      end

      if @data["start"].blank?
        @errors << "start"
      else
        @params[:start] = @data["start"].to_i
      end

      if @data["limit"].blank?
        @errors << "limit"
      elsif @data["limit"].to_i > 1000
        @errors << "limit"
      else
        @params[:limit] = @data["limit"].to_i
      end

      if ["before", "after"].include?(@data["dir"])
        @params[:dir] = @data["dir"]
      else
        @errors << "dir"
      end

      if @channel.blank?
        @errors << "channel"
      else
        @params[:channel] = @channel
      end        
      self
    end
  end
end