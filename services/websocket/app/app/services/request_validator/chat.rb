module RequestValidator

  class Chat

    attr_accessor :channel, :data, :user_id, :errors

    def initialize request = ""
      @request_payload = JSON.parse(request.body.read.to_s) rescue {}
      @action = request.path.split("/").last.to_s
      @data = @request_payload["data"] rescue {}
      @auth = @request_payload["auth"].to_s
      @channel, @user_id, @privilegies, @checksumm = @auth.split(":").collect(&:to_s)
      @errors = []
    end

    def call
      unless validate_auth
        @errors << "auth_checksumm_is_invalid"
      end
      unless validate_action
        @errors << "action_denied"
      end      
      self
    end

    private 

    def validate_auth
      auth = @auth.split(":").first(3).join(":")
      Digest::MD5.hexdigest(auth + ENV["CHAT_SECRET_SALT"].to_s) == @checksumm
    end

    def validate_action
      privilegies_map = {
        'create' => ['create'],
        'get' => ['get'],
        'feature' => ['feature', 'unfeature']
      }
      priviled_actions = @privilegies.split(",").map { |priv| privilegies_map[priv] }.flatten
      priviled_actions.include?(@action)
    end
  end

end