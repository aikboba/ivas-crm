require "rack-timeout"
require_relative 'chat'

use Rack::Timeout, service_timeout: 30
run Chat.new